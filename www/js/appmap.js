var appname="LinemanApp";
var masterMarkerList = {'vehicle_markers':[],'my_dispatched_items':[],'sub_markers':[],'alert_markers':[],'media_markers':[],'prediction_markers':[],'device_markers':[]};
var closeTicketReloadMarkers = ['device_markers','my_dispatched_items','prediction_markers','media_markers'];
var reopenPopup=false;
/*-----------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

var labels;

function specialLoadSteps() {
	if (g_db == null) {
		console.log("opening db");
		hasSpatialDB=true; 
		window.resolveLocalFileSystemURL(cordova.file.dataDirectory+'db/equip.db', function() {
			console.log("database file exists");
			window.sqlitePlugin.openDatabase({name:cordova.file.dataDirectory+'db/equip.db', location: 'default'}, 
				function(db) { 
					//alert("Opened db"); 
					console.log("successfully opened db");
					g_db=db; 
					//setTimeout(buildEquipmentList,2000);
				},
				function(err) { console.log("failed to open database file");alert("Failed to open db: "+JSON.stringify(err));});
			},function(err) { console.log("missing database file"); });
	}
}
/*----------------------------------------------------------------------------*/
function load_my_dispatched_items() {

	var data=masterMarkerList['my_dispatched_items'];
  crews = {};
  crewsSorter=[];
  labels=data.labels;
  var baseArray = new plugin.google.maps.BaseArrayClass(data.rows);
	var infoWindow = new plugin.google.maps.HtmlInfoWindow();
	var idx = 0;
	baseArray.map(function(myData, cb) {
		myData=myData;
		myData.index = idx;	
		crewsSorter[myData.work_item_number]=idx;	
		if (myData.poly != undefined && myData.poly.length > 1) { // display polygon
			var geomCoords = [];
			var avglat = 0;
			var avglon = 0;
			for (var j = 0 ; j <  myData.poly.length ; j++) {
				geomCoords[j] = new plugin.google.maps.LatLng(  myData.poly[j].lat,  myData.poly[j].lon );
				avglat +=parseFloat( myData.poly[j].lat);
				avglon +=parseFloat( myData.poly[j].lon);
			}
			avglat = avglat/ myData.poly.length;
			avglon = avglon/ myData.poly.length;
			fillColor = '#FFDA21';	
			crews[i] = new google.maps.Polygon({
				id:'poly_'+i,
				paths: geomCoords,
				strokeColor: fillColor,
				strokeOpacity: 0.2,
				strokeWeight: 3,
				fillColor: fillColor,
				fillOpacity: 0.2,
				map:googleMap
			});
	
			//crews[i].popup = new google.maps.InfoWindow({'content': content, 'maxWidth': 400});
		
			crews[i].get('myData') = workorder;
			crews[i].get('myData').lat = avglat;
			crews[i].get('myData').lon = avglon;
			crews[i].lat = avglat;
			crews[i].lon = avglon;
			crews[i].icon = 'images/wo.png';
			google.maps.event.addListener(crews[i], 'click',
			    function() {
					//this.popup.open(googleMap, crews[this.data.work_item_number]);
					crewIndex=this.data.work_item_number;
					//this.popup.setPosition(new plugin.google.maps.LatLng(this.lat,this.lon));
					//$(".jqbutton").button();
			    });
		}else{
			if (myData.type == 'work') {
				icon = 'wo.png'
			}else if (myData.type == 'service') {
				icon = 'so.png'
			}else if (myData.type == 'consumer') {
				icon = 'disp_' +getIcon('CON', 1) + '.png';
			}else if (myData.type == 'group') {
				icon = 'disp_null_R.png';									
			}else{
				icon=	'disp_' + getIcon(myData.enettype, 1) + '.png';
			}
			var data = {
				icon: cordova.file.dataDirectory+'www/images/'+icon,
				position: {
				  lat: myData.lat,
				  lng: myData.lon
				},
				draggable: true,
				visible:true,
				//disableAutoPan: true,
				zIndex:50,
		  		visible: localStorage.getItem('ckbx_MyItems') === 'true',
				myData: myData
			};
			googleMap.addMarker(data, cb);
		}
		idx++;
	}, function(markers) {
		crews = markers;
		if ($("#assignments_menu").is(":visible")) {
			popAssignmentsMenu();
		}
		markers.forEach(function(marker) {
			switch (marker.get('myData').type) {
				case 'work':
					marker.on(plugin.google.maps.event.MARKER_CLICK, loadWODispMarkers);
					break;
				case 'service':
					marker.on(plugin.google.maps.event.MARKER_CLICK, loadSODispMarkers);
					break;
				case 'consumer':
					marker.on(plugin.google.maps.event.MARKER_CLICK, loadConsumerDispMarkers);
					break;
				case 'device':
					marker.on(plugin.google.maps.event.MARKER_CLICK, loadDeviceDispMarkers);
					break;
				case 'group':
					marker.on(plugin.google.maps.event.MARKER_CLICK, loadGroupMarkers);
					break;
			}		  
		});
	});
	if (reassignId !== null) {
		for (var i in crews) {
			if (i === reassignId) {
				updateLAIncident(i);
				reassignId = null;
			}
		}
	}
	/*----------------------------------------------------------------------------*/
	function loadDeviceDispMarkers() {
	 $("#closeBtn").trigger("click");
		var marker = this;
		var myData = marker.get("myData");
		var content = '<table> <tr><td></td><td id="closeBtn" align="right"><img src="images/close.png"/></td></tr>' ;
		content += '<tr><td class="popup_heading">Work Item: </td><td class="popup_data">' + myData.work_item_number + '</td></tr>';
		var status= myData.status;
		if (myData.isClosing) {
        	status='<span style="color:red;">CLOSING</span>';
        }
        else if ( myData.status == null) {
		    status='Not Set';
		}
		content+='<tr><td class="popup_heading">Alias: </td><td class="popup_data">' +  myData.account_name + '</td></tr>' ;
		content+='<tr><td class="popup_heading">My Status: </td><td class="popup_data" id="crew_popup_status_' + myData.index + '">' + status + '</td></tr>' ;
		if ( myData.type != 'group') {
			content+= '<tr><td class="popup_heading">Device Type: </td><td class="popup_data">' +  myData.device_code + '</td></tr>' ;
		    content += '<tr><td class="popup_heading">Device Name: </td><td class="popup_data">' + stripNulls( myData.device_code_desc) + '</td></tr>';
		}
		content += '<tr><td class="popup_heading">Duration: </td><td class="popup_data">' +  myData.duration + '</td></tr>';
		var ert =  myData.estimated_restore_time;
		if (ert === null) {
			ert = "Not Set";
		}
		content += '<tr><td class="popup_heading">ERT: </td><td id="crew_popup_ert_' + myData.index + '" class="popup_data">' + ert + '</td></tr>';
		if ( myData.type != 'group') {
			content += '<tr><td class="popup_heading">Map Location: </td><td class="popup_data">' +  myData.map_location + '</td></tr>' ;
			content += '<tr><td class="popup_heading phase_prompt">Phase: </td><td class="popup_data phase_value">' +  myData.PhasesUsed + '</td></tr>';
			if ( myData.size != null &&  myData.size != undefined) {
				content+='<tr><td class="popup_heading">Size: </td><td class="popup_data">' +  myData.size + '</td></tr>';
			}
			if (labels[ myData.gistablename] !== undefined && labels[ myData.gistablename]['label1'] !== null && labels[ myData.gistablename]['label1'].length > 0 &&  myData.label1 != null) {
				content += '<tr><td class="popup_heading">' + labels[ myData.gistablename]['label1'] + ': </td><td>' + stripNulls( myData.label1) + '</td></tr>';
			}
			if (labels[ myData.gistablename] !== undefined && labels[ myData.gistablename]['label2'] !== null && labels[ myData.gistablename]['label2'].length > 0 &&  myData.label2 != null) {
				content += '<tr><td class="popup_heading">' + labels[ myData.gistablename]['label2'] + ': </td><td>' + stripNulls( myData.label2) + '</td></tr>';
			}
			if (labels[ myData.gistablename] !== undefined && labels[ myData.gistablename]['label3'] !== null && labels[ myData.gistablename]['label3'].length > 0 &&  myData.label3 != null) {
				content += '<tr><td class="popup_heading">' + labels[ myData.gistablename]['label3'] + ': </td><td>' + stripNulls( myData.label3) + '</td></tr>';
			}
		}
		content += '<tr><td colspan="2" class="popup_data"><b>Comments:</b><span id="crew_popup_comments_' + myData.index + '"> ' +  myData.description + '</span></span></td></tr>';
		content += '<tr><td>';
		content += '<button style="width:150px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-edit" onclick="javascript:updateLAIncident(\'' + myData.index + '\');">Update</button>';
		content += '</td><td>';
		content += '<button style="width:150px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-delete" onclick="javascript:closeTicket(\'' + myData.index + '\');">Close Item</button>' ;
		content += '</td></tr><tr><td>';
		content += '<button style="width:150px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" onclick="javascript:getDirections(\'' + myData.index + '\',\'crew\');">Directions</button>';
		content += '</td><td>';
		content += '<button style="width:150px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-camera" onclick="javascript:crewIndex=\''+ myData.index +'\';popCameraMenu();">Attach Picture</button></td></tr><tr><td>' ;
		if (myData.enettype != null) {
			content += '<button style="width:150px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-u" onclick="javascript:traceLine(\''+myData.linesection+'\',\'up\');">Trace Upline</button></td><td>';
			content += '<button style="width:150px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-d" onclick="javascript:traceLine(\''+myData.linesection+'\',\'down\');">Trace Downline</button>';
		}
		content += '</td></tr><tr><td>';
		//'<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-bars" onclick="javascript:seeDetails(\'' + myData.index + '\',\'crew\');">Details</button>' ;
		if (can_reassign_items !== 'FALSE') {
			content +='<button style="width:150px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-user" onclick="javascript:reassignTicket(\''+curr_view+'\', \''+  myData.index + '\', \'I\');">Reassign</button>';
			content += '</td><td>';
		}
	 	 if (params.MeterPings == 1) {
			content+='<button style="width:150px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:pingRequest(\''+ myData.index +'\',\'crew\',\'device\');">Ping Meters DL</button>';
		}
		
		content+='</td></tr></table>';
		infoWindow.setContent(content);
		infoWindow.open(marker);
		setTimeout( function() {
			$("#closeBtn").on("click", function() {
				infoWindow.close();
			});
		},500);
	}
	/*----------------------------------------------------------------------------*/
	function loadConsumerDispMarkers() {
		$("#closeBtn").trigger("click");
		var marker = this;
		var myData = marker.get("myData");
		var content = '<table> <tr><td></td><td id="closeBtn" align="right"><img src="images/close.png"/></td></tr>' +
		'<tr><td class="popup_heading">Work Item: </td><td class="popup_data">' +  myData.work_item_number + '</td></tr>' ;
		var status= myData.status;
		if (myData.isClosing) {
        	status='<span style="color:red;">CLOSING</span>';
        }
        else if ( myData.status == null) {
		    status='Not Set';
		}
		content+='<tr><td class="popup_heading">My Status: </td><td class="popup_data" id="crew_popup_status_' + myData.index + '">' + status + '</td></tr>' +
		    '<tr><td class="popup_heading">Customer: </td><td class="popup_data">' +  myData.name + '</td></tr>' +
		'<tr><td class="popup_heading">Location: </td><td class="popup_data">' +  myData.ServiceLocation + '</td></tr>' +
		'<tr><td class="popup_heading meter_prompt">Meter Number: </td><td class="popup_data meter_value">'+ myData.meter_number + '</td></tr>' +
		'<tr><td class="popup_heading">Phone: </td><td class="popup_data">';
		for (var key in  myData.phones) {
			content +=  myData.phones[key] + ': ' + key + '<br>';
		}
		content += '</td></tr>' +
			'<tr><td colspan="2" class="popup_data"><b>Comments:</b><span id="crew_popup_comments_' + myData.index + '">' +  myData.description + '</span></td></tr>';
		if ( myData.wavefile_name != null &&  myData.wavefile_name != '') {
			content+= '<tr><td colspan="2">IVR Message: <audio controls><source src="'+localStorage.ims_url+ myData.wavefile_name+'" type="audio/mpeg"></audio controls></td></tr>';
		}
		content+= '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-edit" onclick="javascript:updateLAIncident(\'' + myData.index + '\');">Update</button>' +
			'<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-delete" onclick="javascript:closeTicket(\'' + myData.index + '\');">Close Item</button>' +
			'<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" onclick="javascript:getDirections(\'' + myData.index + '\',\'crew\');">Directions</button>' +
			'<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-camera" onclick="javascript:crewIndex=\''+ myData.index +'\';popCameraMenu();">Attach Picture</button>' ;
			//'<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-bars" onclick="javascript:seeDetails(\'' + myData.index + '\',\'crew\');">Details</button></td></tr>';
		if (can_reassign_items !== 'FALSE') {
			content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-user" onclick="javascript:reassignTicket(\''+curr_view+'\', \''+ myData.index + '\', \'I\');">Reassign</button>';
		}
		if (params.MeterPings == 1) {
			content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:pingRequest(\''+ myData.index +'\',\'crew\', \'meter\');">Ping Meter</button>';
		}
		content += '</td></tr>';
		content+='</table>';
		infoWindow.setContent(content);
		infoWindow.open(marker);
		setTimeout( function() {
			$("#closeBtn").on("click", function() {
				infoWindow.close();
			});
		},500);
	}
	/*----------------------------------------------------------------------------*/
	function loadWODispMarkers() {
		$("#closeBtn").trigger("click");
		var marker = this;
		var myData = marker.get("myData");
		var content ='<table> <tr><td></td><td id="closeBtn" align="right"><img src="images/close.png"/></td></tr>' +
		'<tr><td class="popup_heading">Work Item: </td><td class="popup_data">' +  myData.work_item_number + '</td></tr>' ;
		if ( myData.scheduled_time != null) {
		    content+='<tr><td class="popup_heading">Meeting Scheduled: </td><td class="popup_data"  id="crew_popup_status_' + myData.index + '">' +  myData.scheduled_time + '</td></tr>';
		}
		var status= myData.status;
		if (myData.isClosing) {
        	status='<span style="color:red;">CLOSING</span>';
        }
        else if ( myData.status == null) {
		    status='Not Set';
		}
		content+='<tr><td class="popup_heading">My Status: </td><td class="popup_data"  id="crew_popup_status_' + myData.index + '">' + status + '</td></tr>' +
		'<tr><td class="popup_heading">Due: </td><td class="popup_data">' +  myData.due_date + ' ' + myData.due_time+'</td></tr>' +
		'<tr><td class="popup_heading">Customer: </td><td class="popup_data">' +  myData.name + '</td></tr>' +
		'<tr><td class="popup_heading">Location: </td><td class="popup_data">' +  myData.ServiceLocation + '</td></tr>' +
		'<tr><td class="popup_heading">Phone: </td><td class="popup_data">';
	for (var key in  myData.phones) {
		content +=  myData.phones[key] + ': ' + key + '<br>';
	}
	content += '</td></tr>' +
		'<tr><td class="popup_heading">Comments:</td><td class="popup_data"><span id="crew_popup_comments_' + myData.index + '">'+  myData.comment + '</span></td></tr></table>'+
		'<div><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" style="width:200px;" data-inline="true" onclick="javascript:getDirections(\'' + myData.index + '\',\'crew\');">Directions</button>' +
		'<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-camera" style="width:200px;" data-inline="true" onclick="javascript:crewIndex=\''+ myData.index +'\';popCameraMenu();">Attach Picture</button>' +
		'<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-edit" style="width:200px;" data-inline="true" onclick="javascript:updateLAIncident(\'' + myData.index + '\');">Update</button>';
	if (can_reassign_items !== 'FALSE') {
		    content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-user" style="width:200px;" data-inline="true" onclick="javascript:reassignTicket(\''+curr_view+'\', \'' +  myData.index + '\', \'W\');">Reassign</button>';
		}
		content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-delete" style="width:200px;" data-inline="true" onclick="javascript:closeTicket(\'' + myData.index + '\');">Close Item</button></div>';
		infoWindow.setContent(content);
		infoWindow.open(marker);
		setTimeout( function() {
			$("#closeBtn").on("click", function() {
				infoWindow.close();
			});
		},500); 
	}
	/*----------------------------------------------------------------------------*/
	function loadSODispMarkers() {
		$("#closeBtn").trigger("click");
		var marker = this;
		var myData = marker.get("myData");
		var content = '<table> <tr><td></td><td id="closeBtn" align="right"><img src="images/close.png"/></td></tr>' +
		'<tr><td class="popup_heading">Work Item: </td><td class="popup_data">' +  myData.work_item_number + '</td></tr>' +
		'<tr><td class="popup_heading">Description: </td><td class="popup_data">' +  myData.description + '</td></tr>' +
		'<tr><td class="popup_heading">Type: </td><td class="popup_data">' +  myData.disptype + '</td></tr>' ;
	 	
		if ( myData.scheduled_time != null ||  myData.status == '') {
		    content+='<tr><td class="popup_heading">Meeting Scheduled: </td><td class="popup_data"  id="crew_popup_status_' + myData.index + '">' +  myData.scheduled_time + '</td></tr>';
		}
		var status= myData.status;
		if (myData.isClosing) {
        	status='<span style="color:red;">CLOSING</span>';
        }
        else if ( myData.status == null ||  myData.status == '') {
		    status='Not Set';
		}
		content+='<tr><td class="popup_heading">My Status: </td><td class="popup_data"  id="crew_popup_status_' + myData.index + '">' + status + '</td></tr>' +
		'<tr><td class="popup_heading">Due: </td><td class="popup_data">' +  myData.due_date + ' ' + myData.due_time+ '</td></tr>' +
		'<tr><td class="popup_heading">Customer: </td><td class="popup_data">' +  myData.name + '</td></tr>' +
		'<tr><td class="popup_heading">Meter#: </td><td class="popup_data">' +  myData.meter_number + '</td></tr>' +
		'<tr><td class="popup_heading">Location: </td><td class="popup_data">' +  myData.ServiceLocation + '</td></tr>' +
		'<tr><td class="popup_heading">Location Description: </td><td class="popup_data">' +  myData.service_loc_description + '</td></tr>' +
		'<tr><td class="popup_heading">Phone: </td><td class="popup_data">'+  myData.customer_billing_daytime_phone +' day / '+ myData.customer_evening_phone + ' eve</td></tr>' +
		'<tr><td class="popup_heading">Comments:</td><td class="popup_data"><span id="crew_popup_comments_' + myData.index + '">'+  myData.comment + '</span></td></tr>'+
		'<tr><td class="popup_heading">Transformer Number: </td><td class="popup_data">'+  myData.service_loc_transformer_number1 + '</td></tr>'+
		'<tr><td class="popup_heading">Transformer Size: </td><td class="popup_data">'+  myData.service_loc_transformer_size + '</td></tr>'+
		'<tr><td class="popup_heading">Substation: </td><td class="popup_data">' +  myData.subfdr + '</td></tr>';
	content += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" onclick="javascript:getDirections(\'' + myData.index + '\',\'crew\');">Directions</button>' +
		'<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-camera" onclick="javascript:crewIndex=\''+ myData.index +'\';popCameraMenu();">Attach Picture</button>' +
		'<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-edit" onclick="javascript:updateLAIncident(\'' + myData.index + '\');">Update</button>';
	if (can_reassign_items !== 'FALSE') {
		    content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-user" onclick="javascript:reassignTicket(\''+curr_view+'\', \'' +  myData.index + '\', \'S\');">Reassign</button>';
		}
		content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-delete" onclick="javascript:closeTicket(\'' + myData.index + '\');">Close Item</button></td></tr></table>';
		infoWindow.setContent(content);
		infoWindow.open(marker);
		setTimeout( function() {
			$("#closeBtn").on("click", function() {
				infoWindow.close();
			});
		},500);
	}
}
/*----------------------------------------------------------------------------*/
function load_device_markers() {
	if (can_view_all_outages == 'FALSE') {
		return;
	}
	var data=masterMarkerList['device_markers'];
	var baseArray = new plugin.google.maps.BaseArrayClass(data.rows);
	var infoWindow = new plugin.google.maps.HtmlInfoWindow();
	var idx = 0;
	baseArray.map(function(myData, cb) {
		myData=myData;
		myData.index = idx;			
		var data = {
		  icon: 'images/' + getIcon(myData.enettype, 1) + '.png',
		  position: {
		    lat: myData.lat,
		    lng: myData.lon
		  },
		  draggable: true,
		  disableAutoPan: true,
		  zIndex: 50,
		  visible:  (localStorage.getItem('ckbx_DevicesOut') === 'true'),
		  myData: myData
		};
		//vehicles[i].popup = plugin.google.maps.InfoWindow({content: content, maxWidth: 400});
		googleMap.addMarker(data, cb);	
		
	}, function(markers) {
		markers = markers;
		markers.forEach(function(marker) {
		  marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);				  
		});
	});

	function onMarkerClick() {
		$("#closeBtn").trigger("click");
		var marker = this;
		var myData = marker.get("myData");
		if (myData.isClosing) {
        	status='<span style="color:red;">CLOSING</span>';
        }
        else if (myData.ert === undefined || myData.ert === null) {
			myData.ert = 'Not Set';
		} else {
			myData.ert = myData.ert.substring(0, myData.ert.lastIndexOf(':')) + " " + myData.ert.substr(-2);
		}
		myData.duration = myData.duration.substring(0, myData.duration.lastIndexOf(':'));
		myData.start_date = myData.start_date.substring(0, myData.start_date.lastIndexOf(':')) + " " + myData.start_date.substr(-2);
		if (myData.enettype === null) {
			//jAlert("device markers null");
		}
		var contentString = '<div style="overflow:auto;"><table> <tr><td></td><td id="closeBtn" align="right"><img src="images/close.png"/></td></tr>' ;
		contentString += '<tr><td class="popup_heading">Ticket:</td><td class="popup_data">' + myData.incident_id + '</td></tr>';
		contentString += '<tr><td class="popup_heading subfdr_prompt">Sub-Fdr:</td><td class="popup_data subfdr_value">' + myData.subfdr + '</td></tr>';

		var linesection = myData.linesection;
		if (linesection === null || linesection == undefined) {
			linesection = 'Not Available';
		}
		contentString += '<tr><td class="popup_heading linesect_prompt">Linesection: </td><td class="linesect_value">' + linesection + '</td></tr>';
		contentString += '<tr><td class="popup_heading">Type:</td><td class="popup_data">' + myData.device_code + '</td></tr>';
		contentString += '<tr><td class="popup_heading">Device Name:</td><td class="popup_data">' + stripNulls(myData.device_code_desc) + '</td></tr>';
		contentString += '<tr><td class="popup_heading">Due Date:</td><td class="popup_data">' + myData.due_date + ' '+myData.due_time+ '</td></tr>';
		contentString += '<tr><td class="popup_heading">Dispatched To:</td><td class="popup_data">' + myData.crew+' '+stripNulls(myData.crew_contact)+ '</td></tr>';
		contentString += '<tr><td class="popup_heading">Dispatched On:</td><td class="popup_data">' + myData.dispatched_on+ '</td></tr>';
		contentString += '<tr><td class="popup_heading">Dispatched By:</td><td class="popup_data">' + myData.dispatched_by+ '</td></tr>';
		contentString += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" onclick="javascript:getDirections(\'' + myData.index + '\',\'device\');">Get Directions</button>';
		if (myData.enettype != null) {
			contentString += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-u" onclick="javascript:traceLine(\''+myData.linesection+'\',\'up\');">Trace Upline</button>';
			contentString += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-d" onclick="javascript:traceLine(\''+myData.linesection+'\',\'down\');">Trace Downline</button>';
		}
		if (can_reassign_items !== 'FALSE') {
			contentString += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-user" onclick="javascript:$(\'#closeBtn\').trigger(\'click\'); reassignId=\''+myData.incident_id+'\';assignTicket(\'' + myData.incident_id + '\',\'I\', \''+curr_view+'\',\''+curr_crew+'\');">Assign To Me</button>';
		}
		contentString += '</td></tr>';
		if (params.MeterPings == 1) {
			contentString+='<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:pingRequest(\''+myData.index+'\',\'device\',\'device\');">Ping Meters Downline</button></td></tr>';
		}
		contentString += '</table></div>';
		infoWindow.setContent(contentString);
		infoWindow.open(marker);
		setTimeout( function() {
			$("#closeBtn").on("click", function() {
				infoWindow.close();
			});
		},500);
	}
}
/*----------------------------------------------------------------------------*/
function load_prediction_markers() {
	var data=masterMarkerList['prediction_markers'];
	if (data.labels != null) {
		labels=data.labels;
	}
	var baseArray = new plugin.google.maps.BaseArrayClass(data.rows);
	var infoWindow = new plugin.google.maps.HtmlInfoWindow();
	var idx = 0;
	baseArray.map(function(myData, cb) {
		myData=myData;
		myData.index = idx;				
		var data = {
		  icon: 'images/' + getIcon(myData.enettype, 3) + '.png',
		  position: {
		    lat: myData.lat,
		    lng: myData.lon
		  },
		  draggable: true,
		  disableAutoPan: true,
		  zIndex: 40,
		  visible:  (localStorage.getItem('ckbx_Predictions') === 'true'),
		  myData: myData
		};
		//vehicles[i].popup = plugin.google.maps.InfoWindow({content: content, maxWidth: 400});
		googleMap.addMarker(data, cb);
		idx++;

	}, function(markers) {
		predictions = markers;
		markers.forEach(function(marker) {
			marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);				  
		});
	});

	function onMarkerClick() {
		$("#closeBtn").trigger("click");
		var marker = this;
		var myData = marker.get("myData");
		var content = '<table> <tr><td></td><td id="closeBtn" align="right"><img src="images/close.png"/></td></tr>' ;
		content+='<tr><td class="popup_heading subfdr_prompt">Sub-Fdr: </td><td class="popup_data subfdr_value">'+myData.subfdr+'</td></tr>';
		var linesection = myData.line_sect;
		if (linesection === null || linesection == undefined) {
		linesection = 'Not Available';
		}
		content += '<tr><td class="popup_heading linesect_prompt">Linesection: </td><td class="linesect_value">' + linesection + '</td></tr>';
		content += '<tr><td class="popup_heading">Total Meters: </td><td class="popup_data">' + myData.cust_affected + '</td></tr>';
		if (labels[myData.tablename] == undefined){
				  console.log('No labels for table:'+myData.tablename);
		}else{
		  if (labels[myData.tablename]['label1'] != null && labels[myData.tablename]['label1']!=''){
			  content+='<tr><td class="popup_heading">'+labels[myData.tablename]['label1']+': </td><td class="popup_data">'+myData.label1+'</td></tr>';
		  }
		  if (labels[myData.tablename]['label2'] != null && labels[myData.tablename]['label2']!=''){
			  content+='<tr><td class="popup_heading">'+labels[myData.tablename]['label2']+': </td><td class="popup_data">'+myData.label2+'</td></tr>';
		  }
		  if (labels[myData.tablename]['label3'] != null && labels[myData.tablename]['label3']!=''){
			  content+='<tr><td class="popup_heading">'+labels[myData.tablename]['label3']+': </td><td class="popup_data">'+myData.label3+'</td></tr>';
		  }
		}

		content+='<tr><td class="popup_heading">Time Predicted: </td><td class="popup_data">' + myData.time_predicted + '</td></tr><tr><td colspan="2">';
		if (can_create_incident !== 'FALSE') {
			content += '<button class="ui-btn ui-btn-a ui-btn-icon-check" onclick="javascript:createDeviceOutage(\'' + myData.index + '\',\'prediction\');">Create Device Outage</button>';
		}
		if (params.MeterPings == 1) {
			content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:pingRequest(\''+ myData.index +'\',\'predictions\', \'device\');">Ping Meters DL</button>';
		}
		if (myData.enettype != null) {
			content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-u" onclick="javascript:traceLine(\''+myData.line_sect+'\',\'up\');">Trace Upline</button>';
			content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-d" onclick="javascript:traceLine(\''+myData.line_sect+'\',\'down\');">Trace Downline</button>';
		}
		content += '</td></tr></table>';
		infoWindow.setContent(content);
		infoWindow.open(marker);
		setTimeout( function() {
			$("#closeBtn").on("click", function() {
				infoWindow.close();
			});
		},500);		
	}


}
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
function seeDetails(index, type) {
//CONSUMER

	var data;
	if (type === 'crew') {
		data = crews[parseInt(index)].data;
	}
	if (type === 'device') {
		data = markers[parseInt(index)].data;
	}
	var html = '';
	if (data.type === 'device') {
		html += '<tr><td class="popup_heading">My Status:</td><td>' + data.status + '</td></tr>';
		html += '<tr><td class="popup_heading">Meters Affected:</td><td>' + data.consumers_affected + '</td></tr>';
		var ert = data.estimated_restore_time;
		if (ert === null) {
			ert = "Not Set";
		}
		html += '<tr><td class="popup_heading">ERT:</td><td>' + ert + '</td></tr>';
		html += '<tr><td class="popup_heading">Duration:</td><td>' + data.duration + '</td></tr>';
		html += '<tr><td class="popup_heading">Device:</td><td>' + data.device_code + '</td></tr>';
		html += '<tr><td class="popup_heading subfdr_prompt">Sub-Fdr:</td><td class="subfdr_value">' + data.subfdr + '</td></tr>';
		if (data.linesection != null && data.linesection != undefined) {
		    html += '<tr><td class="popup_heading linesect_prompt">Linesection: </td><td class="linesect_value">' + data.linesection + '</td></tr>';
    	}
		if (data.pole != null) {
			html += '<tr><td class="popup_heading pole_prompt">Pole:</td><td class="pole_value">' + data.pole + '</td></tr>';
		}
		if (data.map_location != null) {
			html += '<tr><td class="popup_heading">Map Location:</td><td>' + data.map_location + '</td></tr>';
		}
		html += '<tr><td class="popup_heading">Entered By:</td><td>' + data.entered_by + '</td></tr>';
		html += '<tr><td class="popup_heading">Disp By:</td><td>' + data.dispatched_by + '</td></tr>';
		html += '<tr><td class="popup_heading">Disp On:</td><td>' + data.dispatched_on + '</td></tr>';
		var comments = data.outage_comment;
		if (comments === null) {
			comments = '';
		}
		html += '<tr><td class="popup_heading">Comments:</td><td>' + comments + '</td></tr>';
	} else if (data.type === 'consumer') {
		html += '<tr><td class="popup_heading">Customer:</td><td>' + data.name + '</td></tr>';
		html += '<tr><td class="popup_heading">Location:</td><td>' + data.location + '</td></tr>';
		html += '<tr><td class="popup_heading">Phone: </td><td class="popup_data">';
		for (var key in data.phones) {
			html += data.phones[key] + ': ' + key + '<br>';
		}
		html += '</td></tr>';
		html += '<tr><td class="popup_heading">Duration:</td><td>' + data.duration + '</td></tr>';
    	html += '<tr><td class="popup_heading subfdr_prompt">Sub-Fdr: </td><td class="subfdr_value">' + data.subfdr + '</td></tr>';
		if (data.linesection !== null && data.linesection != undefined) {
		    html += '<tr><td class="popup_heading linesect_prompt">Linesection: </td><td class="linesect_value">' + data.linesection + '</td></tr>';
		}

		if (data.pole != null) {
			html += '<tr><td class="popup_heading pole_prompt">Pole:</td><td class="pole_value">' + data.pole + '</td></tr>';
		}
		if (data.map_location !=null) {
			html += '<tr><td class="popup_heading">Map Location:</td><td>' + data.map_location + '</td></tr>';
		}
		html += '<tr><td class="popup_heading">Entered By:</td><td>' + data.entered_by + '</td></tr>';
		html += '<tr><td class="popup_heading">Disp By:</td><td>' + data.dispatched_by + '</td></tr>';
		html += '<tr><td class="popup_heading">Disp On:</td><td>' + data.dispatched_on + '</td></tr>';
		var comments = data.outage_comment;
		if (data.outage_comment != null) {
			html += '<tr><td>Comments:</td><td>' + comments + '</td></tr>';
		}

	} else {
		html += '<tr><td class="popup_heading">Customer:</td><td>' + data.name + '</td></tr>';
		html += '<tr><td class="popup_heading">Location:</td><td>' + data.location + '</td></tr>';
		html += '<tr><td class="popup_heading">Phone:</td><td>' + data.phone + '</td></tr>';
		html += '<tr><td class="popup_heading">Due:</td><td>' + data.due_date + '</td></tr>';
		html += '<tr><td class="popup_heading">Disp By:</td><td>' + data.dispatched_by + '</td></tr>';
		html += '<tr><td class="popup_heading">Disp On:</td><td>' + data.dispatched_on + '</td></tr>';
		var comments = data.description;
		if (comments === null) {
			comments = '';
		}
		html += '<tr><td class="popup_heading">Comments:</td><td>' + comments + '</td></tr>';
	}
	$("#details_table").html(html);
    if ($(window).width() < 600) {
        $("#details_container").css({'position': 'absolute', 'top': 0, 'left': 0, 'z-index': 9999,'width':windowwidth, 'height':windowheight, 'padding':'0px','margin':'0px','overflow':'scroll'});
    }else{
        $("#details_container").css({'position': 'absolute', 'top': windowheight * .3, 'left': windowwidth * .4, 'z-index': 9999});
    }
	$("#details_container").show();
}
/*----------------------------------------------------------------------------*/
function createDeviceOutage(index, from, enettype) {
	$("#btnClose").trigger("click");
	if (from === 'prediction') {
		item = predictions[index];
	} else if (from === 'search') {
		if (searchresults[index].get('myData').outage_type == 'CONSUMER') {
        createIndividualOutage(index, 'search');
        return;
    }
		item = searchresults[index];
    } else if (from == 'disconn_markers') {
    	disconn_markers[enettype][index].get('myData').PhasesUsed = disconn_markers[enettype][index].get('myData').phase;
    	disconn_markers[enettype][index].get('myData').line_sect = disconn_markers[enettype][index].get('myData').enetworkid;
    	item = 	disconn_markers[enettype][index];
    }
	/*span.ui-slider-label ui-slider-label-a ui-btn-a ui-corner-allctive ui-btn-corner-all*/
	/*span.ui-slider-label ui-slider-label-a ui-btn-a ui-corner-allctive ui-btn-corner-all*/
	/*<span class="ui-slider-label ui-slider-label-a ui-btn-a ui-corner-allctive ui-btn-corner-all" role="img" style="width: 100%; color: rgb(126, 178, 56); background-color: transparent;">On</span>*/
	var html = '';
	html += '<tr><td colspan="2">My Status:<select id="c_disp_status" name="disp_status" data-theme="a"><option value="">None</option>';
	for (var i = 0; i < statusCodes.length; i++) {
		var code = statusCodes[i];
		html += '<option value="' + code['class_name'] + '">' + code['class_name'] + ' - ' + code['class_description'] + '</option>';
	}
	html += '</select></td></tr>';
	html += '<tr><td>Estimated Restore Time:</td></tr>' +
		'<tr><td><select name="c_ert_select" id="c_ert_select" onchange="javascript:setLAERT(this);"><option value="">None</option><option value="30">30 minutes</option><option value="60">1 hour</option><option value="120">2 hours</option><option value="240">4 hours</option><option value="480">8 hours</option><option value="1440">24 hours</option></select><input id="c_ert" type="text"></td></tr>';
	html += '<tr><td>Phases:</td></tr><tr><td>';
	if (item.get('myData').PhasesUsed.indexOf('A') !== -1) {
		html += ' A:<select name="layers" class="phase" id="c_phase_a" data-theme="b" data-role="slider" data-mini="true" value="3"><option value="0">Off</option><option value="3">On</option></select>';
	}
	if (item.get('myData').PhasesUsed.indexOf('B') !== -1) {
		html += ' B:<select name="layers" class="phase" id="c_phase_b" data-theme="b" data-role="slider" data-mini="true" value="3"><option value="0">Off</option><option value="3">On</option></select>';
	}
	if (item.get('myData').PhasesUsed.indexOf('C') !== -1) {
		html += ' C:<select name="layers" class="phase" id="c_phase_c" data-theme="b" data-role="slider" data-mini="true" value="3"><option value="0">Off</option><option value="3">On</option></select>';
	}
	html += '</td></tr>';
	html += '<tr><td>Comments:</td></tr>' +
		'<tr><td colspan="2"><TEXTAREA name="comments" id="c_update_comments" rows="5" style="width:380px;"></TEXTAREA></td></tr>';
	html +=
        '<tr><td align="right" id="scout_comment_prompt">Needs Scouting:</td>'+
        '<td align="left"><input type="checkbox" value="1" id="needs_scouting"></td></tr>';
	html += '<tr><td><button class="ui-btn ui-btn-a ui-btn-icon-left ui-icon-check" style="width:200px;text-align:center;" data-theme="a" onclick="javascript:doCreateDeviceOutage();">Create</button></tr>';
	/*PhaseStatus_A: 1
	 PhaseStatus_B: 2
	 PhaseStatus_C: 2
	 PhasesUsed: "A  "
	 cust_affected: 4
	 cust_downline: 4
	 device_name: ""
	 district: "Ender"
	 enettype: "TRN"
	 fdrnum: 2
	 lat: "26.2127365114674"
	 line_sect: "TR857522822"
	 lon: "-98.0063487229337"
	 map_loc: null
	 num_incidents: 1
	 phPwrStatus: null
	 subfdr: "46-2"
	 subnum: 46
	 time_predicted: "2014/01/13 15:29:00"*/
	$("#index").val(index);
	$("#from").val(from);
	$("#create_table").html(html);
	$("#c_ert").val(item.get('myData').estimated_restore_time);
	$("#c_update_comments").val(item.get('myData').outage_comment);
	$("#c_disp_status").val(item.get('myData').status);
	if ((item.get('myData').PhaseStatus_A == undefined || item.get('myData').PhaseStatus_A == 1) && item.get('myData').PhasesUsed.indexOf('A') !== -1) {
		item.get('myData').PhaseStatus_A=0;
	}
	$("#c_phase_a").val(item.get('myData').PhaseStatus_A);
	if ((item.get('myData').PhaseStatus_B == undefined || item.get('myData').PhaseStatus_B == 1)  && item.get('myData').PhasesUsed.indexOf('B') !== -1) {
		item.get('myData').PhaseStatus_B=0;
	}
	$("#c_phase_b").val(item.get('myData').PhaseStatus_B);
	if ((item.get('myData').PhaseStatus_A == undefined || item.get('myData').PhaseStatus_A == 1)  && item.get('myData').PhasesUsed.indexOf('C') !== -1) {
		item.get('myData').PhaseStatus_C=0;
	}
	$("#c_phase_c").val(item.get('myData').PhaseStatus_C);
	//$(".jqbutton").button();
	$("#c_disp_status").selectmenu();
	$(".phase").slider();
	$("#create_table span.ui-slider-label-b").css("color", "red");
	$("#create_table span.ui-slider-label-a").css("color", "#7eb238");
	$("#create_table .ui-btn-a ui-corner-allctive").css({'background-color': 'transparent', 'background-image': 'none', 'background': 'none'});
    if (!(isTablet)) {
        $("#create_container").css({'position': 'absolute', 'top':0, 'left': 0, 'z-index': 9999, 'width':$("#map_canvas").width(), 'height':$("#map_canvas").height(), 'padding':'0px','margin':'0px','overflow':'scroll'});
    }else{
        $("#create_container").css({'position': 'absolute', 'top': $("#mapheader").height()+ios7Pad, 'left': 0, 'z-index': 9999, 'width':$("#map_canvas").width(), 'height':$("#map_canvas").height()-40});
    }
    $("#c_update_comments").css({'width':$("#create_container").width()-12});
    $("#create_container button").parent().css({'max-width':'95%'});
    $("#create_container select").parent().css({'max-width':'95%'});
    $("#create_container textarea").css({'max-width':'95%'});
	$("#create_container").show();
	if (navigator.connection.type == Connection.NONE) {
		return;
	}
	$.ajax({
		type: "POST",
		url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php?callback=?",
		data: {action:'get',target:'auto_ert',linesection:item.get('myData').line_sect, appname: appname},
		dataType: "json",
		timeout: 2000,
		success: function(msg) {
			if (msg.result !== 'true') {
				jAlert(msg.error);
				return;
			}
			$("#c_ert").val(msg.auto_ert);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			jAlert("Unable to load estimated restore time from server.  Error: " + errorThrown + textStatus);
		}
	});
}
/*----------------------------------------------------------------------------*/
function doCreateDeviceOutage(index, from) {
	if ($("#from").val() === 'prediction') {
		item = predictions[$("#index").val()];
	} else if ($("#from").val() === 'search') {
		item = searchresults[$("#index").val()];
	}
	var phaseStatus = '';
	var allon = true;
	if (item.get('myData').PhasesUsed.indexOf('A') !== -1) {
		var phaseA = "off";
		if ($("#c_phase_a").val() == 3) {
			phaseA = "on";
			phaseStatus += 'A';
		}else{
			allon=false;
		}
	}
	if (item.get('myData').PhasesUsed.indexOf('B') !== -1) {
		var phaseB = "off";
		if ($("#c_phase_b").val() == 3) {
			phaseB = "on";
			phaseStatus += 'B';
		}else{
			allon=false;
		}
	}
	if (item.get('myData').PhasesUsed.indexOf('C') !== -1) {
		var phaseC = "off";
		if ($("#c_phase_c").val() == 3) {
			phaseC = "on";
			phaseStatus += 'C';
		}
		else{
			allon=false;
		}
	}
	if (allon) {
		jAlert("At least one phase must be turned off.");
		return;
	}
	$("#create_container").hide();
	/*PhaseStatus:A
	 DispatchUpdated:false
	 AType:Add
	 LineSection:FS.3128
	 IncidentID:
	 ServiceIndex:1
	 EnteredBy:dv_admin
	 DeviceCode:F223-761-1
	 LastUpdated:
	 OriginalPhaseStatus:ABC
	 TextToSpeech:
	 TextToSpeech2:
	 backURL:
	 radioPhaseA:off
	 radioPhaseB:off
	 EnteredDate:2014/01/28 09:10
	 radioPhaseC:off
	 EstRestoreTime:2014/01/28 10:11
	 CustName:
	 remLen1:254
	 remLen2:250
	 undefined:Cancel
	 Command:map
	 District:_B
	 ERT:60
	 ClassCode:B
	 CauseCode:01
	 EquipCode:03
	 EquipFail:01
	 OptCode1:01
	 OptCode2:03
	 Comments:
	 outageText:*/
   	var date_obj = new Date();
	var entered_date = padDigits((parseInt(date_obj.getMonth()) + 1), 2) + '/' + padDigits(date_obj.getDate(), 2) + '/' + date_obj.getFullYear() + ' ' + padDigits(date_obj.getHours(), 2) + ':' + padDigits(date_obj.getMinutes(), 2);
    var ajaxParams={action: 'create',
        target: 'device_outage',
        AType: 'Add',
        LineSection: item.get('myData').line_sect,
        ServiceIndex: service_index,
        EnteredBy: user,
        crew: curr_crew,
        view: curr_view,
        OriginalPhaseStatus: item.get('myData').PhasesUsed,
        radioPhaseA: phaseA,
        radioPhaseB: phaseB,
        radioPhaseC: phaseC,
        District: item.get('myData').district,
        Comments: $("#c_update_comments").val(),
        EstRestoreTime: $("#c_ert").val(),
        Status: $("#c_disp_status").val(),
        EnteredDate: entered_date,
        PhaseStatus: phaseStatus,
        DeviceCode: item.get('myData').device_name,
        correlationId: index,
        appname: appname,
        needs_scouting:($("#needs_scouting").is(":checked"))?'Y':'' };

    if (navigator.connection.type == Connection.NONE) {
        storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php?callback=?", ajaxParams);
        return;
    }
	$.ajax({
		type: "POST",
		url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php?callback=?",
		data: ajaxParams,
		dataType: "json",
		success: function(msg) {
			if (msg.result !== 'true') {
				jAlert(msg.error);
				return;
			}
			refreshGoogleMap();
			if (localStorage.getItem('ckbx_PrimaryLines') !== 'false') {
				addPrimLineLayer();
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			jAlert("Failed to create device outage. " +ajaxUrl + " error: " + errorThrown + textStatus);
		}
	});
}



/*/*----------------------------COLLAPSIBLE ASSIGNMENT MENU------------------------------------------------*/
var crewCount = {};
function popAssignmentsMenu() {
    $("#assignments_menu").css({position: 'absolute', top: $('#map_canvas').offset().top, left: 0, height: $('#map_canvas').height() + 'px', 'max-width': '500px', width:($(window).width()*.98)+'px'});
    clearExistingBottomMenus('assignments_menu');
    var html = '<div data-role="collapsible-set" style="padding:0px;width:100%">';
    html += '<div data-role="collapsible" data-collapsed="false" style="padding:0px;width:100%"><h3>'+user+'<span class="ui-li-count" id="'+user_number+'_count"></span></h3><div data-role="content" class="scrollable" style="padding:0px;width:100%"><table id="crews_' +user_number+ '"></table></div></div>';
    crewCount[user_number] = 0;
    for (var i = 0 ; i < crew_info.length ; i++) {
    	html+='<div data-role="collapsible" data-collapsed="true" style="padding:0px;width:100%">';
    	html+=	'<h3>'+crew_info[i].crew_name+'<span class="ui-li-count" id="'+crew_info[i].crew_number+'_count"></span></h3>';
    	html+=	'<div data-role="content" class="scrollable" style="padding:0px;width:100%"><table id="crews_' +crew_info[i].crew_number+ '"></table></div></div>';
    	crewCount[crew_info[i].crew_number]=0;
    }
    html+='</div>';
    $("#assignments_list").html(html);
    
    
    for (var j = 0 ; j <  crews.length ; j++) {
    	var i = j;
        html = '<div data-role="collapsible" class="can_resort" data-collapsed="true" style="padding:0px;width:100%">';
        if (crews[i].get('myData').isClosing) {
        	html = '<div data-role="collapsible" data-collapsed="true" style="padding:0px;width:100%;">';
        	html += '<h3><div style="float:left;color:red;"><img src="' + crews[i].get('icon').url + '"/></div><div style="float:left;color:red;">**CLOSING-' +crews[i].get('myData').line1 + '<br>'+crews[i].get('myData').line2+ '</div></h3>';
        }else{
			if (crews[i].get('myData').scheduled_time != null && crews[i].get('myData').scheduled_time.length > 0) {
				html += '<h3><div style="float:left;"><img src="' + crews[i].get('icon').url + '"/></div><div style="float:left;">' +crews[i].get('myData').line1 + '<br>'+crews[i].get('myData').line2+ '</div><div style="float:right;"><img width="40" height="40" src="images/appointment.png"/></div></h3>';
			}else if (params.hasScoutingFeature == 'true' && crews[i].get('myData').scouting_ticket == 'Y') {
				html += '<h3><div style="float:left;"><img src="' + crews[i].get('icon').url + '"/></div><div style="float:left;">' +crews[i].get('myData').line1 + '<br>'+crews[i].get('myData').line2+ '</div><div style="float:right;"><img width="40" height="40" src="images/scout-dispatched.png"/></div></h3>';
			}else {
				html += '<h3><div style="float:left;"><img src="' + crews[i].get('icon').url + '"/></div><div style="float:left;">' +crews[i].get('myData').line1 + '<br>'+crews[i].get('myData').line2+ '</div></h3>';
			}
        }
        html += '<div data-role="content"  class="scrollable"><table id="crewtable_' + i + '" style="padding:0px;width:100%">';
        html+= '<tr><td class="popup_heading">Work Item: </td><td class="popup_data">' + crews[i].get('myData').work_item_number + '</td></tr>';
        var status=crews[i].get('myData').status;
        if (crews[i].get('myData').isClosing) {
        	status='<span style="color:red;">CLOSING</span>';
        }
        else if (crews[i].get('myData').status == null || crews[i].get('myData').status == '') {
            status='Not Set';
        }
        html+='<tr><td class="popup_heading" >My Status: </td><td class="popup_data" id="crew_list_status_' + i + '">' + status + '</td></tr>';
if (crews[i].get('myData').disptype != null && crews[i].get('myData').disptype != undefined) {
            html += '<tr><td class="bold">Type: </td><td>' + crews[i].get('myData').disptype + '</td></tr>';
        }
        if (crews[i].get('myData').description != null && crews[i].get('myData').description != '' ){
            html += '<tr><td class="bold">Description: </td><td>' + crews[i].get('myData').description + '</td></tr>';
        }
        if (crews[i].get('myData').wavefile_name != null && crews[i].get('myData').wavefile_name != '') {
			html+= '<tr><td colspan="2">IVR Message: <audio controls><source src="'+localStorage.ims_url+crews[i].get('myData').wavefile_name+'" type="audio/mpeg"></audio controls></td></tr>';
		}
        html += '<tr><td class="bold">Name: </td><td>' + crews[i].get('myData').name + '</td></tr>';
        if (crews[i].get('myData').type == 'service') {
        	html+= '<tr><td class="popup_heading">Location Description: </td><td class="popup_data">' + crews[i].get('myData').service_loc_description + '</td></tr>' +
			'<tr><td class="popup_heading">Transformer Number: </td><td class="popup_data">'+ crews[i].get('myData').service_loc_transformer_number1 + '</td></tr>'+
			'<tr><td class="popup_heading">Transformer Size: </td><td class="popup_data">'+ crews[i].get('myData').service_loc_transformer_size + '</td></tr>'+
			'<tr><td class="popup_heading">Substation: </td><td class="popup_data">' + crews[i].get('myData').subfdr + '</td></tr>'+
			'<tr><td class="popup_heading">District: </td><td class="popup_data">'+ crews[i].get('myData').district + '</td></tr>'+
			'<tr><td class="popup_heading">City: </td><td class="popup_data">'+ crews[i].get('myData').service_loc_city + '</td></tr>';
        }
        if (crews[i].get('myData').comment != null) {
        	html+='<tr><td class="popup_heading">Comments: </td><td class="popup_data" style="word-break:break-all;" id="crew_list_comments_' + i + '">'+ crews[i].get('myData').comment + '</td></tr>';
        }
        if (crews[i].get('myData').type !== 'device') {
            html += '<tr><td class="popup_heading">Phone: </td><td class="popup_data">';
            for (var key in crews[i].get('myData').phones) {
                html += '<br>' + crews[i].get('myData').phones[key] + ': ' + key;
            }
            html += '</td></tr>';
            if (crews[i].get('myData').acctno != null && crews[i].get('myData').acctno != undefined) {
                html += '<tr><td class="bold account_prompt">Account: </td><td class="account_value">' + crews[i].get('myData').acctno + '</td></tr>';
            }
            if (crews[i].get('myData').meter_number != null && crews[i].get('myData').meter_number != undefined) {
            	html+='<tr><td class="bold meter_prompt">Meter Number: </td><td class="meter_value">'+crews[i].get('myData').meter_number+'</td></tr>';
            }
        }else {
            html += '<tr><td class="bold subfdr_prompt">Alias: </td><td class="bold">' + crews[i].get('myData').account_name + '</td></tr>';
            html += '<tr><td class="bold subfdr_prompt">Sub-Fdr: </td><td class="subfdr_value">' + crews[i].get('myData').subfdr + '</td></tr>';
            html+='<tr><td class="bold">Device Type: </td><td>' + crews[i].get('myData').device_code + '</td></tr>';
            html+='<tr><td class="bold">Device Name: </td><td>' + stripNulls(crews[i].get('myData').device_code_desc) + '</td></tr>';
            html += '<tr><td class="bold subfdr_prompt">Phase: </td><td class="bold">' + crews[i].get('myData').PhasesUsed + '</td></tr>';
            if (labels[crews[i].get('myData').gistablename] !== undefined && labels[crews[i].get('myData').gistablename]['label1'] !== null && labels[crews[i].get('myData').gistablename]['label1'].length > 0 && crews[i].get('myData').label1 != null) {
							html += '<tr><td class="bold">' + labels[crews[i].get('myData').gistablename]['label1'] + ': </td><td>' + crews[i].get('myData').label1 + '</td></tr>';
			}
			if (labels[crews[i].get('myData').gistablename] !== undefined && labels[crews[i].get('myData').gistablename]['label2'] !== null && labels[crews[i].get('myData').gistablename]['label2'].length > 0 && crews[i].get('myData').label2 != null) {
				html += '<tr><td class="bold">' + labels[crews[i].get('myData').gistablename]['label2'] + ': </td><td>' + crews[i].get('myData').label2 + '</td></tr>';
			}
			if (labels[crews[i].get('myData').gistablename] !== undefined && labels[crews[i].get('myData').gistablename]['label3'] !== null && labels[crews[i].get('myData').gistablename]['label3'].length > 0 && crews[i].get('myData').label3 != null) {
				html += '<tr><td class="bold">' + labels[crews[i].get('myData').gistablename]['label3'] + ': </td><td>' + crews[i].get('myData').label3 + '</td></tr>';
			}
        }
        if (crews[i].get('myData').map_location != null) {
            html += '<tr><td class="bold maploc_prompt">Map Location: </td><td class="maploc_value">' + crews[i].get('myData').map_location + '</td></tr>';
        }
        if (crews[i].get('myData').linesection != null && crews[i].get('myData').linesection != undefined) {
            html += '<tr><td class="bold linesect_prompt">Linesection: </td><td class="linesect_value">' + crews[i].get('myData').linesection + '</td></tr>';
        }
        if (crews[i].get('myData').pole != null && crews[i].get('myData').pole != undefined) {
            html += '<tr><td class="bold pole_prompt">Pole: </td><td class="pole_value">' + crews[i].get('myData').pole + '</td></tr>';
        }

        html += '<tr><td class="bold">Dispatched By: </td><td>' + crews[i].get('myData').dispatched_by + '</td></tr>';
        html += '<tr><td class="bold">Dispatched On: </td><td>' + crews[i].get('myData').dispatched_on + '</td></tr>';
        if (crews[i].get('myData').type !== 'device' && crews[i].get('myData').type !== 'consumer' && crews[i].get('myData').type !== 'group') {
            if (crews[i].get('myData').scheduled_time != null) {
                html += '<tr><td class="bold">Meeting Scheduled: </td><td style="color:red;">' + crews[i].get('myData').scheduled_time + '</td></tr>';
            }
            var due_date = crews[i].get('myData').due_date+' '+crews[i].get('myData').due_time;
            if (due_date == null || due_date == undefined) {
                due_date = 'None Given';
            }
            html += '<tr><td class="bold">Requested Date: </td><td>' + due_date +'</td></tr>';
        }else{
            html += '<tr><td class="bold">Duration: </td><td>' + crews[i].get('myData').duration + '</td></tr>';
            var ert = crews[i].get('myData').estimated_restore_time;
            if (ert === null) {
                ert = "Not Set";
            }
            html += '<tr><td class="bold">Est Restore: </td><td id="crew_list_ert_' + i + '">' + ert + '</td></tr>';
        }
        var entered_by = crews[i].get('myData').entered_by;
        if (entered_by === null || entered_by == undefined) {
            entered_by = "Unknown";
        }
        html += '<tr><td class="bold">Entered By: </td><td>' + entered_by + '</td></tr>';
        html += '<tr><td colspan="2">'
        if (crews[i].get('myData').lat != null){
            html += '<button class="ui-btn ui-btn-a ui-btn-inline ui-btn-icon-right ui-icon-location" style="width:200px;" onclick="javascript:goto(\'' + i + '\', \'crew\');" >Go To Assignment</button>';
            html += '<button class="ui-btn ui-btn-a ui-btn-inline ui-btn-icon-right ui-icon-navigation" style="width:200px;" onclick="javascript:getDirections(\'' + i + '\', \'crew\');" >Get Directions</button>';
        }
        if (crews[i].get('myData').type !== 'device' && crews[i].get('myData').type !== 'consumer' && crews[i].get('myData').type !== 'group') {
            html+='<button class="ui-btn ui-btn-a ui-btn-inline ui-btn-icon-right ui-icon-eye" style="width:200px;" onclick="javascript:seeNotes(\''+crews[i].get('myData').work_item_type+'\', \''+crews[i].get('myData').work_item_number+'\');">See Notes</button>';
        }
        if (can_reassign_items !== 'FALSE') {
        	html+='<button class="ui-btn ui-btn-a ui-btn-inline ui-btn-icon-right ui-icon-user" style="width:200px;" onclick="javascript:reassignTicket(\''+curr_view+'\',\'' + crews[i].get('myData').work_item_number + '\', \''+crews[i].get('myData').work_item_type+'\');">Reassign</button>';
        }
        html+='<button class="ui-btn ui-btn-a ui-btn-inline ui-btn-icon-right ui-icon-edit" style="width:200px;" onclick="javascript:updateLAIncident(\'' + i + '\');">Update</button>';
        html+='<button class="ui-btn ui-btn-a ui-btn-inline ui-btn-icon-right ui-icon-delete" style="width:200px;" onclick="javascript:closeTicket(\'' + i + '\');">Close</button>';
        html+='<button class="ui-btn ui-btn-a ui-btn-inline ui-btn-icon-right ui-icon-camera" style="width:200px;" onclick="javascript:crewIndex=\''+i+'\';popCameraMenu();">Attach Picture</button></td></tr>';
        html += '</table>';
        html += '</div></div>';
		crewCount[crews[i].get('myData').disp_entity_number]++;
        $("#crews_"+crews[i].get('myData').disp_entity_number).append(html);
    }
	for ( var crew in crewCount) {
		var color = $("#"+crew+"_count").css('color');
		if (crewCount[crew] > 0) {
			color="red";
		}
		$("#"+crew+"_count").html(crewCount[crew]).css('color',color); 
	}
    $('#assignments_list').trigger('create')
    $(".can_resort").sortable();
    $('.can_resort').disableSelection();
            /// Refresh list to the end of sort to have a correct display
    $('.can_resort').bind( "sortstop", function(event, ui) {
    	$('#assignments_list').collapsibleset('refresh');
    });
    $(".ui-collapsible-content").css('padding','0px');
    setTimeout(function() {$("#assignments_list .ui-collapsible-set").css({'will-change':'transform'}); $("#assignments_list h3").css('width',$("#assignments_list").width());},400);
    $("#assignments_list button").parent().css({'max-width':'95%'});
    $("#assignments_list select").parent().css({'max-width':'95%'});
    $("#assignments_list textarea").css({'max-width':'95%'});

	fixTitlesBySvcIdx("1");
}


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
function createIndividualOutage(idx, from) {

	var html = '';
	html += '<tr><td colspan="2">My Status:<select id="c_disp_status" name="disp_status" data-theme="a"><option value="">None</option>';
	for (var i = 0; i < statusCodes.length; i++) {
		var code = statusCodes[i];
		html += '<option value="' + code['class_name'] + '">' + code['class_name'] + ' - ' + code['class_description'] + '</option>';
	}
	html += '</select></td></tr>';
	html += '<tr><td>Comments:</td></tr>' +
		'<tr><td colspan="2"><TEXTAREA name="comments" id="c_update_comments" rows="5"></TEXTAREA></td></tr>';
	html += '<tr><td><button class="ui-btn ui-btn-a ui-btn-icon-left ui-icon-check" style="width:200px;text-align:center;" data-theme="a" onclick="javascript:doCreateIndividualOutage();">Create</button></tr>';
	$("#index").val(idx);
	$("#from").val(from);
	$("#create_table").html(html);
	//$(".jqbutton").button();
	$("#c_disp_status").selectmenu();
    if (!(isTablet)) {
        $("#create_container").css({'position': 'absolute', 'top':0, 'left': 0, 'z-index': 9999, 'width':$("#map_canvas").width(), 'height':$("#map_canvas").height(), 'padding':'0px','margin':'0px','overflow':'scroll'});
    }else{
        $("#create_container").css({'position': 'absolute', 'top': $("#mapheader").height()+ios7Pad, 'left': 0, 'z-index': 9999, 'width':$("#map_canvas").width(), 'height':$("#map_canvas").height()-40});
    }
    $("#c_update_comments").css({'width':$("#create_container").width()-12});
    $("#create_container button").parent().css({'max-width':'95%'});
    $("#create_container select").parent().css({'max-width':'95%'});
    $("#create_container textarea").css({'max-width':'95%'});
	$("#create_container").show();
}
/*----------------------------------------------------------------------------*/
function doCreateIndividualOutage() {
	item = searchresults[$("#index").val()];
	$("#create_container").hide();
    if (navigator.connection.type == Connection.NONE) {
        storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php?callback=?",{action: 'get',target: 'enter_ticket',AccountNumber: item.get('myData').accountnumber,ServiceIndex: service_index,EnteredBy: user,crew: curr_crew,view: curr_view,Comments: $("#c_update_comments").val(),Status: $("#c_disp_status").val(),correlationId: $("#index").val(), appname: appname});
        return;
    }
	$.ajax({
		type: "POST",
		url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php?callback=?",
		data: {action: 'get',
			target: 'enter_ticket',
			AccountNumber: item.get('myData').accountnumber,
			ServiceIndex: service_index,
			EnteredBy: user,
			crew: curr_crew,
			view: curr_view,
			Comments: $("#c_update_comments").val(),
			Status: $("#c_disp_status").val(),
			correlationId: $("#index").val()},
		dataType: "json",
		success: function(msg) {
			if (msg.result !== 'true') {
				jAlert(msg.error);
				return;
			}
			refreshGoogleMap();
			if (localStorage.getItem('ckbx_PrimaryLines') !== 'false') {
				addPrimLineLayer();
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			jAlert(ajaxUrl + "Can't create individual outage. Error: " + errorThrown + textStatus);
		}
	});
	
}
/*----------------------------------------------------------------------------*/
function reassignTicket(view_type, index, worktype) {
  var html = '';
	$("#btnClose").trigger("click");
	var incident_id = crews[index].get('myData').work_item_number;
	if (view_type == 'crew'){
		  html += '<tr><td colspan="2">Assign To:<select id="reassign_view_type" name="reassign_view_type" data-theme="a" onchange="reassignTicket(this.value, \''+incident_id+'\', \''+worktype+'\');"><option value="crew">A Crew</option><option value="individual">An Employee</option>';
		  html += '</select></td></tr>';
		  html += '<tr><td colspan="2"><select id="reassign_crew" name="reassign_crew" data-theme="a"><option value="">Select A Crew</option>';
		  for (var i = 0 ; i < assignableCrews.length ; i++){
		      html+='<option value="'+assignableCrews[i].crew_number+'">'+assignableCrews[i].name+'('+assignableCrews[i].numDispatched+')'+'</option>';
		  }
		  html += '</select></td></tr>';
	}else{
		  html += '<tr><td colspan="2">Assign To:<select id="reassign_view_type" name="reassign_view_type" data-theme="a" onchange="reassignTicket(this.value, \''+incident_id+'\', \''+worktype+'\');"><option value="individual">An Employee</option><option value="crew">A Crew</option>';
		  html += '</select></td></tr>';
		  html += '<tr><td colspan="2"><select id="reassign_crew" name="reassign_crew" data-theme="a"><option value="">Select An Employee</option>';
		  for (var i = 0 ; i < assignableEmployees.length ; i++){
		      html+='<option value="'+assignableEmployees[i].employee_number+'">'+assignableEmployees[i].name+'('+assignableEmployees[i].numDispatched+')'+'</option>';
		  }
		  html += '</select></td></tr>';

	}
	html += '<tr><td><button class="ui-btn ui-btn-a ui-btn-icon-left ui-icon-user" style="width:200px;text-align:center;" data-theme="a" onclick="javascript:$(\'#update_container\').hide();assignTicket(\''+incident_id+'\', \''+worktype+'\', $(\'#reassign_view_type\').val(),$(\'#reassign_crew\').val());">Reassign</button></tr>';
	$("#update_table").html(html);
	//$(".jqbutton").button();
	$("#reassign_view_type").selectmenu();
	$("#reassign_crew").selectmenu();
	if (!(isTablet)) {
		  $("#update_container").css({'position': 'absolute', 'top': 0, 'left': 0, 'z-index': 9999,'width':'100%', 'height':'100%', 'padding':'0px','margin':'0px','overflow':'scroll'});
	}else{
		  $("#update_container").css({'position': 'absolute', 'top': $(window).height() * .2, 'left': $(window).width() * .3, 'z-index': 9999});
	}
	$("#update_container button").parent().css({'max-width':'95%'});
	$("#update_container select").parent().css({'max-width':'95%'});
	$("#update_container textarea").css({'max-width':'95%'});
	$("#update_container").show();
	$("#update_container").css({'width':$("#update_table").width()+'px'});

}
/*----------------------------------------------------------------------------*/
function assignTicket(incident_id, worktype, view, crew) {
    if (navigator.connection.type == Connection.NONE) {
        storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php?callback=?",{action: 'assign',target: 'outage',incident_id: incident_id,worktype: worktype,ServiceIndex: service_index,EnteredBy: user,crew: crew,view: view, appname: appname});
        return;
    }
	$.ajax({
		type: "POST",
		url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php?callback=?",
		data: {action: 'assign',
			target: 'outage',
			incident_id: incident_id,
            worktype: worktype,
			ServiceIndex: service_index,
			EnteredBy: user,
			crew: crew,
			view: view},
		dataType: "json",
		success: function(msg) {
			if (msg.result !== 'true') {
				jAlert(msg.error);
				return;
			}
			//reassignId = msg.incident_id;
			refreshGoogleMap();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			console.log(ajaxUrl + " error: " + errorThrown + textStatus);
			alert("Can't assign ticket Error: "+ errorThrown);
		}
	});
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
function setLAERT(elem) {
    console.log("setLAERT was fired");
    if (navigator.connection.type == Connection.NONE) {
        $('#connectionmessage').hide();
        $('#connectionmessage').html('Unable to update ert with no internet connection');
        $('#connectionmessage').show();
        $('#blockingcanvas').hide();
        setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 3000);
        return;
    }

	minutes = elem.value;
	if (isNaN(minutes) || minutes.length === 0 || minutes === 0 || minutes === '0') {
		$("#c_ert").val('');
		$("#c_ert_select").val('');
		return;
	}
	// Get the server time plus the selected minutes offset
	$.ajax({
		url: localStorage.getItem('ims_url') + "/dvwfm2/backoffice/ajax/ajaxGetCurrentTime.php",
		data: {format: "Y/m/d H:i", offset: minutes},
		dataType: 'jsonp',
		type: 'GET',
		success: function(msg) {
			if (msg.result === 'true') {
				$("#" + elem.id.replace("_select","")).val(msg.time);
				return;
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			jAlert("AJAX failed to get server time: " + textStatus);
			return;
		}
	});
}
function getAllWorkItems(){
    if (navigator.connection.type == Connection.NONE) {
        $('#connectionmessage').hide();
        $('#connectionmessage').html('Unable to retrieve work items not assigned to you with no internet connection');
        $('#connectionmessage').show();
        $('#blockingcanvas').hide();
        setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 3000);
        return;
    }

    $.ajax({
       url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php",
       data: {action: 'get', target: 'all_work_items', appname: appname},
       dataType: 'jsonp',
       type: 'GET',
       success: function(msg) {
            if (msg.result === 'false') {
               jAlert('No Work Items');
               return;
            }
            var html = '<option value=""></option>';
            allitems=msg.items;
            for (var i = 0 ; i < allitems.length ; i++){
                html += '<option value="'+i+'-allitems">'+allitems[i].data.work_item_number+'-'+allitems[i].data.work_item_location+'</option>';
            }
            $("#pic_work_item_number").html(html);
            $("#pic_work_item_number").selectmenu();
        },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
           $("#waiting").hide();
           console.error( textStatus + errorThrown);
           jAlert("Failed to get all work items. Error: "+errorThrown);
       }
    });


}

function seeNotes(work_type, work_item_number) {
    if (navigator.connection.type == Connection.NONE) {
        $('#connectionmessage').hide();
        $('#connectionmessage').html('Unable to view notes with no internet connection');
        $('#connectionmessage').show();
        $('#blockingcanvas').hide();
        setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 3000);
        return;
    }

    $("#waiting").show();
    $.ajax({
           type: "GET",
           url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php?callback=?",
           data: {action: 'get', target: 'notes', work_type: work_type, work_item_number: work_item_number, appname: appname},
           dataType: "jsonp",
           success: function(msg) {
                if (msg.result === 'false') {
                   jAlert(msg.error);
                   return;
                }
                $("#waiting").hide();
                var html = '';
                if (msg.notes.length == 0){
                    html+='<tr><td>No Notes for this Item have been entered.</td></tr>';
                }
                for (var i = 0 ; i < msg.notes.length ; i++) {
                   html += '<tr><td>'+msg.notes[i].note+'</td></tr>';
                }
                $("#details_table").html(html);
                if ($(window).width() < 600) {
                    $("#details_container").css({'position': 'absolute', 'top': 0, 'left': 0, 'z-index': 9999,'width':windowwidth, 'height':windowheight, 'padding':'0px','margin':'0px','overflow':'scroll'});
                }else{
                    $("#details_container").css({'position': 'absolute', 'top': windowheight * .3, 'left': 10, width: $("#assignments_menu").width*.9,'z-index': 9999});
                }
                $("#details_container").show();

           },
           error: function(XMLHttpRequest, textStatus, errorThrown) {
               $("#waiting").hide();
               console.error( textStatus + errorThrown);
               jAlert("failed to get notes. Error: "+errorThrown);
           }
    });

}

var pendingPingRequests = [];
/*----------------------------------------------------------------------------*/
function pingRequest(index, from, type){
    pingTries=0;
    $("#waiting").show();
    if (from === 'predictions') {
        item = predictions[index];
        var meternumber=item.get('myData').meternumber;
    } else if (from === 'search') {
        item = searchresults[index];
        var meternumber=item.get('myData').meternumber;
    } else if (from === 'device') {
        item = markers[index];
        var meternumber=item.get('myData').meternumber;
    }else if (from === 'crew') {
        item = crews[index];
        var meternumber=item.get('myData').meter_number;
    }

    var linesection = item.get('myData').linesection;
    if (item.get('myData').linesection == undefined) {
        linesection=item.get('myData').line_sect;
    }

    if (linesection.indexOf('MTR') == -1) {
        meternumber='NONE';
    }else{
    	type='meter';
    }
    $("#waiting").show();
    $.ajax({
       type: "GET",
       url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php?callback=?",
           data: {action: 'get', target: 'ping_request', user: user, linesection: linesection, meternumber:meternumber, type:type, appname: appname},
       dataType: "jsonp",
       success: function(msg) {
           if (msg.result === 'false') {
               jAlert(msg.error);
               return;
           }
           //$("#waiting").hide();
           //$("#connectionmessage").html('Ping request '+msg.jobNo+' was successfully submitted.<br><input type="button" value="Ok" onclick="javascript:$(\'#connectionmessage\').hide();">');
           //setTimeout(function(){$("#connectionmessage").hide();},3000);
           //if (pendingPingRequests.length == 0) {
               setTimeout(checkPingRequests,3000);
           //}
           pendingPingRequests.push(msg.jobNo);
       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
           $("#waiting").hide();
           console.error( textStatus + errorThrown);
           jAlert("Failed to ping meter. Error: "+errorThrown);
       }

    });
}
/*----------------------------------------------------------------------------*/
var pingTries=0;
function checkPingRequests() {
    console.log('checking ping requests');
    if (pingTries > 50) {
        pingTries=0;
        pendingPingRequests=[];
        $("#connectionmessage").html('Reached maximum retries waiting for ping request. Please try the ping again.<br><input type="button" value="Ok" onclick="javascript:$(\'#connectionmessage\').hide();">');
        //console.log('Ping Jobs Completed are '+jobString+'. The results are being added to the map.');
        $("#connectionmessage").show();
        $("#waiting").hide();
        return;

    }
    if (pendingPingRequests.length == 0) {
        $("#waiting").hide();
        return;
    }
    pingTries++;
        $.ajax({
            type: "GET",
            url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php?callback=?",
            data: {action: 'get', target: 'check_ping_request', user: user, jobs:pendingPingRequests, appname: appname},
            dataType: "jsonp",
            success: function(msg) {
            if (msg.result === 'false') {
                jAlert(msg.error);
                return;
            }
            //$("#waiting").hide();
            var jobString = '';
            for ( var i = 0 ; i < msg.completeJobs.length ; i++ ) {
               for (var j = 0 ; j < pendingPingRequests.length ; j++) {
                   if (msg.completeJobs[i].jobno == pendingPingRequests[j] && msg.outstandingJobs[msg.completeJobs[i].jobno] == false){
                    var status=msg.completeJobs[i].status;
                   	if (msg.completeJobs[i].resp_type == 0){
                    	status = '<span style="color:green;">'+status+'</span>';
                    }else if (msg.completeJobs[i].resp_type == 2) {
                        status='<span style="color:red;">'+status+'</span>';
                    }
                    jobString+='<b>Status:</b> '+ status+
                                '<br><b>Name:</b> '+msg.completeJobs[i].name+
                                '<br><b>Address:</b> '+msg.completeJobs[i].serviceloc+
                                '<br><b>Sub:</b> '+msg.completeJobs[i].substation+
                                '<br><b>Fdr:</b> '+msg.completeJobs[i].feeder+
                                '<br><hr>';

                   }
                 }
               }

            for ( var i = 0 ; i < msg.completeJobs.length ; i++ ) {
               for (var j = 0 ; j < pendingPingRequests.length ; j++) {
               		if (msg.outstandingJobs[pendingPingRequests[j]] == false) {
                    	pendingPingRequests.splice(j,1);
                    }
               }
            }
            if (pendingPingRequests.length != 0) {
                setTimeout(checkPingRequests,3000);
            }
            if (jobString != '') {
               if ($("#connectionmessage").is(':visible')){
                $("#okbutton").remove();
                $("#connectionmessage").hide();
                $("#connectionmessage").append(jobString+'<span id="okbutton"><br><input type="button" value="Ok" onclick="javascript:$(\'#connectionmessage\').hide();setTimeout(addPrimLineLayer,3000);"></span>');
                $("#connectionmessage").show();
               }else{
                   $("#connectionmessage").html(jobString+'<span id="okbutton"><br><input type="button" value="Ok" onclick="javascript:$(\'#connectionmessage\').hide();setTimeout(addPrimLineLayer,3000);"></span>');
                   //console.log('Ping Jobs Completed are '+jobString+'. The results are being added to the map.');
                   $("#connectionmessage").show();
               }
               $("#waiting").hide();
            }


        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#waiting").hide();
            console.error( textStatus + errorThrown);
            jAlert("Failed getting ping statuses. Error: "+errorThrown);
            setTimeout(checkPingRequests,10000);
            pingTries++;
        }

    });
}

function loadedMapMakeCallFromOtherApp() {
	var center = {lat:parseFloat(g_openingArgs.lat), lng:parseFloat(g_openingArgs.lon)};
		googleMap.animateCamera( {
			'target': center,
			'tilt':0,
			'zoom': parseInt(g_openingArgs.zoom),
			'bearing':0,
			'duration':2000
		},
		function() {
			if (g_openingArgs.method == 'fakeSearch') {
				console.log("fakesearch");
				fakeSearch(g_openingArgs.lat,g_openingArgs.lon,g_openingArgs.zoom);
				g_openingArgs=null;
			}
		}
	);
}

