
/*----------------------------------------------------------------------------*/
function storeAjaxRequest(url,params) {
    $('#connectionmessage').hide();
    $('#connectionmessage').html('Request has been stored and will be submitted when internet is available.');
    $('#connectionmessage').show();
    $('#blockingcanvas').hide();
    setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 3000);

    var stored_ajax_requests = JSON.parse(localStorage.getItem('stored_ajax_requests'));
    if (stored_ajax_requests == null) {
        stored_ajax_requests = [];
    }
    stored_ajax_requests.push({url:url, params:params});
    localStorage.setItem('stored_ajax_requests',JSON.stringify(stored_ajax_requests));
    console.log("stored ajax:"+JSON.stringify(stored_ajax_requests));

}
/*----------------------------------------------------------------------------*/
function uploadStoredAjaxRequest(){
    console.log(' upload stored ajax');
    if (navigator.connection.type === Connection.NONE) {
    	console.log("uploadStoredAjaxRequest() = No Connection");
        return;
    }
    var stored_ajax_requests = JSON.parse(localStorage.getItem('stored_ajax_requests'));
    if (stored_ajax_requests == null || stored_ajax_requests.length == 0 ) {
        return;
    }
    needGoogleMapReload=true;
    try {
		$.ajax({
		    type: "POST",
		    url: stored_ajax_requests[0].url,
		    data: stored_ajax_requests[0].params,
		    dataType: 'jsonp',
		    crossDomain:true,
		    timeout:10000,
		    success: function( msg ) {
		        if (msg.result == 'false' || msg.result == false) {
		           alert("Failed to upload data:"+JSON.stringify(stored_ajax_requests[0])+" ERROR:"+msg.error);
		        }
		        stored_ajax_requests.splice(0,1);
		        localStorage.setItem('stored_ajax_requests',JSON.stringify(stored_ajax_requests));
		        if(window[msg.callback_func] != undefined) {
					window[msg.callback_func](msg.correlation_id,msg.recordid);
				}
		        if (stored_ajax_requests.length == 0) {
		        	if(localStorage.googleapi == 'sdk') {
		        		mapzoom = googleMap.getCameraZoom();
		        		mapcenter = googleMap.getCameraTarget();
		        	}else{
						mapzoom=googleMap.getZoom();
						mapcenter=googleMap.getCenter();
		            }
		            refreshGoogleMap();
		            needGoogleMapReload=false;
		            return;
		        }
		        setTimeout(uploadStoredAjaxRequest,2000);
		    },
		    error: function(XMLHttpRequest, textStatus, errorThrown) {
		    	if(textStatus == 'timeout' || XMLHttpRequest.status == 0) {
		    		setTimeout(uploadStoredAjaxRequest,2000);
		    		return;
		    	}		    	
		        console.log("Unable to upload: "+textStatus +" / "+errorThrown+" / "+XMLHttpRequest);
		        alert("Failed to upload data:"+JSON.stringify(stored_ajax_requests[0])+" ERROR:"+textStatus +" / "+errorThrown);
		        stored_ajax_requests.splice(0,1);
		        localStorage.setItem('stored_ajax_requests',JSON.stringify(stored_ajax_requests));
		        if (stored_ajax_requests.length == 0) {
		            if(localStorage.googleapi == 'sdk') {
		        		mapzoom = googleMap.getCameraZoom();
		        		mapcenter = googleMap.getCameraTarget();
		        	}else{
						mapzoom=googleMap.getZoom();
						mapcenter=googleMap.getCenter();
		            }
		            refreshGoogleMap();
		            needGoogleMapReload=false;
		            return;
		        }
		       setTimeout(uploadStoredAjaxRequest,2000);
		    }
		});
    }catch(ex) {
    	alert("Failed to upload data:"+JSON.stringify(stored_ajax_requests[0])+" ERROR:"+ex.message);
            stored_ajax_requests.splice(0,1);
            localStorage.setItem('stored_ajax_requests',JSON.stringify(stored_ajax_requests));
            if (stored_ajax_requests.length == 0) {
                if(localStorage.googleapi == 'sdk') {
					mapzoom = googleMap.getCameraZoom();
					mapcenter = googleMap.getCameraTarget();
				}else{
					mapzoom=googleMap.getZoom();
					mapcenter=googleMap.getCenter();
				}
                refreshGoogleMap();
                needGoogleMapReload=false;
                return;
            }
           setTimeout(uploadStoredAjaxRequest,2000);
    }
}
/*----------------------------------------------------------------------------*/
