var equipmentList = [];
var disconn_markers=[];
/*----------------------------------------------------------------------------*/
var g_idx = 0;
function disconnectedMapMoved() {
	g_idx = 0;
	disconn_markers=[];
	var bounds = googleMap.getVisibleRegion();
	NorthEast = bounds.northeast;
	SouthWest = bounds.southwest;
			
	//getMarkers('FDR','equip_feeders');
	if (googleMap.getCameraZoom() <12) {
		$("#waiting").hide();
	}
	if (googleMap.getCameraZoom() >12) {
		getMarkers('SW','equip_switches');//getSwitches();
	}
	if (googleMap.getCameraZoom() >14) {
		getMarkers('REC','equip_reclosers');//getReclosers();
		getMarkers('CAP','equip_capacitors');//getCapacitors();			
		getMarkers('REG','equip_regulators');//getSwitches();		
	}
	if (googleMap.getCameraZoom() >15) {
		getMarkers('FUSE','equip_fuses');//getFuses();
		getLines('PL3','equip_pl',true);//getPL();
		
	}
	if (googleMap.getCameraZoom() >17) {
		getMarkers('TRN','equip_transformers');//getTransformers();
		getMarkers('STEP','equip_steptransformers');//getTransformers();
		getLines('PL','equip_pl',false);//getPL();
				
		
	}
	if (googleMap.getCameraZoom() >20) {
		getMarkers('POLE','equip_poles');//getPoles();	
		getMarkers('MTR','equip_meters');//getMeters();
		getLines('SL','equip_sl',false);//getSL();				
	}

}
/*----------------------------------------------------------------------------*/
function getMarkers(type, tablename) {

	var sql = "SELECT *, st_x(st_transform(p.GEOMETRY,4326)) as lon, st_y(st_transform(p.GEOMETRY,4326)) as lat from "+tablename+" p WHERE st_intersects(ST_Transform(ST_GEOMFROMTEXT('POLYGON(("+
	NorthEast.lng+" "+NorthEast.lat+", "+
	NorthEast.lng+" "+SouthWest.lat+", "+
	SouthWest.lng+" "+SouthWest.lat+", "+
	SouthWest.lng+" "+NorthEast.lat+", "+
	NorthEast.lng+" "+NorthEast.lat+"))',4326),"+params.Utility_Srid+"), p.GEOMETRY) = 1 AND "+
	"p.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name = '"+tablename+"' AND search_frame ="+
	"ST_Transform(ST_GEOMFROMTEXT('POLYGON(("+
	NorthEast.lng+" "+NorthEast.lat+", "+
	NorthEast.lng+" "+SouthWest.lat+", "+
	SouthWest.lng+" "+SouthWest.lat+", "+
	SouthWest.lng+" "+NorthEast.lat+", "+
	NorthEast.lng+" "+NorthEast.lat+"))',4326),"+params.Utility_Srid+"))";
    g_db.executeSql(sql, [],
	function(results) {
		$("#waiting").hide();
		var myrows = [];
		for (var i = 0 ; i < results.rows.length ; i++){
			myrows[i] = results.rows.item(i);
		}
		var baseArray = new plugin.google.maps.BaseArrayClass(myrows);
		var infoWindow = new plugin.google.maps.HtmlInfoWindow();
		baseArray.map(function(myData, cb) {
			var imageForMarker='images/'+myData.enettype+'_B.png';	
			myData.index = g_idx;
			myData.markerType = 'disconn';
			g_idx++;
			var data = {
				icon: cordova.file.dataDirectory + 'www/' + imageForMarker,
				position: {
					lat: myData.lat,
					lng: myData.lon
				},
				draggable: true,
				disableAutoPan: true,
				zIndex: 100,
				visible: true,
				myData: myData
			};
			//vehicles[i].popup = plugin.google.maps.InfoWindow({content: content, maxWidth: 400});
			googleMap.addMarker(data, cb);

		}, function(markers) {

			$.merge(disconn_markers,markers);
			markers.forEach(function(marker) {
				marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);
			});
		});
		function onMarkerClick() {
			$("#closeBtn").trigger("click");
			var marker = this;
			var myData = marker.get("myData");
			var content = '<table> <tr><td></td><td id="closeBtn" align="right"><img src="images/close.png"/></td></tr>';
			if (myData.pole_id != undefined && myData.pole_id != null) {
				content += '<tr><td>Pole:</td><td>'+myData.pole_id+'</td></tr>';
			}
			content += '<tr><td>Type/Desc:</td><td>'+myData.enettype+'('+myData.description+')</td></tr>';
			if (myData.map_loc != undefined&& myData.map_loc != null) {
				content += '<tr><td>Map Location:</td><td>'+myData.map_loc+'</td></tr>';
			}
			if (myData.name != undefined&& myData.name != null) {
				content += '<tr><td>Name:</td><td>'+myData.name+'</td></tr>';
			}
			if (myData.phase != undefined&& myData.phase != null) {
				content += '<tr><td>Phase:</td><td>'+myData.phase+'</td></tr>';
			}
			if (myData.placement != undefined&& myData.placement != null) {
				content += '<tr><td>Placement:</td><td>'+myData.placement+'</td></tr>';
			}
			if (appname == 'AssessDamage') {
				content +='</table><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-forbidden" onclick="javascript: reportDamage('+myData.index+',\''+myData.enettype+'\'); $(\'#closeBtn\').trigger(\'click\');">Report Damage</button>';
			}else if (appname == 'LinemanApp') {
				content +='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-forbidden" onclick="javascript: $(\'#closeBtn\').trigger(\'click\'); createDeviceOutage('+myData.index+',\'disconn_markers\', \''+myData.enettype+'\');">Report Outage</button>';
			}
		 	infoWindow.setContent(content);
			infoWindow.open(marker);
			setTimeout( function() {
				$("#closeBtn").on("click", function() {
					infoWindow.close();
				});
			},500);
		}
	},
	function(err) {
		$("#waiting").hide();
		if (err.message.indexOf('no such table') != -1) {
			console.log("GIS TABLE IS MISSING: "+err.message);
			return;
		}
		alert("Failed to find equipment. "+JSON.stringify(err));
	});	
}
/*----------------------------------------------------------------------------*/
function getLines(type, tablename, threephase) {
	var whereClause = '';
	if (threephase) {
		whereClause = " AND phase = 'ABC'";
	}else if (type == 'PL') {
		whereClause = " AND phase != 'ABC'";
	}
	var sql = "SELECT * from "+tablename+" p WHERE st_intersects(ST_Transform(ST_GEOMFROMTEXT('POLYGON(("+
	NorthEast.lng+" "+NorthEast.lat+", "+
	NorthEast.lng+" "+SouthWest.lat+", "+
	SouthWest.lng+" "+SouthWest.lat+", "+
	SouthWest.lng+" "+NorthEast.lat+", "+
	NorthEast.lng+" "+NorthEast.lat+"))',4326),"+params.Utility_Srid+"), GEOMETRY) = 1 "+whereClause+" AND "+
	"p.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name = '"+tablename+"' AND search_frame ="+
	"ST_Transform(ST_GEOMFROMTEXT('POLYGON(("+
	NorthEast.lng+" "+NorthEast.lat+", "+
	NorthEast.lng+" "+SouthWest.lat+", "+
	SouthWest.lng+" "+SouthWest.lat+", "+
	SouthWest.lng+" "+NorthEast.lat+", "+
	NorthEast.lng+" "+NorthEast.lat+"))',4326),"+params.Utility_Srid+"));";
	g_db.executeSql(sql, [],
	function(results) {		
		var myrows = [];
		for (var i = 0 ; i < results.rows.length ; i++){
			myrows[i] = results.rows.item(i);
		}
		var baseArray = new plugin.google.maps.BaseArrayClass(myrows);
		var infoWindow = new plugin.google.maps.HtmlInfoWindow();
		baseArray.map(function(myData, cb){ 
			//disconn_markers[type]={data:results.rows.item(i)};
			var strokeWeight = 1;
			if (myData.enettype == 'PL'){
				strokeWeight = 2;
			}
			if (myData.enettype == 'PL' && myData.phase == 'ABC'){
				strokeWeight = 3;
			}
			myData.index = g_idx;
			myData.markerType = 'disconn';
			g_idx++;
			var line = [];
			var json = JSON.parse(myData.json_string);
			for (var j = 0 ; j < json.coordinates.length ; j++){
				line.push({lat:json.coordinates[j][1], lng:json.coordinates[j][0]});
			}
			//var gdistance = plugin.google.maps.geometry.spherical.computeDistanceBetween ( start, stop )*3.28084+" ft.";	
			googleMap.addPolyline({
				points: line,
				color: "#00FF00",
				strokeOpacity: 0.6,
				width: strokeWeight,
				geodesic: true,
				clickable: true,
				zIndex: 97
			}, cb);
		}, function(markers) {

				$.merge(disconn_markers,markers);
				markers.forEach(function(marker) {
					marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);
				});
		});
	
		function onMarkerClick() {
			$("#closeBtn").trigger("click");
			var marker = this;
			var myData = marker.get("myData");
			var content = '<table> <tr><td></td><td id="closeBtn" align="right"><img src="images/close.png"/></td></tr>';
			if (myData.pole_id != undefined && myData.pole_id != null) {
				content += '<tr><td>Pole:</td><td>'+myData.pole_id+'</td></tr>';
			}
			content += '<tr><td>Type/Desc:</td><td>'+myData.enettype+'('+myData.description+')</td></tr>';
			if (myData.map_loc != undefined&& myData.map_loc != null) {
				content += '<tr><td>Map Location:</td><td>'+myData.map_loc+'</td></tr>';
			}
			if (myData.name != undefined&& myData.name != null) {
				content += '<tr><td>Name:</td><td>'+myData.name+'</td></tr>';
			}
			if (myData.phase != undefined&& myData.phase != null) {
				content += '<tr><td>Phase:</td><td>'+myData.phase+'</td></tr>';
			}
			if (myData.placement != undefined&& myData.placement != null) {
				content += '<tr><td>Placement:</td><td>'+myData.placement+'</td></tr>';
			}
			if (appname == 'AssessDamage') {
				content +='</table><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-forbidden" onclick="javascript: reportDamage('+myData.index+',\''+myData.enettype+'\'); $(\'#closeBtn\').trigger(\'click\');">Report Damage</button>';
			}else if (appname == 'LinemanApp') {
				content +='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-forbidden" onclick="javascript: $(\'#closeBtn\').trigger(\'click\'); createDeviceOutage('+myData.index+',\'disconn_markers\', \''+myData.enettype+'\');">Report Outage</button>';
			}
		 	infoWindow.setContent(content);
			infoWindow.open(marker);
			setTimeout( function() {
				$("#closeBtn").on("click", function() {
					infoWindow.close();
				});
			},500);
		}
	},
	function(err) {
		$("#waiting").hide();
		alert("Failed to find equipment. "+JSON.stringify(err));
	});

	
}
/*----------------------------------------------------------------------------*/
function removeDisconnectedMarkers() {
	var marker = this;
	//console.log(marker);
    //console.log(marker.get("points"));
	marker.remove();
}
/*----------------------------------------------------------------------------*/

function loadDaDb() {
  //Move this to index.js when all apps are updated for offline maps
	var sync = jConfirm("This file download can take several minutes and requires a stable internet connection.  Are you good with that?","Database Sync", 
	function(sync) {
		if (sync){
			$("#waiting").show();
			$("#connectionmessage").html('Database is syncing. Please Wait...').show();
			if (g_db != null) {
		 		g_db.close(function(){console.log("closed db ");}, function(err) {alert("Error closing Database: "+JSON.stringify(err));});
		    }
		    var sync = ContentSync.sync({src:localStorage['ims_url']+"/uploaded_files/equip.db.zip", id:"db", type:"replace", copyRootApp:false, copyCordovaAssets:false, header:false});
			sync.on('progress', function(data) {
				   $("#connectionmessage").html('Database Sync Progress:'+data.status+"  / "+data.progress+" % complete.").show();
			});
			sync.on('complete', function(data) {
				   //console.log(data.localPath);
					$("#connectionmessage").hide();
					$("#waiting").hide();
					alert("Database has synced successfully.");
					window.sqlitePlugin.openDatabase({name:cordova.file.dataDirectory+'db/equip.db', location: 'default'}, 
						function(db) { 
							//alert("Opened db"); 
							console.log("successfully opened db");
							g_db=db; 
							g_db.abortAllPendingTransactions();
							//setTimeout(buildEquipmentList,2000);
						},
						function(err) { console.log("failed to open database file");alert("Failed to open db: "+JSON.stringify(err));
					});
					specialLoadSteps();	
			});


			sync.on('error', function(e) {
				   alert('Source Sync Error: ', e.message);
				   $("#connectionmessage").hide();
				   $("#waiting").hide();
				   // e.message
			});

			sync.on('cancel', function() {
				   // triggered if event is cancelled
				   alert("Soure Sync was Cancelled");
				   $("#connectionmessage").hide();
				   $("#waiting").hide();
			});
			/*var fileTransfer = new FileTransfer();
			console.log("About to start transfer");
			fileTransfer.download(localStorage['ims_url']+"/da_db/equip.db", cordova.file.dataDirectory + 'db/equip.db', 
				function(entry) {
				    console.log("Success!");
				    $("#waiting").hide();
				    alert("Database has synced successfully.");
				    specialLoadSteps();				   
				}, 
				function(err) {
				    console.log("Error");
				    alert("Error: "+JSON.stringify(err));
				}
			);*/
		}
	});
}
/*----------------------------------------------------------------------------*/
function loadMapTiles() {
	var sync = jConfirm("This file download can take several hours and requires a stable internet connection.  Are you good with that?","Download Map Tiles", 
	function(sync) {
		if (sync){
			$("#waiting").show();
			console.log("About to start download");
			$("#waiting").show();
			var fileTransfer = new FileTransfer();
			console.log("About to start transfer");
			fileTransfer.download(localStorage['ims_url']+"/da_db/map_tiles.zip", cordova.file.dataDirectory + 'map_tiles/map_tiles.zip', 
				function(entry) {
				    console.log("Success downloading zip file");
				    $("#connectionmessage").html("Map Tiles file has successfully uploaded. Starting unzip process now...");
				    $("#connectionmessage").show();
				    $("#connectionmessage").css('top','0px');
				    zip.unzip(cordova.file.dataDirectory+'/map_tiles/map_tiles.zip', cordova.file.dataDirectory + 'map_tiles', 
				function(success) {
				    if (success == 0){
						console.log("Success!");
						$("#waiting").hide();
						$("#connectionmessage").html('Map Tiles have been successfully updated on device.<br><button onlcick="$(\'"#connection_message\').hide();">Ok</button>');
				    	setTimeout(function() { $("#connectionmessage").hide();},6000);
				    }else{
				    	$("#waiting").hide();
				    	alert("Error Downloading Map Tiles.");
				    }
				}, 
				function(progressEvent) {
					//$( "#progressbar" ).progressbar("value", Math.round((progressEvent.loaded / progressEvent.total) * 100));
					//console.log(Math.round((progressEvent.loaded / progressEvent.total) * 100)+"%");
					//jAlert("Error: "+JSON.stringify(err));
					$("#connectionmessage").html("Map Tiles file has successfully uploaded. Starting unzip process now..."+Math.round((progressEvent.loaded / progressEvent.total) * 100)+'% complete.');
				}
			);
				}, 
				function(err) {
				    console.log("Error");
				}
			);
			
		}else{
			
		}
	});
}

