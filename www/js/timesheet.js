var currTimeEntry = 0;
var timesheet_items = [];
function openTimeSheet () {
	if ($("#task_entry_date").val() == '2015-01-01') {
		$("#task_entry_date").val(new Date().getFullYear()+'-'+padDigits((new Date().getMonth()+1),2)+"-"+padDigits(new Date().getDate(),2)); 
	}
	console.log("FUNCTION = timesheet_page on page show");
    reloadTasks();
    $("#time_sheet_dialog select").selectmenu();
    if (crews == {}){
    	loadMarkers(['my_dispatched_items']);
    } 
    
	$("#task_entry_date").on('change',function() {
		console.log("date change");
		reloadTasks();
		getTimeSheetItems();
	});
	buildTimeSheetDialogHtml();
	//$("#task_entry_date").trigger('datebox',{'method':'dorefresh'});
	getTimeSheetItems();
		
       
}
function getTimeSheetItems(){
	$.getJSON(localStorage.getItem('ims_url') +"/webSrvRequests/mobile/ajaxGetCommonAppData.php?callback=?", 
		{
			target:JSON.stringify(['timesheet_items']),
			action: 'tables',
			serviceIndex: service_index,
			crew: user_number,
			view: curr_view,
			appname: appname,
			date: $("#task_entry_date").val()
						
		},
		function(data) {
            if (data.result == 'false') {jAlert(data.error);}
            timesheet_items = [];
            //labels=data.labels;
            
			$("#time_entry_work_item").html('');
			var html = '<option value=""></option>';
			data.rows=data.tables[0].rows;
			for (var i in data.rows) {
				timesheet_items[data.rows[i].work_item_number]=data.rows[i];
				var description="Work Item is Closed";
				if (crews[data.rows[i].work_item_number] != undefined) {
					description=crews[data.rows[i].work_item_number].data.line1;
				}
				html+='<option value="'+data.rows[i].work_item_number+'">'+data.rows[i].work_item_number +'('+data.rows[i].work_type+')'+description+'</option>';
			}
			$("#time_entry_work_item").html(html);
			reloadTasks();
		},
		function(error){
			alert("Failed to get timesheet work items: " + error.message);
		}
	);
}
function reloadTasks() {
	
    $.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url') + "/dvWfm2/Vehicle/ajaxTimeSlices.php?callback=?",
		data: {action: 'getTimeSlices', employee_number: user_number, date: $("#task_entry_date").val()},
		dataType: "json",
		success: function(msg) {
			if (msg.result == "false") {
				json_error(msg.error);
			}
			$("#timesheet_list").html('');
			time_entries = msg.data;
			var html ='<ul data-role="listview" data-inset="true" data-split-icon="delete" >';
			for (var i in time_entries) {
				//var filtertext=project+' ';
				html += '<li><a href="javascript:updateTimeEntry(\'' + i + '\');">';
				html +='';
				if (crews[time_entries[i].work_item] != undefined) {
					description=crews[time_entries[i].work_item].data.line1;
				}
				html+= '<h2>#:'+time_entries[i].work_item+'('+time_entries[i].work_type+') - '+' '+time_entries[i].time_classification+'</h2>';
				html+= '<p><b>Start:</b> '+time_entries[i].start_time+'  <b>End:</b> '+time_entries[i].end_time;				
				html+= '</p></a>';
				html+= '<a href="javascript:removeTimeEntry(\''+i+'\');"></a> </li>';
				//$("#"+project).attr('data-filtertext',filtertext);
			}
			$('#timesheet_list').html(html+'</ul>');
    		$('#timesheet_list').trigger('create');
    },
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			console.log(XMLHttpRequest + textStatus + errorThrown);
			jAlert("Connectivity to outage server is currently unavailable.  Please try again later.");
		}
	});
}
function openTimeSheetDialog(type) {
	$.mobile.changePage("#time_sheet_dialog");
	if (type == 'add') {
		$("#update_task_button").hide();
		$("#add_task_button").show();
		$("#task_start_time").val(padDigits(new Date().getHours(),2)+":"+padDigits(new Date().getMinutes(),2));
		$("#task_stop_time").val(padDigits(new Date().getHours(),2)+":"+padDigits(new Date().getMinutes(),2));
		$("#task").val('');
		$("#time_entry_work_item").val('');
		$("#time_sheet_dialog select").selectmenu('refresh');    
	}else{
		$("#update_task_button").show();
		$("#add_task_button").hide();
	}
}
function addTime() {
	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url') + "/dvWfm2/Vehicle/ajaxTimeSlices.php?callback=?",
		data: {action: 'addTimeSlice', employee_number: user_number, entityNumber: curr_crew, entityType: curr_view, start_date: $("#task_entry_date").val(), start_time:$("#task_start_time").val(), end_date: $("#task_entry_date").val(), end_time:$("#task_stop_time").val(), work_item_number:$("#time_entry_work_item").val(), work_type:timesheet_items[$("#time_entry_work_item").val()].work_type, time_classification:$("#task").val()},
		dataType: "json",
		success: function(msg) {
			if (msg.result == "false") {
				jAlert(msg.error);
			}
			reloadTasks();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			console.log(XMLHttpRequest + textStatus + errorThrown);
			jAlert("Connectivity to outage server is currently unavailable.  Please try again later.");
		}
	});
}
function updateTimeEntry(elem) {
	currTimeEntry=elem;
	var start_time_array = time_entries[elem].start_time.split(" ");
	var start_time=start_time_array[0];
	var temp = start_time_array[0].split(":");
	if (start_time_array[1] == "PM"  && temp[0] != 12) {
		start_time=(parseInt(temp[0])+12)+":"+temp[1];
	}else if (start_time_array[1] == "AM"  && temp[0] == 12) {	
		start_time=(parseInt(temp[0])-12)+":"+temp[1];
	}	
	var stop_time_array = time_entries[elem].end_time.split(" ");
	var stop_time=stop_time_array[0];
	temp = stop_time_array[0].split(":");
	if (stop_time_array[1] == "PM"  && temp[0] != 12) {		
		stop_time=(parseInt(temp[0])+12)+":"+temp[1];
	}else if (stop_time_array[1] == "AM"  && temp[0] == 12) {	
		stop_time=(parseInt(temp[0])-12)+":"+temp[1];
	}	
	$("#task_start_time").val(start_time);
    $("#task_stop_time").val(stop_time);
    $("#task").val(time_entries[elem].time_classification);
    $("#time_entry_work_item").val(time_entries[elem].work_item);
    $("#time_sheet_dialog select").selectmenu('refresh');    
    openTimeSheetDialog('update');
}
function updateTime() {
	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url') + "/dvWfm2/Vehicle/ajaxTimeSlices.php?callback=?",
		data: {action: 'updateTimeSlice', employee_number: user_number, entityNumber: curr_crew, entityType: curr_view, start_date: $("#task_entry_date").val(), start_time:$("#task_start_time").val(), end_date: $("#task_entry_date").val(), end_time:$("#task_stop_time").val(), work_item_number:$("#time_entry_work_item").val(), work_type:timesheet_items[$("#time_entry_work_item").val()].work_type, time_classification:$("#task").val(), recordid:time_entries[currTimeEntry].recordid},
		dataType: "json",
		success: function(msg) {
			if (msg.result == "false") {
				jAlert(msg.error);
			} 
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			console.log(XMLHttpRequest + textStatus + errorThrown);
			jAlert("Connectivity to outage server is currently unavailable.  Please try again later.");
		}
	});
}
function removeTimeEntry(elem) {
	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url') + "/dvWfm2/Vehicle/ajaxTimeSlices.php?callback=?",
		data: {action: 'deleteTimeSlice', recordid: time_entries[elem].recordid},
		dataType: "json",
		success: function(msg) {
			if (msg.result == "false") {
				jAlert(msg.error);
			} 
			reloadTasks();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			console.log(XMLHttpRequest + textStatus + errorThrown);
			jAlert("Connectivity to outage server is currently unavailable.  Please try again later.");
		}
	});

}
function buildTimeSheetDialogHtml() {

	var html='<div data-role="header" role="banner"><h2>Time Sheet Entry</h2></div>'+
	'<div data-role="content">'+
		'<div id="work_item_select_container"><label for="time_entry_work_item">Work Item:</label><select id="time_entry_work_item"></select></div>'+
		//'<div id="work_item_fixed_container"><label for="time_entry_work_item_fixed">Work Item</label><span id="time_entry_work_item_fixed"</span></div>'+
		'<label for="task">Status:</label><select id="task"><option value=""></option>';
	
	for (var i in statusCodes) {
		html+='<option value="'+statusCodes[i].class_name+'">'+statusCodes[i].class_name+'</option>';
	}	
	html+='</select>'+
		'<label for="task_start_time">Start:</label><input type="time" id="task_start_time">'+
		'<label for="task_stop_time">Stop:</label><input type="time" id="task_stop_time">'+
		'<a id="add_task_button" data-icon="plus" data-role="button" data-iconpos="left" href="#timesheet_page" onclick="javascript:addTime();">Submit Time Entry</a>'+
		'<a id="update_task_button" data-icon="edit" data-role="button" data-iconpos="left" href="#timesheet_page" onclick="javascript:updateTime();">Update Task</a>'+
		'<a id="cancel_task_button" data-icon="delete" data-role="button" data-iconpos="left" href="#timesheet_page">Cancel</a>'+
	'</div></div>';
	$("#time_sheet_dialog").html(html).trigger('create');
	
}
