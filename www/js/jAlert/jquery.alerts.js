// jQuery Alert Dialogs Plugin
//
// Version 1.0
//
// Cory S.N. LaViska
// A Beautiful Site (http://abeautifulsite.net/)
// 29 December 2008
//
// Visit http://abeautifulsite.net/notebook/87 for more information
//
// Usage:
//		jAlert( message, [title, callback] )
//		jConfirm( message, [title, callback] )
//		jPrompt( message, [value, title, callback] )
// 
// History:
//
//		1.00 - Released (29 December 2008)
//
// License:
// 
// This plugin is dual-licensed under the GNU General Public License and the MIT License and
// is copyright 2008 A Beautiful Site, LLC. 
//
(function($) {
	
	$.alerts = {
		
		// These properties can be read/written by accessing $.alerts.propertyName from your scripts at any time
		positionTop: false,					// If true, position the window at the top of the screen
		verticalOffset: -75,                // vertical offset of the dialog from center screen, in pixels
		horizontalOffset: 0,                // horizontal offset of the dialog from center screen, in pixels/
		repositionOnResize: true,           // re-centers the dialog on window resize
		overlayOpacity: .01,                // transparency level of overlay
		overlayColor: '#FFF',               // base color of overlay
		draggable: true,                    // make the dialogs draggable (requires UI Draggables plugin)
		okButton: '&nbsp;OK&nbsp;',         // text for the OK button
		cancelButton: '&nbsp;Cancel&nbsp;', // text for the Cancel button
		dialogClass: null,                  // if specified, this class will be applied to all dialogs
		width: 300,							// The default width of the window
		maxchars: 0,						// max chars that can be entered into jPrompt
		
		// Public methods
		
		alert: function(message, title, callback, timeout) {
			if( title == null ) title = 'Alert';
			$.alerts._show(title, message, null, 'popup_alert', null, function(result) {
				if( callback ) callback(result);
			});
			if(timeout && timeout > 100) {
				setTimeout(function() {
					$("#popup_container").fadeOut(750, function() {
						$("#popup_ok").click();
					});
				}, timeout)
			}
		},
		
		confirm: function(message, title, callback) {
			if( title == null ) title = 'Confirm';
			$.alerts._show(title, message, null, 'confirm', null, function(result) {
				if( callback ) callback(result);
			});
		},
			
		prompt: function(message, value, title, callback) {
			if( title == null ) title = 'Prompt';
			$.alerts._show(title, message, value, 'prompt', null, function(result) {
				if( callback ) callback(result);
			});
		},
		
		select: function(message, values, value, title, callback) {
			if( title == null ) title = 'Select';
			$.alerts._show(title, message, value, 'select', values, function(result) {
				if( callback ) callback(result);
			});
		},
		
		textarea: function(message, value, maxchars, title, callback) {
			if( title == null ) title = 'Text';
			$.alerts._show(title, message, value, 'select', maxchars, function(result) {
				if( callback ) callback(result);
			});
		},
		
		completion: function(message, title, callback, timeout) {
			if( title == null ) title = 'Completion';
			$.alerts._show(title, message, null, 'popup_completion', null, function(result) {
				if( callback ) callback(result);
			});
			if(timeout && timeout > 100) {
				setTimeout(function() {
					$("#popup_container").fadeOut(750, function() {
						$("#popup_ok").click();
					});
				}, timeout)
			}
		},
		
		// Private methods
		_show: function(title, msg, value, type, values, callback) {
			$.alerts._hide();
			//$.alerts._overlay('show');
			
			$("BODY").append(
			  '<div id="popup_container">' +
			    '<h1 id="popup_title"></h1>' +
			    '<div id="popup_content">' +
			      '<div id="popup_message"></div>' +
				'</div>' +
			  '</div>');
			
			if( $.alerts.dialogClass ) $("#popup_container").addClass($.alerts.dialogClass);
			
			// IE6 Fix
			var pos = ($.browser.msie && parseInt($.browser.version) <= 8 ) ? 'absolute' : 'fixed'; 
			
			$("#popup_container").css({
				position: pos,
				zIndex: 9999,
				padding: 0,
				width: $.alerts.width,
				margin: 0
			});
			
			if(type == 'select') {
				msg = '<div style="float:left;padding-bottom:6px;clear:both;">'+msg+'</div>';
			}
			$("#popup_title").text(title);
			$("#popup_content").addClass(type);
			$("#popup_message").text(msg);
			$("#popup_message").html( $("#popup_message").text().replace(/\n/g, '<br />') );
			
			$("#popup_container").css({
				minWidth: 200,
				maxWidth: $("#popup_container").outerWidth()
			});
			
			$.alerts._reposition();
			$.alerts._maintainPosition(true);
			
			switch( type ) {
				case 'popup_alert':
					$("#popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /></div>');
					$("#popup_ok").click( function() {
						$.alerts._hide();
						callback(true);
					});
					$("#popup_ok").focus().keypress( function(e) {
						if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
					});
				break;
				case 'popup_completion':
					$("#popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /></div>');
					$("#popup_ok").click( function() {
						$.alerts._hide();
						if( callback ) callback(true);
					});
					$("#popup_ok").focus().keypress( function(e) {
						if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
					});
				break;
				case 'confirm':
					$("#popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
					$("#popup_ok").click( function() {
						$.alerts._hide();
						if( callback ) callback(true);
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback(false);
					});
					$("#popup_ok").focus();
					$("#popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
				break;
				case 'prompt':
					$("#popup_message").append('<br /><input type="text" size="30" id="popup_prompt" value="" />');
					if($.alerts.maxchars > 0) {
						$("#popup_message").append('<br />Chars Left <input id="jPromptCharsLeft" type="text" size="3" value="0" />');
					}
					$("#popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
					if($.alerts.maxchars > 0) {
						$('#popup_prompt').keydown(function() {
							var len = $("#popup_prompt").val().length;
							$("#jPromptCharsLeft").val($.alerts.maxchars - len);
						});
						$('#popup_prompt').keyup(function() {
							var text = $("#popup_prompt").val();
							var len = text.length;
							if(len > $.alerts.maxchars) {
								text = text.substr(0,$.alerts.maxchars);
								len = $.alerts.maxchars;
								$("#popup_prompt").val(text);
							}
							$("#jPromptCharsLeft").val($.alerts.maxchars - len);
						});
					}
					$("#popup_prompt").width( $("#popup_message").width() );
					$("#popup_ok").click( function() {
						var val = $("#popup_prompt").val();
						$.alerts._hide();
						if( callback ) callback( val );
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( null );
					});
					$("#popup_prompt, #popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
					if( value ) $("#popup_prompt").val(value);
					$("#popup_prompt").focus().select();
				break;
				case 'select':
					$("#popup_message").append('<br /><select style="padding:1px;" id="popup_select" /></select>').after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
					$("#popup_prompt").width( $("#popup_message").width() );
					$("#popup_ok").click( function() {
						var val = { value: $("#popup_select").val(), display: $("#popup_select option:selected").text()};
						$.alerts._hide();
						if( callback ) callback( val );
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( null );
					});
					$("#popup_select, #popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
					if(values) {	// values is an array of objects with 'display', 'value' and optionally 'selected'
						var html = "";
						for(var i = 0;i < values.length;i++) {
							var selected = '';
							if(typeof(values[i]['selected']) != 'undefined') {
								selected = 'selected="selected"';
							}
							html += '<option style="padding:2px 2px;" value="'+values[i]['value']+'"'+selected+'>'+values[i]['display']+'</option>';
						}
						$("#popup_select").html(html).val(value);
					}
					$("#popup_select").focus().select();
				break;
				case 'textarea':
					var maxchars = values;
					$("#popup_message").append('<br /><textarea id="textarea" onkeyup="textCount(\"textarea\", \"\", maxchars);" rows="5" cols="48" id="textarea" readonly=""></textarea>').after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
					$("#popup_prompt").width( $("#popup_message").width() );
					$("#popup_ok").click( function() {
						var val = '';
						$.alerts._hide();
						if( callback ) callback( val );
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( null );
					});
					$("#popup_select, #popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
					if( values ) {	// values is an array of objects with display and value
						var html = "";
						for(var i = 0;i < values.length;i++) {
							html += '<option value="'+values[i]['value']+'">'+values[i]['display']+'</option>';
						}
						$("#popup_select").html(html).val(value);
					}
					$("#popup_select").focus().select();
				break;
			}
			
			//$("#popup_container").draggable({ handle: $("#popup_title") });
			$("#popup_title").css('cursor','move');
		},
		
		_hide: function() {
			$("#popup_container").remove();
			$.alerts._overlay('hide');
			$.alerts._maintainPosition(false);
		},

		_fade: function() {
			$("#popup_container").fadeOut(500, function() {
				$.alerts._hide();
			});
		},
		
		_overlay: function(status) {
			switch( status ) {
				case 'show':
					$.alerts._overlay('hide');
					$("BODY").append('<div id="popup_overlay"></div>');
					$("#popup_overlay").css({
						position: 'absolute',
						zIndex: 9998,
						top: '0px',
						left: '0px',
						width: '100%',
						background: $.alerts.overlayColor,
						opacity: $.alerts.overlayOpacity,
						height: $(document).height()
					});
				break;
				case 'hide':
					$("#popup_overlay").remove();
				break;
			}
		},
		
		_reposition: function() {
			var top = 0;
			if($.alerts.positionTop != true) {
				top = (($(window).height() / 2) - ($("#popup_container").outerHeight() / 2)) + $.alerts.verticalOffset;
			}
			var left = (($(window).width() / 2) - ($("#popup_container").outerWidth() / 2)) + $.alerts.horizontalOffset;
			if( top < 0 ) top = 0;
			if( left < 0 ) left = 0;
			
			// IE6 fix
			if( $.browser.msie && parseInt($.browser.version) <= 6 ) top = top + $(window).scrollTop();
			
			$("#popup_container").css({
				top: top + 'px',
				left: left + 'px'
			});
			$("#popup_overlay").height( $(document).height() );
		},
		
		_maintainPosition: function(status) {
			if( $.alerts.repositionOnResize ) {
				switch(status) {
					case true:
						$(window).bind('resize', function() {
							$.alerts._reposition();
						});
					break;
					case false:
						$(window).unbind('resize');
					break;
				}
			}
		}
		
	}
	
	// Shortuct functions
	jAlert = function(message, title, callback, timeout) {
		$.alerts.alert(message, title, callback, timeout);
	}
	
	jCompletion = function(message, title, callback, timeout) {
		$.alerts.completion(message, title, callback, timeout);
	}
	
	jConfirm = function(message, title, callback) {
		$.alerts.confirm(message, title, callback);
	};
		
	jPrompt = function(message, value, title, callback) {
		$.alerts.prompt(message, value, title, callback);
	};
	
	jSelect = function(message, values, value, title, callback) {
		$.alerts.select(message, values, value, title, callback);
	};
	
	jTextArea = function(message, value, maxchars, title, callback) {
		$.alerts.textarea(message, value, maxchars, title, callback);
	};
	
})(jQuery);
