var clsCurrentWorkItem;
var g_pageTitle = '';
var g_meter = "";
var g_closeWorkItemType = "";
var g_callAfterComplete = null;
var g_closeServiceIndex = null;
var g_hasScoutingFeature;
var g_services;
var g_userid;
var g_districtHtml;
var g_timezone;
var clsGeneralNotifications;
var clsDispatchItems;
var g_denyCallbacksOnCloseDevice;
var g_hasHasMeterManagement;
var g_config_data;
var g_required_optcodes;
var g_hasDistricts;
var g_currentTab;
var clsWaiting;
var Connection;
var crews;
var clsScouting;

function getPhaseStatus(phasesUsed, phaseStatus ){
	var arr = new Array(2,2,2);
	var phases = Array('A','B','C' );
	for(var i=0; i < 3; i++ ){
		if(phasesUsed.search(phases[i]) >= 0 ){
			if(phaseStatus.search(phases[i]) >= 0 ){
				arr[i] = 0;
			}
			else {
				arr[i] = 1;
			}
		}
		else {
			arr[i] = 2;
		}
	}
	return arr;
}
// The user wants to close a work item
function onWorkItemsClose(cmd, g, grid) {
	var numSelectedRows = grid.numSelectedRows();
	if(numSelectedRows > 1) {
		jAlert("You may only close one work item at a time.");
		return;
	}
	if(numSelectedRows < 1) {
		jAlert("You must select a work item to close.");
		return;
	}

	var work_type = grid.getSelectedValueByDbName("service").substr(0,1);
	var id = getCleanWorkItemId(grid.getSelectedValueByDbName("id"));
	var img = $(grid.getSelectedValueByDbName("id").replace(id,'')).find('img');
	if($(img).hasClass('closed_work_item')) {
		jAlert("You may not close an already closed work item.");
		return;
	}
	var meter = grid.getSelectedValueByDbName("meter");

	var service_index = parseInt($($("#gridDivTable").getSelectedValueByDbName("service_index")).attr('index'),10);
	clsCurrentWorkItem.service_index = service_index;

	showCloseDialog(work_type, id, meter);
}

// Call this from anywhere to open a close dialog.
function showCloseDialog(work_type, id, meter) {
	clsCurrentWorkItem.work_item_type = work_type;
	clsCurrentWorkItem.work_item_number = g_work_item_number = id;
	clsCurrentWorkItem.context = 'X';
	clsCurrentWorkItem.incident_type = clsCurrentWorkItem.work_item_number.substr(0,1);

	g_meter = meter;
	if(arguments.length > 3) {
		g_callAfterComplete = arguments[3];
	}
	else {
		g_callAfterComplete = null;
	}
	var sUrl = "";

	if(clsCurrentWorkItem.work_item_type === 'I') {
		switch(clsCurrentWorkItem.incident_type) {
			case 'C':
				sUrl = '/dvwfm2/forms/restoreIncident.php';
				g_pageTitle = "Close Incident: ";
				break;
			case 'D':
				sUrl = '/dvwfm2/forms/restoreDevice.php';
				g_pageTitle = "Close Device Incident: ";
				break;
			case 'G':
				sUrl = '/dvwfm2/forms/restoreGroup.php';
				g_pageTitle = "Close Grouped Incident: ";
				break;
			default:
				jAlert("Invalid incident type");
				return;
				break;
		}
	}
	else if(clsCurrentWorkItem.work_item_type === 'S') {
		sUrl = '/dvwfm2/forms/restoreSo.php';
		g_pageTitle = "Close Service Order: ";
	}
	else if(clsCurrentWorkItem.work_item_type === 'W') {
		sUrl = '/dvwfm2/forms/restoreSo.php';
		g_pageTitle = "Close Work Order: ";
	}
	clsCloseWorkItem.populateCloseDialog(sUrl);
}

function populateCodes(elm, service_index) {
	var closeServiceIndex = 0;
	if(g_closeServiceIndex !== null) {
		closeServiceIndex = g_closeServiceIndex;
		g_closeServiceIndex = null;
	}
	else if(typeof(service_index) === 'string') {
		closeServiceIndex = parseInt(service_index,10);
	}
	else if(typeof(service_index) === 'number') {
		closeServiceIndex = service_index;
	}
	else {
		var grid = $("#gridDivTable");
		var svc = grid.getSelectedValueByDbName("service_index");
		closeServiceIndex = parseInt($(svc).attr('index'),10);
	}

	for(var i = 0;i < g_services.length;i++) {
		var service = g_services[i];
		if(closeServiceIndex === parseInt(service['service_index'],10)) {
			var numDisplayed = 0;
			var opts = service['options'];
			var html = '';
			for(var n = 1;n <= 6;n++) {
				var opttitle = 'opt'+n+'title';
				if(typeof(opts[n-1]) !== 'undefined' && opts[n-1].length > 0 && service[opttitle] && service[opttitle].length > 0) {
					var myopts = opts[n-1];
					$('#Opt'+n+'Name', elm).html(service[opttitle]+":");
					html = '<option value=""></option>';
					for(var x = 0;x < myopts.length;x++) {
						var code = myopts[x]['CodeID'] || '';
						var desc = myopts[x]['Description'] || '';
						if(code.length === 0 || desc.length === 0) {
							continue;
						}
						html += '<option value="'+code+'">'+code+' ['+desc+']</option>\n';
					}
					if(clsCurrentWorkItem.is_mobile === false) {
						$('#Opt'+n+'Select', elm).html(html).show();
					}
					else{
						$('#Opt'+n+'Select', elm).html(html).selectmenu().parent().show();
					}
				}
				else {
					$('#Opt'+n+'Name', elm).html('');
					if(clsCurrentWorkItem.is_mobile === false) {
						$('#Opt'+n+'Select', elm).hide();
					}
					else{
						$('#Opt'+n+'Select', elm).selectmenu().parent().hide();
					}
				}
			}
			break;
		}
	}
}
function insertCloseDialogForm(data, my_callback) {
	var close_item = $("#close_item");
	close_item.empty().html(data);

	$("#work_item_number", close_item).val(clsCurrentWorkItem.work_item_number);

	if(clsCurrentWorkItem.work_item_type === 'I') {
		clsCloseWorkItem.populateCodesByName('close_item', clsCurrentWorkItem.service_index);
	}
	else if(clsCurrentWorkItem.work_item_type === 'S') {
		$("#close_so_return_reading").change(function() {
			if($("#close_so_return_read_date").val() === "") {
				$("#close_so_return_read_date").val(getCurrentDate());
			}
		});
	}

	if(g_denyCallbacksOnCloseDevice === true ) {
		$("#inc_close_denycallbacks", close_item).hide();
		$("#inc_close_denycallbacksPrompt", close_item).hide();
	}

	if(g_hasHasMeterManagement === false ) {
		$("#MtrJobScopeLabel").html("&nbsp;");
		$("#MtrJobScope").hide();
	}

	if(clsCurrentWorkItem.is_mobile === false) {
		close_item.dialog("option","title",g_pageTitle+clsCurrentWorkItem.work_item_number).dialog("open");
		if(clsCurrentWorkItem.incident_type === 'C') {
			close_item.dialog("option","height","700");
		}
		else if(clsCurrentWorkItem.incident_type === 'D') {
			close_item.dialog("option","height","690");
		}
		else if(clsCurrentWorkItem.incident_type === 'G') {
			close_item.dialog("option","height","640");
		}
	}

	if(clsCurrentWorkItem.work_item_type === 'I') {
		if(is_iPad()) {
			$("#remLen1", close_item).parent().css('width','280px');
			$("#RestoreComment", close_item).parent().css('width','544px');
		}
	}

	if(typeof(my_callback) === 'function') {
		setTimeout(my_callback(true),0);
	}
}
function insertCloseData(callback) {
	var my_callback = null;
	if(typeof(callback) === 'function') {
		my_callback = callback;
	}

	var grid = $("#gridDivTable");
	var close_item = $("#close_item");

	g_dispatchItemID = clsCurrentWorkItem.work_item_number = g_work_item_number;
	g_dispatchItemType = clsCurrentWorkItem.work_item_type;

	$("#work_item_number",close_item).val(g_work_item_number);

	if(clsCurrentWorkItem.is_mobile === false) {
		setupDispatchAndNotes();
	}else {
		var crewData = crews[g_currindex].data;
		if (localStorage.googleapi == 'sdk') {
			crewData = crews[g_currindex].get('myData');
		}
	}
	clsCloseWorkItem.requires = {};

	switch(clsCurrentWorkItem.work_item_type) {
	 	case 'S':
	 		if(clsCurrentWorkItem.is_mobile === true && navigator.connection.type === Connection.NONE) {
				if(typeof(crewData.meter_actions) === 'undefined') {
					crewData.meter_actions = [];
				}
	 			insertSoCloseData({data:crewData}, my_callback);
	 			return;
			}
			var title = "Service Order";
			var data ="id="+clsCurrentWorkItem.work_item_number+"&type="+clsCurrentWorkItem.work_item_type;
			var domain = '';
			if(clsCurrentWorkItem.domain && clsCurrentWorkItem.domain.length > 0) {
				domain = clsCurrentWorkItem.domain;
			}
			$.ajax({
				type: "POST",
				url: domain+"/dvwfm2/backoffice/ajaxGetWorkItemDetails.php",
				data: data,
				dataType: "json",
				timeout: 5000,
				success: function(msg) {
					if(msg.result === "false") {
						jAlert(msg.error, "Error");
						return;
					}
					insertSoCloseData(msg, my_callback);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {

					if(clsCurrentWorkItem.is_mobile === true) {
						if (textStatus == 'timeout' || XMLHttpRequest.status == 0) {
							setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'backoffice.insertCloseData(SO)'});
							$("#connectionmessage").html('Slow connection detected using most recent data for close').show();
							setTimeout(function() {$("#connectionmessage").hide();}, 3000);
							if(typeof(crewData.meter_actions) === 'undefined') {
								crewData.meter_actions = [];
							}
				 			insertSoCloseData({data:crewData}, my_callback);
						}else{
							$("#connectionmessage").hide();
							jAlert("Failed to get work item details. "+JSON.stringify(XMLHttpRequest));
							if(typeof(crewData.meter_actions) === 'undefined') {
								crewData.meter_actions = [];
							}
				 			insertSoCloseData({data:crewData}, my_callback);
						}
			 		}
					else {
						alert("AJAX failed to get latest item details: "+errorThrown);
						if(my_callback !== null) {
							setTimeout(my_callback(false),0);
						}
					}
				}
			});
			break;
		case 'W':
			if(clsCurrentWorkItem.is_mobile === true && navigator.connection.type === Connection.NONE) {
	 			insertWoCloseData({data:crewData}, my_callback);
	 			return;
			}
			var title = "Work Order";
			$("input.table_cell",close_item).val("");
			$("textarea",close_item).val("");

			var data ="id="+clsCurrentWorkItem.work_item_number+"&type="+clsCurrentWorkItem.work_item_type;
			var domain = '';
			if(clsCurrentWorkItem.domain && clsCurrentWorkItem.domain.length > 0) {
				domain = clsCurrentWorkItem.domain;
			}
			$.ajax({
				type: "POST",
				url: domain+"/dvwfm2/backoffice/ajaxGetWorkItemDetails.php",
				data: data,
				dataType: "json",
				timeout: 5000,
				success: function(msg) {
					if(msg.result === "false") {
						jAlert(msg.error, "Error");
						return;
					}
					insertWoCloseData(msg, my_callback);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert("AJAX failed to get item details: "+errorThrown);
					if(clsCurrentWorkItem.is_mobile === true) {
						if (textStatus == 'timeout' || XMLHttpRequest.status == 0) {
							setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'backoffice.insertCloseData(WO)'});
							$("#connectionmessage").html('Slow connection detected using most recent data for close').show();
							setTimeout(function() {$("#connectionmessage").hide();}, 3000);
							insertWoCloseData({data:crewData}, my_callback);
						}else{
							$("#connectionmessage").hide();
							jAlert("Failed to get latest work item details. "+JSON.stringify(XMLHttpRequest));
							if(typeof(crewData.meter_actions) === 'undefined') {
								crewData.meter_actions = [];
							}
				 			insertWoCloseData({data:crewData}, my_callback);
						}
			 		}
					else{
						if(my_callback !== null) {
							setTimeout(my_callback(false),0);
						}
					}
				}
			});
			break;
		case 'I':
			if(clsCurrentWorkItem.is_mobile === true && navigator.connection.type === Connection.NONE) {
	 			insertIncidentCloseData({Incidents:[crewData]}, my_callback);
	 			return;
			}
			if(clsCurrentWorkItem.incident_type === 'G' && clsCurrentWorkItem.is_mobile === false) {
				var service = grid.getSelectedValueByDbName("service_index");
				if(service.indexOf('lightbulb.gif') === -1) {
					$("#close_item #MtrJobScope").parent().parent().find('>td').hide();
				}
				else {
					$("#close_item #MtrJobScope").parent().parent().find('>td').show();
				}
			}
			var data ="id="+g_work_item_number+"&type="+clsCurrentWorkItem.work_item_type;
			var domain = '';
			if(clsCurrentWorkItem.domain && clsCurrentWorkItem.domain.length > 0) {
				domain = clsCurrentWorkItem.domain;
			}
			$.ajax({
				type: "POST",
				url: domain+"/dvwfm2/backoffice/ajaxGetWorkItemDetails.php",
				data: data,
				dataType: "json",
				timeout:10000,
				success: function(msg) {
					if(msg === null || typeof(msg) === 'undefined' || msg.Result === "false") {
						jAlert(msg.error, "Error");
						return;
					}
					insertIncidentCloseData(msg, my_callback);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if(clsCurrentWorkItem.is_mobile === true ) {
						if ( textStatus == 'timeout' || XMLHttpRequest.status == 0 ) {
							setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'backoffice.insertCloseData(I)'});
							$("#connectionmessage").html('Slow connection detected using most recent data for close').show();
							setTimeout(function() {$("#connectionmessage").hide();}, 3000);
				 			insertIncidentCloseData({Incidents:[crewData]}, my_callback);
			 			}else{
			 				$("#connectionmessage").hide();
							jAlert("Failed to latest get work item details. "+JSON.stringify(XMLHttpRequest));
							if(typeof(crewData.meter_actions) === 'undefined') {
								crewData.meter_actions = [];
							}
				 			insertIncidentCloseData({Incidents:[crewData]}, my_callback);
			 			}
			 			return;
					}
					alert("AJAX failed to get item details: "+errorThrown);
				}
			});
			break;
		default:
			// !!! return an error here
	}

	$("#skipNotifications").on('change',function() {
		if($(this).is(':checked')) {
			if($("#skipNotificationsCh").length > 0 && $("#skipNotificationsCh").val().length === 0) {
				$("#skipNotificationsCh").val('C');
			}
			else {
				$("#skipNotificationsCh").val('');
			}
		}
	});

	g_dispatchItemID = g_work_item_number;
	close_item.parent().find(".ui-dialog-buttonpane button:contains('Close Item')").button("enable");
}
function insertSoCloseData(msg, my_callback) {
	var close_item = $("#close_item");
	$(".for_wo",close_item).hide();
	$(".for_so",close_item).show();

	clsCurrentWorkItem.pending_state = msg.data.pending_state;
	if(clsCurrentWorkItem.pending_state && clsCurrentWorkItem.pending_state === 'C') {
		var buttons = $("#close_item").dialog('option','buttons');
		var new_button = {
			text: 'Remove Pending Close',
			click: function() {clsCloseWorkItem.removePendingClose();}
		};
		buttons.splice(buttons.length - 2, 0, new_button);
		$("#close_item").dialog('option','buttons', buttons);
		$("#close_item #close_pending_flag").html('<img height="16px" title="Pending final close" src="/dvwfm2/images/greencheck.gif" id="status_'+clsCurrentWorkItem.work_item_number+'" />');
	}

	close_item.find("#work_item_number").val(clsCurrentWorkItem.work_item_number);
	close_item.find("#work_item_type").val(clsCurrentWorkItem.work_item_type);

	$("#service_order_info", close_item).show();
	$("#close_so_by",close_item).val(g_userid);

	close_item.find("#close_so_cust_name").val(msg.data.customer_name);
	close_item.find("#close_so_service_loc").val(msg.data.service_loc_address);
	close_item.find("#close_so_meter_no").val(msg.data.meter_number);
	close_item.find("#close_so_order_code").val(msg.data.so_code);

	if(msg.data.so_show_history_comments === false) {
		$("#history_comments").parents('tr:first').hide();
	}
	else {
		$("#history_comments").parents('tr:first').show();
		close_item.find("#history_comments").html(msg.data.history_comments);
	}

	if(msg.data.payment_type.length > 0) {
		$("#payment_type option[value="+msg.data.payment_type+"]").prop('selected',true);
		close_item.find("#payment_amount").val(msg.data.payment_amount);
		close_item.find("#check_number").val(msg.data.check_number);
		close_item.find("#promise_to_pay_date").val(msg.data.promise_to_pay_date);
		$("#certified_check").prop('checked', msg.data.certified_check);
		setTimeout('$("#payment_type").change();',20);
	}

	close_item.find("#close_so_date").val(getCurrentDateAndTime());
	close_item.find("#close_so_description").val(msg.data.so_description);
//	close_item.find("#order_comment").val(msg.data.comment);
//	close_item.find("#order_comment").val(msg.data.order_comment);
	// MK 05/18/2016 Made to match Houston
	close_item.find("#order_comment").val(msg.data.comment);

	close_item.find("#close_so_by").focus();
	$("#close_so_comments", close_item).val(msg.data['closed_comments']);

	if(msg.data.SkipNotifications) {
		$("#skipNotifications").prop('checked',msg.data.SkipNotifications.length > 0);
	}

	$("#so_notes", close_item).html(msg.data.notes);
	if(clsCurrentWorkItem.is_mobile ===false || navigator.connection.type !== Connection.NONE) {
		setTimeout(function() { populateSoEvents('#close_item #so_event', msg.data['so_event']); }, 0);
	}
	if(typeof(msg.data.lon) !== 'undefined' && msg.data.lon !== null && msg.data.lon.length > 0) {
		$("#lon",close_item).html(msg.data.lon);
		$("#lat",close_item).html(msg.data.lat);
	}

	if(msg.data.pending_close_by.length > 0) {
		$("#pending_close_block",close_item).show();
		$("#pending_close_by",close_item).text(msg.data.pending_close_by);
		$("#pending_close_date",close_item).text(msg.data.pending_close_date);
	}

	msg.data.requires.require_meter_read_demand = false;
	msg.data.requires.require_meter_read_kvar = false;
	msg.data.requires.require_meter_read_kwh = false;

	msg.data.requires.require_meter_install_demand = false;
	msg.data.requires.require_meter_install_kvar = false;
	msg.data.requires.require_meter_install_kwh = false;

	msg.data.requires.require_meter_remove_demand = false;
	msg.data.requires.require_meter_remove_kvar = false;
	msg.data.requires.require_meter_remove_kwh = false;

	var require_image = '<img src="/dvwfm2/images/red-star.gif" alt="R" title="This box must be checked to close this work item." />';
	var require_image3 = '<img src="/dvwfm2/images/red-star.gif" alt="R" title="This box must be filled in to close this work item." />';

	var need_read = false;
	var need_install = false;
	var need_remove = false;

	if(msg.data.so_type !== 'General') {	// FOr a General, we don't require any meter readings
		$.extend(clsCloseWorkItem.requires,msg.data.requires);	// clone the requires attributes
		for(var i = 0;i < msg.data.meter_actions.length;i++) {
			var task = msg.data.meter_actions[i];
			if(task.task_action === 'R' || task.task_action === 'D' || task.task_action === 'O') {	// Read or disconnect
				need_read = true;
			}
			if(task.task_action === 'A' || task.task_action === 'C') {	// Add or change
				need_install = true;
			}
			if(task.task_action === 'X' || task.task_action === 'C') {	// Remove or change
				need_remove = true;
			}
		}
	}

	var requires = msg.data.requires;
	if((requires.require_meter_read === true || need_read === true) && msg.data.meter_number.length > 0) {
		clsCloseWorkItem.requires.require_meter_read = true;
		$("#mtr_read",close_item).parent().append(require_image);
		$("#mtr_read", close_item).prop('checked',true);
		$("#close_item #meter_read_block").show();
		for(var i = 0;i < msg.data.meter_actions.length;i++) {
			var task = msg.data.meter_actions[i];
			if(task.task_action === 'R' || task.task_action === 'D' || task.task_action === 'O') {
				if(task.task_read_type === 'Demand') {
					clsCloseWorkItem.requires.require_meter_read_demand = true;
					$("#read_mtr_reading").attr('required','true').parent().append(require_image3);
				}
				else if(task.task_read_type === 'Kvar') {
					clsCloseWorkItem.requires.require_meter_read_kvar = true;
					$("#read_mtr_kvar_read").attr('required','true').parent().append(require_image3);
				}
				else {
					clsCloseWorkItem.requires.require_meter_read_kwh = true;
					$("#read_mtr_kw_read").attr('required','true').parent().append(require_image3);
				}
			}
		}
//		$("#read_mtr_read_date",close_item).val(getCurrentDateAndTime());
	}
	if(requires.require_meter_install === true || need_install === true || msg.data.meter_number.length === 0) {
		clsCloseWorkItem.requires.require_meter_install = true;
		$("#mtr_installed",close_item).parent().append(require_image);
		$("#mtr_installed", close_item).prop('checked',true);
		$("#close_item #meter_installed_block").show();
		for(var i = 0;i < msg.data.meter_actions.length;i++) {
			var task = msg.data.meter_actions[i];
			if(task.task_action === 'A' || task.task_action === 'C' || task.old_mtr_number.length === 0) {
				if(task.task_read_type === 'Demand' && msg.data.meter_number.length > 0) {
					clsCloseWorkItem.requires.require_meter_install_demand = true;
					if($("#install_mtr_reading").parent().find('img').length === 0) {
						$("#install_mtr_reading").attr('required','true').parent().append(require_image3);
					}
				}
				else if(task.task_read_type === 'Kvar' && msg.data.meter_number.length > 0) {
					clsCloseWorkItem.requires.require_meter_install_kvar = true;
					if($("#install_mtr_kvar_read").parent().find('img').length === 0) {
						$("#install_mtr_kvar_read").attr('required','true').parent().append(require_image3);
					}
				}
				else {
					clsCloseWorkItem.requires.require_meter_install_kwh = true;
					if($("#install_mtr_kw_read").parent().find('img').length === 0) {
						$("#install_mtr_kw_read").attr('required','true').parent().append(require_image3);
					}
				}
			}
		}
//		$("#install_mtr_read_date",close_item).val(getCurrentDateAndTime());
	}
	if(requires.require_meter_remove === true || need_remove === true) {
		clsCloseWorkItem.requires.require_meter_remove = true;
		$("#mtr_removed",close_item).parent().append(require_image);
		$("#mtr_removed", close_item).prop('checked',true);
		$("#close_item #meter_removed_block").show();
		for(var i = 0;i < msg.data.meter_actions.length;i++) {
			var task = msg.data.meter_actions[i];
			if(task.task_action === 'X' || task.task_action === 'C') {
				if(task.task_read_type === 'Demand') {
					clsCloseWorkItem.requires.require_meter_remove_demand = true;
					$("#remove_mtr_reading").attr('required','true').parent().append(require_image3);
				}
				else if(task.task_read_type === 'Kvar') {
					clsCloseWorkItem.requires.require_meter_remove_kvar = true;
					$("#remove_mtr_kvar_read").attr('required','true').parent().append(require_image3);
				}
				else {
					clsCloseWorkItem.requires.require_meter_remove_kwh = true;
					$("#remove_mtr_kw_read").attr('required','true').parent().append(require_image3);
				}
			}
		}
//		$("#remove_mtr_read_date",close_item).val(getCurrentDateAndTime());
	}
	if(requires.require_remove_or_disconnect === true) {
		var require_image2 = '<img style="opacity:.5;" src="/dvwfm2/images/red-star.gif" alt="R" title="Meter removed or disconnected must be checked to close this work item." />';
		$("#mtr_removed",close_item).parent().append(require_image2);
		$("#mtr_disconnected",close_item).parent().append(require_image2);
		$("#close_item #meter_removed_block").show();
	}
	if(requires.require_meter_disconnect === true) {
		$("#mtr_disconnected",close_item).parent().append(require_image);
		$("#mtr_disconnected", close_item).prop('checked',true);
	}

	if(requires.require_transformer_connect === true) {
		$("#xfmr_installed",close_item).parent().append(require_image);
		$("#xfmr_installed", close_item).prop('checked',true);
		$("#close_item #xfmr_installed_block").parent().show();
	}
	if(requires.require_transformer_remove === true) {
		$("#xfmr_removed",close_item).parent().append(require_image);
		$("#xfmr_removed", close_item).prop('checked',true);
		$("#close_item #xfmr_removed_block").parent().show();
	}
	if(requires.require_transformer_disconnect === true) {
		$("#xfmr_disconnected",close_item).parent().append(require_image);
		$("#xfmr_disconnected", close_item).prop('checked',true);
	}

	if(clsCurrentWorkItem.is_mobile === false) {
		var service_index_num = parseInt(msg.data.service_index_num,10);
		clsCurrentWorkItem.service_index = service_index_num;
		if(service_index_num === 1) {
			$(".electric_only", close_item).show();
			$('#close_item').dialog('option','height',$(window).height()-20);
		}
		else if(service_index_num > 1) {
			$(".electric_only", close_item).hide();
			var h = 630;
			if(msg.data.pending_close_by.length > 0) {
				h = 720;
			}
			$('#close_item').dialog('option','height',h);
		}
	}

	// See if this is in pending close state
	if(msg.data.pending_close_date.length > 0) {
		// Fill in any pre-existing meter action data
		for(var i = 0;i < msg.data.meter_actions_results.length;i++) {
			var row = msg.data.meter_actions_results[i];
			if(row.mtr_action_type === 'E') {		// Existing meter
				var meter_read_block = $("#close_item #meter_read_block");
				$("#read_mtr_number",meter_read_block).val(row.mtr_number);
				$("#read_mtr_reading",meter_read_block).val(row.mtr_reading);
				$("#read_mtr_read_date",meter_read_block).val(row.mtr_read_date);
				$("#read_mtr_serial_number",meter_read_block).val(row.mtr_serial_number);
				$("#read_mtr_multiplier",meter_read_block).val(row.mtr_multiplier);
				$("#read_mtr_kw_read",meter_read_block).val(row.mtr_kw_read);
				$("#read_mtr_kvar_read",meter_read_block).val(row.mtr_kvar_read);
			}
			else if(row.mtr_action_type === 'N') {	// New meter
				var meter_installed_block = $("#close_item #meter_installed_block");
				$("#install_mtr_number",meter_installed_block).val(row.mtr_number);
				$("#install_mtr_reading",meter_installed_block).val(row.mtr_reading);
				$("#install_mtr_read_date",meter_installed_block).val(row.mtr_read_date);
				$("#install_mtr_serial_number",meter_installed_block).val(row.mtr_serial_number);
				$("#install_mtr_multiplier",meter_installed_block).val(row.mtr_multiplier);
				$("#install_mtr_kw_read",meter_installed_block).val(row.mtr_kw_read);
				$("#install_mtr_kvar_read",meter_installed_block).val(row.mtr_kvar_read);
			}
			else if(row.mtr_action_type === 'O') {	// Old meter
				var meter_removed_block = $("#close_item #meter_removed_block");
				$("#remove_mtr_number",meter_removed_block).val(row.mtr_number);
				$("#remove_mtr_reading",meter_removed_block).val(row.mtr_reading);
				$("#remove_mtr_read_date",meter_removed_block).val(row.mtr_read_date);
				$("#remove_mtr_serial_number",meter_removed_block).val(row.mtr_serial_number);
				$("#remove_mtr_multiplier",meter_removed_block).val(row.mtr_multiplier);
				$("#remove_mtr_kw_read",meter_removed_block).val(row.mtr_kw_read);
				$("#remove_mtr_kvar_read",meter_removed_block).val(row.mtr_kvar_read);
			}
		}
	}
	else {
		// Not a pending close
		var meter_number = msg.data.meter_number;
		var old_mtr_multiplier = '';

		if(msg.data.meter_actions.length > 0) {
			for(var i = 0;i < msg.data.meter_actions.length;i++) {
				var action = msg.data.meter_actions[i];
				if(action.old_meter_flag === 't' && action.old_mtr_number.length > 0) {
					meter_number = action.old_mtr_number;
				}
				if(action.task_action === 'R') {
					old_mtr_multiplier = action.old_mtr_multiplier;
				}
			}
		}
		if(meter_number.length > 0) {
			$("#close_item #read_mtr_number").val(meter_number);
			$("#remove_mtr_number",close_item).val(meter_number);
		}
		$("#remove_mtr_multiplier",close_item).val(old_mtr_multiplier);
		$("#read_mtr_read_date",close_item).val(getCurrentMilitaryDate());
		$("#remove_mtr_read_date",close_item).val(getCurrentMilitaryDate());
		$("#install_mtr_read_date",close_item).val(getCurrentMilitaryDate());
	}

	// Payment options
	$("td.for_amount_taken,td.for_amount_taken>input").css('opacity',.3).find('td').prop({disabled:true,readonly:true});
	$("tr.check_payment td").css('opacity',.3).find('td').prop({disabled:true,readonly:true});
	$("tr.promise_to_pay_group td").css('opacity',.3).find('td').prop({disabled:true,readonly:true});

	$("#payment_type").unbind('change').on('change',function() {
		var payment_type = $(this).val();
		if(payment_type.length > 0) {
			$("td.for_amount_taken,td.for_amount_taken>input").css('opacity',1).find('input').prop({disabled:false,readonly:false});
		}
		else {
			$("td.for_amount_taken,td.for_amount_taken>input").css('opacity',.3).find('td').prop({disabled:true,readonly:true});
			$("tr.check_payment td").css('opacity',.3).find('td').prop({disabled:true,readonly:true});
			$("tr.promise_to_pay_group td").css('opacity',.3).find('td').prop({disabled:true,readonly:true});
		}
		if(payment_type === 'Promise') {
			$("tr.check_payment td").css('opacity',.3).find('input').prop({disabled:true,readonly:true});
			$("tr.promise_to_pay_group td").css('opacity',1).find('input').prop({disabled:false,readonly:false});
		}
		else if(payment_type === 'Cash') {
			$("tr.check_payment td").css('opacity',.3).find('input').prop({disabled:true,readonly:true});
			$("tr.promise_to_pay_group td").css('opacity',.3).find('input').prop({disabled:true,readonly:true});
		}
		else if(payment_type === 'Check') {
			$("tr.check_payment td").css('opacity',1).find('input').prop({disabled:false,readonly:false});
			$("tr.promise_to_pay_group td").css('opacity',.3).find('input').prop({disabled:true,readonly:true});
		}
	});

	fixTitlesBySvcIdx(clsCurrentWorkItem.service_index, "#close_item");

	if(my_callback !== null) {
		setTimeout(my_callback(true),0);
	}
}

function insertWoCloseData(msg, my_callback) {
	var close_item = $("#close_item");
	$(".for_wo",close_item).show();
	$(".for_so",close_item).hide();
	$(".electric_only", close_item).hide();

	clsCurrentWorkItem.pending_state = msg.data.pending_state;
	if(clsCurrentWorkItem.pending_state && clsCurrentWorkItem.pending_state === 'C') {
		var buttons = $("#close_item").dialog('option','buttons');
		var new_button = {
			text: 'Remove Pending Close',
			click: function() {clsCloseWorkItem.removePendingClose();}
		};
		buttons.splice(buttons.length - 2, 0, new_button);
		$("#close_item").dialog('option','buttons', buttons);
		$("#close_item #close_pending_flag").html('<img height="16px" title="Pending final close" src="/dvwfm2/images/greencheck.gif" id="status_'+clsCurrentWorkItem.work_item_number+'" />');
	}

	$("#work_item_number",close_item).val(clsCurrentWorkItem.work_item_number);
	$("#work_item_type",close_item).val(clsCurrentWorkItem.work_item_type);
	$("#close_so_by",close_item).val(g_userid);
	$("#close_so_date",close_item).val(getCurrentDateAndTime());
	$("#create_wo_date",close_item).val(msg.data.entered_date);
	$("#wo_description",close_item).val(msg.data.wo_description);
	$("#order_comment",close_item).val(msg.data.order_comment);

	$("#close_so_by",close_item).focus();
	$("input[type=checkbox]", close_item).removeAttr("checked");
	$("input[type=radio]", close_item).removeAttr("checked");
	showWorkOrderTypes("#close_item #wo_order_type", msg.data['wo_order_type'], "#close_item #wo_order_subtype", msg.data['wo_order_subtype']);

	if(msg.data.pending_close_by !== null &&msg.data.pending_close_by.length > 0) {
		$("#pending_close_block",close_item).show();
		$("#pending_close_by",close_item).text(msg.data.pending_close_by);
		$("#pending_close_date",close_item).text(msg.data.pending_close_date);
	}

	if(clsCurrentWorkItem.is_mobile === false) {
		var service_index_num = parseInt(msg.data.service_index_num,10);
		clsCurrentWorkItem.service_index = service_index_num;
		$(".electric_only", close_item).hide();
		var h = 650;
		if(msg.data.pending_close_by.length > 0) {
			h += 90;
		}
		$(close_item).dialog('option','height',h);
	}
	fixTitlesBySvcIdx(clsCurrentWorkItem.service_index, "#close_item");

	$("#wo_equipment_type", close_item).val(msg.data['equipment_type']);
	$("#wo_equipment_id", close_item).val(msg.data['equipment_id']);
	$("#close_so_comments", close_item).val(msg.data['closed_comments']); // KA uncommented out this line to fix 619482 step 24
	$("#so_notes",close_item).html(msg.data.notes);
	if(clsCurrentWorkItem.is_mobile === false || navigator.connection.type !== Connection.NONE) {
		setTimeout(function() { populateSoEvents('#close_item #so_event', msg.data['so_event']); }, 20);
	}
	$("#service_order_info", close_item).show();

	if(my_callback !== null) {
		setTimeout(my_callback(true),0);
	}
}
function insertIncidentCloseData(msg, my_callback) {
	var ServiceIndexName = msg.Incidents[0]['ServiceIndexName'];
	var close_item = $("#close_item");

	clsCurrentWorkItem.phases_used = '';

	// get dispatch info
	if(clsCurrentWorkItem.is_mobile === false) {
		clsCurrentWorkItem.service_index = msg.Incidents[0]['ServiceIndex'];
		clsCurrentWorkItem.is_electric = msg.Incidents[0]['is_electric'];
	}

	if(parseInt(msg.Incidents[0]['Status'],10) === 80) {
		clsCurrentWorkItem.pending_state = 'C';
		var buttons = $("#close_item").dialog('option','buttons');
		var new_button = {
			text: 'Remove Pending Close',
			click: function() {clsCloseWorkItem.removePendingClose();}
		};
		buttons.splice(buttons.length - 2, 0, new_button);
		$("#close_item").dialog('option','buttons', buttons);
		$("#close_item #CloseStatus").parent().append('<div id="pending_incident_close_flag" style="display:inline-block;"><img height="16px" title="Pending final close" src="/dvwfm2/images/greencheck.gif" id="status_'+clsCurrentWorkItem.work_item_number+'" /></div>');
	}
	else {
		$("#close_item #CloseStatus #pending_incident_close_flag").remove();
	}


	if(clsCurrentWorkItem.incident_type === 'D' || clsCurrentWorkItem.incident_type === 'C') {
		var needs_scouting = (msg.Incidents[0]['scouting_status'] === 'N');
		$("#needs_scouting",close_item).prop('checked',needs_scouting);
		$("#inc_notes").html(msg.notes).attr("readonly","readonly");
	}

	g_dispatchItemID = g_work_item_number;
	if(clsCurrentWorkItem.incident_type === 'D' || clsCurrentWorkItem.incident_type === 'G') {
		$('#inc_close_district',close_item).html(g_districtHtml);
		if(typeof(msg.Incidents[0]['DistrictCode']) !== 'undefined') {
			$("#inc_close_district option[value='"+msg.Incidents[0]['DistrictCode']+"']",close_item).prop('selected',true);
		}
		else if(typeof(msg.Incidents[0]['District']) !== 'undefined') {
			$("#inc_close_district option[value='"+msg.Incidents[0]['District']+"']",close_item).prop('selected',true);
		}
	}

	if(msg.Incidents[0].SkipNotifications) {
		$("#skipNotifications",close_item).prop('checked',msg.Incidents[0].SkipNotifications.length > 0);
		$("#skipNotificationsCh",close_item).val(msg.Incidents[0].SkipNotifications.length > 0);
	}

	if(msg.Incidents[0].RestoreComment && msg.Incidents[0].RestoreComment.length > 0) {
		$("#RestoreComment",close_item).val(msg.Incidents[0].RestoreComment);
	}
	if(clsCurrentWorkItem.is_mobile === false || navigator.connection.type !== Connection.NONE) {
		setTimeout("populateSoEvents('#close_item #event_id', '"+msg.Incidents[0]['event_id']+"');", 0);
	}

	if(clsCurrentWorkItem.incident_type === 'C' || clsCurrentWorkItem.incident_type === 'D') {
		var inc_close_subfdr = $("#inc_close_subfdr",close_item);
		if($(inc_close_subfdr).length !== 0) {
			if($(inc_close_subfdr).prop('tagName').toLowerCase() === 'input') {
				$(inc_close_subfdr).val(msg['subfdr']);
			}
			else {
				$(inc_close_subfdr).html(msg['subfdr']);
			}
		}
	}
	else {
		$("#inc_close_subnum",close_item).text(msg.Incidents[0].SubstationNumber);
		$("#inc_close_feedernum",close_item).text(msg.Incidents[0].FeederNumber);
	}

	var outage_comment = '';
	if(clsCurrentWorkItem.incident_type === 'D') {
		//$("#inc_details_type").html('Device Outage');
		$("#inc_close_devicename",close_item).html(msg.Incidents[0]['DeviceName']);
		$("#inc_close_devicecode",close_item).html(msg.Incidents[0]['DeviceCode']);
		//$("#inc_restore_alias").html(msg.Incidents[0]['Alias']);
		$("#inc_close_estrestore",close_item).html(msg.Incidents[0]['EstRestoreTime']+'&nbsp;'+g_timezone);
		$("#inc_close_maplocation",close_item).html(msg.Incidents[0]['LsMapLoc']);
		$("#inc_close_pole",close_item).html(msg.Incidents[0]['Pole']);
		$("#inc_close_alias",close_item).html(msg.Incidents[0]['CustName']);
		if(typeof(msg.Incidents[0]['Alias']) !== 'undefined') {
			$('#inc_close_alias',close_item).val(msg.Incidents[0]['Alias']);
		}
		else {
			$('#inc_close_alias',close_item).val(msg.Incidents[0]['CustName']);
		}

		if(msg.Incidents[0]['outage_state'] && msg.Incidents[0]['outage_state'] === 'C') {
			$("#outage_state",close_item).prop('checked',true);
		}
		else {
			$("#outage_state",close_item).prop('checked',false);
		}

		if(ServiceIndexName === 'Electric') {
			// set phases
			$('#inc_close_phaseA',close_item).css("visibility",(msg.Incidents[0]['PhasesUsed'].indexOf('A') >= 0 ? "visible" : "hidden"));
			$('#inc_close_phaseB',close_item).css("visibility",(msg.Incidents[0]['PhasesUsed'].indexOf('B') >= 0 ? "visible" : "hidden"));
			$('#inc_close_phaseC',close_item).css("visibility",(msg.Incidents[0]['PhasesUsed'].indexOf('C') >= 0 ? "visible" : "hidden"));
			phasesUsed = msg.Incidents[0]['PhasesUsed'];
			phaseStatus = msg.Incidents[0]['PhaseStatus'];
			$("#OriginalPhaseStatus",close_item).val(phaseStatus);
			arrPhases = getPhaseStatus(phasesUsed, phaseStatus );

			// This is used to always turn every phase back on.
			for(var i = 0;i < 3;i++) {
				if(arrPhases[i] === 1) {
					arrPhases[i] = 0;
				}
			}

			$("input[name='radioPhaseA']:nth("+arrPhases[0]+")",close_item).attr("checked","checked");
			$("input[name='radioPhaseB']:nth("+arrPhases[1]+")",close_item).attr("checked","checked");
			$("input[name='radioPhaseC']:nth("+arrPhases[2]+")",close_item).attr("checked","checked");
			$("inc_close_phasesused", close_item).val(msg.Incidents[0]['PhasesUsed']);
			clsCurrentWorkItem.phases_used = msg.Incidents[0]['PhasesUsed'].trim();
		}
		outage_comment = msg.Incidents[0]['Comments'];
	}
	else if(clsCurrentWorkItem.incident_type === 'G') {
		$("#inc_close_subnum_label",close_item).html('Substation:');
		$("#inc_close_type",close_item).html('Group Outage');
		$("#inc_close_incincluded",close_item).html(msg.Incidents[0]['NumIncidents']);
		$("#inc_close_custaffected",close_item).val(msg.Incidents[0]['NumCustomers']);
		$("#NumIncidents",close_item).val(msg.Incidents[0]['NumIncidents']);
		$("#NumCustomers",close_item).val(msg.Incidents[0]['NumCustomers']);
		if(ServiceIndexName !== 'Electric') {
			$("#inc_close_subnum_label",close_item).html('');
			$("#inc_close_subnum",close_item).html('');
			$("#inc_close_feedernum_label",close_item).html('');
			$("#inc_close_feedernum",close_item).html('');
		}
		$("#RestoreComment",close_item).html(msg.Incidents[0]['RestoreComment']);

		if(ServiceIndexName === 'Electric') {
			// set phases
			phasesUsed = msg.Incidents[0]['PhasesUsed'];
			phaseStatus = msg.Incidents[0]['PhaseStatus'];
			arrPhases = getPhaseStatus(phasesUsed, phaseStatus );
			$("input[name='radioPhaseA']:nth("+arrPhases[0]+")",close_item).attr("checked","checked");
			$("input[name='radioPhaseB']:nth("+arrPhases[1]+")",close_item).attr("checked","checked");
			$("input[name='radioPhaseC']:nth("+arrPhases[2]+")",close_item).attr("checked","checked");
//			$("inc_close_phasesused", close_item).val(msg.Incidents[0]['PhasesUsed']);
		}
		outage_comment = msg.Incidents[0]['Comment'];
	}
	else if(clsCurrentWorkItem.incident_type === 'C') {
		clsCurrentWorkItem.account_number = msg.Incidents[0]['AccountNo'];
		clsCurrentWorkItem.cust_phone = msg.Incidents[0]['CustPhone'];
		clsCurrentWorkItem.alt_phone = msg.Incidents[0]['CustAltPhone'];
		clsCurrentWorkItem.email = msg.Incidents[0]['EmailAddress'];
		clsCurrentWorkItem.callback_sms = '';
		clsCurrentWorkItem.callback_sms_carrier = '';

		$("#inc_close_type",close_item).html('Cust Outage');
		$("#inc_close_name",close_item).html(msg.Incidents[0]['CustName']);
		$("#inc_close_accnt",close_item).html(msg.Incidents[0]['AccountNo']);
		$("#inc_close_dayphone",close_item).html(formatPhone(msg.Incidents[0]['CustPhone']));
		$("#inc_close_nightphone",close_item).html(formatPhone(msg.Incidents[0]['CustAltPhone']));
		$("#inc_close_email",close_item).html(msg.Incidents[0]['EmailAddress']);
		$("#inc_close_meterno",close_item).html(msg.Incidents[0]['CustMeterNo']);
		$("#inc_close_priority",close_item).html(formatYN(msg.Incidents[0]['Priority']));
		$("#inc_close_maplocation",close_item).html(msg.Incidents[0]['CustMapLoc']);
		$("#inc_close_servicelocation",close_item).html(getCleanWorkItemId(msg.Incidents[0]['ServiceLocation']));
		$("#inc_close_callerid",close_item).html(msg.Incidents[0]['CidPhone']);

		if(msg.Incidents[0]['Pole']) {
			$("#inc_close_pole",close_item).html(msg.Incidents[0]['Pole']);
		}
		else if(msg.Incidents[0]['CustPole']) {
			$("#inc_close_pole",close_item).html(msg.Incidents[0]['CustPole']);
		}

		if(msg.Incidents[0]['Transformer']) {
			$("#inc_close_transformer",close_item).html(msg.Incidents[0]['Transformer']);
		}
		else if(msg.Incidents[0]['CustXfmr']) {
			$("#inc_close_transformer",close_item).html(msg.Incidents[0]['CustXfmr']);
		}

		if(ServiceIndexName === 'Electric') {
			phasesUsed = msg.Incidents[0]['PhasesUsed'];
			phaseStatus = msg.Incidents[0]['PhaseStatus'];
			arrPhases = getPhaseStatusImages(phasesUsed, phaseStatus );
			$("#inc_close_phaseA",close_item).attr('src', arrPhases[0]);
			$("#inc_close_phaseB",close_item).attr('src', arrPhases[1]);
			$("#inc_close_phaseC",close_item).attr('src', arrPhases[2]);
		}
		$("#RestoreComment").html(msg.Incidents[0]['RestoreComment']);

		clsCurrentWorkItem.callback_phone = html = formatPhone(msg.Incidents[0]['CallBackPhone']);
		clsCurrentWorkItem.callback_email = msg.Incidents[0]['EmailAddress'];
		$("#inc_callback_phone",close_item).text(html);
		$("#inc_callback_email",close_item).text(msg.Incidents[0]['EmailAddress']);

		if(msg.Incidents[0]['CallBack'] === 'Y') {
			$("#inc_callbackflag option[value=Y]",close_item).prop('selected', true);
			$("#inc_callback_phone",close_item).css('opacity',1.0);
			$("#inc_callback_email",close_item).css('opacity',1.0);
		}
		else {
			$("#inc_callbackflag option[value=N]",close_item).prop('selected', true);
			$("#inc_callback_phone",close_item).css('opacity',.2);
			$("#inc_callback_email",close_item).css('opacity',.2);
		}

		if(msg.Incidents[0]['EmailAddress'] !== undefined && msg.Incidents[0]['EmailAddress'].length > 0 ) {
			$("#update_item_cb_email",close_item).prop('checked',true);
			$("#inc_update_email",close_item).val(msg.Incidents[0]['EmailAddress']);
			clsCurrentWorkItem.callback_email = msg.Incidents[0]['EmailAddress'];
		}
		outage_comment = msg.Incidents[0]['Comments'];

		$("#btnChangePhone",close_item).css({cursor:'pointer'}).unbind('click').bind('click touchstart', function() {
			setTimeout('setCustomerContacts()', 0);
		});
	}

	if($("#inc_close_createdate",close_item).is("input")) {
		$("#inc_close_createdate",close_item).val(msg.Incidents[0]['EnteredDate']);
	}
	else {
		$("#inc_close_createdate",close_item).html(msg.Incidents[0]['EnteredDate']+'&nbsp;'+g_timezone);
	}

	if($("#inc_close_enteredby",close_item).is("input")) {
		$("#inc_close_enteredby",close_item).val(msg.Incidents[0]['EnteredBy']);
	}
	else {
		$("#inc_close_enteredby",close_item).html(msg.Incidents[0]['EnteredBy']);
	}

	$("#inc_close_linesection",close_item).html(msg.Incidents[0]['LineSection']);

	if(msg.Incidents[0]['latitude'] && parseFloat(msg.Incidents[0]['latitude']) > 0.0) {
		$("#latlon_prompt",close_item).html("Lat/Lon:");
		$("#latlon_value",close_item).html(msg.Incidents[0]['latitude']+' / '+msg.Incidents[0]['longitude']);
	}

	$("#Opt1Select option[value='"+msg.Incidents[0]['opt_code_0']+"']",close_item).prop('selected',true);
	$("#Opt2Select option[value='"+msg.Incidents[0]['opt_code_1']+"']",close_item).prop('selected',true);
	$("#Opt3Select option[value='"+msg.Incidents[0]['opt_code_2']+"']",close_item).prop('selected',true);
	$("#Opt4Select option[value='"+msg.Incidents[0]['opt_code_3']+"']",close_item).prop('selected',true);
	$("#Opt5Select option[value='"+msg.Incidents[0]['opt_code_4']+"']",close_item).prop('selected',true);
	$("#Opt6Select option[value='"+msg.Incidents[0]['opt_code_5']+"']",close_item).prop('selected',true);

	$("#inc_close_comments").html(outage_comment).attr('title',outage_comment);
	if(msg.Incidents[0]['RestoreTime'] !== undefined && msg.Incidents[0]['RestoreTime'].length > 8) {
		$("#CloseTime",close_item).val(msg.Incidents[0]['RestoreTime']);
	}
	else {
		$("#CloseTime",close_item).val(msg.current_time);
	}

	$("#inc_notes",close_item).html(msg.notes).attr("readonly","readonly");
	hideFields(msg);

	if(my_callback !== null) {
		setTimeout(my_callback(true),0);
	}
	fixTitlesBySvcIdx(clsCurrentWorkItem.service_index, "#close_item");
}
function setCallbackNumber() {
	if($("#inc_callbackflag").val() === 'Y') {
		setTimeout('openRestoreNotifation();', 0);
	}
}

function openRestoreNotifation() {
//	fnAccountNotification();
	$("#set_restore_notification").load('/dvwfm2/forms/restoreNotification.php', function() {
		if(clsCurrentWorkItem.is_mobile === false) {
			$(this).dialog('open');
		}
		$("#set_restore_notification #tempPhone").val('');
		$("#restoreNotificationContainer #tempEmailAddress").val('');

		var context = null;
		if($("#update_item").hasClass("ui-dialog-content") && $("#update_item").dialog('isOpen') === true) {
			context = $("#update_item");
		}
		else if($("#close_item").hasClass("ui-dialog-content") && $("#close_item").dialog('isOpen') === true) {
			context = $("#close_item");
		}
		else {
			jAlert("No context");
		}
		if(clsCurrentWorkItem.callback_phone.length > 0) {
			$("#set_restore_notification #tempPhone").val(clsCurrentWorkItem.callback_phone);
		}
		else {
			$("#set_restore_notification #tempPhone").val('');
		}

		$("#restoreNotificationContainer #tempEmailAddress").val(clsCurrentWorkItem.callback_email);
		$("#restoreNotificationContainer #tempSmsNumber").val(clsCurrentWorkItem.callback_sms);
		$("#restoreNotificationContainer #carrier option[value='"+clsCurrentWorkItem.callback_sms_carrier+"']").prop('selected',true);
	});
}

function setCustomerContacts() {
	var domain = '';
	if(clsCurrentWorkItem.domain && clsCurrentWorkItem.domain.length > 0) {
		domain = clsCurrentWorkItem.domain;
	}
	$("#update_contact_info").load(domain+'/dvwfm2/forms/updateCustomerInfo.html', function() {
		if(clsCurrentWorkItem.is_mobile === false) {
			$(this).dialog('open');
		}
		$("#update_contact_info #old_pri_phone").val(clsCurrentWorkItem.cust_phone);
		$("#update_contact_info #old_alt_phone").val(clsCurrentWorkItem.cust_alt_phone);
		$("#update_contact_info #old_email").val(clsCurrentWorkItem.cust_email);
		$("#update_contact_info #pri_phone").val(clsCurrentWorkItem.cust_phone);
		$("#update_contact_info #alt_phone").val(clsCurrentWorkItem.cust_alt_phone);
		$("#update_contact_info #email").val(clsCurrentWorkItem.cust_email);
	});
}

function scrollToElementTop(target, elem) {
	var pos = parseInt($("#"+elem).offset().top,10);
	pos -= $("#"+elem).height();
	pos -= 6;
	pos += $("#"+target).scrollTop();
	$("#"+target).animate({scrollTop: pos+'px'}, "fast");
}

function onClickMtrRead() {
	if($("#close_item #mtr_read").is(":checked") === true) {
		$("#close_item #meter_read_block").show();
		scrollToElementTop('close_item','mtr_read');
		if(clsCurrentWorkItem.is_mobile === false) {
			$("#read_mtr_read_date").val(getCurrentMilitaryDate());
		}
	}
	else {
		$("#close_item #meter_read_block").hide();
	}
}

function onClickMtrInstalled() {
	if($("#close_item #mtr_installed").is(":checked") === true) {
		$("#close_item #meter_installed_block").show();
		scrollToElementTop('close_item','mtr_installed');
		if(clsCurrentWorkItem.is_mobile === false) {
			$("#install_mtr_read_date").val(getCurrentMilitaryDate());
		}
	}
	else {
		$("#close_item #meter_installed_block").hide();
	}
}

function onClickMtrRemoved() {
	if($("#close_item #mtr_removed").is(":checked") === true) {
		$("#close_item #meter_removed_block").show();
		scrollToElementTop('close_item','mtr_removed');
		if(clsCurrentWorkItem.is_mobile === false) {
			$("#remove_mtr_read_date").val(getCurrentMilitaryDate());
		}
	}
	else {
		$("#close_item #meter_removed_block").hide();
	}
}

function onClickXfmrInstalled() {
	if($("#close_item #xfmr_installed").is(":checked") === true) {
		$("#close_item #xfmr_installed_block").parent().show();
		scrollToElementTop('close_item','xfmr_installed');
	}
	else {
		$("#close_item #xfmr_installed_block").parent().hide();
	}
}

function onClickXfmrRemoved() {
	if($("#close_item #xfmr_removed").is(":checked") === true) {
		$("#close_item #xfmr_removed_block").parent().show();
		scrollToElementTop('close_item','xfmr_removed');
	}
	else {
		$("#close_item #xfmr_removed_block").parent().hide();
	}
}

var clsCloseWorkItem = {
	notify_flag: false,
	accounts: [],
	display: [],
	requires: {},
	close_time: '',

	showDispatchItems: function(funcname) {
		if(typeof(clsDispatchItems) !== 'undefined') {
			clsDispatchItems.init();
			clsDispatchItems.setUserID(g_userid);
			clsDispatchItems.addWorkItem(clsCurrentWorkItem.work_item_number,clsCurrentWorkItem.work_item_type);
			clsDispatchItems.setCallback(funcname);
			clsDispatchItems.show();
		}
	},

	// Update the dispatched items list on the close incident dialog.

	populateCodesByName: function(elm_name, service_index) {
		// This function gets the serviceID from the active grid and populates
		// the options names and codes in the specified dialog(elm).
		populateCodes($("#"+elm_name), service_index);
	},

	populateCloseDialog: function(sUrl, service_index, callback) {
		var domain = '';
		if(clsCurrentWorkItem.domain && clsCurrentWorkItem.domain.length > 0) {
			domain = clsCurrentWorkItem.domain;
		}
		var my_callback = null;
		if(typeof(callback) === 'function') {
			my_callback = callback;
		}
		if(clsCurrentWorkItem.is_mobile === true) {
			insertCloseDialogForm(localStorage[sUrl], my_callback);
			return;
		}
		// Plug in the AJAX to get the html
		$.ajax({
			url: domain+sUrl,
			dataType: 'text',
			success: function(data) {
				insertCloseDialogForm(data, my_callback);
			}
		});
	},

	removePendingClose: function() {
		jConfirm("Are you sure you want to remove the pending close state from "+clsCurrentWorkItem.work_item_number+"?", "Remove Pending CLose", function(val) {
			if(val === true) {
				if(clsCurrentWorkItem.work_item_type === 'S' || clsCurrentWorkItem.work_item_type === 'W') {
					var tablename = 'service_order';
					var fieldname = 'close_state';
					var fieldtype = 'text';
					var newval = 'true';
					var id = clsCurrentWorkItem.work_item_number;

					if(clsCurrentWorkItem.work_item_type === 'W') {
						tablename = 'work_order';
					}

					$.ajax({
						type: "POST",
						url: "/dvwfm2/BackOffice/ajaxSetFieldData.php",
						data: { tablename:tablename, fieldname:fieldname, fieldtype:fieldtype, newval:newval, id:id },
						dataType: 'json',
						success: function(data) {
							if(typeof(data.error) !== 'undefined') {
								jAlert(data.error, 'Remove Pending Close');
								return;
							}
							$("#close_item #close_pending_flag").html('');
							clsCurrentWorkItem.pending_state = '';
							var buttons = $("#close_item").dialog('option','buttons');
							buttons.splice(buttons.length - 3, 1);
							$("#close_item").dialog('option','buttons', buttons);
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							alert("Can't Remove Pending Close: "+errorThrown);
						}
					});
				}
				else {
					// This is an incident
					$.ajax({
						type: "POST",
						url: "/dvwfm2/BackOffice/ajaxCloseItem.php",
						data: { action:'remove_pending_close', work_item_number:clsCurrentWorkItem.work_item_number },
						dataType: 'json',
						success: function(data) {
							if(typeof(data.error) !== 'undefined') {
								jAlert(data.error, 'Remove Pending Close');
								return;
							}
							$("#close_item #close_pending_flag").html('');
							clsCurrentWorkItem.pending_state = '';
							var buttons = $("#close_item").dialog('option','buttons');
							buttons.splice(buttons.length - 3, 1);
							$("#close_item").dialog('option','buttons', buttons);
							$("#close_item #CloseStatus #pending_incident_close_flag").remove();
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							alert("Can't Remove Pending Close: "+errorThrown);
						}
					});
				}
			}
		});

	},

	add_to_error: function(errors, error) {
		if(errors.length > 0) {
			errors += "\n";
		}
		errors += error;
		return errors;
	},

	preClose: function() {
		var domain = '';
		if(clsCurrentWorkItem.domain && clsCurrentWorkItem.domain.length > 0) {
			domain = clsCurrentWorkItem.domain;
		}
		$.ajax({
			url: domain+"/dvwfm2/backoffice/ajax/ajaxGetCurrentTime.php",
			data: {format:"m-d-Y H:i:s",offset:0},
			dataType: 'json',
			timeout: 5000,
			success: function(msg) {
				if(msg.result === 'true') {
					clsCloseWorkItem.close_time = msg.time;
					clsCloseWorkItem.close_validate();
					return;
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert("AJAX failed to get server time: "+errorThrown);
				return;
			}
		});
	},

	close_validate: function(callbackFxn) {
		var work_item_type = $("#work_item_type", close_item).val();
		var data = {work_item_number:g_work_item_number, work_item_type:work_item_type};
		var close_date = '';
		var close_item = $("#close_item");

		if(work_item_type === 'I') {
			var inc_type = $("#inc_type", close_item).val();
			if(inc_type === 'D') {
				var PhaseStatus = '';
				if($("#close_item input[name=radioPhaseA]:checked").val() === 'on' || 
					$("#close_item select[name=radioPhaseA]").val() === 'on') {
//				if($("span#inc_close_phaseA").css('visibility') !== 'hidden') {
					PhaseStatus += 'A';
				}
				if($("#close_item input[name=radioPhaseB]:checked").val() === 'on' || 
					$("#close_item select[name=radioPhaseB]").val() === 'on') {
//				if($("span#inc_close_phaseB").css('visibility') !== 'hidden') {
					PhaseStatus += 'B';
				}
				if($("#close_item input[name=radioPhaseC]:checked").val() === 'on' || 
					$("#close_item select[name=radioPhaseC]").val() === 'on') {
//				if($("span#inc_close_phaseC").css('visibility') !== 'hidden') {
					PhaseStatus += 'C';
				}
				data.PhaseStatus = PhaseStatus;
			}
			data.Phase = clsCurrentWorkItem.phases_used;

			if(data.PhaseStatus !== data.Phase && inc_type === 'D') {
				var msg = "To restore one or two phases and leave one or two phases out of service, press Yes.";
				msg += "<br>Otherwise, press No to cancel this operation.";
				jYesNo(msg,"Close Outage",function(val) {
					if(val === true) {
						clsCloseWorkItem.close_work_item(callbackFxn);
					}
				});
			}
			else {
				clsCloseWorkItem.close_work_item(callbackFxn);
			}
		}
		else {
			clsCloseWorkItem.close_work_item(callbackFxn);
		}
	},

	///////////////////////////////////////////////////////////////////
	// This does that actual request to AJAX to close the work item. //
	///////////////////////////////////////////////////////////////////
	close_work_item: function(callback) {
		var my_callback = null;
		if(typeof(callback) === 'function') {
			my_callback = callback;
		}

		var nowStr = this.close_time;
		var close_item = $("#close_item");

		var work_item_type = $("#work_item_type", close_item).val();
		var data = {work_item_number:g_work_item_number, work_item_type:work_item_type};
		var close_date = '';
		switch(work_item_type) {
		case 'I':
			var errors = '';
			close_date = $("#CloseTime", close_item).val();
			if(close_date.length === 0) {
				errors = this.add_to_error(errors,"Entered close time must be valid and in the format YYYY/MM/DD");
			}
			if(!isValidDate(close_date)) {
				errors = this.add_to_error(errors,"Entered close time must be valid and in the format YYYY/MM/DD");
			}
			var tm = close_date.substr(11,8);
			if(tm.length === 5) {
				tm += ':00';
			}
			if(tm.length < 8) {
				errors = this.add_to_error(errors,"Entered close time must be valid and in the format HH:MM");
			}
			if(!isValidTime(tm)) {
				errors = this.add_to_error(errors,"Entered close time must be valid and in the format HH:MM");
			}

			var close_date_tm = Date.parse(close_date);
			var now = Date.parse(nowStr);

			if(g_config_data['allow_future_close'] !== 't') {
				var max_future_time =(now + 120000);	// Default is 2 minutes
				if(close_date_tm > max_future_time) {
					errors = this.add_to_error(errors,"Close date/time may not be after 2 minutes from now.");
				}
			}

			// Check the close date against the create date
			var entered = '';
			if($("#inc_close_createdate").is("input")) {
				entered = $("#inc_close_createdate").val();
			}
			else {
				entered = $("#inc_close_createdate").text();
			}
			entered = entered.replace(/-/g,'/');
			if(entered.length === 0) {
				errors = this.add_to_error(errors,"Confirmed out time must be valid and in the format YYYY/MM/DD");
			}
			if(!isValidDate(entered)) {
				errors = this.add_to_error(errors,"Confirmed out time must be valid and in the format YYYY/MM/DD");
			}
			var tm = entered.substr(11,8);
			if(tm.length === 5) {
				tm += ':00';
			}
			if(tm.length < 8) {
				errors = this.add_to_error(errors,"Confirmed out time must be valid and in the format HH:MM");
			}
			if(!isValidTime(tm)) {
				errors = this.add_to_error(errors,"Confirmed out time must be valid and in the format HH:MM");
			}

			if(clsCurrentWorkItem.is_mobile === true) {
				// The following block of code handles the case when the linemanApp
				// device is in a different timezone than the server.
				var serverTimezoneOffset = parseInt(localStorage.getItem('time_zone_offset'),10);
				var d = new Date();
				var localTimezoneOffset = d.getTimezoneOffset();
				if(serverTimezoneOffset !== localTimezoneOffset) {
					var myOffset = (localTimezoneOffset - serverTimezoneOffset);
					close_date_tm += (myOffset * 60 * 1000);
					d.setTime(close_date_tm);
					var year = d.getFullYear();
					var mon = d.getMonth() + 1;
					if(mon < 10) {
						mon = '0' + mon;
					}
					var day = d.getDate();
					if(day < 10) {
						day = '0' + day;
					}
					var hour = d.getHours();
					if(hour < 10) {
						hour = '0' + hour;
					}
					var min = d.getMinutes();
					if(min < 10) {
						min = '0' + min;
					}
					var sec = d.getSeconds();
					if(sec < 10) {
						sec = '0' + sec;
					}

					close_date = year+'/'+mon+'/'+day+' '+hour+':'+min+':'+sec;
				}
			}

			if(close_date_tm < Date.parse(entered)) {
				errors = this.add_to_error(errors,"Close date and time must be after the Confirmed out date and time.");
			}

			// Reformat the close time to match what the third party interface is expecting
			close_date = formatFullDateAndTime(close_date);

			var inc_type = $("#inc_type", close_item).val();
			data.CloseTime = close_date;
			data.EnteredDate = entered;

			if($("#inc_close_enteredby").is("input")) {
				data.EnteredBy =$("#inc_close_enteredby").val();
			}
			else {
				data.EnteredBy = $("#inc_close_enteredby").html();
			}

			if(data.EnteredBy.length === 0) {
				errors = this.add_to_error(errors,"Opened by is blank.");
			}

			var CloseStatus = $("#CloseStatus", close_item).val();
			if(parseInt(CloseStatus,10) !== 98) {
				if($("#Opt1Select", close_item).val() !== null && $("#Opt1Select", close_item).val().length > 0) {
					data.OptCode1 = $("#Opt1Select", close_item).val();
				}
				else {
					if(g_services[clsCurrentWorkItem.service_index-1].required[0] === true) {
						var name = $("#Opt1Name").text().replace(':','');
						errors = this.add_to_error(errors,"'"+name+"' is a required field");
					}
					data.OptCode1 = "";
				}
				if($("#Opt2Select", close_item).val() !== null && $("#Opt2Select", close_item).val().length > 0) {
					data.OptCode2 = $("#Opt2Select", close_item).val();
				}
				else {
					if(g_services[clsCurrentWorkItem.service_index-1].required[1] === true) {
						var name = $("#Opt2Name").text().replace(':','');
						errors = this.add_to_error(errors,"'"+name+"' is a required field");
					}
					data.OptCode2 = "";
				}
				if($("#Opt3Select", close_item).val() !== null && $("#Opt3Select", close_item).val().length > 0) {
					data.OptCode3 = $("#Opt3Select", close_item).val();
				}
				else {
					if(g_services[clsCurrentWorkItem.service_index-1].required[2] === true) {
						var name = $("#Opt3Name").text().replace(':','');
						errors = this.add_to_error(errors,"'"+name+"' is a required field");
					}
					data.OptCode3 = "";
				}
				if($("#Opt4Select", close_item).val() !== null && $("#Opt4Select", close_item).val().length > 0) {
					data.OptCode4 = $("#Opt4Select", close_item).val();
				}
				else {
					if(g_services[clsCurrentWorkItem.service_index-1].required[3] === true) {
						var name = $("#Opt4Name").text().replace(':','');
						errors = this.add_to_error(errors,"'"+name+"' is a required field");
					}
					data.OptCode4 = "";
				}
				if($("#Opt5Select", close_item).val() !== null && $("#Opt5Select", close_item).val().length > 0) {
					data.OptCode5 = $("#Opt5Select", close_item).val();
				}
				else {
					if(g_services[clsCurrentWorkItem.service_index-1].required[4] === true) {
						var name = $("#Opt5Name").text().replace(':','');
						errors = this.add_to_error(errors,"'"+name+"' is a required field");
					}
					data.OptCode5 = "";
				}
				if($("#Opt6Select", close_item).val() !== null && $("#Opt6Select", close_item).val().length > 0) {
					data.OptCode6 = $("#Opt6Select", close_item).val();
				}
				else {
					if(g_services[clsCurrentWorkItem.service_index-1].required[5] === true) {
						var name = $("#Opt6Name").text().replace(':','');
						errors = this.add_to_error(errors,"'"+name+"' is a required field");
					}
					data.OptCode6 = "";
				}
			}

			if(errors.length > 0) {
				jAlert(errors, "Close Work Item");
				return;
			}

			data.ClosedBy = g_userid;
			data.CloseStatus = CloseStatus;
			data.RestoreComment = escape($("#RestoreComment", close_item).val());
			data.EstRestoreTime = $("#inc_close_estrestore", close_item).text().substring(0,19);
			data.event_id = $("#event_id", close_item).val();

			var MtrJobScope = $("#MtrJobScope", close_item);
			if(MtrJobScope.length > 0) {
				data.MtrJobScope = MtrJobScope.val();
			}

			switch(inc_type) {
			case 'D':
		        var PhaseStatus = '';
				if($("#close_item input[name=radioPhaseA]:checked").val() === 'on' || 
					$("#close_item select[name=radioPhaseA]").val() === 'on') {
					PhaseStatus += 'A';
				}
				if($("#close_item input[name=radioPhaseB]:checked").val() === 'on' || 
					$("#close_item select[name=radioPhaseB]").val() === 'on') {
					PhaseStatus += 'B';
				}
				if($("#close_item input[name=radioPhaseC]:checked").val() === 'on' || 
					$("#close_item select[name=radioPhaseC]").val() === 'on') {
					PhaseStatus += 'C';
				}
				data.PhaseStatus = PhaseStatus;
				data.Phase = clsCurrentWorkItem.phases_used;

				data.District = $("#inc_close_district", close_item).val();
				// Per Kelli on 06/29/2017 we should allow closing if the status is oops
				if(g_hasDistricts === true && data.District && data.District.length === 0 && parseInt(CloseStatus,10) !== 98) {
					jAlert("You must first specify a district");
					return;
				}
				data.AllowCallbacks = $("#AllowCallbacks", close_item).val();
				data.Alias = $("#inc_close_alias", close_item).val();
				data.LineSection = $("#inc_close_linesection", close_item).html();
				data.Comments = $("#inc_close_comments").html();
				data.CustName = $("#inc_close_alias").val();
				data.OriginalPhaseStatus = $("#OriginalPhaseStatus", close_item).val();
				data.CustName = $("#inc_close_alias", close_item).val();
				data.outage_state = $("#outage_state", close_item).is(':checked');
//				data.mark_scouted = $("#mark_scouted").is(':checked');
				break;
			case 'G':
				data.Phase = clsCurrentWorkItem.phases_used;
				data.District = $("#inc_close_district", close_item).val();
				if(g_hasDistricts === true && data.District.length === 0 && parseInt(CloseStatus,10) !== 98) {
					jAlert("You must first specify a district");
					return;
				}
				data.customers_aff = $("#inc_close_custaffected").val();
				data.num_incidents = $("#inc_close_incincluded").html();
				break;
			case 'C':
				data.CallBackFlag = $("#inc_callbackflag").val();
				if(data.CallBackFlag === 'Y') {
					data.CallBackPhone = clsCurrentWorkItem.callback_phone;
				}
				else {
					data.CallBackPhone = '';
				}
				data.Status = $("#inc_hold_status").val();
				var sms =  $("#inc_callback_sms").val();
				if(clsCurrentWorkItem.callback_email.length > 0) {
					data.EmailAddress = clsCurrentWorkItem.callback_email;
				}
				else if(clsCurrentWorkItem.callback_sms.length > 0) {
					data.EmailAddress = clsCurrentWorkItem.callback_sms+clsCurrentWorkItem.callback_sms_carrier;
				}
				break;
			}
			break;

		case 'W':	// This falls through on purpose
		case 'S':
			// Start validation
			close_date = $("#close_so_date", close_item).val();
			if(close_date.length === 0) {
				jAlert("Please enter the Close Date/Time.");
				return;
			}
			if(checkdate(close_date) === false) {
				jAlert("You must enter a valid completion date");
				$("#close_so_date", close_item).focus();
				return;
			}
			var close_so_by = $("#close_so_by", close_item).val();
			if(close_so_by.length === 0) {
				jAlert("Please enter the Close By.");
				return;
			}
			data.pending_close_by = $("#pending_close_by", close_item).text();
			data.pending_close_date = $("#pending_close_date", close_item).text();

			// Reformat the close time to match what the third party interface is expecting
			close_date = formatFullDateAndTime(close_date);
//			var close_date_object = new Date(close_date);
//			close_date = "" +(close_date_object.getMonth() + 1) + "/" + close_date_object.getDate() + "/" + close_date_object.getFullYear() + " " +
//								close_date_object.getHours() + ":" + close_date_object.getMinutes();

			data.work_item_type = work_item_type;
			data.close_so_by = close_so_by;
			data.close_so_date = close_date;
			data.close_so_comments = escape($("#close_so_comments", close_item).val());
			data.event_id = $("#so_event", close_item).val();
			data.close_as_pending = $("#close_so_pending").length === 1 && $("#close_so_pending").val() === 'Y' ? true : false;

			if(work_item_type === 'W') {
				var create_date = $("#create_wo_date", close_item).val();
				if(create_date.length === 0) {
					jAlert("Please enter the Entered Date/Time.");
					return;
				}
				if(checkdate(create_date) === false) {
					jAlert("You must enter a valid Entered date");
					$("#create_wo_date", close_item).focus();
					return;
				}
				data.create_date = create_date;
				data.equipment_type = $("#wo_equipment_type", close_item).val();
				data.equipment_id = $("#wo_equipment_id", close_item).val();
				data.wo_type = $("#wo_order_type", close_item).val();
				data.wo_subtype = $("#wo_order_subtype", close_item).val();
				data.wo_description = $("#wo_description", close_item).val();
				data.order_comment = $("#order_comment", close_item).val();
			}
			else if(work_item_type === 'S' && (clsCurrentWorkItem.is_mobile === false || (clsCurrentWorkItem.is_mobile === true && clsCurrentWorkItem.mobile_app_name === 'LinemanApp'))) {
				// Check requires - clsCloseWorkItem.requires - require_meter_connect
				var failed = false;
				var alerts = '';

				// First make sure all the meter operations are checked according to what's defined for this service order
				if(clsCloseWorkItem.requires.require_meter_read === true && !$("#mtr_read", close_item).is(':checked')) {
					alerts += "read";
					$("#mtr_read", close_item).prop('checked',true);
					$("#close_item #meter_read_block").show();
					failed = true;
				}
				if(clsCloseWorkItem.requires.require_meter_install === true && !$("#mtr_installed", close_item).is(':checked')) {
					if(alerts.length > 0) { alerts += ' and '; }
					alerts += "installed";
					$("#mtr_installed", close_item).prop('checked',true);
					$("#close_item #meter_installed_block").show();
					failed = true;
				}
				if(clsCloseWorkItem.requires.require_meter_remove === true && !$("#mtr_removed", close_item).is(':checked')) {
					if(alerts.length > 0) { alerts += ' and '; }
					alerts += "removed";
					$("#mtr_removed", close_item).prop('checked',true);
					$("#close_item #meter_removed_block").show();
					failed = true;
				}
				if(clsCloseWorkItem.requires.require_meter_connect === true && !$("#mtr_connected", close_item).is(':checked')) {
					if(alerts.length > 0) { alerts += ' and '; }
					alerts += "installed";
					$("#mtr_connected", close_item).prop('checked',true);
					failed = true;
				}
				if(clsCloseWorkItem.requires.require_meter_disconnect === true && !$("#mtr_disconnected", close_item).is(':checked')) {
					if(alerts.length > 0) { alerts += ' and '; }
					alerts += "installed";
					$("#mtr_disconnected", close_item).prop('checked',true);
					failed = true;
				}
				if(clsCloseWorkItem.requires.require_remove_or_disconnect === true) {
					if(!$("#mtr_disconnected", close_item).is(':checked') && !$("#mtr_removed", close_item).is(':checked')) {
						if(alerts.length > 0) { alerts += ' and '; }
						alerts += "removed or check disconnected";
						failed = true;
					}
				}
				if(failed) {
					jAlert("You must enter meter "+alerts+" information.");
					return;
				}

				if(clsCloseWorkItem.requires.require_transformer_connect === true && !$("#xfmr_installed", close_item).is(':checked')) {
					$("#xfmr_installed", close_item).prop('checked',true);
					setTimeout('onClickXfmrInstalled();',0);
					jAlert("You must enter transformer information.");
					return;
				}

				failed = false;
				alerts = '';
				if($("#mtr_read", close_item).is(':checked')) {
					if($("#read_mtr_number", close_item).val().length === 0) {
						if(alerts.length > 0) { alerts += ' and '; }
						alerts += "a read meter number";
						failed = true;
					}

					if(clsCloseWorkItem.requires.require_meter_read_demand === true) {
						if($("#read_mtr_reading", close_item).val().length === 0) {
							if(alerts.length > 0) { alerts += ' and '; }
							alerts += "a read demand meter reading";
							failed = true;
						}
					}
					if(clsCloseWorkItem.requires.require_meter_read_kvar === true) {
						if($("#read_mtr_kvar_read", close_item).val().length === 0) {
							if(alerts.length > 0) { alerts += ' and '; }
							alerts += "a read Kvar meter reading";
							failed = true;
						}
					}
					if(clsCloseWorkItem.requires.require_meter_read_kwh === true) {
						if($("#read_mtr_kw_read", close_item).val().length === 0) {
							if(alerts.length > 0) { alerts += ' and '; }
							alerts += "a read Kwh meter reading";
							failed = true;
						}
					}

					if($("#read_mtr_read_date", close_item).val().length === 0) {
						if(alerts.length > 0) { alerts += ' and '; }
						alerts += "a meter reading date";
						failed = true;
					}
					if(checkdate($("#read_mtr_read_date", close_item).val()) === false) {
						if(alerts.length > 0) { alerts += ' and '; }
						alerts += "a valid meter reading date";
						$("#read_mtr_read_date", close_item).focus();
						failed = true;
					}
					$("#meter_read_block input,#meter_read_block select", close_item).each(function() {
						data[$(this).attr('id')] = $(this).val();
					});
					data.mtr_read = true;
				}

				if($("#mtr_installed", close_item).is(':checked')) {
					if($("#install_mtr_number", close_item).val().length === 0) {
						if(alerts.length > 0) { alerts += ' and '; }
						alerts += "a meter installed number";
						failed = true;
					}
					if(clsCloseWorkItem.requires.require_meter_install_demand === true) {
						if($("#install_mtr_reading", close_item).val().length === 0) {
							if(alerts.length > 0) { alerts += ' and '; }
							alerts += "an install demand meter reading";
							failed = true;
						}
					}
					if(clsCloseWorkItem.requires.require_meter_install_kvar === true) {
						if($("#install_mtr_kvar_read", close_item).val().length === 0) {
							if(alerts.length > 0) { alerts += ' and '; }
							alerts += "an install Kvar meter reading";
							failed = true;
						}
					}
					if(clsCloseWorkItem.requires.require_meter_install_kwh === true) {
						if($("#install_mtr_kw_read", close_item).val().length === 0) {
							if(alerts.length > 0) { alerts += ' and '; }
							alerts += "an install Kwh meter reading";
							failed = true;
						}
					}
					if($("#install_mtr_read_date", close_item).val().length === 0) {
						if(alerts.length > 0) { alerts += ' and '; }
						alerts += "a meter installed reading date";
						failed = true;
					}
					if(checkdate($("#install_mtr_read_date", close_item).val()) === false) {
						if(alerts.length > 0) { alerts += ' and '; }
						alerts += "You must enter a valid meter installed reading date";
						failed = true;
					}
					$("#meter_installed_block input,#meter_installed_block select", close_item).each(function() {
						data[$(this).attr('id')] = $(this).val();
					});
					data.mtr_installed = true;
				}


				if($("#mtr_removed", close_item).is(':checked')) {
					if($("#remove_mtr_number", close_item).val().length === 0) {
						if(alerts.length > 0) { alerts += ' and '; }
						alerts += "a meter removed number";
						failed = true;
					}
					if(clsCloseWorkItem.requires.require_meter_remove_demand === true) {
						if($("#remove_mtr_reading", close_item).val().length === 0) {
							if(alerts.length > 0) { alerts += ' and '; }
							alerts += "a remove demand meter reading";
							failed = true;
						}
					}
					if(clsCloseWorkItem.requires.require_meter_remove_kvar === true) {
						if($("#remove_mtr_kvar_read", close_item).val().length === 0) {
							if(alerts.length > 0) { alerts += ' and '; }
							alerts += "a remove Kvar meter reading";
							failed = true;
						}
					}
					if(clsCloseWorkItem.requires.require_meter_remove_kwh === true) {
						if($("#remove_mtr_kw_read", close_item).val().length === 0) {
							if(alerts.length > 0) { alerts += ' and '; }
							alerts += "a remove Kwh meter reading";
							failed = true;
						}
					}
					if($("#remove_mtr_read_date", close_item).val().length === 0) {
						if(alerts.length > 0) { alerts += ' and '; }
						alerts += "a meter removed reading date";
						failed = true;
					}
					if(checkdate($("#remove_mtr_read_date", close_item).val()) === false) {
						if(alerts.length > 0) { alerts += ' and '; }
						alerts += "a valid meter removed reading date";
						$("#remove_mtr_read_date", close_item).focus();
						failed = true;
					}
					$("#meter_removed_block input,#meter_removed_block select", close_item).each(function() {
						data[$(this).attr('id')] = $(this).val();
					});
					data.mtr_removed = true;
				}
				if(failed) {
					jAlert("You must enter "+alerts+".");
					return;
				}

				data.mtr_disconnected = $("#mtr_disconnected", close_item).is(':checked');
				if(data.mtr_disconnected === false && clsCloseWorkItem.requires.require_meter_disconnect === true) {
					jAlert("This order code requires a meter disconnect.");
					return;
				}

				data.mtr_connected = $("#mtr_connected", close_item).is(':checked');
				if(data.mtr_connected === false && clsCloseWorkItem.requires.require_meter_connect === true) {
					jAlert("This order code requires a meter connect.");
					return;
				}

				if($("#xfmr_installed", close_item).is(':checked')) {
					var xfmr_installed_serial1 = $("#xfmr_installed_serial1", close_item).val();
					var xfmr_installed_mfg1 = $("#xfmr_installed_mfg1", close_item).val();
					var xfmr_installed_size1 = $("#xfmr_installed_size1", close_item).val();
					if(xfmr_installed_serial1.length === 0 || xfmr_installed_mfg1.length === 0 || xfmr_installed_size1.length === 0) {
						jAlert("You must fill in transformer serial number, manufacturer, and size.");
						return;
					}
					data.xfmr_installed = true;
					if($("#xfmr_installed_coop1", close_item).val().length > 0 || $("#xfmr_installed_serial1", close_item).val().length > 0) {
						data['xfmr_installed_coop-1'] = $("#xfmr_installed_coop1", close_item).val();
						data['xfmr_installed_serial-1'] = xfmr_installed_serial1;
						data['xfmr_installed_mfg-1'] = xfmr_installed_mfg1;
						data['xfmr_installed_size-1'] = xfmr_installed_size1;
						data['xfmr_installed_fuse-1'] = $("#xfmr_installed_fuse1", close_item).val();
						data['xfmr_installed_mount-1'] = $("#xfmr_installed_mount1", close_item).val();
						data['xfmr_installed_pri-1'] = $("#xfmr_installed_pri1", close_item).val();
						data['xfmr_installed_sec-1'] = $("#xfmr_installed_sec1", close_item).val();
						data['xfmr_installed_bush-1'] = $("#xfmr_installed_bush1", close_item).val();
						data['xfmr_installed_conn-1'] = $("#xfmr_installed_conn1", close_item).val();
					}
					if($("#xfmr_installed_coop2", close_item).val().length > 0 || $("#xfmr_installed_serial2", close_item).val().length > 0) {
						data['xfmr_installed_coop-2'] = $("#xfmr_installed_coop2", close_item).val();
						data['xfmr_installed_serial-2'] = $("#xfmr_installed_serial2", close_item).val();
						data['xfmr_installed_mfg-2'] = $("#xfmr_installed_mfg2", close_item).val();
						data['xfmr_installed_size-2'] = $("#xfmr_installed_size2", close_item).val();
						data['xfmr_installed_fuse-2'] = $("#xfmr_installed_fuse2", close_item).val();
						data['xfmr_installed_mount-2'] = $("#xfmr_installed_mount2", close_item).val();
						data['xfmr_installed_pri-2'] = $("#xfmr_installed_pri2", close_item).val();
						data['xfmr_installed_sec-2'] = $("#xfmr_installed_sec2", close_item).val();
						data['xfmr_installed_bush-2'] = $("#xfmr_installed_bush2", close_item).val();
						data['xfmr_installed_conn-2'] = $("#xfmr_installed_conn2", close_item).val();
					}
					if($("#xfmr_installed_coop3", close_item).val().length > 0 || $("#xfmr_installed_serial3", close_item).val().length > 0) {
						data['xfmr_installed_coop-3'] = $("#xfmr_installed_coop3", close_item).val();
						data['xfmr_installed_serial-3'] = $("#xfmr_installed_serial3", close_item).val();
						data['xfmr_installed_mfg-3'] = $("#xfmr_installed_mfg3", close_item).val();
						data['xfmr_installed_size-3'] = $("#xfmr_installed_size3", close_item).val();
						data['xfmr_installed_fuse-3'] = $("#xfmr_installed_fuse3", close_item).val();
						data['xfmr_installed_mount-3'] = $("#xfmr_installed_mount3", close_item).val();
						data['xfmr_installed_pri-3'] = $("#xfmr_installed_pri3", close_item).val();
						data['xfmr_installed_sec-3'] = $("#xfmr_installed_sec3", close_item).val();
						data['xfmr_installed_bush-3'] = $("#xfmr_installed_bush3", close_item).val();
						data['xfmr_installed_conn-3'] = $("#xfmr_installed_conn3", close_item).val();
					}
				}

				if($("#xfmr_removed", close_item).is(':checked')) {
					data.xfmr_removed = true;
					if($("#xfmr_removed_coop1", close_item).val().length > 0 || $("xfmr_removed_serial1", close_item).val().length > 0) {
						data['xfmr_removed_coop-1'] = $("#xfmr_removed_coop1", close_item).val();
						data['xfmr_removed_serial-1'] = $("#xfmr_removed_serial1", close_item).val();
						data['xfmr_removed_mfg-1'] = $("#xfmr_removed_mfg1", close_item).val();
						data['xfmr_removed_size-1'] = $("#xfmr_removed_size1", close_item).val();
						data['xfmr_removed_fuse-1'] = $("#xfmr_removed_fuse1", close_item).val();
						data['xfmr_removed_mount-1'] = $("#xfmr_removed_mount1", close_item).val();
						data['xfmr_removed_pri-1'] = $("#xfmr_removed_pri1", close_item).val();
						data['xfmr_removed_sec-1'] = $("#xfmr_removed_sec1", close_item).val();
						data['xfmr_removed_bush-1'] = $("#xfmr_removed_bush1", close_item).val();
						data['xfmr_removed_conn-1'] = $("#xfmr_removed_conn1", close_item).val();
					}
					if($("#xfmr_removed_coop2", close_item).val().length > 0 || $("#xfmr_removed_serial2", close_item).val().length > 0) {
						data['xfmr_removed_coop-2'] = $("#xfmr_removed_coop2", close_item).val();
						data['xfmr_removed_serial-2'] = $("#xfmr_removed_serial2", close_item).val();
						data['xfmr_removed_mfg-2'] = $("#xfmr_removed_mfg2", close_item).val();
						data['xfmr_removed_size-2'] = $("#xfmr_removed_size2", close_item).val();
						data['xfmr_removed_fuse-2'] = $("#xfmr_removed_fuse2", close_item).val();
						data['xfmr_removed_mount-2'] = $("#xfmr_removed_mount2", close_item).val();
						data['xfmr_removed_pri-2'] = $("#xfmr_removed_pri2", close_item).val();
						data['xfmr_removed_sec-2'] = $("#xfmr_removed_sec2", close_item).val();
						data['xfmr_removed_bush-2'] = $("#xfmr_removed_bush2", close_item).val();
						data['xfmr_removed_conn-2'] = $("#xfmr_removed_conn2", close_item).val();
					}
					if($("#xfmr_removed_coop3", close_item).val().length > 0 || $("#xfmr_removed_serial3", close_item).val().length > 0) {
						data['xfmr_removed_coop-3'] = $("#xfmr_removed_coop3", close_item).val();
						data['xfmr_removed_serial-3'] = $("#xfmr_removed_serial3", close_item).val();
						data['xfmr_removed_mfg-3'] = $("#xfmr_removed_mfg3", close_item).val();
						data['xfmr_removed_size-3'] = $("#xfmr_removed_size3", close_item).val();
						data['xfmr_removed_fuse-3'] = $("#xfmr_removed_fuse3", close_item).val();
						data['xfmr_removed_mount-3'] = $("#xfmr_removed_mount3", close_item).val();
						data['xfmr_removed_pri-3'] = $("#xfmr_removed_pri3", close_item).val();
						data['xfmr_removed_sec-3'] = $("#xfmr_removed_sec3", close_item).val();
						data['xfmr_removed_bush-3'] = $("#xfmr_removed_bush3", close_item).val();
						data['xfmr_removed_conn-3'] = $("#xfmr_removed_conn3", close_item).val();
					}
				}
//				data.xfmr_disconnected = $("#xfmr_disconnected", close_item).is(':checked');
//				if(data.xfmr_disconnected === false && clsCloseWorkItem.requires.require_transformer_disconnect === true) {
//					jAlert("This order code requires a transformer disconnect.");
//					return;
//				}

//				data.xfmr_connected = $("#xfmr_connected", close_item).is(':checked');
//				if(data.xfmr_connected === false && clsCloseWorkItem.requires.require_transformer_connect === true) {
//					jAlert("This order code requires a transformer connect.");
//					return;
//				}

				var installedLights = parseInt($("#numInstalledLights").val(),10);
				if(installedLights > 0) {
					data.installedLights = installedLights;
					data.installedLightType = $("#installedLightType").val();
					data.installedComment = $("#installedComment").val();
				}
				var numRemovedLights = parseInt($("#numRemovedLights").val(),10);
				if(numRemovedLights > 0) {
					data.removedLights = numRemovedLights;
					data.removedLightType = $("#removedLightType").val();
					data.removedComment = $("#removedComment").val();
				}

				// Payment information
				data.payment_amount = $("#payment_amount", close_item).val();
				data.payment_type = $("#payment_type").val();
				if(data.payment_type === 'Cash' || data.payment_type === 'Check') {
					data.payment_amount = $("#payment_amount").val();
				}
				data.check_number = $("#check_number").val();
				data.certified_check = $("#certified_check").is(':checked');
				if(data.payment_type === 'Promise') {
					data.promise_to_pay_date = $("#so_ptp_date").val();
					data.payment_amount = $("#payment_amount").val();
				}
			}
			break;
		} // end switch on work item type

		data.SkipNotifications = '';
		if(clsCurrentWorkItem.work_item_number.substr(0,1) === 'G' && clsCloseWorkItem.notify_flag === true && clsGeneralNotifications.cancelled === false) {
			data.SkipNotifications = 'T';
		}
		if($("#skipNotifications", close_item).length > 0) {
			if($("#skipNotifications", close_item).is(":checked")) {
				data.SkipNotifications = 'C';
			}
		}

		if(clsCurrentWorkItem.is_mobile && clsCurrentWorkItem.is_mobile === true) {
			if(clsCurrentWorkItem.mobile_app_name && clsCurrentWorkItem.mobile_app_name.length > 0) {
				data.origin = clsCurrentWorkItem.mobile_app_name;
			}
			else {
				data.origin = "LineMan App";
			}
		}
		else {
			data.origin = "Back Office";
		}

		close_item.parent().find(".ui-dialog-buttonpane button:contains('Close Item')").button("disable");
		if(work_item_type === 'I' && g_work_item_number.substr(0,1) === 'D') {
			if(typeof(clsWaiting) === 'object') {
				clsWaiting.show();
			}
		}

		var domain = '';
		if(clsCurrentWorkItem.domain && clsCurrentWorkItem.domain.length > 0) {
			domain = clsCurrentWorkItem.domain;
		}
		if(typeof(clsWaiting) === 'object') {
			clsWaiting.show();
		}


		if(clsCurrentWorkItem.is_mobile === true) {
			if(typeof(clsCurrentWorkItem.lat) !== 'undefined' && clsCurrentWorkItem.lat > 0) {
				data['lat'] = clsCurrentWorkItem.lat;
				data['lon'] = clsCurrentWorkItem.lon;
			}

			var d = new Date();
			data.timezoneOffset = d.getTimezoneOffset();
		}

		if(clsCurrentWorkItem.is_mobile === true) {
			if(typeof(localStorage.usr) !== 'undefined') {
				data.UserID = localStorage.usr;
			}
		}

		//This always needs to be the last thing that happens before the ajax call !!!
		if(clsCurrentWorkItem.is_mobile === true && navigator.connection.type === Connection.NONE) {
		    storeAjaxRequest(domain+"/dvwfm2/backoffice/ajaxCloseItem.php?callback=?", data);
		    $("#close_item_container").hide();
		    $("#close_item").hide();
		    return;
		}
		$.ajax({
			type: "POST",
			url: domain+"/dvwfm2/backoffice/ajaxCloseItem.php",
			data: data,
			dataType: "json",
			timeout: 30000,
			success: function(msg) {
				if(typeof(clsWaiting) === 'object') {
					clsWaiting.hide();
				}
				if(msg.result !== "true") {
					close_item.parent().find(".ui-dialog-buttonpane button:contains('Close Item')").button("enable");
					jAlert("Close Item: "+msg.error);
					return;
				}
				if(typeof(msg.error) === 'string') {
					jAlert("Your item has been closed but: "+msg.error, "Warning");
				}
				else {
					jCompletion("Your item has been closed", "Close Item", null, 1500);
				}

				if($("#dlgIncludedProjectItems").dialog('isOpen') === true) {
					$("#includedProjectItems").flexReload();
				}

				if(!clsCurrentWorkItem.is_mobile || clsCurrentWorkItem.is_mobile === false) {
					setTimeout('$("#close_item").dialog("close");', 20);
                }
                else {
                	$("#close_item_container").hide();
                }

				if(typeof(my_callback) === 'function') {
					setTimeout(my_callback(true),0);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(typeof(clsWaiting) === 'object') {
					clsWaiting.hide();
				}
				if(clsCurrentWorkItem.is_mobile === true) {
					if (textStatus == 'timeout' || XMLHttpRequest.status == 0) {
						setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'backoffice.close_work_item'});
						if (localStorage.googleapi == 'sdk') {
							crews[crewsSorter[data.work_item_number]].get('myData').isClosing = true;
						}
						storeAjaxRequest(domain+"/dvwfm2/backoffice/ajaxCloseItem.php?callback=?", data);
						localStorage.masterMarkerList = JSON.stringify(masterMarkerList);
						$("#close_item_container").hide();
						$("#waiting").hide();
						$("#close_item").hide();
						return;
					}
				}
				close_item.parent().find(".ui-dialog-buttonpane button:contains('Close Item')").button("enable");
				alert("AJAX failed to close item: "+errorThrown);
				if(typeof(my_callback) === 'function') {
					setTimeout(my_callback(false),0);
				}
			}
		});
	}
};

$(document).ready(function() {
	if(clsCurrentWorkItem.is_mobile === false) {
		$(document.body).append('<div id="update_contact_info" style="display:none;"></div>');
		$("#update_contact_info").dialog({
			bgiframe: true,
			autoOpen: false,
			width: "600px",
			height: 200,
			modal: true,
			position: {my:'center top',at:'center top',of:window},
			zIndex: 3999,
			title: "Update Customer Contact Information",
			open: function(event, ui) {
			},
			close: function() {
				$("#update_contact_info").empty();
			},
			buttons: {
				'Update Now': function() {
					var data = {
						action:"update",
						account_number: clsCurrentWorkItem.account_number,
						service_index: clsCurrentWorkItem.service_index,
						old_pri_phone:$("#update_contact_info #old_pri_phone").val(),
						old_alt_phone:$("#update_contact_info #old_alt_phone").val(),
						old_email:$("#update_contact_info #old_email").val(),
						pri_phone:$("#update_contact_info #pri_phone").val(),
						alt_phone:$("#update_contact_info #alt_phone").val(),
						email:$("#update_contact_info #email").val()
					};
					var domain = '';
					if(clsCurrentWorkItem.domain && clsCurrentWorkItem.domain.length > 0) {
						domain = clsCurrentWorkItem.domain;
					}
					$.ajax({
						url: domain+"/dvwfm2/backoffice/ajax/ajaxUpdateCustomerInfo.php",
						data: data,
						dataType: 'json',
						timeout: 100000,
						success: function(msg) {
							if(msg.result === 'false') {
								jAlert("Can't update customer information: "+msg.error);
								return;
							}
							if($("#inc_cust_email_address").length > 0 && data.email.length > 0) {
								$("#inc_cust_email_address").text(data.email);
							}
							jCompletion("Note: Update incident to save new information.<br>New Contact Information will not<br>be updated until the incident is updated.", "Update Customer Contact Information", null, 10000);
							$("#update_contact_info").dialog('close');
							return;
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							alert("AJAX failed to update customer information: "+errorThrown);
							return;
						}
					});
				},
				Cancel: function() {
					$("#update_contact_info").dialog('close');
				}
			}
		});

		$(document.body).append('<div id="set_restore_notification" style="display:none;"></div>');
		$("#set_restore_notification").dialog({
			bgiframe: true,
			autoOpen: false,
			width: "640px",
			height: 350,
			modal: true,
			position: {my:'center top',at:'center top',of:window},
			zIndex: 3999,
			title: "Restore Notification",
			open: function(event, ui) {
			},
			close: function() {
			},
			buttons: {
				'Update Now': function() {
					var email = $("#restoreNotificationContainer #tempEmailAddress").val() || '';
					var sms = $("#restoreNotificationContainer #tempSmsNumber").val() || '';
					if(email && email.length > 0 && sms && sms.length > 0) {
						jAlert("You may specify an email address OR text message number but not both.");
						return;
					}
					var carrier = $("#restoreNotificationContainer #carrier").val() || '';
					if(sms && sms.length > 0) {
						if(carrier && (carrier.length === 0 || carrier === "-1")) {
							jAlert("If you specify an SMS number you must specify a carrier.");
							return;
						}

						sms = sms.replace(/[+\(\)\-,. ]/g, "");
						if(isNaN(sms)) {
							jAlert("Invalid text message number "+$("#restoreNotificationContainer #tempSmsNumber").val());
							return;
						}
					}

					clsCurrentWorkItem.callback_phone = $("#restoreNotificationContainer #tempPhone").val();
					clsCurrentWorkItem.callback_email = email;
					clsCurrentWorkItem.callback_sms = sms;
					clsCurrentWorkItem.callback_sms_carrier = carrier;

					var email_str = '';
					if(email.length > 0) {
						email_str = email;
					}
					else if(sms.length > 0) {
						email_str = sms+carrier;
					}
					if($("#close_item").hasClass("ui-dialog-content") && $("#close_item").dialog("isOpen") === true) {
						$("#close_item #inc_callback_phone").text(clsCurrentWorkItem.callback_phone);
						$("#close_item #inc_callback_email").text(email_str);
						$("#close_item #inc_callbackflag").val('Y');
					}
					else if($("#update_item").hasClass("ui-dialog-content") && $("#update_item").dialog("isOpen") === true) {
						$("#update_item #inc_callback_phone").text(clsCurrentWorkItem.callback_phone);
						$("#update_item #inc_callback_email").text(email_str);
						$("#update_item #inc_callbackflag").val('Y');
					}

					$("#set_restore_notification").dialog('close');
				},
				Cancel: function() {
					$("#set_restore_notification").dialog('close');
				}
			}
		});

		$(document.body).append('<div id="close_item" style="display:none;overflow-x:hidden;"></div>');
		$("#close_item").dialog({
			bgiframe: true,
			autoOpen: false,
			width: "1100px",
			height: 850,
			modal: true,
			position: {my:'center top',at:'center top',of:window},
			zIndex: 3999,
			dragStop: function(event, ui) {
			},
			open: function(event, ui) {
				$("#close_item").parent().find(".ui-dialog-buttonpane button:contains('Close Item')").button("disable");
				setTimeout('insertCloseData()', 0);
				setTimeout('addDialogPrint("close_item");',100);

				if(clsCurrentWorkItem.is_mobile === false) {
					var buttons = [
						{ text:'Close Item', click: function() {
							clsCloseWorkItem.preClose();
							}
						}
					];
					if(clsCurrentWorkItem.work_item_number.substr(0,1) === 'G'/* && clsCurrentWorkItem.is_electric === false*/) {
						buttons[buttons.length] = {
							text: 'Notify', click: function() {
								if(clsCurrentWorkItem.work_item_number.substr(0,1) === 'G') {
									clsCloseWorkItem.notify_flag = true;
									clsGeneralNotifications.initGroupIncidentID(clsCurrentWorkItem.service_index, clsCurrentWorkItem.work_item_number);
					//				clsGeneralNotifications.callback = function() {
					//				};
									clsGeneralNotifications.show();
									return;
								}
							}
						};
					}

					if(g_hasScoutingFeature === true) {
						buttons[buttons.length] = { text:'Mark Scouted', click: function() {
								var markScouted = $("#markScouted");
								clsScouting.init();
								clsScouting.work_item_number = clsCurrentWorkItem.work_item_number;
								clsScouting.work_type = 'I';
								clsScouting.showMarkScoutedDialog();
							}
						};
					}
					buttons.sort(SortButtonsByName);
					buttons[buttons.length] = {
						text:'Cancel', click: function() {
							$("#close_item").dialog('close');
						}
					};
					$("#close_item").dialog("option", "buttons", buttons);
				}
			},
			close: function() {
				$("#close_item").empty().html("");
				if($("#gridDivTable").length > 0) {
					$("#gridDivTable").flexReload();
				}
				if(g_callAfterComplete !== null) {
					setTimeout(g_callAfterComplete, 100);
				}
				if(g_currentTab === 'WorkItems') {
					setTimeout('refreshGrid();', 20);
				}
			}
		});
	}
});

