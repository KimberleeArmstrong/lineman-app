/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery.extend({
	// Use of jQuery.browser is frowned upon.
	// More details: http://docs.jquery.com/Utilities/jQuery.browser
	uaMatch: function(ua) {
		ua = ua.toLowerCase();
		// Useragent RegExp
		rwebkit = /(webkit)[ \/]([\w.]+)/;
		ropera = /(opera)(?:.*version)?[ \/]([\w.]+)/;
		rmsie = /(msie) ([\w.]+)/;
		rmozilla = /(mozilla)(?:.*? rv:([\w.]+))?/;

		var match = rwebkit.exec(ua) ||
			ropera.exec(ua) ||
			rmsie.exec(ua) ||
			ua.indexOf("compatible") < 0 && rmozilla.exec(ua) ||
			[];

		return { browser: match[1] || "", version: match[2] || "0" };
	},
	browser: {}
});

browserMatch = jQuery.uaMatch(navigator.userAgent);
if(browserMatch.browser) {
	jQuery.browser[ browserMatch.browser ] = true;
	jQuery.browser.version = browserMatch.version;
}

// Deprecated, use jQuery.browser.webkit instead
if(jQuery.browser.webkit) {
	jQuery.browser.safari = true;
}

