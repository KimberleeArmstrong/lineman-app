// Fix all the quick search title changes
var g_column_titles = [];

// Search all the columns in <gridSearchArray> and
// replace <old_title> with <new_title>.
// This is for flexigrid column definitions.
function fixColumnTitles(gridSearchArray, old_title, new_title) {
	for(var i = 0;i < gridSearchArray.length;i++) {
		if(gridSearchArray[i]['display'] === old_title) {
			gridSearchArray[i]['display'] = new_title;
			return;
		}
	}
}

// Find the new title for <old_title>. If none exist, then
// just return <old_title>.
function findNewTitle(old_title) {
	for(var i = 0;i < g_column_titles.length;i++) {
		if(g_column_titles[i]['orig_column_name'] === old_title) {
			return g_column_titles[i]['new_column_name'];
		}
	}
	return '';
}

// Find any new title for <old_title> and replace it in <selector>.
function fixSelectedTitle(selector, old_title) {
	selector = selector + ':visible';
	var elem = $(selector);
	if($(elem).length > 0) {
		var new_title = findNewTitle(old_title);
		if(new_title.length !== 0) {
			$(elem).html(new_title);
		}
	}
}

// Find any new title for <old_title> and replace it in <selector>.
function fixTitleBySvcIdx(service_index, selector, old_title) {
	selector = selector + ':visible';
	var elem = $(selector);
	if($(elem).length > 0) {
		var new_title = findNewTitle(old_title);
		if(new_title.length !== 0) {
			$(elem).html(new_title);
		}
	}
}

// Check all changed titles for use in <gridSearchArray> and
// perform the appropriate replcement.
// This is for flexigrid column definitions.
function fixAllTitles(gridSearchArray) {
	if (typeof gridSearchArray === "undefined") {
		return;
	}
	for(var i = 0;i < g_column_titles.length;i++) {
		var old_title = g_column_titles[i]['orig_column_name'];
		var new_title = g_column_titles[i]['new_column_name'];
		fixColumnTitles(gridSearchArray,old_title, new_title);
	}
}

// Check all changed titles for use in <gridSearchArray> and
// perform the appropriate replcement.
// This is for flexigrid column definitions.
function fixAllTitlesBySvcIdx(service_index, gridSearchArray) {
	if (typeof gridSearchArray === "undefined") {
		return;
	}
	for(var i = 0;i < g_column_titles.length;i++) {
		var old_title = g_column_titles[i]['orig_column_name'];
		var new_title = g_column_titles[i]['new_column_name'];
		fixColumnTitles(gridSearchArray,old_title, new_title);
	}
}

// Pull the latest column titles from the database.
function getLatestColumnTitles(funcName, domain) {
	if(typeof(funcName) === 'undefined') {
		funcName = null;
	}
	if(typeof(domain) === 'undefined') {
		domain = '';
	}
	var myFuncName = funcName;
	$.ajax({
		type: "POST",
		url: domain+"/dvWfm2/BackOffice/ajaxConfigData.php",
		data: {type: 'get', id: 'config_column_names', mode:'associative'},
		dataType: "json",
		success: function(data) {
			if(data.result === "false") {
				jAlert(data.error, "Error Getting Column Title Changes");
				return;
			}
			g_column_titles = data['rows'].slice(0);
			if(myFuncName !== null && myFuncName !== undefined && myFuncName.length > 0) {
				setTimeout(myFuncName+'();', 50);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.status !== 0) {
				alert("AJAX failed to get column title changes: "+textStatus);
			}
		}
	});
}

// Pull the latest column titles from the database.
function getAndFixLatestColumnTitles(flexigridColModel) {
	var myFlexigridColModel = flexigridColModel;
	$.ajax({
		type: "POST",
		url: "/dvWfm2/BackOffice/ajaxConfigData.php",
		data: {type: 'get', id: 'config_column_names', mode:'associative'},
		dataType: "json",
		success: function(data) {
			if(data.result === "false") {
				jAlert(data.error, "Error Getting Column Title Changes");
				return;
			}
			g_column_titles = data['rows'].slice(0);
			if(myFlexigridColModel !== null && myFlexigridColModel.length > 0) {
				//setTimeout('fixAllTitles('+myFlexigridColModel+');', 50);
				fixAllTitles(myFlexigridColModel);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.status !== 0) {
				alert("AJAX failed to get column title changes: "+textStatus);
			}
		}
	});
}

function fixDialogTitles(dlgID) {
	if(dlgID.substr(0,1) !== '#') {
		dlgID = '#'+dlgID;
	}
	$(dlgID+" .label").each(function() {
		var oldText = $(this).text().replace(/\n|\s+$/g, '');
		if(oldText.length === 0) {
			return true;	// continue
		}
		var appendage = '';
		if(oldText.indexOf(':') !== -1) {
			appendage = ':';
		}
		if(oldText.substr(-6) === '&nbsp;') {
			appendage += '&nbsp;';
		}
		var text = oldText.replace(/(\s*:\s*|&nbsp;$|\s+$)/g,'').toLowerCase();
		for(var i = 0;i < g_column_titles.length;i++) {
			var old_title = g_column_titles[i]['orig_column_name'].toLowerCase();
			if(text === old_title) {
				var new_title = g_column_titles[i]['new_column_name'];
				var nt = new_title+appendage;
				$(this).text(nt);
				return true;	// continue
			}
		}
	});
}

var fields = [
	{name:'Accnt', value:'account'},
	{name:'DevCode', value:'devcode'},
	{name:'DevDesc', value:'devdesc'},
	{name:'LS', value:'linesect'},
	{name:'Meter', value:'meter'},
	{name:'MapLoc', value:'maploc'},
	{name:'Phase', value:'phase'},
	{name:'Pole', value:'pole'},
	{name:'SvcLoc', value:'svcloc'},
	{name:'SubFdr', value:'subfdr'},
	{name:'Tap', value:'tap'},
	{name:'Xfmr', value:'transformer'}
];
var grids = [
	{name:'Accnt', value:'acctno', value2: 'AccountNumber', value3: 'customer_account_number'},
	{name:'DevCode', value:''},
	{name:'DevDesc', value:''},
	{name:'LS', value:'line_sect', value2:'LineSection', value3: 'linesection'},
	{name:'Meter', value:'meternum', value2:'MeterNumber', value3: 'meternumber'},
	{name:'MapLoc', value:'maploc', value2:'map_loc', value3: 'service_maploc'},
	{name:'Phase', value:'phases', value2:'phase'},
	{name:'Pole', value:'pole', value2: 'pole_id', value3: 'service_loc_pole'},
	{name:'SvcLoc', value:'svcloc', value2:'srvloc'},
	{name:'SubFdr', value:'subfdr', value2:'substation'},
	{name:'Tap', value:'tap'},
	{name:'Xfmr', value:'custxfmr'}
];

function fixTitlesBySvcIdx(service_index) {
	service_index = parseInt(service_index,10);
	var titles = localStorage.getItem('column_titles');
	if(!titles || titles.length === 0) {
		return;
	}

	column_titles = JSON.parse(titles);
	var hidden_columns = [];

	for(var index = 0;index < column_titles.length;index++) {
		var title = column_titles[index];

		if(title.service_index !== 0 && title.service_index !== service_index) {
			continue;
		}

		// Try by identifier
		var identifier = '';
		var field_prompt = '';
		var field_value = '';
		for(var i = 0;i < fields.length;i++) {
			if(fields[i].value.length === 0 || fields[i].name !== title.identifier) {
				continue;
			}
			identifier = fields[i].value;
			field_prompt = '.'+identifier+'_prompt';
			field_value = '.'+identifier+'_value';
			break;
		}
		if(identifier.length === 0) {
			continue;
		}

		var text = $(field_prompt).text();
		if(text && text.length > 0) {
			text = text.trim();
		}

		if(title.new_column_name && title.new_column_name.length > 0) {
			var new_title = title.new_column_name;
			if(new_title.length > 0) {
				var colon = '';
				var space = '';

				new_title = new_title.trim();

				if(text.substr(-6) === '&nbsp;') {
					space = '&nbsp;';
					text = text.slice(0,-6);
				}
				if(text.substr(-1) === ' ') {
					space += ' ';
					text = text.slice(0,-1);
				}
				if(text.substr(-1) === ':') {
					colon = ':';
				}
				var newtext = title.new_column_name+colon+space;
				newtext = newtext.replace(/::/,':');
				newtext = newtext.replace(/: :/,':&nbsp;');
				$(field_prompt).html(newtext);
			}
		}
		else {
			$(field_prompt).text('');
		}

		if(title.show === 'FALSE') {
			$(field_prompt).text('');
			if($(field_value).is('input')) {
				$(field_value).hide();
			}
			else {
				$(field_value).text('');
			}
			if(title.hide.length > 0) {
				$(title.hide).hide();
			}
		}
		else {
			if($(field_value).is('input')) {
				$(field_value).show();
			}

		}
	}
}

function fixFlexigridColumns(service_index, colModel, searchItems) {	// namesArr is an array of column name objects: [ {display:"", name:""} ]
	if(typeof(colModel) === 'undefined') {
		return colModel;
	}
	service_index = parseInt(service_index,10);
	var titles = localStorage.getItem('column_titles');
	if(!titles || titles.length === 0) {
		return colModel;
	}

	var column_titles = JSON.parse(titles);
	var hidden_columns = [];

	for(var index = 0;index < column_titles.length;index++) {
		var title = column_titles[index];
		if(title.service_index !== 0 && title.service_index !== service_index) {
			continue;
		}

		var dbname = '';
		var dbname2 = '';
		var dbname3 = '';
		for(var i = 0;i < grids.length;i++) {
			if(grids[i].value.length === 0 || grids[i].name !== title.identifier) {
				continue;
			}
			dbname3 = dbname2 = dbname = grids[i].value;
			if(grids[i].value2) {
				dbname2 = grids[i].value2;
			}
			if(grids[i].value3) {
				dbname3 = grids[i].value3;
			}
			break;
		}
		if(dbname.length === 0) {
			continue;
		}

		// Search the original column model based on dbname and return the display name
		for(var i = 0;i < colModel.length;i++) {
			if(colModel[i].name === dbname || colModel[i].name === dbname2 || colModel[i].name === dbname3) {
				if(title.show === 'FALSE') {
					colModel[i].hide = true;
					colModel[i].exclude = true;
				}
				else {
					colModel[i].hide = false;
					colModel[i].display = title.new_column_name;
					colModel[i].exclude = false;
				}
			}
		}

		// Check the search items model based on dbname and return the display name
		if(typeof(colModel) !== 'undefined') {
			for(var i = 0;i < searchItems.length;i++) {
				if(searchItems[i].name === dbname || searchItems[i].name === dbname2 || searchItems[i].name === dbname3) {
					searchItems[i].display = title.new_column_name;
				}
			}
		}
	}

	return colModel;
}
