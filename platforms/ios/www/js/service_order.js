var clsCurrentWorkItem;
var g_hasScoutingFeature;
var g_districtHtml;
var g_serviceHtml;
var g_substationSelectNumHtml;
var g_departmentHtml;
var g_serviceAreaHtml;
var g_currentTab;
var g_has_imageManagement;
var dialogFlexParams;
var clsWaiting;
var dialogParams;
var imageListManagement;
var g_createItemArgs;
var g_oo_needs_refresh;
var g_updateWorkItemType;
var g_work_item_number;
var clsScouting;
var clsDispatchItems;
var g_userid;
var g_allow_SOWO_user_updates;

function getNextOrderNumber(type) {
	$.ajax({
		type: "POST",
		url: "/dvwfm2/backoffice/ajaxSo.php",
		data: { action: "getNextSoNumber", type:type },
		dataType: "json",
		success: function(data) {
			if(data.result === "false") {
				jAlert(data.error, "Error");
			}
			else {
				$("#createSoDiv #so_number").val(data['next_order_number']);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX failed to get next order number: "+textStatus);
		}
	});
}

// Get the currently defined work order types from the database
// and display them in the type_elem element. If type_default
// is a string and has any length, set that option as the default.
function showWorkOrderTypes(type_elem, type_default, subtype_elem, subtype_default) {
	if(typeof(type_elem) === 'string') {
		if(type_elem.substr(0,1) !== '#') {
			type_elem = '#'+type_elem;
		}
	}
	if(typeof(type_default) === 'undefined') {
		type_default = '';
	}
	if(typeof(subtype_elem) === 'string') {
		if(subtype_elem.substr(0,1) !== '#') {
			subtype_elem = '#'+subtype_elem;
		}
	}
	if(typeof(subtype_default) === 'undefined') {
		subtype_default = '';
	}
	if(clsCurrentWorkItem.is_mobile === true && navigator.connection.type === Connection.NONE) {
		if(type_default === null) { type_default = ''; }
		if(subtype_default === null) { subtype_default = ''; }
		$(type_elem).html('<option value="'+type_default+'" selected>'+type_default+'</option>');
		$(subtype_elem).html('<option value="'+subtype_default+'" selected>'+subtype_default+'</option>');
		return;
	}

	$.ajax({
		type: "POST",
		url: clsCurrentWorkItem.domain+"/dvwfm2/BackOffice/ajaxConfigData.php",
		data: {type:'get', id:'config_wo_types', mode:'associative'},
		dataType: "json",
		success: function(msg) {
			if(msg.result === "false") {
				jAlert(msg.error, "Error");
				return;
			}
			var options = '<option recordid="0" value=""></option>';
			for(var i = 0;i < msg.rows.length;i++) {
				var option = msg.rows[i];
				options += '<option recordid="'+option.recordid+'" value="'+option.order_type+'">'+option.description.substr(0,20)+'</option>';
			}
			$(type_elem).html(options);
			if(type_default.length > 0) {
				$(type_elem+' option[value="'+type_default+'"]').prop('selected',true);
			}

			showWorkOrderSubTypes(subtype_elem, type_elem, subtype_default);
			setOrderTypesBind(type_elem, subtype_elem);		}
	});
}

function setOrderTypesBind(type_elem, subtype_elem) {
	$(type_elem).unbind('change').on('change', function() {
		showWorkOrderSubTypes(subtype_elem,type_elem);
	});

}

// Get the currently defined work order subtypes from the database
// and display them in the subtype_elem element. If subtype_default
// is a string and has any length, set that option as the default.
function showWorkOrderSubTypes(subtype_elem, type_elem, subtype_default) {
	if(typeof(subtype_default) === 'undefined') {
		subtype_default = '';
	}
	if(typeof(subtype_elem) === 'string') {
		if(subtype_elem.substr(0,1) !== '#') {
			subtype_elem = '#'+subtype_elem;
		}
	}

	var order_type = $(type_elem).val();

	if(order_type.length === 0) {
		$(subtype_elem).html('<option value=""></option>');
		return;
	}
	$.ajax({
		type: "POST",
		url: clsCurrentWorkItem.domain+"/dvwfm2/BackOffice/ajaxConfigData.php",
		data: {type:'subtypes', order_type:order_type, mode:'associative'},
		dataType: "json",
		success: function(msg) {
			if(msg.result === "false") {
				jAlert(msg.error, "Error");
				return;
			}
			var options = '<option value=""></option>';
			for(var i = 0;i < msg.rows.length;i++) {
				var option = msg.rows[i];
				options += '<option value="'+option.order_subtype+'">'+option.description.substr(0,20)+'</option>';
			}
			$(subtype_elem).html(options);
			$(subtype_elem+' option[value="'+subtype_default+'"]').prop('selected',true);
			if(clsCurrentWorkItem.is_mobile === true) {
				$("#close_item select:visible").selectmenu('refresh');
			}
		}
	});
}

// This is called when the user presses the Select button in the create SO search results grid.
function onFindCustSelect() {
	var grid = $("#findCustomersResultsGrid");
	if(ensureSingleSelection(grid) === false) {
		return;
	}
	var createSoDiv = $("#createSoDiv");

	$("#customer_account_number", createSoDiv).val(grid.getSelectedValueByDbName('customer_account_number'));
	$("#service_loc_account_number", createSoDiv).val(grid.getSelectedValueByDbName('customer_account_number'));
	$("#customer_name", createSoDiv).val(grid.getSelectedValueByDbName('customer_name'));
	$("#customer_billing_address", createSoDiv).val(grid.getSelectedValueByDbName('customer_billing_address'));
	$("#service_loc_line_section", createSoDiv).val(grid.getSelectedValueByDbName('service_loc_line_section'));
	$("#service_loc_address", createSoDiv).val(grid.getSelectedValueByDbName('service_loc_address'));
	$("#service_loc_city", createSoDiv).val(grid.getSelectedValueByDbName('service_loc_city'));
	$("#service_loc_state", createSoDiv).val(grid.getSelectedValueByDbName('service_loc_state'));
	$("#service_loc_zip", createSoDiv).val(grid.getSelectedValueByDbName('service_loc_zip'));
	$("#customer_email", createSoDiv).val(grid.getSelectedValueByDbName('customer_email'));
	// $("#service_loc_substation", createSoDiv).val(grid.getSelectedValueByDbName('service_loc_substation'));
	$("#service_loc_pole", createSoDiv).val(grid.getSelectedValueByDbName('service_loc_pole'));
	$("#service_loc_transformer", createSoDiv).val(grid.getSelectedValueByDbName('service_loc_transformer'));
	$("#service_loc_meter_number", createSoDiv).val(grid.getSelectedValueByDbName('service_loc_meter'));
	var phone = grid.getSelectedValueByDbName('customer_billing_daytime_phone');
	if(phone !== '9999999999') {
		if(phone.substr(0,1) !== '(') {
			$("#customer_billing_daytime_phone", createSoDiv).val(tel_format(phone));
		}
		else {
			$("#customer_billing_daytime_phone", createSoDiv).val(phone);
		}
	}
	else {
		$("#customer_billing_daytime_phone", createSoDiv).val('');
	}
	phone = grid.getSelectedValueByDbName('customer_evening_phone');
	if(phone !== '9999999999') {
		if(phone.substr(0,1) !== '(') {
			$("#customer_evening_phone", createSoDiv).val(tel_format(phone));
		}
		else {
			$("#customer_evening_phone", createSoDiv).val(phone);
		}
	}
	else {
		$("#customer_evening_phone", createSoDiv).val('');
	}

	$("#service_loc_district option:contains('"+grid.getSelectedValueByDbName('service_loc_district')+"')").attr("selected", true);
	// $("#service_loc_substation option:contains('"+grid.getSelectedValueByDbName('service_loc_substation')+"')").attr("selected", true);
	$("#due_date ", createSoDiv).val(getCurrentDate());
	$("#service_loc_substation", createSoDiv).val(grid.getSelectedValueByDbName('service_loc_substation'));
	var fdr_num = grid.getSelectedValueByDbName('service_loc_feeder');
	updateSubFdrList(grid.getSelectedValueByDbName('service_loc_substation'), fdr_num, "div#createSoDiv #service_loc_feeder");
	var phases_used = grid.getSelectedValueByDbName('service_loc_phases_used');
	$("#phases_used option[value="+phases_used+"]", createSoDiv).prop('selected',true);

	var service_index = $("#findCustomerDiv").data('service_index');
	$('#service_index option[value="'+service_index+'"]', createSoDiv).attr('selected',true);
	$("#geodata", createSoDiv).val(grid.getSelectedValueByDbName('geodata'));

	$("#findCustomersResults").dialog("close");
}

function loadSoForm(title) {
	var createSoDiv = $("#createSoDiv");
	createSoDiv.empty().load('/dvwfm2/forms/createServiceOrder.php', function() {
		$("#createSoDiv").show();
		$("#so_form_title", createSoDiv).html(title);
		if(g_hasScoutingFeature === true) {
			$("needs_scouting",createSoDiv).show();
		}
		else {
			$("needs_scouting",createSoDiv).hide();
		}

		$('#service_index').on('change',function() {
			var service_index = $('#service_index option:selected').attr('index');
			fixTitlesBySvcIdx(service_index, "#createSoDiv");
		});
		fixTitlesBySvcIdx(0, "#createSoDiv");
		setTimeout('loadSoEventHandlers()',300);
	});
}

var serviceOrderColModel = [
	{display: "ID",  			width: 40, name: "recordid",    		sortable: true, align: 'left', hide:true},
	{display: "SO Num",			width: 60, name: "so_number", 			sortable: true, align: 'left'},
	{display: "Service", 		width: 60, name: "so_service", 			sortable: true, align: 'left'},
	{display: "SO Type",		width: 70, name: "so_type", 			sortable: true, align: 'left'},
	{display: "Code",			width: 40, name: "so_code", 			sortable: true, align: 'left'},
	{display: "Customer Name",	width: 140,name: "customer_name",		sortable: true, align: 'left'},
	{display: "Service Location",width: 200,name: "service_loc_address",sortable: true, align: 'left'},
	{display: "Service Loc Desc",width: 210,name: "service_loc_description",sortable: true, align: 'left'},
	{display: "Service Zip",	width: 80, name: "service_loc_zip",		sortable: true, align: 'left'},
	{display: "Entered", 		width: 80, name: "entered_date", 		sortable: true, align: 'left', hide:true},
	{display: "Serv Area",		width: 72, name: "assigned_area",		sortable: true, align: 'left'},
	{display: "District",		width: 80, name: "district",			sortable: true, align: 'left'},
	{display: "Dept",			width: 40, name: "department",			sortable: true, align: 'left', hide:true},
	{display: "Rate", 			width: 80, name: "so_rate",				sortable: true, align: 'left', hide:true},
	{display: "Map Loc", 		width: 110, name: "service_maploc",		sortable: true, align: 'left'},
	{display: "Account Num", 	width: 94, name: "customer_account_number",sortable: true, align: 'left'},
//	{display: "Meter", 			width: 60, name: "meter",				sortable: true, align: 'left'},
	{display: "Pole", 			width: 60, name: "service_loc_pole",	sortable: true, align: 'left', hide:true},
	{display: "Substation", 	width: 50, name: "substation",			sortable: true, align: 'left', hide:true},
	{display: "Geodata", 		width: 50, name: "geodata",				sortable: true, align: 'left', hide:true},
	{display: "Feeder",			width: 80, name: "feeder",				sortable: true, align: 'left', hide:true},
	{display: "Overdue Date", 	width: 80, name: "overdue_date",		sortable: true, align: 'left', hide:true},
	{display: "Meet Date",		width: 60, name: "request_date",		sortable: true, align: 'left', hide:true},
	{display: "Meet Time",		width: 60, name: "request_time",		sortable: true, align: 'left', hide:true},
	{display: "Comments", 		width: 240, name: "comment",			sortable: true, align: 'left'},
	{display: "Created",		width: 120, name: "create_date",		sortable: true, align: 'left', hide:true}
];

var serviceOrderSearchitems = [
	{display: 'Account Num',	name: 'customer_account_number'},
	{display: 'Comment',		name: 'comment'},
	{display: 'Cust Name',		name: 'customer_name'},
	{display: 'Cust Phone',		name: 'cust_phone'},
	{display: "Dept",			name: "department"},
	{display: "District",		name: "district"},
	{display: 'Requested Date',	name: 'duedate'},
	{display: 'Map Location',	name: 'service_maploc'},
	{display: 'Meter',			name: 'meter'},
	{display: 'Pole',			name: 'service_loc_pole'},
	{display: 'Service Area',	name: 'assigned_area'},
	{display: 'SO Number',		name: 'so_number', isdefault: true},
	{display: "SO Code",		name: "so_code"},
	{display: "SO Type",		name: "so_type"},
	{display: 'Service Location',name: 'service_loc_address'},
	{display: "Service Loc Desc",name: "service_loc_description"}
];

var workOrderColModel = [
	{display: "ID",  			width: 40, name: "recordid",    		sortable: true, align: 'left', hide:true},
	{display: "WO Num",			width: 60, name: "wo_number", 			sortable: true, align: 'left'},
	{display: "Service", 		width: 60, name: "service_index", 		sortable: true, align: 'left'},
	{display: "Order Type", 	width: 60, name: "wo_type",				sortable: true, align: 'left'},
	{display: "Order SubType", 	width: 60, name: "wo_subtype",			sortable: true, align: 'left'},
	{display: "Description", 	width: 240, name: "wo_description",		sortable: true, align: 'left'},
	{display: "Customer Name",	width: 140,name: "customer_name",		sortable: true, align: 'left', hide:true},
	{display: "Service Location",width: 200,name: "service_loc_address",sortable: true, align: 'left', hide:true},
	{display: "Service Zip",	width: 80, name: "service_loc_zip",		sortable: true, align: 'left', hide:true},
	{display: "Serv Area",		width: 72, name: "assigned_area",		sortable: true, align: 'left'},
	{display: "District",		width: 80, name: "district",			sortable: true, align: 'left'},
	{display: "Dept",			width: 40, name: "department",			sortable: true, align: 'left', hide:true},
	{display: "Map Loc", 		width: 110, name: "service_maploc",		sortable: true, align: 'left', hide:true},
	{display: "Account Num", 	width: 94, name: "customer_account_number",sortable: true, align: 'left', hide:true},
	{display: "Geodata", 		width: 50, name: "geodata",				sortable: true, align: 'left', hide:true},
	{display: "Created",		width: 120, name: "create_date",		sortable: true, align: 'left', hide:true}
];

var workOrderSearchitems = [
	{display: 'Account Num',	name: 'customer_account_number'},
	{display: 'Cust Name',		name: 'customer_name'},
	{display: 'Cust Phone',		name: 'cust_phone'},
	{display: 'Description',	name: 'wo_description'},
	{display: "Dept",			name: "department"},
	{display: "District",		name: "district"},
	{display: 'Requested Date',	name: 'duedate'},
	{display: 'Map Location',	name: 'service_maploc'},
	{display: 'Service Area',	name: 'assigned_area'},
	{display: 'WO Number',		name: 'wo_number', isdefault: true},
	{display: 'Service Location',name: 'service_loc_address'}
];

// function clearUploadIframe() {
// 	$("#uploadIframe").hide();
// }

function pressFindCustomerButton() {
	if($("#findCustomerDiv").hasClass("ui-dialog-content") && $("#findCustomerDiv").dialog("isOpen")) {
		$("#findCustomerDiv").parent().find(".ui-dialog-buttonpane button:contains('Find Now')").click();
	}
}

function updateSubFdrList(sub_name, fdr_num, elem) {
	$(target_elem).html('');
	var target_elem = elem;
	$.ajax({
		type: 'POST',
		url: "/dvwfm2/backoffice/ajaxGetSystemInfo.php",
		data: {action: 'GetFeeders', subname: sub_name},
		dataType: 'json', // json, html, or text
		timeout: 30000,
		success: function (msg) {
			if(msg.result !== 'true') {
				jAlert("Can't get feeders for sub: "+sub_name, "Get Feeders", null, 1000);
				return;
			}
			var html = '<option value=""></option>'+"\n";
			fdr_num = parseInt(fdr_num,10);
			for(var i = 0;i < msg.Feeders.length;i++) {
				var fdr = msg.Feeders[i];
				var selected = "";
				if(fdr_num === parseInt(fdr.id,10)) {
					selected = ' selected="selected"';
				}
				html += '<option value="'+fdr.id+'"'+selected+'>'+fdr.name+'</option>'+"\n";
			}

			$(target_elem).html(html);
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX failed: Can't get feeders sub: " + sub_name + " - " + errorThrown);
		}
	});
}

function loadSoEventHandlers() {
	var type;

	// load the drop down lists
	$("div#createSoDiv #service_loc_substation").html(g_substationSelectNumHtml);
	$("div#createSoDiv #service_loc_district").html(g_districtHtml);
	$("div#createSoDiv #service_index").html(g_serviceHtml);
	$("div#createSoDiv #department").html(g_departmentHtml);
	$("div#createSoDiv #service_area").html(g_serviceAreaHtml);

	$("#generateOrderNumber").unbind().bind('click touchstart', function() {
		if($("#radioServiceOrder").is(":checked") === true) {
			type = 'S';
		}
		else {
			type = 'W';
		}
		getNextOrderNumber(type);
	});

	$("div#createSoDiv #service_loc_substation").unbind().on('touchstart change', function() {
		setTimeout('updateSubFdrList($("div#createSoDiv #service_loc_substation").val(), "", "div#createSoDiv #service_loc_feeder");', 0);
	});

	$("#radioServiceOrder,#radioWorkOrder").unbind().bind('click touchstart', function() {
		if($(this).attr("id") === 'radioServiceOrder') {
			$("#nameServiceOrder").css('font-weight','bold');
			$("#nameWorkOrder").css('font-weight','normal');
			$("#radioWorkOrder").attr("checked",false);
			$("span#account_number_required").html("*");
			$("#td_service_order_information").html("Service Order Information");
			$("td.for_so,tr.for_so").show();
			$("td.for_wo,tr.for_wo").hide();
		}
		else {	// Must be a work order
			$("#nameServiceOrder").css('font-weight','normal');
			$("#nameWorkOrder").css('font-weight','bold');
			$("#radioServiceOrder").attr("checked",false);
			$("span#account_number_required").html("");
			$("#td_service_order_information").html("Work Order Information");
			$("td.for_so,tr.for_so").hide();
			$("td.for_wo,tr.for_wo").show();
			setTimeout('populateWoTypes();',0);
		}
		setTimeout(function() { populateSoEvents('#createSoDiv #so_event', null); }, 0);

		// clear fields
		var createSoDiv = $("#createSoDiv");
		$("#so_type", createSoDiv).find("option:first").attr("selected");
		$("#so_code", createSoDiv).find("option:first").attr("selected");
		$("#service_area", createSoDiv).find("option:first").attr("selected");
		$("#due_date", createSoDiv).val();
		$("#depends_on_so", createSoDiv).val('');
		$("#depends_on_wo", createSoDiv).val('');
		$("#so_description", createSoDiv).val('');
		$("#order_comment", createSoDiv).val('');
		$("#contact_address_address", createSoDiv).val('');
		$("#contact_address_city", createSoDiv).val('');
		$("#contact_address_state", createSoDiv).val('');
		$("#contact_address_zip", createSoDiv).val('');
		$("#customer_information input[type=text]").val('');
		$("#customer_information select").val('');
	});
	$("#radioServiceOrder").click();

	/////////////////////////////////////////////////
	// This section handles submitting the record. //
	/////////////////////////////////////////////////
	function finishWorkItemCreation() {
		if(g_currentTab !== 'Create') {
			hideAll();
			showGrid();
		}
		else {
			$("#divWorkItemsTab").click();
		}
	}

	function createWorkItem(dispatch) {
		var order_type = $("#radioServiceOrder").is(":checked") ? 'S' : 'W';
		var orderCaseName = 'Service';
		if(order_type === 'W') {
			orderCaseName = 'Work';
		}
		var createSoDiv = $("#createSoDiv");

		if(order_type === 'S' && $("#customer_account_number", createSoDiv).val().length === 0) {
			jAlert("Account Number is a required field");
			return;
		}
		if(order_type === 'S' && $("#customer_name", createSoDiv).val().length === 0) {
			jAlert("Account Name is a required field");
			return;
		}
		if($("#so_number", createSoDiv).val().length === 0) {
			jAlert(orderCaseName+" Order Number is a required field");
			return;
		}
		if(order_type === 'S' && $("#so_type", createSoDiv).val().length === 0) {
			jAlert("Service Order Type is a required field");
			return;
		}
		if($("#service_index", createSoDiv).val() === null) {
			jAlert("Service Index is a required field");
			return;
		}

		var data = {};
		$("input[type=text]:visible,select:visible", createSoDiv).each(function() {
			var name = $(this).attr("id");
			var val = $(this).val();
			data[name] = val;
		});
		data.so_number = $("#so_number", createSoDiv).val();

		clsCurrentWorkItem.work_item_number = data.so_number;
		clsCurrentWorkItem.work_item_type = order_type;

		if(order_type === 'S') {
			data.so_type = $("#so_type", createSoDiv).val();
			data.so_code = $("#so_code", createSoDiv).val();
		}
		data.service_index = $("#service_index", createSoDiv).val();
		data.department = $("#department", createSoDiv).val();
		data.service_area = $("#service_area", createSoDiv).val();
		data.so_description = $("#so_description", createSoDiv).val();
		data.order_comment = $("#order_comment", createSoDiv).val();
		data.customer_billing_daytime_phone = $("#customer_billing_daytime_phone", createSoDiv).val();
		data.customer_email = $("#customer_email", createSoDiv).val();
		data.depends_on_so = $("#depends_on_so", createSoDiv).val();
		data.depends_on_wo = $("#depends_on_wo", createSoDiv).val();
		data.service_loc_account_number = '';
		data.service_loc_address = $("#service_loc_address", createSoDiv).val();
		data.service_loc_city = $("#service_loc_city", createSoDiv).val();
		data.service_loc_state = $("#service_loc_state", createSoDiv).val();
		data.service_loc_zip = $("#service_loc_zip", createSoDiv).val();
		data.service_loc_line_section = $("#service_loc_line_section", createSoDiv).val();
		data.service_loc_pole = $("#service_loc_pole", createSoDiv).val();
		data.service_loc_transformer = $("#service_loc_transformer", createSoDiv).val();
		data.service_loc_meter_number = $("#service_loc_meter_number", createSoDiv).val();
//		 data.service_loc_substation = $("#service_loc_substation", createSoDiv).val();
//		data.service_loc_substation = $("#service_loc_substation option:selected", createSoDiv).text();
		data.service_loc_district = $("#service_loc_district", createSoDiv).val();
		//data.service_area = $("#service_area", createSoDiv).val();
//		data.service_loc_substation = $("#substations", createSoDiv).val();
		data.contact_address_address = $("#contact_address_address", createSoDiv).val();
		data.contact_address_city = $("#contact_address_city", createSoDiv).val();
		data.contact_address_state = $("#contact_address_state", createSoDiv).val();
		data.contact_address_zip = $("#contact_address_zip", createSoDiv).val();
		data.geodata = $("#geodata", createSoDiv).val();
		data.so_event = $("#so_event", createSoDiv).val();
		data.geoshape = $("#wo_geofence", createSoDiv).val();
		data.project_recordid = $("#so_project", createSoDiv).val();
		data.target_app = $("#target_app:visible", createSoDiv).val();
		data.sub_num = $("#service_loc_substation", createSoDiv).val();
		data.sub_name = $("#service_loc_substation option:selected", createSoDiv).text();
		data.fdr_num = $("#service_loc_feeder", createSoDiv).val();
		data.fdr_name = $("#service_loc_feeder option:selected", createSoDiv).text();
		data.phases_used = $("#phases_used", createSoDiv).val();

		if(order_type === 'W') {
			data.wo_equipment_type = $("#wo_equipment_type", createSoDiv).val();
			data.wo_equipment_id = $("#wo_equipment_id", createSoDiv).val();
		}
		var recordIDs = [];
		$("div#createSoDiv img.image_selected").each(function() {
			recordIDs[recordIDs.length] = $(this).attr('recordid');
		});
		data.attachImages = recordIDs.toString();

		if(g_currentTab === 'Create') {		// 'Submit'
			data.action = 'createSO';
		}
		else {
			data.action = 'update';
			data.recordid = $("#so_recordid", createSoDiv).val();
		}
		data.order_type = order_type;

		if(g_hasScoutingFeature === true) {
			data.needs_scouting = $("#needs_scouting",createSoDiv).is(':checked');
		}

		var doDispatch = dispatch;

		$.ajax({
			type: "POST",
			url: "/dvwfm2/backoffice/ajaxSo.php",
			data: data,
			dataType: "json",
			success: function(data) {
				if(data.result === "false") {
					jAlert(data.error, "Error");
					return;
				}
				if(doDispatch === false) {
					jCompletion(data.msg, "Create Work Item", null, 1500);
					finishWorkItemCreation();
				}
				else {
					clsDispatchItems.init();
					clsDispatchItems.setUserID(g_userid);
					clsDispatchItems.addWorkItem(clsCurrentWorkItem.work_item_number,clsCurrentWorkItem.work_item_type);
					clsDispatchItems.setCallback(finishWorkItemCreation);
					clsDispatchItems.show();
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert("AJAX failed to update service order: "+textStatus);
			}
		});
	}

 	$("#btnCreateSoSubmit").unbind().bind('click touchstart',function() {
		setTimeout(function() {
			createWorkItem(false);
		},20);
	});

	$("#btnCreateAndDispatch").unbind().bind('click touchstart',function() {
		setTimeout(function() {
			createWorkItem(true);
		},20);
	});

	$("div#createSoDiv #btnFindCustomer").unbind().bind('click touchstart', function() {
		$("#findCustomerDiv").empty().load('/dvwfm2/forms/findCustomer.php',function() {
			$("#findCustomerDiv").dialog("option", "position", {my:'center top',at:'center top',of:window}).dialog("open");
			$("#findCustomerDiv").parent().fadeTo(300,1);
			var accnt = $("#customer_account_number").val();
			if(accnt.length > 0) {
				$("#findCustomerDiv #accnt").val(accnt);
				setTimeout('pressFindCustomerButton();',100);
			}
		});
	});

	$("div#createSoDiv #btnFindServiceOrder").unbind().bind('click touchstart', function() {
		var incid = $(this).attr('incid');
		$("#dialogGridDiv")
		.dialog("option", "position", {my:'center top',at:'center top+50px',of:window})
		.dialog('option','title','Service Orders')
		.dialog('open');

		dialogFlexParams.colModel = serviceOrderColModel;
		dialogFlexParams.buttons = findServiceOrderButtons;
		dialogFlexParams.searchitems = serviceOrderSearchitems;
		dialogFlexParams.url = 'ajaxGetSubgrid.php';
		dialogFlexParams.autoload = true;
		dialogFlexParams.sortname = 'so_number';
		dialogFlexParams.tablename = 'wfm.service_order';
		dialogFlexParams.onSuccess = function() {
			clsWaiting.hide();
		};
		dialogFlexParams.onFailure = function(reason) {
			clsWaiting.hide();
			jAlert(reason);
			return;
		};

		getDialogFieldNames(serviceOrderColModel);
		dialogParams[dialogParams.length] = {name: "where", value: "closed_when IS NULL"};
		dialogFlexParams.params = dialogParams;

		clsWaiting.show();
		$("#dialogGridDiv").empty().html('<table id="dialogGridTable"></table>');
		$("#dialogGridDiv").show();
		$("#dialogGridTable").flexigrid(dialogFlexParams);
	});

	$("div#createSoDiv #btnFindWorkOrder").unbind().bind('click touchstart', function() {
		$("#dialogGridDiv")
		.dialog("option", "position", {my:'center top',at:'center top+50px',of:window})
		.dialog('option','title','Work Orders')
		.dialog('open');

		dialogFlexParams.colModel = workOrderColModel;
		dialogFlexParams.buttons = findWorkOrderButtons;
		dialogFlexParams.searchitems = workOrderSearchitems;
		dialogFlexParams.url = 'ajaxGetSubgrid.php';
		dialogFlexParams.autoload = true;
		dialogFlexParams.sortname = 'wo_number';
		dialogFlexParams.tablename = 'wfm.work_order';
		dialogFlexParams.onSuccess = function() {
			clsWaiting.hide();
		};
		dialogFlexParams.onFailure = function(reason) {
			clsWaiting.hide();
			jAlert(reason);
			return;
		};

		getDialogFieldNames(workOrderColModel);
		dialogParams[dialogParams.length] = {name: "where", value: "closed_when IS NULL"};
		dialogFlexParams.params = dialogParams;

		clsWaiting.show();
		$("#dialogGridDiv").empty().html('<table id="dialogGridTable"></table>');
		$("#dialogGridDiv").show();
		$("#dialogGridTable").flexigrid(dialogFlexParams);
	});

	$.ajax({
		type: "POST",
		url: "/dvwfm2/backoffice/ajaxGetDbData.php",
		data: "table=wfm.so_order_code&fields=code,description&where=enabled=true&sort=description asc",
		dataType: "json",
		success: function(msg) {
			if(msg.result === 'true') {
				selectId = $("#createSoDiv #so_code");
				for(var r = 0;r < msg.data.length;r++) {
					selectId.append('<option value="'+msg.data[r][0]+'">'+msg.data[r][1]+' ('+msg.data[r][0]+')</option>');
				}
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX failed to get crew names: "+textStatus);
		}
	});

	//loadAvailableImages("#createSoDiv #imageList", "#createSoDiv");
	imageListManagement.showAttached = false;
	imageListManagement.uploadOffset = '180px';
	if(g_has_imageManagement === false) {
		imageListManagement.disabled = true;
	}
	imageListManagement.init('#imageList');

	if(g_createItemArgs.length > 0) {
		// The format of g_createItemArgs is: {
		// type:,			// LS or AcctNo
		// id:				// if <type> is 'LS' then LineSection else if <type> is 'AcctNo' then account number
		// show_me_layer:	// map showme layer
		// service_index:	// service index
		// polygon:			// polygon geodata
		var args = JSON.parse(g_createItemArgs);
		g_createItemArgs = '';
		if(typeof(args.type) !== 'undefined') {
			if(args.type === 'LS') {
				$("#radioWorkOrder").click();
				$("#createSoDiv #wo_equipment_type option[value="+args.show_me_layer+"]").prop('selected',true);
				$("#wo_equipment_id").val(args.id);

				$.ajax({
					type: 'POST',
					url: "/dvwfm2/backoffice/ajax/ajaxGetSubFdrPhase.php",
					data: {equipment_type: args.show_me_layer, equipment_id: args.id},
					dataType: 'json', // json, html, or text
					timeout: 30000,
					success: function (msg) {
						if(msg.result !== 'true') {
							jAlert("Can't get equipment Sub, Feeder, and Phase","Get Equopment Info", null, 1800);
							return;
						}
						var data = msg.data;
						$("#service_loc_substation option[value="+data.sub+"]").prop('selected',true);
						var sub_name = $("#service_loc_substation option[value="+data.sub+"]").text();
						updateSubFdrList(sub_name, data.fdr, "div#createSoDiv #service_loc_feeder");
						var phases_used = data.phase;
						$("#createSoDiv #phases_used option[value="+phases_used+"]").prop('selected',true);
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						alert("AJAX failed: Can't get equipment sub/fdr/phase - " + errorThrown);
					}
				});
			}
			else if(args.type === 'AcctNo') {
				$("#customer_account_number").val(args.id);
				$("#service_loc_account_number").val(args.id);
				var svc = args.service_index;
				$("#findCustomerDiv #service_index option[value="+svc+"]").prop('selected',false);
				$("div#createSoDiv #btnFindCustomer").click();
			}
			else if(args.type === 'work_order') {
				// we have a request to create a work order bassed on a polygon.
				var polygon = args.polygon;
				$("#wo_geofence").val(polygon.geomfromtext);
				$("#so_number").parent().append('<div style="padding:0 0 0 10px;display:inline-block;vertical-align: -4px;"><img src="/std_images/polygon.gif" title="This work order has a polygon associated with it." /></div>');
//				$("#radioWorkOrder").click();
//				setTimeout('$("#radioWorkOrder").prop("checked",true);$("#radioServiceOrder").prop("checked",false);',0);
				setTimeout('$("#radioWorkOrder").click(); $("#generateOrderNumber").click();',200);
			}
		}
		var service_index = '';
		if(args.service_index !== 0) {
			service_index = getServiceNameFromIndex(args.service_index);
			$("#createSoDiv #service_index option[value="+service_index+"]").prop('selected',true);
		}
		$("#generateOrderNumber").click();
	}
}

var findServiceOrderButtons = [
	{
		name: 'Select',
		bclass: 'onShowmeFind',
		onpress: function(cmd,g,grid) {
			var so_number = grid.getSelectedValueByDbName('so_number');
			$("#createSoDiv #depends_on_so").val(so_number);
			$("#dialogGridDiv").dialog('close');
		}
	}
];

var findWorkOrderButtons = [
	{
		name: 'Select',
		bclass: 'onShowmeFind',
		onpress: function(cmd,g,grid) {
			var wo_number = grid.getSelectedValueByDbName('wo_number');
			$("#createSoDiv #depends_on_wo").val(wo_number);
			$("#dialogGridDiv").dialog('close');
		}
	}
];

function onUpdateItem() {
	var work_type = clsCurrentWorkItem.work_item_type;

	var data = {action:'update', work_item_type:clsCurrentWorkItem.work_item_type, work_item_number:clsCurrentWorkItem.work_item_number};
	var popSoDetails = $("#popSoDetails");

	data.department = '';
	data.substation = $("#substation",popSoDetails).val();
	data.feeder = $("#feeder",popSoDetails).val();
	data.phases_used = $("#phases_used",popSoDetails).val();

	if(g_allow_SOWO_user_updates === true) {
		data.service_loc_address = $("#service_loc_address",popSoDetails).val();
		data.service_loc_city = $("#service_loc_city",popSoDetails).val();
		data.service_loc_state = $("#service_loc_state",popSoDetails).val();
		data.service_loc_state = $("#service_loc_state",popSoDetails).val();
		data.service_loc_zip = $("#service_loc_zip",popSoDetails).val();
		data.service_maploc = $("#service_maploc",popSoDetails).val();
		if(work_type === 'S') {
			data.service_loc_line_section = $("#service_loc_line_section",popSoDetails).val();
			data.service_loc_meter_number = $("#service_loc_meter_number",popSoDetails).val();
			data.service_loc_cycle = $("#service_loc_cycle",popSoDetails).val();
			data.service_loc_route = $("#service_loc_route",popSoDetails).val();
			data.locbook = $("#locbook",popSoDetails).val();
			data.service_loc_sequence = $("#service_loc_sequence",popSoDetails).val();
			data.service_loc_transformer_number1 = $("#service_loc_transformer_number1",popSoDetails).val();
			data.service_loc_xfmr_size = $("#service_loc_xfmr_size",popSoDetails).val();
			data.service_loc_pole = $("#service_loc_pole",popSoDetails).val();
		}
	}

	if(work_type === 'S') {	// $("#popSoDetails #department")
		data.due_date = $("#due_date",popSoDetails).val();
		data.service_index = $("#service_index option:selected",popSoDetails).val();
		data.service_loc_district = $("#service_loc_district option:selected",popSoDetails).val();
		data.service_area = $("#service_area",popSoDetails).val();
		if($("#department option:selected",popSoDetails).is(":selected") && $("#department option:selected",popSoDetails).val().length > 0) {
			data.department = $("#department option:selected",popSoDetails).text();
		}
		data.depends_on_so = $("#depends_on_so",popSoDetails).val();
		data.depends_on_wo = $("#depends_on_wo",popSoDetails).val();
		data.appointment_time = $("#appointment_time",popSoDetails).val();
		data.so_event = $("#so_event",popSoDetails).val();
		data.so_description = $("#so_description",popSoDetails).val();
		data.so_hold = $("#so_hold",popSoDetails).is(':checked');
		data.comment = $("#order_comment",popSoDetails).val();
	}
	else {
		data.due_date = $("#due_date",popSoDetails).val();
		data.service_index = $("#service_index option:selected",popSoDetails).val();
		data.service_loc_district = $("#service_loc_district option:selected",popSoDetails).val();
		data.service_area = $("#service_area",popSoDetails).val();
		if($("#department option:selected",popSoDetails).is(":selected") && $("#department option:selected",popSoDetails).val().length > 0) {
			data.department = $("#department option:selected",popSoDetails).text();
		}
		data.depends_on_wo = $("#depends_on_wo",popSoDetails).val();
		data.appointment_time = $("#appointment_time",popSoDetails).val();
		data.so_event = $("#so_event",popSoDetails).val();
		data.wo_description = $("#wo_description",popSoDetails).val();
		data.order_comment = $("#order_comment",popSoDetails).val();
		data.wo_order_type = $("#wo_order_type",popSoDetails).val();
		data.wo_order_subtype = $("#wo_order_subtype",popSoDetails).val();
		data.wo_equipment_type = $("#wo_equipment_type",popSoDetails).val();
		data.wo_equipment_id = $("#wo_equipment_id",popSoDetails).val();
		data.so_hold = $("#so_hold",popSoDetails).is(':checked');
	}
	data.target_app = $("#target_app:visible",popSoDetails).val();
	data.project_recordid = $("#so_project",popSoDetails).val();
	if(g_hasScoutingFeature === true) {
		data.needs_scouting = $("#needs_scouting",popSoDetails).is(':checked');
	}

	$.ajax({
		type: "POST",
		url: "/dvwfm2/backoffice/ajaxUpdateItem.php",
		data: data,
		dataType: "json",
		success: function(msg) {
			if(msg.result !== "true") {
				jAlert("Update Item: "+msg.error);
				return;
			}
			if($("#gridDivTable").length && g_update_so_needs_refresh && g_update_so_needs_refresh === true) {
				if($("#gridDivTable").length) {
					setTimeout('$("#gridDivTable").flexReload();',100);
				}
			}
			setTimeout('loadSoDetails()',0);
			jCompletion("Your item has been updated", "Update Work Item", null, 1500);
			setTimeout('$("#popSoDetails").dialog("close");',1300);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX failed to update item: "+textStatus);
		}
	});
}

$(document).ready(function() {
	$("#close_so").dialog({
		bgiframe: true,
		autoOpen: false,
		width: 1000,
		height:550,
		modal: true,
		zIndex: 3999,
		position: {my:'center top',at:'center top',of:window},
		buttons: {
			"Close Now": function() {
				var me = $("#close_so");
				var data = {
					work_item_number: $("#work_item_number", me).val()
					,work_item_type: $("#work_item_type", me).val()
					,close_so_by: $("#close_so_by", me).val()
					,createdate: $("#create_wo_date", me).val()
					,close_so_date: $("#close_so_date", me).val()
					,close_so_comments: $("#close_so_comments", me).val()
					,close_so_meter_number: $("#close_so_meter_number", me).val()
					,close_so_return_reading: $("#close_so_return_reading", me).val()
					,close_so_return_read_date: $("#close_so_return_read_date", me).val()
					,close_so_meter_multiplier: $("#close_so_meter_multiplier", me).val()
					,close_so_mtr_kw_read: $("#close_so_mtr_kw_read", me).val()
					,close_so_mtr_kvar_read: $("#close_so_mtr_kvar_read", me).val()
					,close_so_reconnect: getYn($("#close_so_reconnect", me).val())
					,close_so_removed: getYn($("#close_so_removed", me).val())
					,close_so_loadMgtDevDisc: getYn($("#close_so_loadMgtDevDisc", me).val())
					,close_so_loadMgtDevRemd: getYn($("#close_so_loadMgtDevRemd", me).val())
					,close_so_disconnected: getYn($("#close_so_disconnected", me).val())
					,close_so_mtr_status: ''
					,close_so_xfmr_status: ''
					,close_so_pending: $("#close_so_pending").val() === 'Y'
				};

				data.mtr_installed = $("#mtr_installed", me).is(':checked');
				data.mtr_removed = $("#mtr_removed", me).is(':checked');
				data.mtr_disconnected = $("#mtr_disconnected", me).is(':checked');

				if($("#close_so_by", me).val().length === 0) {
					jAlert("Close By is a required field");
					return;
				}
				if($("#close_so_date", me).val().length === 0) {
					jAlert("Close Date/Time is a required field");
					return;
				}
				$.ajax({
					type: "POST",
					url: "/dvwfm2/backoffice/ajaxCloseItem.php",
					data: data,
					dataType: "json",
					success: function(msg) {
						if(msg.result === "false") {
							jAlert(msg.error, "Error");
							return;
						}
						jCompletion("Item Closed", "Delete Work Items");
						$("#gridDivTable").flexReload();
						$("#close_so").dialog("close");
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						alert("AJAX failed to close item: "+textStatus);
					}
				});
			},
			Cancel: function() {
				clsWaiting.hide();
				$(this).dialog('close');
			}
		},
		close: function() {
			clsWaiting.hide();
		}
	});

	$("#so_form_left").unbind().bind('click touchstart', function() {
		hideAll();
		showGrid();
	});

	// This is the readonly version of create that show up when the user click the
	// work item's name in the grid details/
	$("#popSoDetails").dialog({
		bgiframe: true,
		autoOpen: false,
		width: 1100,
		height: 800,
		modal: true,
		zIndex: 3999,
		title: "Item Details",
//		position: ['center',0],
		position: {my:'center top',at:'center top',of:window},
		open: function(event, ui) {
			addDialogPrint("popSoDetails", "#popSoDetails");

			var buttons = [
				{ text:'Media', click: function() {
					imageListManagement.showAttached = true;
					imageListManagement.init('body');
					imageListManagement.show(g_updateWorkItemType,g_work_item_number);
					}
				},
				{ text:'Update', click:onUpdateItem }
			];

			if(g_hasScoutingFeature === true) {
				var work_type = 'S';
				if($("#FilterGridFormatTop").val() === 'WorkOrders') {
					work_type = 'W';
				}
				buttons[buttons.length] = { text:'Mark Scouted', click: function() {
					clsScouting.init();
					clsScouting.work_item_number = $("#popSoDetails #so_number").val();
					clsScouting.work_type = work_type;
					clsScouting.showMarkScoutedDialog();
					}
				};
			}
			buttons.sort(SortButtonsByName);
			buttons[buttons.length] = { text:'Cancel', click:function() { $("#popSoDetails").dialog('close'); }};
			$("#popSoDetails").dialog("option", "buttons", buttons);

			if(g_has_imageManagement === false) {
				var dlg = $(this).parent();
				$("div.ui-dialog-buttonpane button", dlg).each(function() {
					if($(">span",this).html() === 'Media') {
						$(this).css({ opacity: 0.4, cursor:'default' }).unbind('click');
					}
				});
			}

			$("#popSoDetails #wo_description").change();
			$("#popSoDetails #order_comment").change();
		},
		dragStop: function(event, ui) {
		},
		close: function(event, ui) {
			$("#popSoDetails").empty().html("");	// Take the HTML out of the DOM when the dialog is not being shown.
		}
	});
});
