/*  REMOTE SDK COMMON_MAP_FUNCS.js */
/***************************************************
TODOs 
------------------------------------------------------
map.getVisibleRegion() returns the results without callback now:
(But you can still use the old style)

// Before
map.getVisibleRegion(function(latLngBounds) {

});

// After
var latLngBounds = map.getVisibleRegion();
HtmlInfoWindow is able to use without the infoWindow property.
And setContent() method is also available.

var htmlInfoWindow = new plugin.google.maps.HtmlInfoWindow();

var html = "&lt;img src='./House-icon.png' width='64' height='64' &gt;" +
           "&lt;br&gt;" +
           "This is an example";
htmlInfoWindow.setContent(html);

map.addMarker({
  position: {lat: 0, lng: 0}
}, function(marker) {

  marker.on(plugin.google.maps.event.MARKER_CLICK, function() {
    htmlInfoWindow.open(marker);
  });
  marker.trigger(plugin.google.maps.event.MARKER_CLICK);

});
addTileOverlay() is now become a very easy.
(In your code, you generates lots of ground overlay, but tile overlay is simple)

map.addTileOverlay({

  getTile: function(x, y, zoom) {
    return "../images/map-for-free/" + zoom + "_" + x + "-" + y + ".gif"
  }
}, function(tileOverlay) {

});
I keep the backward compatibility, so your code should work without problem.
But you can change your code with new code style.
*/

/*  
this.map.addMarker(markerOptions).then((marker) => {
                marker.addEventListener(GoogleMapsEvent.INFO_CLICK).subscribe(e => {
                    var marker = e[1];
                    var place = marker.get("place");
                    console.log(JSON.stringify(e));
                });
            });*/
/*-------------------------------------------*/
/*     GLOBALS                               */
/*-------------------------------------------*/
//google.maps.visualRefresh = true;
var weather = null;
var mapsrv = null;
var nexrad = null;
var map = null;
var googleMap = null;
var previousMapType;
//var directions;
//var directionsService = plugin.google.maps.DirectionsService();
var placeSearch, autocomplete;
var backoffice;
var projects={};
var polygons=[];
var markers = [];
var vehicles = [];
var crews = {};
var crewsSorter=[];
var crewIndex = null;
var g_currindex;
var subs = [];
var predictions = [];
var alerts = [];
var media = [];
var searchresults = [];
var allitems = [];
var search_value=null;
var currLat = null; //set by GPS.js
var currLon = null; //set by GPS.js
var getTouchLocationEvent;
var findType = 'equipment';
var addType = 'alert';
var bounds;
var NorthEast;
var SouthWest;
var proj;
var hasSpatialDB = false;
var southWestMercBound;
var northEastMercBound;
var firstTime = true;
var androidFooterPad=0;
var map_refresh_timer = null;
var reassignId = null;
var gridOverlay = '';
var weatherOverlay = '';
var imageUriToUpload = null;
var mapzoom = null;
var mapcenter = null;
var stopFakeSearch=false;
var g_db = null;
var populateTables;
var measure_line = null;
var measureTimer = null;
var g_db = null;
var locFail = function(error) {
    console.log('code: '    + error.code    + '\n' +'message: ' + error.message + '\n');
    jAlert("Unable to get current location.");
};

//$('#map_contents').bind("touchmove",function(event){event.preventDefault();}); //makes the page not scroll on the map  THIS IS A HACK

/*-----------------------------------------------------------------------------------------------------------------------*/
/*$("#map_page").on('pagecontainerload', function(evt, data) {
	console.log("loaded "+data.url);
    $("#waiting").show();
    initializeMap();
}
);*/
$(document).on("pageshow","#map_page",function(){ // When entering pagetwo
  console.log("loaded map page");
    $("#waiting").show();
    $('#map_contents').bind("touchmove",function(event){event.preventDefault();});
    var links = document.getElementsByClassName('bottom_menu');
    for (var i = 0 ; i < links.length ; i++) {
    	links[i].style.webkitTouchCallout = 'none';
    }
    initializeMap();
});

/*----------------------------------------------------------------------------*/
var isTablet=true;
function initializeMap() {
	if (googleMap != null) {
		googleMap.remove();
	}
    var width = null;
    var height = null;
    console.log("$(window)  =====   "+window.orientation+": "+$(window).width()+"X"+$(window).height());
    console.log("avail  ======= "+window.orientation+": "+screen.width+"X"+screen.height);
    windowheight=screen.height;
    windowwidth=screen.width;
    isTablet = (windowheight > 1000 || windowwidth > 1000);
    if ((platform == 'I' && (window.orientation == 90 || window.orientation == -90)) ||
        (platform == 'A' && isTablet && (window.orientation == 0 || window.orientation == 180)) ||
        (platform == 'A' && !isTablet && (window.orientation == 90 || window.orientation == -90))){ //LANDSCAPE
        width = Math.max(windowheight,windowwidth);
        height = Math.min(windowheight,windowwidth);
    }else {
        width = Math.min(windowheight,windowwidth);
        height = Math.max(windowheight,windowwidth);
    }
    if (platform == 'A'){
        height = height - 25;
        width = width - 5;
    }
    windowheight=height;
    windowwidth=width;
    $('#connectionmessage').css({'top':((parseInt(windowheight)/2)-50)+'px','left':(parseInt((windowwidth)/2)-175)+'px',width:'250px'});
    $("close_item_container").css({'z-index':'9999'});
		//$(".bottom_menu").css({'transform': 'translateZ(0)', '-webkit-transform': 'translateZ(0)', 'will-change': 'transorm'});
    $(".ui-page").css({'min-height':height,'height':height,'width':width,'min-width':width});
    $('#mapheader').css({'width': width,'min-width': width, position: 'absolute', left: '0px', top:ios7Pad});
    if (isTablet==false) {
        $("#mapfooter button").css({'margin-left':'5px'});
        $("#mapheader button").css({'margin-right':'5px'});
        $(".right_icon1").css({left:(width-(5+$(".right_icon3").width())*3)+'px',position:'absolute', margin:'0px', top:$(".top_icon").css('top')});
        $(".right_icon2").css({left:(width-(5+$(".right_icon3").width())*2)+'px',position:'absolute', margin:'0px', top:$(".top_icon").css('top')});
        $(".right_icon3").css({left:(width-5-$(".right_icon3").width())+'px',position:'absolute', margin:'0px', top:$(".top_icon").css('top')});
        $("#close_container, #new_alert,#new_equipment, #chooseAddType, #addMenu, #send_message, #send_backoffice_message, #close_item, #createSoDiv, #update_container").css({margin:'0px',padding:'0px'});
        $("#map_page>div button").parent().css({'max-width':'95%'});
        $("#map_page>div select").parent().css({'max-width':'95%'});
        $("#map_page>div textarea").css({'max-width':'95%'});
        console.log('small window width');
    }else{
        $(".right_icon1").css({left:(width-185)+'px',position:'absolute', margin:'0px', top:$(".top_icon").css('top')});
        $(".right_icon2").css({left:(width-110)+'px',position:'absolute', margin:'0px', top:$(".top_icon").css('top')});
        $(".right_icon3").css({left:(width-45)+'px',position:'absolute', margin:'0px', top:$(".top_icon").css('top')});
    }
    $(document).find("div[data-role='popup']").on({
        popupbeforeposition: function() {
            googleMap.setClickable("false");
        },
        popupafterclose: function() {
            googleMap.setClickable("true");
        }
    });
    $("#close_item_container").on('show', function() {
        $("#closeBtn").trigger("click");
    });
    $("#chooseAddType").on('show', function() {
        googleMap.setClickable("false");
    });
    $("#chooseAddType").on('hide', function() {
        googleMap.setClickable("true");
    });
    
    $('#map_canvas').css({height: (height - $("#mapheader").height() - ios7Pad - $("#mapfooter").height()) - androidFooterPad, width: width, position: 'absolute', top: ($("#mapheader").height() + ios7Pad) + 'px', left: '0px', padding:'0px', margin: '0px'});
    $('#mapfooter').css({'width': width,'min-width': width, position: 'absolute', left: '0px',top: $("#map_canvas").height()+ios7Pad+$("#mapheader").height()});

    //directions = plugin.google.maps.DirectionsRenderer();*/
    var center = {lat:parseFloat(params.googleLat), lng:parseFloat(params.googleLon)};
    console.log("creating google map with zoom:"+parseInt(params.googleZoom)+" and center of "+parseFloat(params.googleLat)+" / "+parseFloat(params.googleLon)+" Google=" +JSON.stringify(center));
    var options = {
	  'mapType': plugin.google.maps.MapTypeId.NORMAL,
	  'controls': {
		'compass': true,
		'myLocationButton': true,
        'myLocation': true,
		'indoorPicker': true,
		'zoom': true
	  },
	  'gestures': {
		'scroll': true,
		'tilt': true,
		'rotate': true,
		'zoom': true
	  }
	};
	googleMap = plugin.google.maps.Map.getMap(document.getElementById("map_canvas"), options);
	googleMap.on(plugin.google.maps.event.MAP_READY, function() {
		console.log("panning google map with zoom:"+parseInt(params.googleZoom)+" and center of "+parseFloat(params.googleLat)+" / "+parseFloat(params.googleLon)+" Google=" +JSON.stringify(center));
		
		googleMap.animateCamera( {
			'target': center,
			'tilt':0,
			'zoom': parseInt(params.googleZoom),
			'bearing':0,
			'duration':2000
		},
		function() {
			if (typeof(g_openingArgs) != 'undefined' && g_openingArgs != null ) {
				loadedMapMakeCallFromOtherApp();
			}
		});
		googleMap.on(plugin.google.maps.event.CAMERA_MOVE_END, function(event) {
			console.log('fired CAMERA_MOVE_END');
			for (var key in googleMap.OVERLAYS) {
				if (key.indexOf('groundoverlay') != -1) {
					if (typeof(googleMap.OVERLAYS[key].remove) == 'function') {
						googleMap.OVERLAYS[key].remove();
						console.log("removed a ground overlay");
					}else{
						console.log("ground overlay has no remove function");
					}
				}
			}
			mapMoved();
		});
		refreshGoogleMap();
		if (typeof(g_openingArgs) != 'undefined' && g_openingArgs != null ) {
			loadedMapMakeCallFromOtherApp();
		}
	});
	googleMap.on(plugin.google.maps.event.MAP_CLICK, function(event) {
		console.log('fired MAP_CLICK');
		if (navigator.connection.type == Connection.NONE) {
			stopFakeSearch=false;
			findType = '';
			return;
		}
	  if ( stopFakeSearch ){
		search_value={coords:{latitude:event.lat,longitude:event.lng}};												  
		if (findType === 'customer') {
			findCustomer();
		} else if (findType == 'equipment'){
			findEquipment();
		}
		stopFakeSearch=false;
		findType = '';
		googleMap.animateCamera( {
			'target': {lat:parseFloat(event.lat), lng:parseFloat(event.lng)},
			'tilt':0,
			'zoom': 16,
			'bearing':0,
			'duration':1000
		},
		function() {});
	  }else{
	  	fakeSearch(event.lat,event.lng,googleMap.getCameraZoom());
	  }
  	});
	googleMap.on(plugin.google.maps.event.MAP_LONG_CLICK, function(event) {
		console.log('fired MAP_LONG_CLICK');
		stopFakeSearch=true;
		crewIndex=null;
		search_value={coords:{latitude:event.lat,longitude:event.lng}};
		if (appname == 'FieldDesign') {
			addEquipment();
		}else {
			showDivCentered("chooseAddType");
		}
	});
	//loadBackoffice();
}
/*----------------------------------------------------------------------------*/
function refreshGoogleMap() {
	console.log("refreshGoogleMap()");
	clearTimeout(map_refresh_timer);
	map_refresh_timer = setTimeout(refreshGoogleMap, params.LinemanAppMapRefreshRate * 1000);
	if (firstTime === true) {
		firstTime = false;
	}
	for (var i = 0 ; i < tracedLines.length ; i++) {
		if ( tracedLines[i] != null && $.isFunction(tracedLines[i].remove)) {
			tracedLines[i].remove();
		}
	}
	tracedLines = [];
	if (g_db != null) {
		g_db.abortAllPendingTransactions();
	}
	$("#closeBtn").trigger("click");
	/*for (var key in googleMap.OVERLAYS) {
		if (typeof(googleMap.OVERLAYS[key].hideInfoWindow) == "function") {
			googleMap.OVERLAYS[key].hideInfoWindow();
		}else{
			console.log("hideInfoWindow() is not a function");
		}
	}*/  
	disconn_markers = [];
	if (navigator.connection.type == Connection.NONE) {
		//$("#mapheader h1").html(appname+' Map&nbsp;<span style="color:red;">(OFFLINE)</span>');	
		googleMap.clear();
		setTimeout(function() {	
			if (hasSpatialDB){
				loadDisconnectedMap();
			}
			loadMarkers(closeTicketReloadMarkers);
		},500);
		$("#waiting").hide();
		return;
	}
	googleMap.clear();
	setTimeout(function(){
		loadMarkers(Object.keys(masterMarkerList));
		if ($.isFunction(populateTables)) {
			populateTables();
		}
		if (localStorage.getItem('ckbx_PrimaryLines') !== 'false') {
			addPrimLineLayer();
		}
		if (localStorage.getItem('ckbx_Weather') !== 'false') {
			addWeatherLayer();
		}
		$("#waiting").hide();
	}, 500);
}
var mapMovedToPopInfoWindow = false;
function mapMoved() {   // This is called on plugin.google.maps.event.CAMERA_MOVE_END
	if (mapMovedToPopInfoWindow) {
		mapMovedToPopInfoWindow = false;
		console.log("***Map move due to info window click!");
		return;
	}
	for (var key in googleMap.OVERLAYS) {
		if (key.includes("groundoverlay")  && typeof(googleMap.OVERLAYS[key].remove) == "function") {
			googleMap.OVERLAYS[key].remove();
		}
	}
	if (hasSpatialDB) {
		disconn_markers = [];
		for (var key in googleMap.OVERLAYS) {
			if ( googleMap.OVERLAYS[key].get('myData') != undefined && googleMap.OVERLAYS[key].get('myData').markerType != undefined && googleMap.OVERLAYS[key].get('myData').markerType == 'disconn') {
				googleMap.OVERLAYS[key].remove();
			}
		}
		//googleMap.trigger("removeDisconnectedMarkers");
		disconn_markers = [];
	}
	if (navigator.connection.type == Connection.NONE && hasSpatialDB) {
		//$("#mapheader h1").html(appname+' Map&nbsp;<span style="color:red;">(OFFLINE)</span>');
		//clearTimeout(map_refresh_timer);
		//googleMap.clear();
		disconnectedMapMoved();
	}else{
		if (localStorage.getItem('ckbx_PrimaryLines') !== 'false') {
			addPrimLineLayer();
		}
		if (localStorage.getItem('ckbx_Weather') !== 'false') {
			addWeatherLayer();
		}
	}
}
function panToDefaultView() {
	try{
		gridOverlay.remove();
		weatherOverlay.remove();
	}catch(ex){
	}
	googleMap.setCameraZoom(parseInt(params.googleZoom));
	googleMap.setCameraTarget({lat:parseFloat(params.googleLat),lng:parseFloat(params.googleLon)});
}
function loadMarkers(marker_list){
	if (localStorage.masterMarkerList == undefined) {
		localStorage.masterMarkerList = JSON.stringify(masterMarkerList);
	}
	masterMarkerList = JSON.parse(localStorage.masterMarkerList);
	if (navigator.connection.type == Connection.NONE) {
		for (var i = 0 ; i < marker_list.length ; i++){
	    	window["load_"+marker_list[i]]();
	    }
	    return;
	}
	$.ajax({
		url:localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php", 
		data:{target: JSON.stringify(marker_list), action: 'tables', serviceIndex: service_index, appname: appname, userid: user_number, crew_list: crew_list},
		type: "GET",
		dataType: "jsonp",
		timeout:25000,
		success: function(data) {
			setOMSConnection(true);
			if (data.result != 'true') {
				alert("Error: " +data.error);
				return;
			}
			for( var i = 0 ; i < data.tables.length ; i++) {
				masterMarkerList[data.tables[i].target]=data.tables[i];
				window["load_"+data.tables[i].target]();
			}
			localStorage.masterMarkerList = JSON.stringify(masterMarkerList);
			if ($(".bottom_menu:visible").length > 0) {
			 clearExistingBottomMenus($(".bottom_menu:visible")[0].getAttribute('id'));
			}
		},
		error: function( jqxhr, textStatus, error ) {
			console.log( "Load Map Data Request Failed: " + JSON.stringify(jqxhr)  );
			if (textStatus == 'timeout'|| XMLHttpRequest.status == 0) { 
				setOMSConnection(false, {http:jqxhr, status:textStatus, error:error,call_fxn:'common.loadMarkers'});
				$("#connectionmessage").html( "Due to slow connection we will load the map with most recent map data.").show();
				setTimeout(function() { $("#connectionmessage").hide(); } , 4000);
			}else {
				jAlert("ERROR= " + JSON.stringify(jqxhr));
			}
			masterMarkerList = JSON.parse(localStorage.masterMarkerList);
			for (var target in masterMarkerList) {	    	
				window["load_"+target]();
			}
		}
  });
}
/*----------------------------------------------------------------------------*/
function load_vehicle_markers() {
	if (can_view_vehicles == 'FALSE') {
		return;
	}
  vehicles = [];
  var data=masterMarkerList['vehicle_markers'].rows;
  var baseArray = new plugin.google.maps.BaseArrayClass(data);
    var infoWindow = new plugin.google.maps.HtmlInfoWindow();
  var idx = 0;
	baseArray.map(function(myData, cb) {
		var color = 'green';
		var zindex = 13;
		myData.index = idx;
		idx++;
		if (myData.speed < 1) {
			color = 'red';
			zindex = 12;
		}
		if (myData.timedout === 't') {
			color = 'grey';
			zindex = 11;
		}
		var imageForMarker = 'images/' + color + 'bucket' + myData.direction + '.png';
		if (localStorage.getItem('truck') === myData.vehicle_number) {
			imageForMarker = 'images/' + color + 'bucket' + myData.direction + '_me.png';
		}
	  var data = {
	    icon: cordova.file.dataDirectory + 'www/' + imageForMarker,
	    position: {
	      lat: myData.lat,
	      lng: myData.lon
	    },
	    draggable: true,
	    disableAutoPan: false,
	    zIndex: 10,
	    visible: localStorage.getItem('ckbx_Vehicles') === 'true',
	    myData: myData
	  };
	  //vehicles[i].popup = plugin.google.maps.InfoWindow({content: content, maxWidth: 400});
	  googleMap.addMarker(data, cb);

	}, function(markers) {

	  vehicles = markers;
	  markers.forEach(function(marker) {
	    marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);
	  });
	});
	function onMarkerClick() {
		$("#closeBtn").trigger("click");
		var marker = this;
		var myData = marker.get("myData");
        var content = '<div style="max-width:300px;">'+
        '<div style="float:right;" id="closeBtn" ><img src="images/close.png"/></div><div>' +
        'Name: ' + myData.name + '<br>' +
        'Direction: ' + myData.direction + '<br>' +
        'Speed: ' + myData.speed + ' mph<br>' +
        'Last Contact: ' + myData.avltimestamp + '<br>' +
        '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-mail" data-theme="a" onclick="javascript:$(\'#vehicle_index\').val(' + myData.index + ');$(\'#send_message\').show();">Send Message</button></div></div>';
        infoWindow.setContent(content, {width:'300px'});
        infoWindow.open(marker);
        setTimeout( function() {
           $("#closeBtn").on("click", function() {
           infoWindow.close();
	  	});
	  },500);
	}
}
/*----------------------------------------------------------------------------*/
function load_sub_markers() {
  subs = [];
  var data = masterMarkerList['sub_markers'].rows;
  var baseArray = new plugin.google.maps.BaseArrayClass(data);
  var infoWindow = new plugin.google.maps.HtmlInfoWindow();
  baseArray.map(function(myData, cb) {

    var data = {
      icon: cordova.file.dataDirectory + 'www/images/' + myData.image,
      position: {
        lat: myData.lat,
        lng: myData.lon
      },
      draggable: true,
      disableAutoPan: false,
      zIndex: 10,
      visible: localStorage.getItem('ckbx_Substations') === 'true',
      myData: myData
    };
    //vehicles[i].popup = plugin.google.maps.InfoWindow({content: content, maxWidth: 400});
    googleMap.addMarker(data, cb);

  }, function(markers) {

    subs = markers;
    markers.forEach(function(marker) {
      marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);
    });
  });

  function onMarkerClick() {
  	$("#closeBtn").trigger("click");
    var marker = this;
    var myData = marker.get("myData");
    var content = 
      '<div  style="max-width:250px;">'+
      '<div style="float:right;" id="closeBtn"><img src="images/close.png"/></div>' +
      'Sub: ' + myData.name + '<br>' +
      'Total Meters: ' + myData.total + '<br>' +
      'Meters Out: ' + myData.out + '<br>' +
      'Percent On: ' + myData.pct.toFixed(2) + '%</div>';
      infoWindow.setContent(content, {width:'250px'});
    infoWindow.open(marker);
    setTimeout( function() {
    	$("#closeBtn").on("click", function() {
		    infoWindow.close();
    	});
    },500);
	}
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
var mediaIdx = 0;
var swipedMedia;
function load_media_markers() {

  media = [];
  mediaSorter = {};
  var data = [];
  for (var i = 0 ; i < masterMarkerList['media_markers'].rows.length ; i ++ ) {
  	if (mediaSorter[masterMarkerList['media_markers'].rows[i].geodata] == undefined) {
  		mediaSorter[masterMarkerList['media_markers'].rows[i].geodata] = data.length;
  		data.push({
  			lat: masterMarkerList['media_markers'].rows[i].lat,
  			lon: masterMarkerList['media_markers'].rows[i].lon, 
  			image_path: [masterMarkerList['media_markers'].rows[i].image_path], 
  			work_item_number: [masterMarkerList['media_markers'].rows[i].work_item_number], 
  			uploaded_by: [masterMarkerList['media_markers'].rows[i].uploaded_by], 
  			uploaded_on: [masterMarkerList['media_markers'].rows[i].uploaded_on], 
  			comments: [masterMarkerList['media_markers'].rows[i].comments]
  		});
  	}else{
  		data[mediaSorter[masterMarkerList['media_markers'].rows[i].geodata]].image_path.push(masterMarkerList['media_markers'].rows[i].image_path);
  		data[mediaSorter[masterMarkerList['media_markers'].rows[i].geodata]].work_item_number.push(masterMarkerList['media_markers'].rows[i].work_item_number);
  		data[mediaSorter[masterMarkerList['media_markers'].rows[i].geodata]].uploaded_by.push(masterMarkerList['media_markers'].rows[i].uploaded_by);
  		data[mediaSorter[masterMarkerList['media_markers'].rows[i].geodata]].uploaded_on.push(masterMarkerList['media_markers'].rows[i].uploaded_on);
  		data[mediaSorter[masterMarkerList['media_markers'].rows[i].geodata]].comments.push(masterMarkerList['media_markers'].rows[i].comments);
  	}
  }
  var infoWindow = new plugin.google.maps.HtmlInfoWindow();
  var baseArray = new plugin.google.maps.BaseArrayClass(data);
  var idx = 0;
  baseArray.map(function(myData, cb) {
		myData.index = idx;
		idx++;
    var data = {
      icon: {url: cordova.file.dataDirectory+'www/images/cameraicon.png'},
      position: {
        lat: myData.lat,
        lng: myData.lon
      },
      draggable: true,
      disableAutoPan: true,
      zIndex: 30,
      visible: localStorage.getItem('ckbx_Media') === 'true',
      myData: myData
    };
    googleMap.addMarker(data, cb);

  }, function(markers) {

    media = markers;
    markers.forEach(function(marker) {
        marker.setIconAnchor(35,35);
        //marker.setInfoWindowAnchor(-15,0);
        marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);
    });
  });
  swipedMedia = function(markerIdx) {
	var thisData = media[markerIdx].get('myData');
      var content = '<div style="width:400px;height:400px;"> <div style="float:right;" id="closeBtn" align="right"><img src="images/close.png"/></div>';
      content += '<div style="height:300px;"><img style="max-width:400px;max-height:300px;" src="'+localStorage.getItem('ims_url')+''+thisData.image_path[mediaIdx]+'"/></div><br>';
    content += 'Uploaded On: ' + thisData.uploaded_on[mediaIdx] + '<br>' +
     'Uploaded By: ' + thisData.uploaded_by[mediaIdx] + '<br>';
    if (thisData.comments[mediaIdx] != null) {
     	content+='Comment: ' + thisData.comments[mediaIdx] + '<br>';
    }
    content += '<div style="width:200px;float:left;">';
    if (mediaIdx > 0) {
        content+='<button style="width:190px;" class="ui-btn ui-btn-a ui-btn-icon-left ui-icon-arrow-l" onclick="javascript: mediaIdx= mediaIdx -1; swipedMedia('+thisData.index+');">View Previous</a>';
    }
    content += '</div><div style="width:200px;float:right;">';
    if (mediaIdx < thisData.uploaded_on.length-1){
        content+='<button style="width:190px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-r" onclick="javascript: mediaIdx= mediaIdx + 1; swipedMedia('+thisData.index+');">View Next</a>';
    }
    content += '</div></div>';
    infoWindow.setContent(content,{width:'400px'});
    setTimeout( function() {
    	$("#closeBtn").on("click", function() {
		    infoWindow.close();
    	});
    },500);
	};
	function onMarkerClick() {
		$("#closeBtn").trigger("click");
		mediaIdx = 0;
		var marker = this;
		var myData = marker.get("myData");
        var content = '<div style="width:400px;height:400px;"> <div style="float:right;" id="closeBtn" align="right"><img src="images/close.png"/></div>';
        content += '<div style="height:300px;"><img style="max-width:400px;max-height:300px;" src="'+localStorage.getItem('ims_url')+''+myData.image_path[mediaIdx]+'"/></div><br>';
        content += 'Uploaded On: ' + myData.uploaded_on[mediaIdx] + '<br>' +
        'Uploaded By: ' + myData.uploaded_by[mediaIdx] + '<br>';
        if (myData.comments[mediaIdx] != null) {
            content+='Comment: ' + myData.comments[mediaIdx] + '<br>';
        }
        content += '<div style="width:200px;float:left;">';
		if (mediaIdx > 0) {
            content+='<button style="width:190px;" class="ui-btn ui-btn-a ui-btn-icon-left ui-icon-arrow-l" onclick="javascript: mediaIdx= mediaIdx -1; swipedMedia('+myData.index+');">View Previous</button>';
		}
        content += '</div><div style="width:200px;float:right;">';
	   if (mediaIdx < myData.uploaded_on.length-1){
           content+='<button style="width:190px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-r" onclick="javascript: mediaIdx= mediaIdx + 1; swipedMedia('+myData.index+');">View Next</button>';
		}
		content += '</div></div>';
        infoWindow.setContent(content,{width:'400px'});
		infoWindow.open(marker);
        var pixels = googleMap.fromLatLngToPoint(marker.getPosition(),
            function(pixels){
                pixels[1] = pixels[1] - 200;
                var newcenter = googleMap.fromPointToLatLng(pixels, function(newcenter){
                    googleMap.animateCamera( {
                        'target': newcenter,
                        'tilt':googleMap.getCameraTilt(),
                        'zoom': googleMap.getCameraZoom(),
                        'bearing':googleMap.getCameraBearing(),
                        'duration':1000
                    });
                });
            }
        );
		setTimeout( function() {
			$("#closeBtn").on("click", function() {
				infoWindow.close();
			});
		},500);
	}
}

/*----------------------------------------------------------------------------*/
function load_alert_markers() {
	/*for (var i = 0; i < alerts.length; i++) {
		alerts[i].hideInfoWindow();
		alerts[i].remove();
	}*/
	alerts = [];
	var data=masterMarkerList['alert_markers'].rows;
	var infoWindow = new plugin.google.maps.HtmlInfoWindow();
  var baseArray = new plugin.google.maps.BaseArrayClass(data);
  baseArray.map(function(myData, cb) {
		var icon=cordova.file.dataDirectory+'www/images/warning.gif';
		if (myData.icon_image != undefined && navigator.connection.type != Connection.NONE) {
		    icon = localStorage.getItem('ims_url') + '/dvmapviewer2/images/' + g_alerticons[myData.icon_image].alertpoint_img;
		}
    var data = {
      icon: {url: icon},
      position: {
        lat: myData.lat,
        lng: myData.lon
      },
      draggable: true,
      disableAutoPan: false,
      zIndex: 5,
      visible: localStorage.getItem('ckbx_AlertPoints') === 'true',
      myData: myData
    };
    googleMap.addMarker(data, cb);

  }, function(markers) {

    alerts = markers;
    var i = 0;
    markers.forEach(function(marker) {
      marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);
    });
  });
  function onMarkerClick() {
  	$("#closeBtn").trigger("click");
    var marker = this;
    var myData = marker.get("myData");
    var content = '<div style="width:300px;">'+
        '<div style="float:right;" id="closeBtn" align="right"><img src="images/close.png"/></div>'+
        'Entered By: ' + myData.enteredby + '<br>' +
        'Entered On: ' + myData.entereddatetime + '<br>' +
        'Comment: ' + myData.comments + '<br>' +
        '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-delete" onclick="javascript: removeAlertPoint(\'' + myData.gos_id + '\');">Remove</button>' +
        '</div>';
    infoWindow.setContent(content,{width:'300px'});
    infoWindow.open(marker);
    setTimeout( function() {
    	$("#closeBtn").on("click", function() {
		    infoWindow.close();
    	});
    },500);
	}
}
/*-----------------------------------------------------------------------------------------------------------------------*/
function removeAlertPoint(gos_id) {
	$("#index").val(index);
	$("#waiting").show();
  var ajaxParams={action: 'get',
      target: 'delete_alert',
      gos_id: gos_id,
      appname: appname
  };
  if (navigator.connection.type == Connection.NONE) {
      storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php", ajaxParams);
      return;
  }
	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php",
		data: ajaxParams,
		dataType: "json",
		success: function(msg) {
			if (msg.result !== "false") {
				for (var i = 0 ; i < alerts.length ; i++) {
					if (alerts[i].get("myData").gos_id == gos_id) {
						alerts[i].remove();
					}
				}
			} else {
				jAlert(msg.error);
			}
			$("#waiting").hide();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			console.error( textStatus + errorThrown);
			if (textStatus == 'timeout'|| XMLHttpRequest.status == 0) { 
				setOMSConnection(false, {http:jqxhr, status:textStatus, error:error,call_fxn:'common.loadMarkers'});
				$("#connectionmessage").html( "Due to slow connection we will remove alert point when internet is available.").show();
				setTimeout(function() { $("#connectionmessage").hide(); } , 4000);
				storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php", ajaxParams);
			}else {
				jAlert("ERROR= " + JSON.stringify(jqxhr));
			}
		}
	});
	$('#closeBtn').trigger('click');
}
/*----------------------------------------------------------------------------*/
function addPrimLineLayer() {
  var bounds = googleMap.getVisibleRegion();
	NorthEast = bounds.northeast;
	SouthWest = bounds.southwest;
	southWestMercBound = Proj4js.transform(new Proj4js.Proj("EPSG:4326"), new Proj4js.Proj("EPSG:900913"), new Proj4js.Point(SouthWest.lng, SouthWest.lat));
	northEastMercBound = Proj4js.transform(new Proj4js.Proj("EPSG:4326"), new Proj4js.Proj("EPSG:900913"), new Proj4js.Point(NorthEast.lng, NorthEast.lat));
	var mapfile= 'e_outage';
	if (service_name == 'Water')  {
		mapfile='WATER';
	} else if (service_name == 'SEWER')  {
		mapfile='SEWER';
	}

	if (typeof(bounds) !== "undefined") {
		var mapserv = localStorage.getItem('ims_url').toLowerCase() + "/Scripts/Maps/Mapserv.exe?mode=map&map=d:/dataVoice/config/Maps/" + params.UtilityId + "/" + params.UtilityId + "_WMS_WIDGET_"+mapfile+".map";
		mapserv += "&layers=" + params[appname+'MapLayers'].replace(/ /g, '+');
		if (localStorage.getItem('ckbx_ConsumerOutages') === 'true') {
			mapserv += "+OUTAGES";
		}
		mapserv += "&DaFilter=";
		mapserv += "&mapext=" + southWestMercBound.x + "+" + southWestMercBound.y + "+" + northEastMercBound.x + "+" + northEastMercBound.y + "&";
		mapserv += "map_size=" + $("#map_canvas").width() + "+" + $("#map_canvas").height() + "&";
		mapserv += "&map_imagetype=gif&cache=" + (new Date()).getTime();

		try{
			googleMap.addGroundOverlay( {
				url:mapserv,
				bounds:[{lat:bounds.northeast.lat,lng:bounds.northeast.lng},{lat:bounds.southwest.lat,lng:bounds.southwest.lng}],
				visible:true,
				zIndex:1,
				opacity:1 },
				function(l_overlay) {
					gridOverlay=l_overlay;
					setOMSConnection(true);
				}
			);
		}catch(ex) {
			alert("Lines layer is currently not available. Error:"+ex.message);
			if (navigator.connection.type == Connection.NONE && hasSpatialDB) {
				disconnectedMapMoved();
			}
		}
	}
}

/*----------------------------------------------------------------------------*/
function addWeatherLayer() {
	var bounds = googleMap.getVisibleRegion();
	NorthEast = bounds.northeast;
	SouthWest = bounds.southwest;
	southWestMercBound = Proj4js.transform(new Proj4js.Proj("EPSG:4326"), new Proj4js.Proj("EPSG:900913"), new Proj4js.Point(SouthWest.lng, SouthWest.lat));
	northEastMercBound = Proj4js.transform(new Proj4js.Proj("EPSG:4326"), new Proj4js.Proj("EPSG:900913"), new Proj4js.Point(NorthEast.lng, NorthEast.lat));

	if (typeof(bounds) !== "undefined") {


		var mapserv = "http://mesonet.agron.iastate.edu/cgi-bin/wms/nexrad/n0r.cgi?tag=" + (new Date).valueOf() + "?service=WMS&request=GetMap&version=1.1.0&layers=nexrad-n0r-900913&styles=&format=image/png&transparent=true&height=" + $("#map_canvas").height() + "&width=" + $("#map_canvas").width() + "&srs=EPSG:3857&bbox=" + southWestMercBound.x + "," + southWestMercBound.y + "," + northEastMercBound.x + "," + northEastMercBound.y;
		
		try{
			googleMap.addGroundOverlay( {
				url:mapserv,
				bounds:[{lat:bounds.northeast.lat,lng:bounds.northeast.lng},{lat:bounds.southwest.lat,lng:bounds.southwest.lng}],
				visible:true,
				zIndex:1,
				opacity:0.3 },
				function(w_overlay) {
					weatherOverlay=w_overlay;
				}
			);
		}catch(ex) {
			alert("Weather layer is currently not available");
		}
	}
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
function loadProjectPolygons() {
	console.log("Loanding Project Polygons");
	for ( var proj in polygons) {
		if (polygons[proj] != undefined) {
			polygons[proj].remove();
		}
	}
	polygons = [];
	$.each( projects,
		function(i, m) {
			var fillColor = '#FFDA21';
			var geomCoords = [];
			var avglat = 0;
			var avglon = 0;
			if ( m.project_poly != undefined && m.project_poly[0].lat != null ) {
				for (var j = 0 ; j < m.project_poly.length ; j++) {

					geomCoords[j] = { lat: m.project_poly[j].lat, lng: m.project_poly[j].lon };
					avglat +=parseFloat(m.project_poly[j].lat);
					avglon +=parseFloat(m.project_poly[j].lon);
				}
				avglat = avglat/m.project_poly.length;
				avglon = avglon/m.project_poly.length;
				googleMap.addPolygon( {
					points:geomCoords,
					strokeColor: fillColor,
					strokeWidth: 3,
					fillColor: fillColor,
					clickable: true,
					function(polygon) {
						polygon.on(plugin.google.maps.event.POLYGON_CLICK, function(LatLng) {
							googleMap.addMarker( {
								position:LatLng,
								title:'Project',
								function(marker){
									marker.showInfoWindow();
									showServiceOrders(this.get('myData').project);
								}
							});
						});
						polygon.data = {'center':{'lat':avglat,'lon':avglon},'project':i};
						polygons.push(polygon);
					}
						
				});
			}
		}
	);

}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
function updateLAIncident(index) {
	crewIndex = index;
	var html = '';
	html += '<tr><td colspan="2">My Status:<select id="disp_status" name="disp_status" data-theme="a" style="width:380px;max-width:'+(windowwidth-15)+';"><option value="">None</option>';
	for (var i = 0; i < statusCodes.length; i++) {
		var code = statusCodes[i];
		html += '<option value="' + code['class_name'] + '">' + code['class_name'] + ' - ' + code['class_description'] + '</option>';
	}
	html += '</select></td></tr>';
	if (crews[index].get('myData').type === 'device' || crews[index].get('myData').type === 'consumer') {
//CONSUMER or DEVICE


		html += '<tr><td>Estimated Restore Time:</td></tr>' +
				'<tr><td><select name="ert_select" id="ert_select" onchange="javascript:console.log(\'changed\'); setLAERT(this);"><option value="">None</option><option value="30">30 minutes</option><option value="60">1 hour</option><option value="120">2 hours</option><option value="240">4 hours</option><option value="480">8 hours</option><option value="1440">24 hours</option></select><input id="ert" type="text"></td></tr>';

		//html+='<tr><td colspan="2"><div id="OptionsContainer" style="float:left;clear:both;margin:8px 0 0 50px;"></div></td></tr>':
		html += '<tr><td>Comments:</td></tr>' +
			'<tr><td colspan="2"><TEXTAREA name="comments" id="update_comments" rows="5" style="width:380px;max-width:'+(windowwidth-15)+'px"></TEXTAREA></td></tr>';
	}else {
        html += '<tr><td>Notes:</td></tr>' +
        '<tr><td colspan="2"><TEXTAREA name="note" id="update_note" rows="5" style="width:380px;max-width:'+(windowwidth-15)+'px;"></TEXTAREA></td></tr>';
    }
    if(crews[index].get('myData').scouting_ticket == 'Y' /*&& crews[index].get('myData').scout_status == 'D'*/) {

        html +=
        '<tr><td align="right" id="scout_comment_prompt">Scouting Complete</td>'+
        '<td align="left"><input onclick="javascript: if ($(\'#mark_scouted\').is(\':checked\')){ $(\'#work_row\').show(); }else{ $(\'#work_row\').hide(); };" style="margin-left:0;" type="checkbox" value="1" id="mark_scouted"></td></tr>'+
        '<tr id="work_row" style="display:none;"><td align="right" id="scout_comment_prompt">Needs Further Work</td>'+
        '<td align="left"><input style="margin-left:0;" type="checkbox" value="1" id="scout_needs_further_work"></td></tr>';
        /*'<tr><td width="30%" align="right" class="prompt" id="scout_undispatch_label">Undispatch</td>'+
        '<td align="left"><input style="margin-left:0;" type="checkbox" value="1" id="scout_undispatch" checked="checked"></td></tr>';*/

    }
    if (crews[index].get('myData').type === 'service') {
		html += '<tr><td><button class="ui-btn ui-btn-a ui-btn-icon-left ui-icon-edit" style="width:200px;max-width:'+(windowwidth-15)+'px;text-align:center;" data-theme="a" onclick="javascript:doupdateLAIncident(true);">Place On Hold</button></tr>';
	}
	html += '<tr><td><button class="ui-btn ui-btn-a ui-btn-icon-left ui-icon-edit" style="width:200px;max-width:'+(windowwidth-15)+'px;text-align:center;" data-theme="a" onclick="javascript:doupdateLAIncident();">Update</button></tr>';

	$("#update_table").html(html);
	//$(".jqbutton").button();
	$("#disp_status").val(crews[index].get('myData').status);
	$("#disp_status").selectmenu();
    if (!(isTablet)) {
        $("#update_container").css({'position': 'absolute', 'top': 0, 'left': 0, 'z-index': 9999,'width':windowwidth, 'height':windowheight, 'padding':'0px','margin':'0px','overflow':'scroll'});
    }else{
        $("#update_container").css({'position': 'absolute', 'top': $(window).height() * .2, 'left': $(window).width() * .3, 'z-index': 9999,'padding':'5px','margin':'5px', 'width':$("#update_table").width()});
    }
    if (crews[index].get('myData').estimated_restore_time != null) {
        $("#ert").val(crews[index].get('myData').estimated_restore_time);
    }
	$("#update_comments").val(crews[index].get('myData').outage_comment);
	$("#disp_status").val(crews[index].get('myData').status);
    //$("#update_comments").css({'width':$("#update_container").width()-12});
    $("#update_container button").parent().css({'max-width':windowwidth});
    $("#update_container select").parent().parent().css({'max-width':windowwidth});
    $("#update_container select").parent().css({'max-width':windowwidth});
    $("#update_container textarea").css({'max-width':windowwidth});
	fixTitlesBySvcIdx(service_index);
	$("#update_container").show();
    $("#update_container").css({'width':$("#update_table").width()+'px'});
}
/*----------------------------------------------------------------------------*/
function doupdateLAIncident(hold) {
	if (hold == undefined) {
		hold = false;
	}
	$("#update_container").hide();
	
    var updatedata = {action :'get',
                  target: 'update_incident',
                  work_item_number: crews[crewIndex].get('myData').work_item_number,
                  id: crews[crewIndex].get('myData').work_item_number,
                  work_item_type: crews[crewIndex].get('myData').work_item_type,
                  EstRestoreTime: $("#ert").val(),
                  Comments: $("#update_comments").val(),
                  disp_status: $("#disp_status").val(),
                  crew: crews[crewIndex].get('myData').disp_entity_number,
                  view: crews[crewIndex].get('myData').disp_entity_type,
                  EnteredBy: user,
                  mark_scouted: $("#mark_scouted").is(":checked"),
                  needs_further_work: $("#scout_needs_further_work").is(":checked"),
                  appname: appname, 
                  hold: hold
                };
    if (navigator.connection.type == Connection.NONE) {
        storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php", updatedata);
    }else{
		$.ajax({
			type: "GET",
			url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php",
			data: updatedata,
			dataType: "json",
			timeout:6000,
			success: function(msg) {
				//$("#waiting").hide();
				setOMSConnection(true);
				if (msg.result !== "false") {
					if(hold || (appname=='AssessDamage' && $("#mark_scouted").is(":checked"))) {
						loadMarkers(['my_dispatched_items']);
						return;
					}
					/*$row['EstRestoreTime'] $row['Comments']=$Comments $row['EquipCode']=$EquipCode $row['CauseCode']=$CauseCode $row['disp_status']=$disp_status;*/					
					crews[crewIndex].get('myData').estimated_restore_time = msg.updates.EstRestoreTime;
					crews[crewIndex].get('myData').outage_comment = msg.updates.Comments;
					crews[crewIndex].get('myData').status = $("#disp_status").val();
					crews[crewIndex].get('myData').estimated_restore_time = msg.updates.EstRestoreTime
					$("#crew_popup_ert_" + crewIndex).html(msg.updates.EstRestoreTime);
					$("#crew_popup_status_" + crewIndex).html($("#disp_status").val());
					$("#crew_list_status_" + crewIndex).html($("#disp_status").val());
					$("#crew_list_ert_" + crewIndex).html(msg.updates.EstRestoreTime);
					$("#crew_popup_comments_" + crewIndex).html(msg.updates.Comments);
					$("#crew_list_comments_" + crewIndex).html(msg.updates.Comments);
				} else {
					jAlert(msg.error);
				}
				return;
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$("#waiting").hide();
				if (textStatus == 'timeout' || XMLHttpRequest.status == 0) {
					setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'common.updateIncident'});
					storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php", updatedata);
					return;
				}
				jAlert("Failed to Update Outage Ticket: " + textStatus + " " + errorThrown);
			}
		});
	}
    if ($("#update_note").val() != null && $("#update_note").val().length > 0) {
    	notedata = {  action:'addNote',
                    work_type:crews[crewIndex].get('myData').work_item_type,
                    work_item:crews[crewIndex].get('myData').work_item_number,
                    note:$("#update_note").val(),
                    userid:user};
    	if (navigator.connection.type == Connection.NONE) {
		    storeAjaxRequest(localStorage.getItem('ims_url') + "/dvwfm2/vehicle/ajaxAddNote.php", notedata);
		}else {
		    $.ajax({
		       type: "POST",
		       url: localStorage.getItem('ims_url') + "/dvwfm2/vehicle/ajaxAddNote.php",
		       data: notedata,
		       dataType: "jsonp",
		       success: function(msg) {
		        //$("#waiting").hide();
		        setOMSConnection(true);
		        if (msg.result === "false") {
		            jAlert("Failed to Add Note: "+msg.error);
		        }
		       },
		       error: function(XMLHttpRequest, textStatus, errorThrown) {
		           $("#waiting").hide();
					if (textStatus == 'timeout' || XMLHttpRequest.status == 0) {
						setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'common.updateIncident'});
						storeAjaxRequest(localStorage.getItem('ims_url') + "/dvwfm2/vehicle/ajaxAddNote.php", notedata);
						return;
					}
					jAlert("Failed to Update Outage Ticket: " + textStatus + " " + errorThrown);
		       }
		    });
        }
    }
    if ($("#disp_status").val() != '') {
    	timedata = {action: 'addTimeSlice', employee_number: user_number, entityNumber: crews[crewIndex].get('myData').disp_entity_number, entityType: crews[crewIndex].get('myData').disp_entity_type, start_date: getCurrentMilitaryDate(), start_time:new Date().getHours()+":"+ new Date().getMinutes(), work_item_number:crews[crewIndex].get('myData').work_item_number, work_type:crews[crewIndex].get('myData').work_item_type, time_classification:$("#disp_status").val()};
    	if (navigator.connection.type == Connection.NONE) {
		    storeAjaxRequest(localStorage.getItem('ims_url') + "/dvWfm2/Vehicle/ajaxTimeSlices.php", timedata);
		}else{
			$.ajax({
				type: "GET",
				url: localStorage.getItem('ims_url') + "/dvWfm2/Vehicle/ajaxTimeSlices.php",
				data: timedata,
				dataType: "json",
				success: function(msg) {
					setOMSConnection(true);
					if (msg.result == "false") {
						jAlert(msg.error);
					}
					//reloadTasks();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$("#waiting").hide();
					if (textStatus == 'timeout' || XMLHttpRequest.status == 0) {
						setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'common.timeslices'});
						storeAjaxRequest(localStorage.getItem('ims_url') + "/dvWfm2/Vehicle/ajaxTimeSlices.php", timedata);
						return;
					}
					console.error( textStatus + errorThrown);
					jAlert("Failed to enter time for job. Error: "+errorThrown);
				}
			});
		}
    }
}
/*----------------------------------------------------------------------------*/
function markScouted() {
    
    ajaxParams = {action :'get',
    target: 'mark_scouted',
    work_item_number: crews[crewIndex].get('myData').work_item_number,
    id: crews[crewIndex].get('myData').work_item_number,
    work_item_type: crews[crewIndex].get('myData').work_item_type,
    crew: crews[crewIndex].get('myData').disp_entity_number,
    view: crews[crewIndex].get('myData').disp_entity_type,
    EnteredBy: user,
    mark_scouted: $("#mark_scouted").is(":checked"),
    appname: appname
    };
    if (navigator.connection.type == Connection.NONE) {
        storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php", ajaxParams);
        return;
    }
    $.ajax({
           type: "GET",
           url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php",
           data: ajaxParams,
           dataType: "json",
           success: function(msg) {
               //$("#waiting").hide();
               if (msg.result == "false") {
                jAlert(msg.error);
               }
           },
           error: function(XMLHttpRequest, textStatus, errorThrown) {
               $("#waiting").hide();
               if (textStatus == 'timeout' || XMLHttpRequest.status == 0) {
               	storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetLinemanAppData.php", ajaxParams);
				return;
               }
               jAlert("Failed to Mark Scouted Outage Ticket.  Error: " + textStatus + " " + errorThrown);
           }
    });
}
/*----------------------------------------------------------------------------*/

function closeTicket(clsindex) {
	$("#btnClose").trigger("click");
  crewIndex=clsindex;
  g_currindex=clsindex;
  g_work_item_number=crews[crewIndex].get('myData').work_item_number;
  g_userid=user;
  g_services=tempServices;
  g_distrtictsData=tempDistrictsData;
  g_districtHtml=tempDistrictHtml;
  crews[crewIndex].get('myData').requires=params.requires[crews[crewIndex].get('myData').so_code];
  crews[crewIndex].get('myData')['so_show_history_comments']=params.so_show_history_comments;
  $("#work_item_type").val(crews[crewIndex].get('myData').work_item_type);
  clsCurrentWorkItem={
      is_mobile:true,
      //is_scouting_ticket:crews[index].get('myData').scouting_ticket == 'Y',
      mobile_app_name:appname,
  	work_item_type:crews[crewIndex].get('myData').work_item_type,
  	work_item_number:crews[crewIndex].get('myData').work_item_number,
  	incident_type:crews[crewIndex].get('myData').work_item_number.substr(0,1),
      context:'X',
      service_index:crews[crewIndex].get('myData').service_index,
      cust_phone:'',
      cust_alt_phone: '',
      cust_email: '',
      callback_phone: '',
      callback_email: '',
      callback_sms: '',
      callback_sms_carrier: '',
  	account_number:crews[crewIndex].get('myData').accountnumber,
  	meter:crews[crewIndex].get('myData').meter_number,
  	is_electric:true,
  	domain: localStorage.ims_url,
  };

	g_meter = crews[crewIndex].get('myData').meter_number;
	if(arguments.length > 3) {
		g_callAfterComplete = arguments[3];
	}
	else {
		g_callAfterComplete = null;
	}
	var sUrl = "";

	if(clsCurrentWorkItem.work_item_type === 'I') {
		switch(clsCurrentWorkItem.incident_type) {
			case 'C':
				sUrl = 'restoreIncident';
				g_pageTitle = "Close Incident: ";
				break;
			case 'D':
				sUrl = 'restoreDevice';
				g_pageTitle = "Close Device Incident: ";
				break;
			case 'G':
				sUrl = 'restoreGroup';
				g_pageTitle = "Close Grouped Incident: ";
				break;
			default:
				jAlert("Invalid incident type");
				return;
				break;
		}
	}
	else if(clsCurrentWorkItem.work_item_type === 'S') {
		sUrl = 'restoreSo';
		g_pageTitle = "Close Service Order: ";
	}
	else if(clsCurrentWorkItem.work_item_type === 'W') {
		sUrl = 'restoreSo';
		g_pageTitle = "Close Work Order: ";
	}
	clsCloseWorkItem.populateCloseDialog(sUrl, service_index,insertData);
}
function insertData(result) {
	if (result) {
		insertCloseData(styleCloseItems);
	}else{
		insertSoCloseData(crews[crewIndex], styleCloseItems);
	}
}
/*----------------------------------------------------------------------------*/
function doCloseTicket() {
	$("#waiting").show();
	//$("#btnClose").trigger("click");
	g_currindex=crewIndex;
	if ((params['always_update_service_order_geodata'] == true && clsCurrentWorkItem.work_item_type == 'S') && 
		(close_item.daffron_lat == undefined || close_item.daffron_lat == null) && 
		(!$("#ignore_gps").is(':checked'))) {	
		getBgGps();
		return;
	}
	clsCurrentWorkItem.lat = close_item.daffron_lat;
	clsCurrentWorkItem.lon = close_item.daffron_lon;
	close_item.daffron_lat = null;
	close_item.daffron_lon = null;
	$("#close_item [type=datetime-local]").each(function() {
		
		$("#"+this.id.replace('_ios',''), close_item).val($("#"+this.id.replace('_ios',''), close_item).val().replace(/-/gi,'/').replace('T',' ').split('.')[0]);
	});
	clsCloseWorkItem.close_validate(ticketIsClosed);
	//clsCloseWorkItem.close_work_item(ticketIsClosed);
}
/*----------------------------------------------------------------------------*/
function ticketIsClosed(results) {
	
	if (results) {
		refreshGoogleMap();
	}else{
		console.log("ticket close FAILED results="+JSON.stringify(results));
	}
	if (appname == 'AssessDamage') {
		clearDamageByWorkItem(crews[crewIndex].get('myData').work_item_number);
	}	   
}
/*----------------------------------------------------------------------------*/
function styleCloseItems() {
    clearExistingBottomMenus();
    $("#connectionmessage").hide();
    $('#close_item input[type="button"][value="Close"]').removeAttr('disabled');
    $("#waiting").hide();
    if (crews[crewIndex].get('myData').scouting_ticket == 'Y') {
        $("#scouting_row").show();
    }
    $("#close_item [type=datetime-local]").each(function() {
		this.value=new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60 *1000).toISOString().replace('Z','');
		//this.value=new Date().toUTCString();
		$(this).trigger('change');
	});
	$("#close_item [type=date]").each(function() {
		this.value=new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60 *1000).toISOString().split('T')[0];
		$(this).trigger('change');
	});
    fixDialogTitles("close_item");
    $("#close_item [type=button]").button();
    $("#close_item select").each(function() {
    	if ($(this).attr('name') != undefined && $(this).attr('name').substr(0,10) == 'radioPhase') {
    		if (crews[crewIndex].get('myData').PhasesUsed.indexOf($(this).attr('name').substr(-1)) >= 0){
				$(this).slider();
				$("#phase_container span.ui-slider-label-a").css("color", "red");
				$("#phase_container span.ui-slider-label-b").css("color", "#7eb238");
			}else{
				$(this).slider({disabled:true});
				//$(this).hide();
			}
		}else{
			$(this).selectmenu();
			//setTimeout(function() {$("#close_item select").selectmenu('refresh');}, 1000);
		}
	});
    $("#btnClose").trigger("click");
    //$("#close_item checkbox:visible").checkboxradio();
    $("#close_item").show();/*.css({'background-color':'#FFF',
                                'width':(windowwidth-50)+'px',
                                'height':(windowheight-($("#mapheader").height()+$("#mapfooter").height()+ios7Pad))+'px',
                                'top':($("#mapheader").height())+'px',
                                'left':'5px',
                                'position':'absolute',
                                'overflow': 'scroll',
                                '-webkit-overflow-scrolling': 'touch'});*/
    $("#close_item_container").show().css({'background-color':'#FFF',
                                'width':(windowwidth-10)+'px',
                                'height':(windowheight-($("#mapheader").height()+$("#mapfooter").height()+ios7Pad))+'px',
                                'top':($("#mapheader").height())+'px',
                                'left':'1px',
                                'border-color':'grey',
                                'position':'absolute',
                                'overflow': 'scroll',
                                '-webkit-overflow-scrolling': 'touch'});


}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
function getDirections(dirindex, type) {	
	var data, popup;
	if (type === 'crew') {
		data = crews[dirindex].get('myData');
		popup = crews[dirindex].marker;
	}else if (type === 'search') {
		data = searchresults[parseInt(dirindex)].get('myData');
		popup = searchresults[parseInt(dirindex)].marker;
	}else if (type == 'device') {
		data = markers[parseInt(dirindex)].get('myData');
		popup = markers[parseInt(dirindex)].marker;
	}
	if (data.lat== null ||  data.lon == ''){
		$('#connectionmessage').html('There is no location for this item. You can try search by pole number or street address to get directions.');
		$('#connectionmessage').show();
		setTimeout(function() {
			$('#connectionmessage').fadeOut('slow');
		}, 4000);
		return;
	
	}
  $("#btnClose").trigger('click');
	launchnavigator.navigate([data.lat, data.lon], 
	/*function(){ console.log("Launched Google Maps"); },
	function(error){ console.log(error);},*/
	{app:launchnavigator.APP.USER_SELECT, 
	enableDebug:true,
	successCallback: function() { console.log("Google Maps Directions Success");},
	errorCallback: function(err) { console.error(err);}});
}

var g_clearSearchCallback;
function clearSearchResults(callback) {
	g_clearSearchCallback = callback;
	$("#btnClose").trigger('click');
	setTimeout( function() {
   	for (var i = 0; i < searchresults.length; i++) {
			if (searchresults[i] != null && searchresults[i] != undefined && searchresults[i].remove != undefined) {
		        searchresults[i].remove();
		    }
		}
		//searchresults = [];		
		var fn = window[g_clearSearchCallback];
		if(typeof fn === 'function') {
			fn();
		}
  },500);
}
function clearExistingBottomMenus(divToOpen) {
	if (divToOpen == '' || divToOpen == undefined){
		googleMap.setClickable( true );
	}else{
		googleMap.setClickable( false );
	}
	if (measure_line != null) {
		measure_line.remove();
	}
	clearInterval(measureTimer);
	if ($(".bottom_menu:visible").length === 0) {
		/*$("#" + divToOpen).animate({opacity: 0.90, width: "toggle"}, 500, function() {
		});*/
		$("#" + divToOpen).show();
		googleMap.setClickable(true);
		return;
	}
	$(".bottom_menu:visible").each(
		function() {
			var id = this.id;
			$(this).hide();			
			/*$("#" + id).animate({opacity: 0.15, width: 'toggle'}, 500, function(id) {
				$("#" + this.id).hide();
				if (divToOpen !== null) {
					$("#" + divToOpen).animate({
						opacity: 0.90,
						width: "toggle"
					}, 500, function() {
						// Animation complete.
					});

				}
			});*/
			
		});
	$("#" + divToOpen).css({opacity: 0.9}).show();
	googleMap.setClickable(true);
}
/*----------------------------------------------------------------------------*/
function showKey() {
	$("#key_menu").css({position: 'absolute', top: $("#mapheader").height()+ios7Pad, left: 0, height: $('#map_canvas').height() + 'px', 'max-width': '400px', width:($(window).width()*.85)+'px'});
	clearExistingBottomMenus('key_menu');
}
function canViewLayer(layer) {
	if(layer.indexOf('Out') != -1 && can_view_all_outages == 'FALSE' ) {
		return false;
	}
	if((layer.indexOf('Predictions') != -1 || layer.indexOf('Out') != -1 ) && appname != 'LinemanApp') {
		return false;
	}
	if (layer.indexOf('Vehicles') != -1 && can_view_vehicles == 'FALSE') {
		return false;
	}
	return true;
}
/*----------------------------------------------------------------------------*/
function popLayerMenu() {
	$("#layers_menu").css({position: 'absolute', top: $("#mapheader").height()+ios7Pad, left: 0, height: $('#map_canvas').height() + 'px', 'max-width': '400px', width:($(window).width()*.85)+'px'});
	clearExistingBottomMenus('layers_menu');
	$('#layers_list').html('');
	$('#layers_list').append('<table>');
	var layers = params.LinemanAppSelectableLayers.split(",");
	for (var key in layers) {
		//html+='<tr><td>'+layers[key]+'</td>';
        if (localStorage.getItem('ckbx_' + layers[key]) == null) {
            localStorage.setItem('ckbx_' + layers[key],'true');
        }
        if (canViewLayer(layers[key])){
		    var html = '<tr><td><select name="layers" class="layer" id="' + layers[key] + '" data-theme="a" data-role="slider" data-mini="true" onchange="javascript:changeLayer(this);" value="' + localStorage.getItem('ckbx_' + layers[key]) + '"><option value="false">Off</option><option value="true">On</option></select></td><td style="vertical-align:middle;">' + layers[key] + '</td></tr>';
			//var html = '<tr><td onclick="javascript:popFilteringMenu(\''+layers[key]+'\');"><a class="ui-icon-my-funnel" data-iconpos="notext" data-theme="b" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-my-funnel"></a></td><td><select name="layers" class="layer" id="' + layers[key] + '" data-theme="a" data-role="slider" data-mini="true" onchange="javascript:changeLayer(this);" value="' + localStorage.getItem('ckbx_' + layers[key]) + '"><option value="false">Off</option><option value="true">On</option></select></td><td style="vertical-align:middle;">' + layers[key] + '</td></tr>';
			$('#layers_list').append(html);
			$('#' + layers[key]).val(localStorage.getItem('ckbx_' + layers[key]));
		}
	}
	if (appname == "AssessDamage") {
		 var html = '<tr><td><select name="layers" class="layer" id="Damage" data-theme="a" data-role="slider" data-mini="true" onchange="javascript:changeLayer(this);" value="' + localStorage.getItem('ckbx_Damage') + '"><option value="false">Off</option><option value="true">On</option></select></td><td style="vertical-align:middle;">Damage</td></tr>';
		$('#layers_list').append(html);
		$('#Damage').val(localStorage.getItem('ckbx_Damage'));	
	}
	$('#layers_list').append('</table>');
	$('.layer').slider();
	$(".ui-icon-my-funnel").button();	//$("#layers_menu").show();
	//$("#layers_menu").animate({opacity: 0.85, width: "toggle"}, 1500, function() {// Animation complete.});
	//$("#layers_menu").slideToggle();
}
/*----------------------------------------------------------------------------*/
function popSearchMenu() {
	$("#search_menu").css({position: 'absolute', top: $("#mapheader").height()+ios7Pad, left: 0, height: $('#map_canvas').height(), 'max-width': '400px', width:($(window).width()*.85)+'px'});
    $("#search_menu :input").each(function(){$("#"+this.id).val('');});
    $("#search_menu :input").each(function(){
    	$(this).attr('type','text');
    });

	// Add classes for column title changes in fixTitlesBySvcIndex
	var titleClasses = [
		{ find: "#find_eq_polenumber", classprefix: "pole" },
		{ find: "#find_eq_maplocation", classprefix: "maploc" },
		{ find: "#find_eq_linesection", classprefix: "linesect" },
		{ find: "#find_meternumber", classprefix: "meter" },
		{ find: "#find_accountnumber", classprefix: "account" },
		{ find: "#find_maplocation", classprefix: "maploc" },
		{ find: "#find_servicelocation", classprefix: "svcloc" }
	];

	$("#search_menu div [data-role='fieldcontain']").each(function() {
		var jthis = $(this);
		for(var i = 0; i < titleClasses.length; ++i) {
			if(jthis.find(titleClasses[i].find).length) {
				jthis.find("label").addClass(titleClasses[i].classprefix+"_prompt");
				jthis.find("input").addClass(titleClasses[i].classprefix+"_value");
				break;
			}
		}
	});

	fixTitlesBySvcIdx(service_index);
	clearExistingBottomMenus('search_menu');
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
function popCameraMenu() {
	if (navigator.connection.type == Connection.NONE) {
		$('#connectionmessage').hide();
		$('#connectionmessage').html("Sorry, we are unable to upload photos when internet is unavailable.  You can open your camera and take photos.  Then when internet connectivity is available, you can upload the photos from your photo library.");
		$('#connectionmessage').show();
		$('#blockingcanvas').hide();
		setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 9000);
		if (reopenPopup) {
			reopenPopup=false;
			$("#inspect_data_page").popup('open');
		}
		return;
	}
	var index = crewIndex;
    var html = '<option value=""></option>';
    for (var so in crews){
        html += '<option value="'+so+'-crews">'+crews[so].get('myData').work_item_number+'-'+crews[so].get('myData').ServiceLocation+'</option>';
    }
    $("#pic_work_item_number").html(html);
	$("#camera_menu").css({position: 'absolute', top: $("#mapheader").height()+ios7Pad, left: 0, height: $('#map_canvas').height() + 'px', 'max-width': '400px', width:($(window).width()*.85)+'px'});
    $("#camera_menu :input").each(function(){$("#"+this.id).val('');});
    $("#show-picture").attr('src','');
    $('#pic_location').val('current');
    $('#pic_location').trigger('change');
    clearExistingBottomMenus('camera_menu');
    if (crewIndex != null) {
        $("#pic_work_item_number").val(crewIndex+'-crews');
        $('#pic_location').val('work_item');
    }
    $('#pic_location').trigger('change');
    $("#pic_work_item_number").trigger("change");
    $("#camera_content .ui-slider").css('width','135px');
    //takePicture();
}
/*----------------------------------------------------------------------------*/
function goto(elem, type) {
	var data, popup;
	clearExistingBottomMenus();
	if (type === 'crew') {
		data = crews[elem].get('myData');
		popup = crews[elem];
	}

	if (type === 'search') {
		data = searchresults[parseInt(elem)].get('myData');
		popup = searchresults[parseInt(elem)];
	}
	//$(".jqbutton").button();
	//googleMap.setCameraZoom(16);
	if (data.lat== '' ||  data.lon == ''){
		$('#connectionmessage').html('There is no location for this item. You can try search by pole number or street address.');
		$('#connectionmessage').show();
		setTimeout(function() {
			$('#connectionmessage').fadeOut('slow');
		}, 4000);
	
	}else{
        popup.trigger(plugin.google.maps.event.MARKER_CLICK);
        googleMap.setCameraZoom(20);
        fixTitlesBySvcIdx(service_index);
	}
}
/*----------------------------------------------------------------------------*/
function changeLayer(ckbx) {

	localStorage.setItem('ckbx_' + ckbx.id, ckbx.value);
	switch (ckbx.id) {
		case 'Weather':
			if (ckbx.value === 'true') {
				addWeatherLayer();
			} else {
				weatherOverlay.remove();
			}
			break;
		case 'PrimaryLines':
			if (ckbx.value === 'true') {
				addPrimLineLayer();
			} else {
				console.log("removing lines overlay as selected")
				gridOverlay.remove();
			}
			break;
		case 'Vehicles':
			for (var i = 0; i < vehicles.length; i++) {
				vehicles[i].setVisible(ckbx.value === 'true');
			}
			break;
		case 'DevicesOut':
			for (var i = 0; i < markers.length; i++) {
				markers[i].setVisible(ckbx.value === 'true');
			}
			break;
		case 'MyItems':
			for (var so in crews) {
				crews[so].setVisible(ckbx.value === 'true');
			}
			break;
		case 'ConsumerOutages':
			addPrimLineLayer();
			break;
		case 'Substations':
			for (var i = 0; i < subs.length; i++) {
				subs[i].setVisible(ckbx.value === 'true');
			}
			break;
		case 'Predictions':
			for (var i = 0; i < predictions.length; i++) {
				predictions[i].setVisible(ckbx.value === 'true');
			}
			break;
		case 'AlertPoints':
			for (var i = 0; i < alerts.length; i++) {
				alerts[i].setVisible(ckbx.value === 'true');
			}
			break;
        case 'Backoffice':
			backoffice.setVisible(ckbx.value === 'true');
            break;
        case 'Media':
			for (var i = 0; i < media.length; i++) {
				media[i].setVisible(ckbx.value === 'true');
			}
			break;
		case 'Damage':
			for (var i = 0; i < damageMarkers.length; i++) {
				damageMarkers[i].setVisible(ckbx.value === 'true');
			}
			break;

	}
}


/*----------------------------------------------------------------------------*/
var alertidx=0;
function popAddAlert() {
    var html = '';
    for (var i = 0 ; i < g_alerticons.length ; i++) {
        var img = localStorage.getItem('ims_url') + '/dvmapviewer2/images/' + g_alerticons[i].alertpoint_img
        img.replace('/','\/');
        html += '<img id="alertidx'+i+'" src="' + img + '" onclick="javascript:picAlertIcon('+i+');">';
    }
    $("#alert_icon_images").html(html);
}
function picAlertIcon(elem) {
    alertidx=elem;
    $('#alert_icon_images img').css({'border-width':'0px'});
    $("#alertidx"+elem).css({'border-width':'5px','border-style':'ridge'});
}
function addAlert() {

    if (navigator.connection.type == Connection.NONE) {
        storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php", {action: 'get',target: 'add_alert',lineman: user,serviceIndex: service_index,Comments: $("#alert_desc").val(),lat: search_value.coords.latitude,lon: search_value.coords.longitude, icon: alertidx, appname: appname});
        return;
    }

    $.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php",
		data: {action: 'get',
			target: 'add_alert',
			lineman: user,
            icon: alertidx,
			serviceIndex: service_index,
			Comments: $("#alert_desc").val(),
			lat: search_value != null ? search_value.coords.latitude : null,
			lon: search_value != null ? search_value.coords.longitude : null,
		},
		dataType: "jsonp",
		success: function(msg) {
			if (msg.result === 'false') {
				jAlert(msg.error);
				return;
			}
           refreshGoogleMap();
           stopFakeSearch=false;
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			console.error( textStatus + errorThrown);
			if (textStatus == 'timeout' || XMLHttpRequest.status == 0) {
				setOMSConnection(false, {http:jqxhr, status:textStatus, error:error,call_fxn:'common.loadMarkers'});
				$("#connectionmessage").html( "Due to slow connection we will add alert point when internet is available.").show();
				setTimeout(function() { $("#connectionmessage").hide(); } , 4000);
				storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php", {action: 'get',target: 'add_alert',lineman: user,serviceIndex: service_index,Comments: $("#alert_desc").val(),lat: search_value.coords.latitude,lon: search_value.coords.longitude, icon: alertidx, appname: appname});
				return;
			}
			jAlert("Unable to add alert. Error: "+errorThrown.toString());
		}
	});
}
/*----------------------------------------------------------------------------*/
function getTouchLatLon() {
	stopFakeSearch=true;
	googleMap.setClickable(true);
	$('#connectionmessage').html('Touch a spot on the map to seach near.');
	$('#connectionmessage').show();
	setTimeout(function() {
		$('#connectionmessage').fadeOut('slow');
	}, 4000);
    $("#search_menu").animate({opacity: 0.90, width: 'toggle'}, 1000, function() { $("#search_menu").hide();});
}
/*----------------------------------------------------------------------------*/

function findCustomer() {
    //google.maps.event.removeListener(getTouchLocationEvent);
    stopFakeSearch=false;
    if (navigator.connection.type == Connection.NONE) {
        $('#connectionmessage').hide();
        $('#connectionmessage').html('Unable to search for customers with no internet connection');
        $('#connectionmessage').show();
        $('#blockingcanvas').hide();
        setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 3000);
        return;
    }
	$("#waiting").show();
	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php",
		data: {action: 'find',
			target: 'customer',
			name: $("#find_name").val(),
			meternumber: $("#find_meternumber").val(),
			accountnumber: $("#find_accountnumber").val(),
			maplocation: $("#find_maplocation").val(),
			servicelocation: $("#find_servicelocation").val(),
			phone: $("#find_phone").val(),
			lat: search_value != null ? search_value.coords.latitude : null,
			lon: search_value != null ? search_value.coords.longitude : null,
			appname: appname
		},
		dataType: "jsonp",
		success: function(msg) {
			setOMSConnection(true);
			if (msg.result === 'false') {
				jAlert(msg.error);
				return;
			}
			$("#waiting").hide();
			$("#search_menu").hide();
			$("#searchresults_list").html('');
			searchresults = msg.resultslist;
			if (searchresults.length === 0) {
				$("#searchresults_list").html('No Results Found');
				$('#searchresults_list').trigger('create');
				$("#searchresults_list button").parent().css({'max-width':'95%'});
				$("#searchresults_list select").parent().css({'max-width':'95%'});
				$("#searchresults_list textarea").css({'max-width':'95%'});
				fixTitlesBySvcIdx('1');
				$("#searchresults_menu").show();
				stopFakeSearch=false;
			}
      var data = searchresults;
        var baseArray = new plugin.google.maps.BaseArrayClass(data);
        var infoWindow = new plugin.google.maps.HtmlInfoWindow();
        var idx = 0;
        var avglat = 0;
        var avglng = 0;
        baseArray.map(function(myData, cb) {
            myData=myData.data;
            avglat+= parseFloat(myData.lat);
            avglng+= parseFloat(myData.lon);
            myData.index = idx;
            var data = {
              icon: 'images/' + getIcon('CON', 99) + '.png',
              position: {
                lat: myData.lat,
                lng: myData.lon
              },
              draggable: true,
              disableAutoPan: true,
              zIndex: 120,
              myData: myData
        };
        //vehicles[i].popup = plugin.google.maps.InfoWindow({content: content, maxWidth: 400});
        googleMap.addMarker(data, cb);
        // ------------------  POPULATE COLLAPSIBLE SIDE MENU --------------------------------------------------------
        var html = '<div data-role="collapsible" data-collapsed="true">';
        html += '<h3 onclick="javascript:showSearchResultDetails(' + idx + ');"><img src="images/' + getIcon('CON', 99) + '.png"/>' + myData.label + '</h3>';
        html += '<div data-role="content"  class="scrollable"><table id="searchresultstable_' + idx + '">';
        html += '<tr><td class="bold">Name: </td><td>' + myData.lastname + '</td></tr>';
        html += '<tr><td class="bold">Phone: </td><td>' + myData.phone + '</td></tr>';
        html += '<tr><td class="bold account_prompt">Account: </td><td class="account_value">' + myData.accountnumber + '</td></tr>';
        html += '<tr><td class="bold maploc_prompt">Map Location: </td><td class="maploc_value">' + myData.maplocation + '</td></tr>';
        html += '<tr><td class="bold subfdr_prompt">Sub-Fdr: </td><td class="subfdr_value">' + myData.subfdr + '</td></tr>';
        var linesection = myData.linesection;
        if (linesection === null || linesection == undefined) {
            linesection = 'Not Available';
        }
        html += '<tr><td class="bold linesect_prompt">Linesection: </td><td class="linesect_value">' + linesection + '</td></tr>';
        var pole = myData.pole;
        if (pole === null || pole == undefined) {
        	pole = "Not Available";
        }
				html += '<tr><td class="bold meter_prompt">Meter Number: </td><td class="meter_value">' + myData.meternumber + '</td></tr>';
				html += '<tr><td class="bold svcloc_prompt">Service Location: </td><td class="svcloc_value">' + myData.servicelocation + '</td></tr>';
				if (can_create_incident !== 'FALSE' && (appname == 'LinemanApp' || appname == 'WaterCrewApp' || appname == 'SewerCrewApp')) {
					html+='<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" style="width:200px;" onclick="javascript:createIndividualOutage(' + idx + ',\'search\');clearSearchResults(\'clearExistingBottomMenus\');" >Create Outage</button></td></tr>';
				}
				if (params.hasWFM == 'TRUE') {
					html+='<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" style="width:200px;" onclick="javascript:$(\'#btnClose\').trigger(\'click\');createOrder(0,\'search\')">Create SO</button></td></tr>';
				}
				html+='<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" style="width:200px;" onclick="javascript:getDirections(' + idx + ',\'search\');clearSearchResults(\'clearExistingBottomMenus\');" >Get Directions</button></td></tr>';
				html += '</table>';
				html += '</div></div>';
				$("#searchresults_list").append(html);
				$("#searchresults_list").trigger('create');
                $("#searchresults_list button").parent().css({'max-width':'95%'});
                $("#searchresults_list select").parent().css({'max-width':'95%'});
                $("#searchresults_list textarea").css({'max-width':'95%'});
                $("#searchresults_menu").css({position: 'absolute', top: $("#mapheader").height()+ios7Pad, left: 0, height: $('#map_canvas').height() + 'px', 'max-width': '400px', width:($(window).width()*.85)+'px',opacity:.90});
                fixTitlesBySvcIdx(service_index);
                $("#searchresults_menu").show();
				idx++;

			}, function(markers) {
                if (markers.length == 0) {
                    return;
                }
				searchresults = markers;
				markers.forEach(function(marker) {
				  marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);				  
				});
                avglat = avglat/markers.length;
                avglng = avglng/markers.length;
                var pixels = googleMap.fromLatLngToPoint({lat:avglat,lng:avglng},
                    function(pixels){
                        pixels[0] = pixels[0] - 200;
                        var newcenter = googleMap.fromPointToLatLng(pixels, function(newcenter){
                            googleMap.animateCamera( {
                                'target': newcenter,
                                'tilt':googleMap.getCameraTilt(),
                                'zoom': googleMap.getCameraZoom(),
                                'bearing':googleMap.getCameraBearing(),
                                'duration':1000
                            });
                        });
                    }
                );
			});

			function onMarkerClick() {
				$("#closeBtn").trigger("click");
				var marker = this;
				var myData = marker.get("myData");
           var content = '<div style="width:350px;"> '+
                '<div style="float:right;" id="closeBtn"><img src="images/close.png"/></div>' +
				'Name: ' + myData.lastname + '<br>'+
				'<span class="svcloc_prompt">Service Location: </span><span class="svcloc_value">'+myData.servicelocation+'</span><br>' +
				'Phone: ' + myData.phone + '<br>' +
           '<button style="width:171px;display:inline;margin:2px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" onclick="javascript:getDirections(\'' + myData.index + '\',\'search\');">Get Directions</button>';
				//content += '<button style="width:200px;display:inline;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-u" onclick="javascript:traceLine(\''+myData.enetworkid+'\',\'up\');">Trace Upline</button>';
				if (params.MeterPings == 1 && appname == 'LinemanApp') {
           content+='<button style="width:171px;display:inline;margin:2px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:pingRequest('+myData.index+',\'search\', \'meter\');">Ping Meter</button>'+
           '<button style="width:171px;display:inline;margin:2px;" class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" onclick="javascript:createIndividualOutage(' + myData.index + ',\'search\');clearSearchResults(\'clearExistingBottomMenus\');" >Create Outage</button>';
           
				}
				content+='</div>';
                infoWindow.setContent(content,{width:'350px'});
                fixTitlesBySvcIdx(service_index);
				infoWindow.open(marker);
				setTimeout( function() {
					$("#closeBtn").on("click", function() {
						infoWindow.close();
					});
				},500);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			if (textStatus == 'timeout') {
				setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'common.findCustomer'});
			}
			$("#waiting").hide();
			console.log(XMLHttpRequest + textStatus + errorThrown);
			jAlert("Error finding customer: "+errorThrown.toString());
		}
	});
	search_value=null;
	findType = 'equipment';
}
/*----------------------------------------------------------------------------*/
function findEquipment(searchtype) {
    //google.maps.event.removeListener(getTouchLocationEvent);
    if (navigator.connection.type == Connection.NONE) {
        $('#connectionmessage').hide();
        $('#connectionmessage').html('Unable to search for equipment with no internet connection');
        $('#connectionmessage').show();
        $('#blockingcanvas').hide();
        setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 3000);
        return;
    }
    $("#waiting").show();

	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php",
		data: {action: 'find',
			target: 'equipment',
			searchstring: $("#find_equipment").val().toUpperCase(),
			searchtype: searchtype,
			lat: search_value != null ? search_value.coords.latitude : null,
			lon: search_value != null ? search_value.coords.longitude : null,
			appname: appname
		},
		dataType: "jsonp",
		success: function(msg) {
			setOMSConnection(true);
			if (msg.result === 'false') {
				jAlert(msg.error);
				return;
			}
			$("#waiting").hide();
			$("#searchresults_menu").css({position: 'absolute', top: $('#map_canvas').offset().top, left: 0, height: $('#map_canvas').height() + 'px', 'max-width': '450px',width:($(window).width()*.95)+'px', opacity: 0.90});
			$("#search_menu").hide();
			$("#searchresults_list").html('');
			searchresults = [];
			for (var type in msg.equipment) {
				for ( var i = 0 ; i < msg.equipment[type].length ; i++){
					searchresults.push(msg.equipment[type][i]);
				}
			}
			if (searchresults.length === 0) {
				$("#searchresults_list").html('No Results Found');
				$('#searchresults_list').trigger('create');
				$("#searchresults_list button").parent().css({'max-width':'95%'});
				$("#searchresults_list select").parent().css({'max-width':'95%'});
				$("#searchresults_list textarea").css({'max-width':'95%'});
				fixTitlesBySvcIdx('1');
				$("#searchresults_menu").show();
				stopFakeSearch=false;
				return;
			}
			var data = searchresults;
			var baseArray = new plugin.google.maps.BaseArrayClass(data);
			var infoWindow = new plugin.google.maps.HtmlInfoWindow();
			var idx = 0;
            var avglat = 0;
            var avglng = 0;
			baseArray.map(function(myData, cb) {
				myData=myData;
                avglat+= parseFloat(myData.lat);
                avglng+= parseFloat(myData.lon);
				myData.index = idx;		
				var icon = 'images/' + myData.enettype + 'A.png';
				var status = 'Unknown';
				var icon = 'images/' +myData.enettype + '_' + 'B.png';
				if (myData.enettype == 'POLE') {
					status = 'N/A';
					icon = localStorage.ims_url+'/MobileApps/Common/images/pole.png';
				} else if (myData.o_status === '0') {
					status = 'On';
					icon = 'images/' +myData.enettype + '_' + 'G.png';
				} else if (myData.o_status === '1') {
					status = 'Predicted As Out';
					icon = 'images/' +myData.enettype + '_' + 'Y.png';
				} else if (myData.o_status === '2') {
					status = 'Downline From a Prediction';
					icon = 'images/' +myData.enettype + '_' + 'DY.png';
				} else if (myData.o_status === '3') {
					status = 'Out';
					icon = 'images/' +myData.enettype + '_' + 'R.png';
				} else if (myData.o_status === '4') {
					status = 'Downline From an Outage';
					icon = 'images/' +myData.enettype + '_' + 'DR.png';
				}		
				var data = {
				  icon: icon,
				  position: {
				    lat: myData.lat,
				    lng: myData.lon
				  },
				  draggable: true,
				  disableAutoPan: true,
				  zIndex: 120,
				  myData: myData
				};
				//vehicles[i].popup = plugin.google.maps.InfoWindow({content: content, maxWidth: 400});
				googleMap.addMarker(data, cb);
				// ------------------  POPULATE COLLAPSIBLE SIDE MENU --------------------------------------------------------
				html = '<div data-role="collapsible" data-collapsed="true">';
				html += '<h3 onclick="javascript:showSearchResultDetails(' + idx + ');"><img src="' + icon + '"/>' + myData.label + '</h3>';
				html += '<div data-role="content"  class="scrollable"><table id="searchresultstable_' + idx + '">';
				if (myData.enetworkid != null) {
					html += '<tr><td class="bold linesect_prompt">Linesection: </td><td class="linesect_value">' + myData.enetworkid + '</td></tr>';
				}
				if (myData.pole_id != null) {
					html += '<tr><td class="bold pole_prompt">Pole: </td><td class="pole_value">' + myData.pole_id + '</td></tr>';
				}
				if (myData.map_loc != null) {
					html += '<tr><td class="bold maploc_prompt">Map Location: </td><td class="maploc_value">' + myData.map_loc + '</td></tr>';
				}
				if (myData.size != null) {
					html += '<tr><td class="bold maploc_prompt">Size: </td><td class="maploc_value">' + myData.size + '</td></tr>';
				}
				if (myData.enettype != null) {
					html += '<tr><td class="bold">Device Type: </td><td>' + myData.enettype + '</td></tr>';
				}
				html += '<tr><td class="bold">Distance: </td><td>' + parseFloat(myData.distance).toFixed(2) + ' meters</td></tr>';
				if (myData.enettype !== 'POLE') {
					html += '<tr><td class="bold phase_prompt">Phase: </td><td class="phase_value">' + myData.phase + '</td></tr>';
					if (myData.enettype === 'PL' || myData.enettype === 'SL') {
						html += '<tr><td class="bold">Placement: </td><td>' + myData.overhead + '</td></tr>';
					}
					html += '<tr><td class="bold">Status: </td><td>' + status + '(' + myData.o_status + ')</td></tr>';
					html += '<tr><td class="bold subfdr_prompt">Sub-Fdr: </td><td class="subfdr_value">' + myData.subfdr + '</td></tr>';
					html += '<tr><td class="bold">Parent Device: </td><td>' + myData.parent_enetworkid + '</td></tr>';		
				}
				var key = myData.gistablename;
				if (msg.labels[key] !== undefined && msg.labels[key]['label1'] !== null && msg.labels[key]['label1'].length > 0) {
					html += '<tr><td class="bold">' + msg.labels[key]['label1'] + ': </td><td>' + myData.label1 + '</td></tr>';
				}
				if (msg.labels[key] !== undefined && msg.labels[key]['label2'] !== null && msg.labels[key]['label2'].length > 0) {
					html += '<tr><td class="bold">' + msg.labels[key]['label2'] + ': </td><td>' + myData.label2 + '</td></tr>';
				}
				if (msg.labels[key] !== undefined && msg.labels[key]['label3'] !== null && msg.labels[key]['label3'].length > 0) {
					html += '<tr><td class="bold">' + msg.labels[key]['label3'] + ': </td><td>' + myData.label3 + '</td></tr>';
				}
				if (can_create_incident !== 'FALSE' && appname == 'LinemanApp' && myData.enettype !== 'POLE') {
					html += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" style="width:200px;" onclick="javascript:createDeviceOutage(' + idx + ',\'search\');clearSearchResults(\'clearExistingBottomMenus\');">Create Outage</button></td></tr>';
				}
				if (params.hasWFM == 'TRUE') {
					html+='<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" style="width:200px;" onclick="javascript:createOrder(' + idx + ',\'search\');clearSearchResults(\'clearExistingBottomMenus\');">Create Work Order</button></td></tr>';
				}
				html += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" style="width:200px;" onclick="javascript:getDirections(\'' + idx + '\',\'search\');">Get Directions</button></td></tr>';
		
				if (appname == 'AssessDamage') {
					html += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" style="width:200px;" onclick="javascript:reportDamage(\'' + idx + '\',\'search\');">Report Damage</button></td></tr>';
				}
                html += '</table>';
                html += '</div></div>';
                $("#searchresults_list").append(html);
                $('#searchresults_list').trigger('create');
				$("#searchresults_list button").parent().css({'max-width':'95%'});
				$("#searchresults_list select").parent().css({'max-width':'95%'});
				$("#searchresults_list textarea").css({'max-width':'95%'});
				$("#searchresults_menu").css({position: 'absolute', top: $("#mapheader").height()+ios7Pad, left: 0, height: $('#map_canvas').height() + 'px', 'max-width': '400px', width:($(window).width()*.85)+'px',opacity:.90});
				fixTitlesBySvcIdx(service_index);
				$("#searchresults_menu").show();
				idx++;

			}, function(markers) {
                if(markers.length == 0) {
                    return;
                }
				searchresults = markers;
				markers.forEach(function(marker) {
				  marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);				  
				});
                avglat = avglat/markers.length;
                avglng = avglng/markers.length;
                googleMap.fromLatLngToPoint({lat:avglat,lng:avglng},
                    function(pixels){
                        pixels[0] = pixels[0] - 200;
                        var newcenter = googleMap.fromPointToLatLng(pixels, function(newcenter){
                            googleMap.animateCamera( {
                                'target': newcenter,
                                'tilt':googleMap.getCameraTilt(),
                                'zoom': googleMap.getCameraZoom(),
                                'bearing':googleMap.getCameraBearing(),
                                'duration':1000
                            });
                        });
                    }
                );
			});

			function onMarkerClick() {
				var marker = this;
				var myData = marker.get("myData");
                var content = '<div style="width:375px;"> '+
               '<div style="float:right;" id="closeBtn"><img src="images/close.png"/></div>';
				var status = 'Out';
				if (myData.o_status === '0') {
					status = 'On';
				}
				if (myData.o_status === '3') {
					status = 'Predicted As Out';
				}
				if (myData.enetworkid != null) {
					content += '<span class="linesect_prompt">Linesection: </span><span class="linesect_value">' + myData.enetworkid + '</span><br>';
				}
				if (myData.pole_id != null) {
					content += '<span class="pole_prompt">Pole: </span><span class="pole_value">' + myData.pole_id + '</span><br>';
				}
				if (myData.map_loc != null) {
					content += '<span class="maploc_prompt">Map Location: </span><span class="maploc_value">' + myData.map_loc + '</span><br>';
				}
				if (myData.size != null) {
					content += '<span class="maploc_prompt">Size: </span><span class="maploc_value">' + myData.size + '</span><br>';
				}
				if (myData.enettype != null) {
					content += 'Device Type: ' + myData.enettype + '<br>';
				}
				content += 'Distance: ' + parseFloat(myData.distance).toFixed(2) + ' meters<br>';
				if (myData.enettype !== 'POLE') {
					content += '<span class="bold phase_prompt">Phase: </span><span class="phase_value">' + myData.phase + '</span><br>';
					if (myData.enettype === 'PL' || myData.enettype === 'SL') {
						content += 'Placement: ' + myData.overhead + '<br>';
					}
					content += 'Status: ' + status + '(' + myData.o_status + ')<br>';
					content += '<span class="subfdr_prompt">Sub-Fdr: </span><span class="subfdr_value">' + myData.subfdr + '</span><br>';
					content += 'Parent Device: ' + myData.parent_enetworkid + '<br>';
				}
				var key = myData.gistablename;
				if (msg.labels[key] !== undefined && msg.labels[key]['label1'] !== null && msg.labels[key]['label1'].length > 0) {
					content += msg.labels[key]['label1'] + ': ' + myData.label1 + '<br>';
				}
				if (msg.labels[key] !== undefined && msg.labels[key]['label2'] !== null && msg.labels[key]['label2'].length > 0) {
					content += msg.labels[key]['label2'] + ': ' + myData.label2 + '<br>';
				}
				if (msg.labels[key] !== undefined && msg.labels[key]['label3'] !== null && msg.labels[key]['label3'].length > 0) {
					content += msg.labels[key]['label3'] + ': ' + myData.label3 + '<br>';
				}
				if (can_create_incident !== 'FALSE' && appname == 'LinemanApp' && myData.enettype !== 'POLE') {
					content += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" style="width:183px;display:inline;margin:2px;" onclick="javascript:createDeviceOutage(' + myData.index + ',\'search\');clearSearchResults(\'clearExistingBottomMenus\');">Create Outage</button></td></tr>';
				}
				if (params.hasWFM == 'TRUE') {
                content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" style="width:183px;display:inline;margin:2px;" onclick="javascript:createOrder(' + myData.index + ');">Create Work Order</button>';
                }
                content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" style="width:183px;display:inline;margin:2px;" onclick="javascript:getDirections(\'' + myData.index + '\',\'search\');">Get Directions</button>';
                if (myData.enettype != null && myData.enettype != 'POLE') {
                    content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-u" style="width:183px;display:inline;margin:2px;" onclick="javascript:traceLine(\''+myData.line_sect+'\',\'up\');">Trace Upline</button>';
                    if ( myData.enettype != 'MTR') {
                        content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-d" style="width:183px;display:inline;margin:2px;" onclick="javascript:traceLine(\''+myData.line_sect+'\',\'down\');">Trace Downline</button>';
                    }
                }
                if (appname == 'AssessDamage') {
                    content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" style="width:183px;display:inline;margin:2px;" onclick="javascript:reportDamage(\'' + myData.index + '\',\'search\');">Report Damage</button>';
                }
                //content += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-eye" onclick="javascript:streetView('+myData.lat+', '+myData.lon+');">Street View</button></td></tr>';
                content += '</div>';
                infoWindow.setContent(content,{width:'375px'});
                fixTitlesBySvcIdx(service_index);
				infoWindow.open(marker);
				setTimeout( function() {
					$("#closeBtn").on("click", function() {
						infoWindow.close();
					});
				},500);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			if ( textStatus == 'timeout') {
				setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'common.findEquipment'});
			}
			$("#waiting").hide();
			console.error( textStatus + errorThrown);
			jAlert(errorThrown);
		}
	});
	search_value=null;
}
/*----------------------------------------------------------------------------*/

function fillInAddress() {
    // Get the place details from the autocomplete object.
    /*var place = autocomplete.getPlace();

    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
        }
    }*/
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (bgGeo != null) {
        bgGeo.getCurrentPosition(function(position) {
             var geolocation = {lat:position.coords.latitude, lng:position.coords.longitude};
             currLat=position.coords.latitude;
             currLon=position.coords.longitude;
             if (autocomplete != undefined) {
             	autocomplete.setBounds(plugin.google.maps.LatLngBounds(geolocation,geolocation));
             }
         });
    }else{
    	jAlert("GPS must be turned on to locate you");
    }
}
function findStreet() {
    $("#search_menu").hide();
	$("#searchresults_list").html('');
    var distance = (google.maps.geometry.spherical.computeDistanceBetween (plugin.google.maps.LatLng(currLat,currLon), autocomplete.getPlace().geometry.location)*0.000621371).toFixed(2);
    searchresults = [{data:{lat:autocomplete.getPlace().geometry.location.lat(),lon:autocomplete.getPlace().geometry.location.lng(),streetname:autocomplete.getPlace().formatted_address, distance:distance}}];

    var place = autocomplete.getPlace();
	var request = {
    reference: place.reference
    };

    var service = plugin.google.maps.places.PlacesService(googleMap);

    service.getDetails(request, function(place, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
        }
    });
}
/*----------------------------------------------------------------------------*/
function showSearchResultDetails(elem) {
    googleMap.setCameraZoom(parseInt(18));
    var pixels = googleMap.fromLatLngToPoint({lat:searchresults[elem].get('myData').lat,lng:searchresults[elem].get('myData').lon},
        function(pixels){
            pixels[0] = parseFloat(pixels[0])-200;
            var newcenter = googleMap.fromPointToLatLng(pixels, function(newcenter){
                googleMap.animateCamera( {
                'target': newcenter,
                'tilt':googleMap.getCameraTilt(),
                'zoom': parseInt(18),
                'bearing':googleMap.getCameraBearing(),
                'duration':500
                }, function() {
                    searchresults[elem].trigger(plugin.google.maps.event.MARKER_CLICK);
                });
            });
        }
    );
}
/*----------------------------------------------------------------------------*/
function addInstructions(type) {
    addType=type;
    if (addType == 'alert'){
        $('#connectionmessage').html('Long Touch a spot on the screen to add an alert point there.');
    }else{
        $('#connectionmessage').html('Long Touch a spot on the screen to add equipment there.');
    }
	$('#connectionmessage').show();
	setTimeout(function() {
		$('#connectionmessage').fadeOut('slow');
	}, 4000);
}
/*----------------------------------------------------------------------------*/
function getIcon(device, state) {
	if (device == 'XFM') {
		device='TRN';
	}
	if (device != null && device.indexOf('SW')  > -1) {
		device='SW';
	}
	if (device == undefined) {
		device = 'null';
	}
	if (state === 1) {
		return device + '_R';
	}
	if (state === 3) {
		return device + '_Y';
	}
	if (state === 0) {
		return device + '_G';
	}
	if (state === 2) {
		return device + '_DY';
	}
	if (state === 4) {
		return device + '_DR';
	}
	return device + '_B';
}
/*----------------------------------------------------------------------------*/
var messages;
function popMessagingMenu() {
	$("#messages_list").html('');
	$("#messaging_menu").css({position: 'absolute', top: $('#map_canvas').offset().top, left: 0, height: $('#map_canvas').height() + 'px', 'max-width': '550px', width:($(window).width()*.85)+'px'});
	clearExistingBottomMenus('messaging_menu');
    if (navigator.connection.type == Connection.NONE) {
        $('#connectionmessage').hide();
        $('#connectionmessage').html('Unable to message with no internet connection');
        $('#connectionmessage').show();
        $('#blockingcanvas').hide();
        setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 3000);
        return;
    }

	$.ajax({
           type: "GET",
           url: localStorage.getItem('ims_url') + "/GoogleMapViewer/ajax/ajaxGoogleMapViewer.php",
           data: {action: 'get', target: 'messages', user: user},
           dataType: "jsonp",
           success: function(data) {
           if (!data.result || data.result !== 'true') {
               alert(data.error);
               return;
           }
           messages = data.messages;
           for (var messager in messages) {
               var html = '<div data-role="collapsible" data-collapsed="true" data-mini="true">';
               if (messager != '' && messager != null && messager !=user && messages[messager].employee_name != undefined){

                   if (messages[messager].num_unacked > 0) {
                       html += '<h3>' + messages[messager].employee_name + '&nbsp;&nbsp;<span style="color:red;">('+messages[messager].num_unacked+')</span></h3>';
                   }else {
                       html += '<h3>' + messages[messager].employee_name + '</h3>';
                   }
                   html += '<table id="message_table_' + messager + '" style="table-layout:fixed;width:500px;">';
                   html += '<tr><th>'+messages[messager].employee_name+'</th><th>'+user+'</th></tr>';
                   for (var i = 0 ; i < messages[messager].messages.length ; i++){
                       if (messages[messager].messages[i].sent_by_userid == messager) {
                           html+= '<tr><td style="background-color:rgba(0, 153, 153, 0.5);width:225px;" align="left">';
                           if (messages[messager].messages[i].subject != null) {
                               html+='Subj:'+messages[messager].messages[i].subject+'<br>';
                           }
                           html+=messages[messager].messages[i].message+'<br><span style="font-size:8pt;font-style:italic;">'+messages[messager].messages[i].sent+'</span></td></tr>';
                       }else{
                           html+= '<tr><td> </td><td style="background-color:rgba(102, 204, 0, 0.5);width:225px;">';
                           if (messages[messager].messages[i].subject != null) {
                               html+='Subj:'+messages[messager].messages[i].subject+'<br>';
                           }
                           html+=messages[messager].messages[i].message+'<br><span align="right" style="font-size:8pt;font-style:italic;text-align:right;">'+messages[messager].messages[i].sent+'</span></td></tr>';
                       }
                   }

                   html +='</tr>';
                   html +='<tr><td onclick="javascript:acknowledge(\'' + messager + '\');"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" style="width:200px;">Acknowledge All</button></td></tr>'
                   html +='<tr><td colspan="2"> <input type="text" id="new_message_'+messager+'"> </td></tr><tr><td onclick="javascript:sendMessage(\'' + messager + '\');"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-mail" style="width:200px;">Send</button></td>'
                   html += '</table>';

                   html += '<input type="hidden" value="'+messages[messager].employee_number+'" id="emp_num_'+messager+'">';
                   html += '<input type="hidden" value="'+messages[messager].user_employee_number+'" id="emp_num_'+user+'">';
                   html += '</div>';

                   $("#messages_list").append(html);
               }
           }
           $('#messages_list').trigger('create');
           $("#messages_list button").parent().css({'max-width':'95%'});
           $("#messages_list input").parent().css({'max-width':'95%'});
           $("#messages_list textarea").css({'max-width':'95%'});
           },
           error: function(XMLHttpRequest, textStatus, errorThrown) {
           alert("Can't get messages. Error: "+errorThrown);
           }
           });
      push.setApplicationIconBadgeNumber(function(){console.log("clear badge");}, function(){console.log("failed to clear badge");},0);
}
/*----------------------------------------------------------------------------*/
function acknowledge(messager) {
    $.ajax({
       type: "GET",
       url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php",
           data: {action: 'acknowledge', target: 'messages', user: user, sent_by:messager, appname: appname},
       dataType: "jsonp",
       success: function(data) {
           if (!data.result || data.result !== 'true') {
           alert(data.error);
           return;
           }
           popMessagingMenu();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#waiting").hide();
            console.error( textStatus + errorThrown);
            jAlert("Couldn't acknowledge message: "+errorThrown);
        }
    });
}
/*----------------------------------------------------------------------------*/
function sendMessage(messager) {
	if ($("#new_message_"+messager).val() == "") { return; }
  var ajaxParams={action: 'sendMessage', to: messager, subject: '', message: $("#new_message_"+messager).val(), sent_by_userid: user, user: user, sent_to_employee_number: $("#emp_num_"+messager).val(),sent_origin: 'B', send_priority: 4, sent_by_employee_number:$("#emp_num_"+user).val()};
  if (navigator.connection.type == Connection.NONE) {
      storeAjaxRequest(localStorage.getItem('ims_url') + "/dvwfm2/vehicle/ajaxSendMessage.php",ajaxParams);
      return;
  }
	$.ajax({
           type: "GET",
           url: localStorage.getItem('ims_url') + "/dvwfm2/vehicle/ajaxSendMessage.php",
           data: ajaxParams,
           dataType: "jsonp",
           success: function(msg) {
           if (msg.result === 'false') {
           jAlert(msg.error);
           return;
           }
           $.ajax({
                  type: "GET",
                  url: localStorage.getItem('ims_url') + "/GoogleMapViewer/ajax/ajaxGoogleMapViewer.php",
                  data: {action: 'get', target: 'messages', user: user, messager:messager},
                  dataType: "jsonp",
                  success: function(msg) {
                  if (msg.result === 'false') {
                  jAlert(msg.error);
                  return;
                  }
                  messager=msg.messager;
                  messages[messager].messages=msg.messages;
                  var html='';
                  for (var i = 0 ; i < messages[messager].messages.length ; i++){
                  if (messages[messager].messages[i].sent_by_userid == messager) {
                  html+= '<tr><td style="background-color:rgba(0, 153, 153, 0.5);width:225px;" align="left">';
                  if (messages[messager].messages[i].subject != null) {
                  html+='Subj:'+messages[messager].messages[i].subject+'<br>';
                  }
                  html+=messages[messager].messages[i].message+'<br><span style="font-size:8pt;font-style:italic;">'+messages[messager].messages[i].sent+'</span></td></tr>';
                  }else{
                  html+= '<tr><td> </td><td style="background-color:rgba(102, 204, 0, 0.5);width:225px;">';
                  if (messages[messager].messages[i].subject != null) {
                  html+='Subj:'+messages[messager].messages[i].subject+'<br>';
                  }
                  html+=messages[messager].messages[i].message+'<br><span align="right" style="font-size:8pt;font-style:italic;text-align:right;">'+messages[messager].messages[i].sent+'</span></td></tr>';
                  }
                  }
                  html+='</tr>';
                  html +='<tr><td><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" onclick="javascript:acknowledge(\'' + messager + '\');" style="width:200px;">Acknowledge All</button></td></tr>';
                  html +='<tr><td colspan="2"> <input type="text" id="new_message_'+messager+'"> </td></tr>';
                  html+='<tr><td><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-mail" onclick="javascript:sendMessage(\'' + messager + '\');" style="width:200px;">Send</button></td>';
                  $("#message_table_" + messager).html(html);
                  $("#message_table_" + messager).trigger("create");
                  },
                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                  $("#waiting").hide();
                  console.error( textStatus + errorThrown);
                  jAlert("Failed to get messages. Error: "+errorThrown);
                  }
                  });
           $("#waiting").hide();
           },
           error: function(XMLHttpRequest, textStatus, errorThrown) {
           $("#waiting").hide();
           console.error( textStatus + errorThrown);
           jAlert("Failed to send your message. Error: "+errorThrown);
           }
           });
	$("#new_message_"+messager).val('');
}

/*----------------------------------------------------------------------------*/
function sendVehicleMessage() {
	var index = $("#vehicle_index").val();
    var ajaxParams = {action: 'sendMessage', sent_origin: 'M', send_priority: 0, sent_to_employee_number:vehicles[index].get('myData').employee_number ,to: vehicles[index].get('myData').userid, message: $("#v_message").val(), sent_by_userid: user, user: user, device_type: platform};
    if (navigator.connection.type == Connection.NONE) {
        storeAjaxRequest(localStorage.getItem('ims_url') + "/dvwfm2/vehicle/ajaxSendMessage.php",ajaxParams);
        return;
    }
	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url') + "/dvwfm2/vehicle/ajaxSendMessage.php",
        data: ajaxParams,
		dataType: "json",
		success: function(msg) {
			if (msg.result === 'false') {
				jAlert(msg.error);
				return;
			}
			$("#waiting").hide();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			console.error( textStatus + errorThrown);
			jAlert("Failed to send your message. Error: "+errorThrown);
		}
	});
	$("#v_message").val('');
}
/*----------------------------------------------------------------------------*/
function loadBackoffice() {

}
/*----------------------------------------------------------------------------*/
function sendBackofficeMessage() {
    var ajaxParams = {action: 'sendMessage', sent_to_employee_number:'backoffice', to: 'backoffice', subject: $("#b_subject").val(), message: $("#b_message").val(), sent_by_userid: user, user: user, sent_origin: 'M', send_priority: $("#b_priority").val(),sent_by_employee_number:user_number};
    if (navigator.connection.type == Connection.NONE) {
        storeAjaxRequest(localStorage.getItem('ims_url') + "/dvwfm2/vehicle/ajaxSendMessage.php",ajaxParams);
        return;
    }
	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url') + "/dvwfm2/vehicle/ajaxSendMessage.php",
           data: ajaxParams,
		dataType: "json",
		success: function(msg) {
			if (msg.result === 'false') {
				jAlert(msg.error);
				return;
			}
			$("#waiting").hide();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#waiting").hide();
			console.error( textStatus + errorThrown);
			jAlert("Failed to send your message. Error: "+errorThrown);
		}
	});
	$("#b_message").val('');
	$("#b_subject").val('');
	$("#b_priority").val(5);
	$("#send_backoffice_message").hide();
}
/*----------------------------------------------------------------------------*/
function setBackOfficeDateElem(id){
	var close_item = $("#close_item");
	$("#"+id.replace('_ios',''), close_item).val($("#"+id, close_item).val());
}


function padDigits(number, digits) {
	return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}
function checkdate(value) {
	if(value.length < 10) {
		return false;
	}
	value = value.substr(0,10);	// We only want the date portion
	var IsoDateRe = new RegExp("^([0-9]{4})[-/]([0-9]{2})[-/]([0-9]{2})$");
	if(IsoDateRe.test(value) == false) {
		return false;
	}
	else { //Detailed check for valid date ranges
		var yearfield = value.split("/")[0];
		var monthfield = value.split("/")[1];
		var dayfield = value.split("/")[2];
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if((dayobj.getMonth()+1 != monthfield) || (dayobj.getDate() != dayfield) || (dayobj.getFullYear() != yearfield)) {
			return false;
		}
	}
	return true
}
function getCurrentMilitaryDate() {
	var dt = new Date();
	return dt.getFullYear()+'/'+padDigits(dt.getMonth()+1,2)+"/"+padDigits(dt.getDate(),2);
}

function createServiceOrder(index){
    clearExistingBottomMenus();
    loadSoForm("Create Work Order",'W');
}
function createWorkOrder(index){
    clearExistingBottomMenus();
    loadSoForm("Create Service Order", 'S');
}
function takePicture(source) {
	if (navigator.connection.type == Connection.NONE) {
		$('#connectionmessage').hide();
		$('#connectionmessage').html("Sorry, we are unable to upload photos when internet is unavailable.  You can open your camera and take photos.  Then when internet connectivity is available, you can upload the photos from your photo library.");
		$('#connectionmessage').show();
		$('#blockingcanvas').hide();
		setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 9000);
		if (reopenPopup) {
			reopenPopup=false;
			$("#inspect_data_page").popup('open');
		}
		return;
	}
    navigator.camera.getPicture(onSuccess, onFail, {
                                destinationType: Camera.DestinationType.FILE_URI,
                                quality:10,
                                targetWidth: 1000,
                                targetHeight: 1000,
                                sourceType: source});
                                /*destinationType: Camera.DestinationType.DATA_URL
                                source: source});*/
}
function onSuccess(imageData) {
    console.log("picture success");
    var image = document.getElementById('show-picture');
    image.src = imageData;
    //image.src = "data:image/jpeg;base64," + imageData;
}

function onFail(message) {
    console.log('Failed because: ' + message);
}/*if (takePicture && showPicture) {
 // Set events
 takePicture.onchange = function (event) {
 console.log('onchange for take-picture');
 // Get a reference to the taken picture or chosen file
 var files = event.target.files,
 picturefile;
 if (files && files.length > 0) {
 picturefile = files[0];
 try {
 // Get window.URL object
 var URL = window.URL || window.webkitURL;

 // Create ObjectURL
 var imgURL = URL.createObjectURL(picturefile);


 window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
 window.storageInfo = window.storageInfo || window.webkitStorageInfo;

 // Request access to the file system
 var fileSystem = null         // DOMFileSystem instance
 , fsType = PERSISTENT       // PERSISTENT vs. TEMPORARY storage
 , fsSize = 10 * 1024 * 1024 // size (bytes) of needed space
 ;

 window.storageInfo.requestQuota(fsType, fsSize, function(gb) {
 window.requestFileSystem(fsType, gb, function(fs) {
 fileSystem = fs;
 }, errorHandler);
 }, errorHandler);
 fileSystem.root.getFile(path, {create: true}, function(fileEntry) {
 fileEntry.createWriter(function(writer) {
 writer.write(data);
 }, errorHandler);
 }, errorHandler);
 // Set img src to ObjectURL
 console.log('setting source of show-picture to '+imgURL);
 showPicture.src = imgURL;
 // Revoke ObjectURL
 //URL.revokeObjectURL(imgURL);
 }
 catch (e) {
 try {
 console.log('Fallback if createObjectURL is not supported');
 var fileReader = new FileReader();
 fileReader.onload = function (event) {
 showPicture.src = event.target.result;
 };
 fileReader.readAsDataURL(picturefile);
 }
 catch (e) {
 //
 var error = document.querySelector("#error");
 if (error) {
 error.innerHTML = "Neither createObjectURL or FileReader are supported";
 }
 }
 }
 }
 };
 }
 }*/

function uploadPicture(lat,lon) {
	if ($('#pic_location').val() != 'work_item') {
		if ( lat == undefined && lon == undefined) {
			getBgGps("uploadPicture");
			return
		}else{
			currLat = lat;
			currLon = lon;
		}
	}
  var dateobj = new Date();
  var parser=$("#pic_work_item_number").val().split('-');
  var index=parser[0];
  var type=parser[1];
  var picLat = currLat;
  var picLon = currLon;
  if (type == 'crews') {
      var work_item_number=crews[index].get('myData').work_item_number;
      var work_type=crews[index].get('myData').work_item_type;
      if ($('#pic_location').val() == 'work_item') {
          if (crews[index].get('myData').lat) {
              picLat =crews[index].get('myData').lat;
              picLon =crews[index].get('myData').lon;
          }
      }
  }else if ( index != "" ) {
      var work_item_number=allitems[index].get('myData').work_item_number;
      var work_type=allitems[index].get('myData').work_item_type;
      if ($('#pic_location').val() == 'work_item') {
          if (allitems[index].lat != null) {
              picLat =allitems[index].lat;
              picLon =allitems[index].lon;
          }
      }

  }
  var url=encodeURI(localStorage.getItem('ims_url')+"/dvWfm2/remoteServices/upload.php");
  var options = new FileUploadOptions();
  options.fileKey = user+'_'+dateobj.getTime(); //depends on the api
  var imageUriToUpload=$("#show-picture").attr('src');
  options.fileName = imageUriToUpload.substr(imageUriToUpload.lastIndexOf('/')+1);
  //options.fileName = user+'_'+dateobj.getTime();
  options.mimeType = "image/jpeg";
  options.params = { name:user, comments:$("#pic_comments").val(), lat:picLat, lon:picLon, work_item_number:work_item_number, work_type:work_type, identifier:$("#pic_desc").val(), appname:appname};
  options.chunkedMode = true; //this is important to send both data and files

  var headers={'Authorization':"Basic" };
  options.headers = headers;
  $("#waiting").show();
  var ft = new FileTransfer();
  ft.upload(imageUriToUpload, url, win, failUpl, options);

  //params.date =  results.rows.item(i).datetime;
  /*options.params = { name:user, comments:$("#pic_comments").val(), lat:currLat, lon:currLon, work_item_number:work_item_number, work_type:work_type, identifier:$("#pic_desc").val(), accuracy:currAccuracy};
   var ft = new FileTransfer();
   jAlert("calling upload on file: "+$("#show-picture").attr('src'));
   try{
   ft.upload($("#show-picture").attr('src'), localStorage.getItem('ims_url')+"/dvWfm2/remoteServices/upload.php", win, failUpl, options);

   }catch(e) {
   //alert("Something failed!");
   URL.revokeObjectURL($("#show-picture").attr('src'));
   jAlert(e.message.toString());
   }*/


}
function win(r) {
    //alert("win");
    //alert("Code = " + r.responseCode+" / Response = " + r.response+ " / Sent = " + r.bytesSent);
    $("#waiting").hide();
    $("#show-picture").hide();
    URL.revokeObjectURL($("#show-picture").attr('src'));
    $("#pic_desc").val('');
    $('#pic_comments').val('');
    $("#show-picture").attr('src', '');
    $("#pic_work_item_number").val('');
    $("#pic_work_item_number").selectmenu();
    jAlert(r.response);
    refreshGoogleMap();
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
	$("#show-picture").show();
    clearExistingBottomMenus();
    if (reopenPopup) {
    	reopenPopup=false;
    	$("#inspect_data_page").popup('open');
    }
}
function failUpl(error) {
    //alert("fail");
    $("#waiting").hide();
    $("#show-picture").hide();
    URL.revokeObjectURL($("#show-picture").attr('src'));
    $("#pic_desc").val('');
	$('#pic_comments').val('');
    $("#show-picture").attr('src','');
    $("#show-picture").hide();
    $("#pic_work_item_number").val('');
    $("#pic_work_item_number").selectmenu();
    jAlert("An error has occurred: Code = " + error.code+" / upload error source " + error.source+" / upload error target " + error.target);
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
    $("#show-picture").show();
    clearExistingBottomMenus();
}
/* ---------------------------------------------------------------------------*/
function showDivCentered(id) {
    $("#"+id).css({'position': 'absolute', 'top': windowheight*.5-($("#"+id).height()/2), 'left': windowwidth*.5-($("#"+id).width()/2), 'z-index': 9999});
    $("#"+id).show();
}
/*----------------------------------------------------------------------------*/
var fakeMarker = null;
var labels=null;
var alldata = null;
function fakeSearch(lat,lon,zoom){
    $("#closeBtn").trigger("click");
    if ( fakeMarker != null && $.isFunction(fakeMarker.remove)) {
    	fakeMarker.remove();
    }
    $("#waiting").show();
    $.ajax({
       type: "GET",
       url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php",
       data: {action: 'get', target: 'fake_search', lat: lat, lon: lon, zoom: parseInt(zoom), appname: appname, service_name: service_name},
       dataType: "json",
       timeout:10000,
       success: function(msg) {
       	 setOMSConnection(true);
         if (msg.result === 'false') {
					if (msg.error.indexOf('longitude exceeded limits')) {
						return;
					}
					alert(msg.error);
					return;
         }
         $("#waiting").hide();
         labels = msg.labels;
         alldata = msg.data;
         showFakeSearchResults(0);
       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
		if( textStatus == 'timeout') {
			setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'common.fakeSearch'});
		}
		$("#waiting").hide();
		console.error( textStatus + errorThrown);
		console.log("Failed to find item at touch point. Error: "+errorThrown);
       }
   });
}
/*----------------------------------------------------------------------------*/
function showFakeSearchResults(index){
	var data = alldata;
	if (fakeMarker != null && typeof(fakeMarker) == 'object') {
		fakeMarker.remove();
	}
	var baseArray = new plugin.google.maps.BaseArrayClass(data);
	var infoWindow = new plugin.google.maps.HtmlInfoWindow();
	var idx = 0;
	baseArray.map(function(myData, cb) {
		myData.index = idx;
		idx++;
		var data = {
			icon: 'images/close.png',
			position: {
			  lat: myData.lat,
			  lng: myData.lon
			},
			disableAutoPan: false,
			zIndex: 5,
			visible: index == myData.index,
			myData: myData
		};
		//if (index == myData.index) {
			googleMap.addMarker(data, cb);
		//}

	}, function(markers) {

		fakeMarker = markers[index];
		searchresults = markers;
		markers.forEach(function(marker) {
			marker.on(plugin.google.maps.event.MARKER_CLICK, onMarkerClick);
			if (index == marker.get('myData').index) {
				onMarkerClick();
			}else{
				marker.remove();
			}
		});
	});
	function onMarkerClick() {
		$("#closeBtn").trigger("click");
		var data = fakeMarker.get("myData");
		var ping_type='device';
		var content = '<table> <tr><td></td><td id="closeBtn" align="right"><img src="images/close.png"/></td></tr>' ;
		if (data == undefined ) {
			return;
		}
		if (data != undefined && data.enetworkid != undefined && data.enetworkid != null){
			  content += '<tr><td><b class="linesect_prompt">Line Section:</b></td><td><span class="linesect_value"> '+data.enetworkid+'</span></td></tr>';
		}
		if (data.enettype != null){
			  content+='<tr><td><b>Device Type: </b></td><td>'+data.enettype+'</td></tr>';
		}
		if (data.subfdr != null){
			  content+='<tr><td><b class="bold subfdr_prompt">Sub-Fdr:</b></td><td><span class="subfdr_value"> '+data.subfdr+'</b></td><td></td></tr>';
		}
		if (data.PhasesUsed != null && data.PhasesUsed != 'N/A'){
			  content+='<tr><td><b class="bold phase_prompt">Phase:</b></td><td><span class="phase_value"> '+data.PhasesUsed+'</b></td><td></td></tr>';
		}
		if (data.map_loc != null){
			  content+='<tr><td><b class="bold maploc_prompt">Map Location:</b></td><td><span class="maploc_value"> '+data.map_loc+'</b></td><td></td></tr>';
		}
		if (data.outage_type == 'CONSUMER') {
			  content+='<tr><td><b>Name:</b></td><td> '+data.name+'</td></tr>';
			  content+='<tr><td><b class="bold account_prompt">Account Number:</b></td><td><span class="account_value"> '+data.accountnumber+'</b></td><td></td></tr>';
			  content+='<tr><td><b class="bold meter_prompt">Meter Number:</b></td><td><span class="meter_value"> '+data.meternumber+'</b></td><td></td></tr>';
			  content+='<tr><td><b>Address:</b></td><td> '+data.address+'</td></tr>';
			  content+='<tr><td><b>Phone:</b></td><td> '+data.phone+'</td></tr>';
		}else {
			  if (data.name != null) {
			      content+='<tr><td><b>Name:</b></td><td> '+data.name+'</td></tr>';
			  }
			  if (data.enettype != 'POLE' && data.SumCustomers != undefined) {
			      content+='<tr><td><b>Total Meters:</b></td><td>:'+data.SumCustomers+'</td></tr>';
			  }
			  if (data.pole_id != null){
			      content+='<tr><td><b class="bold pole_prompt">Pole:</b></td><td><span class="phase_value"> '+data.pole_id+'</span></td></tr>';
			  }
			  if (data.map_loc != null){
			      content+='<tr><td><b class="bold maploc_prompt">Map Location:</b></td><td><span class="maploc_value"> '+data.map_loc+'</span></td></tr>';
			  }
			  if (data.size != null){
			      content+='<tr><td><b >Size:</b></td><td><span> '+data.size+'</span></td></tr>';
			  }
		}
		if (labels[data.gistablename].label1 != "" && labels[data.gistablename].label1 != null) {
			  content+='<tr><td><b>'+labels[data.gistablename].label1+':</b></td><td>'+data.label1+'</td></tr>';
		}
		if (labels[data.gistablename].label2 != "" && labels[data.gistablename].label2 != null) {
			  content+='<tr><td><b>'+labels[data.gistablename].label2+':</b></td><td>'+data.label2+'</td></tr>';
		}
		if (labels[data.gistablename].label3 != "" && labels[data.gistablename].label3 != null) {
			  content+='<tr><td><b>'+labels[data.gistablename].label3+':</b>'+data.label3+'</td></tr>';
		}
		content += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-navigation" onclick="javascript:getDirections(\'0\',\'search\');">Get Directions</button>';
		if (data.enettype != null && data.enettype != 'POLE') {
			content += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-u" onclick="javascript:traceLine(\''+data.enetworkid+'\',\'up\');">Trace Upline</button>';
			if ( data.enettype != 'MTR') {
				content += '<tr><td colspan="2"><button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-arrow-d" onclick="javascript:traceLine(\''+data.enetworkid+'\',\'down\');">Trace Downline</button>';
			}
		}//content += '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-eye" onclick="javascript:streetView('+data.lat+', '+data.lon+');">Street View</button>';
	
		if (appname == 'FieldDesign') {
			  if (data.enettype != null && (data.enettype == 'PL' || data.enettype == 'SL')) {
			      content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:$(\'#btnClose\').trigger(\'click\');;splitLine(0,\'search\');">Break Line</button>';
			      content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:$(\'#btnClose\').trigger(\'click\');removeLine(0,\'search\');">Remove Line</button>';
			  }else if (data.enettype != null && data.enettype == 'POLE') {
			      content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:$(\'#btnClose\').trigger(\'click\');editPoleAttachments(0,\'search\');">Add Attachments</button>';
			      content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:$(\'#btnClose\').trigger(\'click\');movePole(0,\'search\');">Move Pole</button>';
			      content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:$(\'#btnClose\').trigger(\'click\');removePole(0,\'search\');">Remove Pole</button>';
			  }else{
			      content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:$(\'#btnClose\').trigger(\'click\');moveEquipment(0,\'search\');">Move Equipment</button>';
			      content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:$(\'#btnClose\').trigger(\'click\');removeEquipment(0,\'search\');">Remove Equipment</button>';
			  }
		} else if (appname == 'AssessDamage') {
			  content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-forbidden" onclick="javascript:reportDamage('+index+',\'search\',\'damage\');">Report Damage</button>';
			  if (data.incident_id == null) {
			   	if (data.outage_type == 'CONSUMER' && can_create_incident  !== 'FALSE' ) {
			   		content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" onclick="javascript:openLinemanApp('+index+',\'search\');">Open In Lineman-App</button>';
			     }else if (can_create_incident !== 'FALSE'){
			     	content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" onclick="openLinemanApp('+index+',\'search\');">Open In Lineman-App</button>';
			     	
			     }
			}
				 
		}else {
			if (data.enettype != null && data.enettype != 'POLE') {
			    if (params.MeterPings == 1  && appname == 'LinemanApp') {
			        content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:$(\'#btnClose\').trigger(\'click\');pingRequest(0,\'search\', \''+ping_type+'\');">Issue Ping</button>';
			    }
			    if (data.incident_id == null || data.incident_id == '') {
			        if (data.outage_type == 'CONSUMER' && can_create_incident !== 'FALSE' && (appname == 'LinemanApp' || appname == 'WaterCrewApp' || appname == 'SewerCrewApp')) {
			            content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" onclick="javascript:$(\'#btnClose\').trigger(\'click\');createIndividualOutage(0,\'search\')">Create Outage</button>';
			        }else if (can_create_incident !== 'FALSE' && appname == 'LinemanApp'){
			            content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" onclick="javascript:$(\'#btnClose\').trigger(\'click\');createDeviceOutage(0,\'search\');">Create Outage</button>';
			        }
			    }else if (can_reassign_items !== 'FALSE' && (appname == 'LinemanApp' || appname == 'WaterCrewApp' || appname == 'SewerCrewApp')) {
			        content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-user" onclick="javascript:reassignId=\''+data.incident_id+'\';assignTicket(\'' + data.incident_id + '\',\'I\', \'individual\',\''+user+'\');">Assign Outage To Me</button>';
			    }
			    
			}
			if (params.hasWFM == 'TRUE') {
				if (data.outage_type == 'CONSUMER'){
					content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" onclick="javascript:$(\'#btnClose\').trigger(\'click\');createOrder(0,\'search\')">Create SO</button>';
				}else{
					content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-check" onclick="javascript:$(\'#btnClose\').trigger(\'click\');createOrder(0,\'search\')">Create WO</button>';
				}
			}
		}
		if( index == 0) {
			for (var i = 1 ; i < alldata.length ; i++) {
				content+='<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-recycle" onclick="javascript:showFakeSearchResults('+i+');">Show '+alldata[i].enettype+'</button>';
			}
		}
		content += '</td></tr>';
	 	infoWindow.setContent(content);
		infoWindow.open(fakeMarker);
		fixTitlesBySvcIdx(service_index);
		/*infoWindow.on(plugin.google.maps.event.INFO_CLOSE, function() {
			console.log("Info window is closed");
			fakeMarker.remove();
		});*/
		setTimeout( function() {
			$("#closeBtn").on("click", function() {
				infoWindow.close();
				fakeMarker.remove();
			});
		},500);
	}
}
/*----------------------------------------------------------------------------*/
function gotoMeterSO(elem) {
	crews[elem].showInfoWindow();
	crewIndex=elem;
	data = crews[elem].get('myData');
	popup = crews[elem];
	if (data.lat== null ||  data.lon == ''){
		$('#connectionmessage').html('There is no location for this item. You can try search by pole number or street address.');
		$('#connectionmessage').show();
		setTimeout(function() {
			$('#connectionmessage').fadeOut('slow');
		}, 4000);
	
	}else{
		 googleMap.animateCamera( {
				'target': {lat:crews[elem].get('myData').lat, lng:crews[elem].get('myData').lon},
				'tilt':0,
				'zoom': parseInt(15),
				'bearing':0,
				'duration':1000
		}, function() {
			popup.trigger(plugin.google.maps.event.MARKER_CLICK);
			fixTitlesBySvcIdx(service_index);
		});
	}
}
/*----------------------------------------------------------------------------*/
function openFromOtherApp() {
	console.log(g_openingUrl);
	//setTimeout(function() { alert(g_openingUrl); },0);
	var paramString = g_openingUrl.split('?');
	//console.log(paramString[1]);
	var paramsJson = '{\"'+paramString[1]+'\"}';
	//console.log(paramsJson);
	paramsJson = paramsJson.replace(/\&/g,'\",\"');
	//console.log(paramsJson);
	paramsJson = paramsJson.replace(/\=/g,'\":\"');
	//console.log(paramsJson);
	g_openingArgs =  JSON.parse(paramsJson);
	if (!loggedin) {
		console.log("not logged in");
		$("#imsu").val(g_openingArgs.user);
		$("#imsp").val(g_openingArgs.pwd);
		curr_crew = g_openingArgs.curr_crew;
		curr_view = g_openingArgs.curr_view;
		login(loggedInFromOtherApp);
	}else {
		console.log("is logged in already");
		loggedInFromOtherApp();
	}
}
function loggedInFromOtherApp() {
	console.log('loggedInFromOtherApp');
	if ($('#'+g_openingArgs.page).is(':visible') && googleMap != null) {
		console.log("map is visible");
		loadedMapMakeCallFromOtherApp();
	}else{
		console.log("change page to "+g_openingArgs.page);
		$.mobile.changePage('#'+g_openingArgs.page); 		
	}
}

/*----------------------------------------------------------------------------*/
function streetView(lat,lon) {
	var panorama = googleMap.getStreetView();
	googleMap.getStreetView().setPosition({lat:parseFloat(lat),lng:parseFloat(lon)});
	if (googleMap.getStreetView().getStatus() == "ZERO_RESULTS") {
		setTimeout(function(){jAlert("No Street View Available");},800);
		return;
	}
    googleMap.getStreetView().setPov({heading: 265, pitch: 10/*0*/});
    googleMap.getStreetView().setVisible(true);
    /*setTimeout(function(){ 
    	if (googleMap.getStreetView().getStatus() == "ZEO_RESULTS") {
    		jAlert("No Street View Available");
    	}
    },1000);*/
}
/*----------------------------------------------------------------------------*/
function createOrder(index,from) {
	item = searchresults[index];
	if (from == 'damage') {
		item = damage[index];
	}
	var html = '<tr><td><b>Order#:</b><span id="order_number"></span></td></tr>';
	html += '<tr><td><b>Description:</b></td></tr>' +
		'<tr><td colspan="2"><TEXTAREA name="comments" id="order_description" rows="2" style="width:380px;"></TEXTAREA></td></tr>';
	//SERVICE ORDER-------------------------------
	var order_type;
	if (item.get('myData').outage_type == 'CONSUMER') {
		order_type='S';
		html += '<tr><td colspan="2"><b>Account#:</b><span id="order_account_number">'+item.get('myData').accountnumber+'</span></td></tr>';
		html += '<tr><td><b>SO Type:</td></b></td></tr><tr><td colspan="2">'+
		 '<select id="so_type" data-theme="a">'+
				'<option value=""></option>'+
				'<option value="Connect">Connect</option>'+
				'<option value="Disconn">Disconnect</option>'+
				'<option value="General">General</option>'+
				'<option value="MeterExchange">Meter Exchange</option>'+
		 '</select></td></tr>';
		 html += '<tr><td><b>SO Code:</td></b></td></tr><tr><td  colspan="2">'+
		 	'<select id="so_code" data-theme="a">'+
				'<option value=""></option>';
		for (var key in params.requires) {
			html += '<option value="' + key + '">' + key + ' - ' + params.requires[key].description + '</option>';
		}
		html += '</select></td></tr>';
	//WORK ORDER-----------------------------------
	}else{
		order_type='W';
		html += '<tr><td><b>WO Type:</b></td></tr><tr><td colspan="2">'+
		 	'<select onchange="Sub_Switch();" id="wo_code" data-theme="a">'+
				'<option value=""></option>';
		for (var key in params.wocodes) {
			html += '<option value="' + key + '">' + key + ' - ' + params.wocodes[key] + '</option>';
		}
		html += '</select></td></tr>';
		html += '<tr><td><b>WO Sub Type:</b></td></tr><tr><td colspan="2">'+
		 	'<select id="wo_subcode" data-theme="a">'+
				'<option value=""></option>';
		html += '</select></td></tr>';
	}
	if (tempServiceAreasData.length > 0 ) {
		html += '<tr><td><b>Service Area:</b></td></tr><tr><td colspan="2">'+
		 	'<select id="servicearea" data-theme="a">'+
				'<option value=""></option>';
		html += tempServiceAreaHtml;
		html += '</select></td></tr>';
	}
	if (tempDepartmentsData.length > 0 ) {
		html += '<tr><td><b>Department:</b></td></tr><tr><td colspan="2">'+
		 	'<select id="department" data-theme="a">'+
				'<option value=""></option>';
		html += tempDepartmentHtml;
		html += '</select></td></tr>';
	}
	html += '<tr><td><b>Comments:</b></td></tr>' +
		'<tr><td colspan="2"><TEXTAREA name="comments" id="order_comments" rows="5" style="width:380px;"></TEXTAREA></td></tr>';
	html +=
        '<tr><td align="right"><b>Auto Dispatch To Me:</b></td>'+
        '<td align="left"><input type="checkbox" value="1" id="dispatch_to_me"></td></tr>';
	html += '<tr><td><button class="ui-btn ui-btn-a ui-btn-icon-left ui-icon-check" style="width:200px;text-align:center;" data-theme="a" onclick="javascript:doCreateOrder('+index+');">Create</button></tr>';
	$("#index").val(index);
	$("#from").val(from);
	$("#create_table").html(html);
	/*$("#so_type").selectmenu();
	$("#so_code").selectmenu();
	$("#wo_code").selectmenu();*/
    if (!(isTablet)) {
        $("#create_container").css({'position': 'absolute', 'top':0, 'left': 0, 'z-index': 9999, 'width':$("#map_canvas").width(), 'height':$("#map_canvas").height(), 'padding':'0px','margin':'0px','overflow':'scroll'});
    }else{
        $("#create_container").css({'position': 'absolute', 'top': $("#mapheader").height()+ios7Pad, 'left': 0, 'z-index': 9999, 'width':$("#map_canvas").width(), 'height':$("#map_canvas").height()-40});
    }
    $("#order_comments").css({'width':$("#create_container").width()-12});
    $("#order_description").css({'width':$("#create_container").width()-12});
    $("#create_container button").parent().css({'max-width':'95%'});
    $("#create_container select").selectmenu().parent().css({'max-width':'95%'});
    $("#create_container textarea").css({'max-width':'95%'});
	$("#create_container").show();

	$.ajax({
       type: "GET",
       url: localStorage.getItem('ims_url') + "/dvWfm2/Backoffice/ajaxSo.php",
       data: {action: 'getNextSoNumber', type: order_type},
       dataType: "json",
       success: function(msg) {
           if (msg.result === 'false') {
               jAlert(msg.error);
               return;
           }
           $("#waiting").hide();
           $("#order_number").html(msg.next_order_number);

       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
           $("#waiting").hide();
           console.error( textStatus + errorThrown);
           jAlert("Failed to get next order number. Error: "+errorThrown);
       }
   });

}
/*----------------------------------------------------------------------------*/
function Sub_Switch() {
	var WOTypeValues = $('#wo_code').val();
	$('#wo_subcode').html('<option value=""></option>');
	for (var key in params.wosubcodes[WOTypeValues]) {
		$('#wo_subcode').append('<option value="' + key + '">' + key + ' - ' + params.wosubcodes[WOTypeValues][key] + '</option>');
	}
}
/*----------------------------------------------------------------------------*/
function doCreateOrder(index) {
	item = searchresults[index];
	if ($("#from").val() == 'damage') {
		item=damage[index];
	}

	var data = {
		action: 'createSO',
		so_number: $("#order_number").html(),
		service_index: service_name,
		target_app:'L',
		order_comment: $("#order_comments").val(),
		so_description: $("#order_description").val(),
		department: $("#department").val(),
		service_area: $("#servicearea").val(),
		sub_num: item.get('myData').substation,
		fdr_num: item.get('myData').feeder,
		geodata:item.get('myData').marker_geodata
	};
	if (item.get('myData').outage_type == 'CONSUMER') {
		data.order_type='S';
		data.so_type = $("#so_type").val();
		data.so_code = $("#so_code").val();
		data.customer_account_number = item.get('myData').accountnumber;
		data.customer_name = item.get('myData').name;
		data.customer_billing_daytime_phone = item.get('myData').phone;
		data.service_loc_address = item.get('myData').address; 
		data.service_loc_meter_number = item.get('myData').meternumber;
		data.service_loc_line_section = item.get('myData').enetworkid;
		data.service_loc_substation = item.get('myData').substation;
	}else{
		data.order_type='W';
		data.wo_order_type = $("#wo_code").val();
		data.wo_order_subtype = $("#wo_subcode").val();
		data.wo_equipment_type = item.get('myData').enettype;
		data.wo_equipment_id = item.get('myData').enetworkid;
		data.service_loc_line_section = item.get('myData').enetworkid;
		data.service_loc_substation = item.get('myData').substation;
	}	
	if (navigator.connection.type == Connection.NONE) {
        storeAjaxRequest(localStorage.getItem('ims_url') + "/dvWfm2/Backoffice/ajaxSo.php", data);
        return;
    }
    $("#waiting").show();
    $.ajax({
       type: "GET",
       url: localStorage.getItem('ims_url') + "/dvWfm2/Backoffice/ajaxSo.php",
       data: data,
       dataType: "json",
       success: function(msg) {
       		setOMSConnection(true);
           if (msg.result === 'false') {
               jAlert(msg.error);
               return;
           }
           $("#create_container").hide();
           $("#waiting").hide();
           if ($("#dispatch_to_me").is(':checked')) {
           		//reassignId=$("#order_number").html();
           		assignTicket($("#order_number").html(),data.order_type, 'individual',user);           		
           }           
       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
       		if ( textStatus == 'timeout') {
       			setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'common.doCreateOrder'});
       		}
           $("#waiting").hide();
           console.error( textStatus + errorThrown);
           jAlert("Failed to create order. Error: "+errorThrown);
       }
   });
}
var tracedLines = [];
function traceLine(enetworkid, direction) {
	$.ajax({
            url: localStorage.getItem('ims_url') + "/dvservices/ims_rpc_request.php",
			data: {
				jsonrpc: "2.0",
				method: 'NetworkTrace',
				params: {
					"action": "NetworkTrace",
					"trace_id": user,
					"serviceindex": service_index,
					"enetworkid": enetworkid,
					"direction": direction,
					"id": 1
				},
				id: 2
			},
			dataType: 'text',
			success: function(data) {
                if(!data) {
					alert("Error: no data returned");
					return;
				}
				var json = $.parseJSON(data);
				if(typeof json.error !== 'undefined') {
					alert("Error: " + json.error.error_text);
					return;
				}
				$.ajax({
					type: "GET",
					url: localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php",
					data: {action:'get', target:'trace_line',user:user,id:user},
					dataType: "json",
					success: function(msg) {
						$("#waiting").hide();
						if (msg.result == "false") {
							jAlert(msg.error);
							return;
						} 
						for (var i = 0 ; i < tracedLines.length ; i++) {
							if ( tracedLines[i] != null && $.isFunction(tracedLines[i].remove)) {
								tracedLines[i].remove();
							}
						}
						//var infoWindow = new plugin.google.maps.HtmlInfoWindow();

						for (var i = 0 ; i < msg.rows.length ; i++){
							var json = JSON.parse(msg.rows[i].json_string);
							var line = [];
							for ( var j = 0 ; j < json.coordinates.length ; j++) {
								line.push({"lat":json.coordinates[j][1], "lng": json.coordinates[j][0]});
							}
							fillColor = '#FFFF00';	
							var data = {
								points: line,
								color: "rgb(75,0,130,0.2)",
								width: 20,
								geodesic: true
							};
							googleMap.addPolyline(data, function(line) {
								tracedLines.push(line);
								//line.on(plugin.google.maps.event.POLYLINE_CLICK, onLineClick); Doesn't work
							});
						}
						/*function onLineClick(latLng) {
							var marker = this;
							$("#closeBtn").trigger("click");
							map.addMarker({
								position: latLng,
								title: "You clicked on the polyline",
								snippet: latLng.toUrlValue(),
								disableAutoPan: true
							  }, function(marker) {
								content+= '<button class="ui-btn ui-btn-a ui-btn-icon-right ui-icon-delete" onclick="javascript:$(\'#closeBtn\').trigger(\'click\'); removeTracedLines();">Remove</button>';
								infoWindow.setContent(content);
								infoWindow.open(marker);
								setTimeout( function() {
									$("#closeBtn").on("click", function() {
										infoWindow.close();
									});
								},500);
								marker.showInfoWindow();
							  });
						}*/
		
						//var gdistance = google.maps.geometry.spherical.computeDistanceBetween ( start, stop )*3.28084+" ft.";	
						
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						$("#waiting").hide();
						console.log(XMLHttpRequest + textStatus + errorThrown);
						jAlert("Failed to remove Alert Point. Error: "+errorThrown);
					}
				});
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				jAlert("Trace Downline failed: " + textStatus);
			}
		});
}
function removeTracedLines() {
	
}
/*----------------------------------------------------------------------------*/
function stripNulls(input) {
	if (input == null) {
		return "";
	}
	return input;
}

function loadDisconnectedMap(){
	if (appname == 'AssessDamage') {
		load_damage_markers();
	}
	disconnectedMapMoved();
}
//  Allows for a show hide event on a div
(function ($) {
	  $.each(['show', 'hide'], function (i, ev) {
	    var el = $.fn[ev];
	    $.fn[ev] = function () {
	      this.trigger(ev);
	      return el.apply(this, arguments);
	    };
	  });
	})(jQuery);

