var g_basedataLoaded = false;

var g_accessLevels = [];
var g_accessLevelsHtml = "";
var g_accessLevelsActiveHtml = "";

var g_crewsData = [];
var g_crewsHtml = "";
var g_crews_options = '';

var g_districtCheckboxHtml = '';

var g_substationData = [];
var g_substationHtml = "";
var g_substationSelectNumHtml = "";
var g_substationNumHtml = "";
var g_substationsSelectList = [];

var g_departmentsData = [];
var g_departmentHtml = "";
var g_departmentSelectList = [];

var g_serviceHtml = "";
var g_serviceCheckBoxesHtml = "";
var g_serviceCheckBoxesHtmlReadOnly = "";
var g_services = [];

var g_serviceAreaTypes = [];
var g_serviceAreaHtml = "";
var g_serviceAreaSelectList = [];

var g_districtsData = [];
var g_districtHtml = "";
var g_districtEmployeeDetailsHtml = "";
var g_districtsSelectList = [];
var g_districtMultiSelectHtml = '';

var g_employeesData = [];
var g_employeesHtml = "";
var g_employeeNames = "";
var g_employeesMessagingOptions = "";

var g_titles = [];
var g_titlesOptions = '';
var g_classifications = [];
var g_classificationsOptions = '';

var g_vehiclesData = [];
var g_vehiclesHtml = "";
var g_vehiclesAllHtml = "";

var g_countiesData = [];
var g_countiesHtml = "";
var g_countiesAllHtml = "";

var g_iconsData = [];
var g_iconsHtml = "";
var g_userServicesArr = [];

var g_eventData = [];
var g_eventHtml = "";
var g_eventNumHtml = "";
var g_eventsSelectList = [];

var g_service_types = [];
var g_serviceTypesHtml = '';
var g_service_codes = [];
var g_serviceCodesHtml = '';

var g_equipment = [];
var g_equipmentHtml = "";

var g_damage_assessment_type = [];
var g_damage_assessment_typeHtml = "";

var g_time_classifications = [];
var g_config_data;

var g_lastEmpData = '';
var g_lastVehData = '';
var g_lastCrewData = '';

window.addEventListener('storage', function(e) {
	if(e.key === 'employeesData') {
//		jAlert("Reloading Entities Data", "Data Change", null, 1200);
		getEmployeesData();
	}

	else if(e.key === 'crewsData') {
		getCrewsData();
	}

	else if(e.key === 'vehiclesData') {
		getVehiclesData();
	}
	else {
		return;
	}

//	if(e.oldValue !== e.newValue && window.location.href.substr(-14) === 'backoffice.php') {
//		var text = "window="+window.location.href+"\n URL="+e.url+"\n key="+e.key+"\n oldval="+e.oldValue+"\n newval="+e.newValue;
//
//		alert(text);
//	}
});

// Rebuild the employees drop down list based on the the onlyUndispatched flag
// If an additional argument is included, use it as a filter.
function reloadEmployeesList(onlyUndispatched,service_index) {
	var dispatch_undispatch = $("#dispatch_undispatch");
	var districts = [];

	if($("#dispatch_undispatch").dialog( "isOpen" ) && $("#dispatchEmployeesDistricts",dispatch_undispatch).length > 0 && $("#dispatchEmployeesDistricts",dispatch_undispatch).html().length === 0) {
		$("#dispatchEmployeesDistricts",dispatch_undispatch).html(g_districtCheckboxHtml);
	}

	if($("#dispatchEmployeesDistricts",dispatch_undispatch).length > 0 && $("#dispatchEmployeesDistricts",dispatch_undispatch).html().length > 0) {
		$("#dispatchEmployeesDistricts div.districts_block>input.DistrictFilter:checked",dispatch_undispatch).each(function() {
			districts[districts.length] = $(this).attr('districtcode');
		});
	};

	var doFilter = (g_config_data['dispatch_filter_by_service'] === 't');
	var showEmpClass = (g_config_data['show_employee_classification'] === 't');

	if(typeof(service_index) === 'undefined') {
		service_index = false;
	}

	html = '<option value="">Select an Employee</option>'+"\n";
	for(var r = 0;r < g_employeesData.length;r++) {
		var row = g_employeesData[r];	// name,employee_number,dispatchable,available,enabled,recordid (contact), service_area

		// check the service index
		if(doFilter && service_index !== false && row['service_index'].indexOf(service_index) === -1) {
			continue;
		}

		// Check the district if specified as a filter.
		// records marked for all districts skip this step.
		if(districts.length > 0 && row['district'] !== 'ALL' && row['district'] !== '') {
			var OK = false;

			for(var i = 0;i < districts.length;i++) {
				if(districts[i] === row['district']) {
					OK = true;
					break;
				}
			}

			if(OK === false) {
				continue;
			}
		}

		if(row["enabled"] !== 't' || row["available"] !== 't') {
			continue;
		}

		if(onlyUndispatched && parseInt(row["numDispatched"],10) > 0) {
			continue;
		}

		html += '<option entity_name="'+row["name"]+'" entity_number="'+row["employee_number"]+'"';
		html += ' service_index="'+row["service_index"]+'" value="'+row["employee_number"]+'">'+row["name"];

		if(showEmpClass && row["classification"].length > 0) {
			html += ' ('+row["classification"]+')';
		}

		html += ' ['+row["numDispatched"]+']';
		html += ' '+ row["service_area"]+'</option>'+"\n";
	}
	$("#EmployeeOptionslist",dispatch_undispatch).empty().html(html);
}

// Rebuild the crews drop down list based on the onlyUndispatched flag
function reloadCrewsList(onlyUndispatched,service_index) {
	var dispatch_undispatch = $("#dispatch_undispatch");
	var districts = [];

	if($("#dispatch_undispatch").dialog( "isOpen" ) && $("#dispatchCrewsDistricts",dispatch_undispatch).length > 0 && $("#dispatchCrewsDistricts",dispatch_undispatch).html().length === 0) {
		$("#dispatch_undispatch").find("#dispatchCrewsDistricts").empty().html(g_districtCheckboxHtml);
	}

	if($("#dispatchCrewsDistricts",dispatch_undispatch).length > 0 && $("#dispatchCrewsDistricts",dispatch_undispatch).html().length > 0) {
		$("#dispatchCrewsDistricts div.districts_block>input.DistrictFilter:checked",dispatch_undispatch).each(function() {
			districts[districts.length] = $(this).attr('districtcode');
		});
	};

	var doFilter = (g_config_data['dispatch_filter_by_service'] === 't');

	if(typeof(service_index) === 'undefined') {
		service_index = false;
	}

	html = '<option value="">Select a Crew</option>'+"\n";
	for(var r = 0;r < g_crewsData.length;r++) {
		var row = g_crewsData[r];	// name,crew_number,enabled,service_area,dispatchable,leader_contact,leader_number,leader_name, num dispatched

		// check the service index
		if(doFilter && service_index !== false && row['service_index'].indexOf(service_index) === -1) {
			continue;
		}

		// Check the district if specified as a filter.
		// records marked for all districts skip this step.
		if(districts.length > 0 && row['district'] !== 'ALL' && row['district'] !== '') {
			var OK = false;
			for(var i = 0;i < districts.length;i++) {
				if(districts[i] === row['district']) {
					OK = true;
					break;
				}
			}

			if(OK === false) {
				continue;
			}
		}

		if(row["enabled"] !== 't') {
			continue;
		}

		if(onlyUndispatched && parseInt(row["numDispatched"],10) > 0) {
			continue;
		}

		html += '<option entity_name="'+row["name"]+'" entity_number="'+row["crew_number"]+'" leader_name="'+row["leaderName"]+'" leader_number="'+row["leaderNumber"]+'" service_index="'+row["service_index"]+'" value="'+row["crew_number"]+'">'+row["name"]+' ['+row["numDispatched"]+'] '+row["service_area"]+'</option>'+"\n";
	}
	$("#dispatch_undispatch").find("#CrewOptionslist").empty().html(html);
}

// Rebuild the vehicles drop down list based on the the onlyUndispatched flag
function reloadVehiclesList(onlyUndispatched,service_index) {
	var dispatch_undispatch = $("#dispatch_undispatch");
	var districts = [];

	if($("#dispatch_undispatch").dialog( "isOpen" ) && $("#dispatchVehiclesDistricts",dispatch_undispatch).length > 0 && $("#dispatchVehiclesDistricts",dispatch_undispatch).html().length === 0) {
		$("#dispatch_undispatch").find("#dispatchVehiclesDistricts").empty().html(g_districtCheckboxHtml);
	}

	if($("#dispatchVehiclesDistricts",dispatch_undispatch).length > 0 && $("#dispatchVehiclesDistricts",dispatch_undispatch).html().length > 0) {
		$("#dispatchVehiclesDistricts div.districts_block>input.DistrictFilter:checked",dispatch_undispatch).each(function() {
			districts[districts.length] = $(this).attr('districtcode');
		});
	};

	var doFilter = (g_config_data['dispatch_filter_by_service'] === 't');

	if(typeof(service_index) === 'undefined') {
		service_index = false;
	}

	html = '<option value="">Select a Vehicle</option>'+"\n";
	for(var r = 0;r < g_vehiclesData.length;r++) {
		var row = g_vehiclesData[r];	// name,vehicle_number,icon_name,enabled,service_area,vehicle_radio

		// check the service index
		if(doFilter && service_index !== false && row['service_index'].indexOf(service_index) === -1) {
			continue;
		}

		// Check the district if specified as a filter.
		// records marked for all districts skip this step.
		if(districts.length > 0 && row['district'] !== 'ALL' && row['district'] !== '') {
			var OK = false;

			for(var i = 0;i < districts.length;i++) {
				if(districts[i] === row['district']) {
					OK = true;
					break;
				}
			}

			if(OK === false) {
				continue;
			}
		}

		if(row["enabled"] !== 't' || row['dispatchable'] !== 't') {
			continue;
		}

		if(onlyUndispatched && parseInt(row["numDispatched"],10) > 0) {
			continue;
		}

		html += '<option entity_name="'+row["name"]+'" entity_number="'+row["vehicle_number"]+'" service_index="'+row["service_index"]+'" value="'+row["vehicle_number"]+'" icon="'+row["icon_name"]+'">'+row["name"]+' ['+row["numDispatched"]+'] '+row["service_area"]+'</option>'+"\n";
	}
	$("#dispatch_undispatch").find("#VehicleOptionslist").empty().html(html);
	$("#assign_unassign").find("#VehicleOptionslist").empty().html(html);

//	if($("#dispatch_undispatch").dialog( "isOpen" ) && $("#dispatchVehiclesDistricts",dispatch_undispatch).length > 0 && $("#dispatchVehiclesDistricts",dispatch_undispatch).html().length === 0) {
//		$("#dispatch_undispatch").find("#dispatchVehiclesDistricts").empty().html(g_districtCheckboxHtml);
//		$("#dispatchCrewsDistricts div.districts_block>input.DistrictFilter",dispatch_undispatch).unbind().bind('click touchstart', function() {
//			setTimeout('reloadVehiclesList('+$("#vehicleDispatchedFlag").is(":checked")+')',20);
//		});
//	}
}

var g_backofficeDataLoaded = false;

function loadBackofficeData() {
	var domain = '';
	if(typeof(clsCurrentWorkItem) !== 'undefined') {
		domain = clsCurrentWorkItem.domain;
	}
	$.ajax({
		type: "POST",
		url: domain+"/dvwfm2/backoffice/ajaxGetSystemInfo.php",
		data: "action=GetBackofficeData",
		dataType: "json",
		success: function(data) {
			if(data.result === "false") {
				jAlert("Can't get backoffice data: "+data.error, "Error");
				return;
			}

			// services
			g_services = data.services;
			g_serviceHtml = '<option value="" index="">Select a Service</option>'+"\n";

			for(var i = 0;i < g_services.length;i++) {
				var service = g_services[i];
				g_serviceCheckBoxesHtml += '<input style="margin-left:0;" type="checkbox" id="service_'+i+'" ServiceIndex="'+service['service_index']+'" value="'+service['service_name']+'" /> '+service['service_name']+'<br>';
				g_serviceCheckBoxesHtmlReadOnly += '<input type=checkbox id="service_'+i+'" value="'+service['service_name']+'" ServiceIndex="'+service['service_index']+'" disabled /> '+service['service_name']+'<br>';

				if(!g_userIsAdministrator && !hasUserService(service['service_index'])) {
					continue;
				}

				g_serviceHtml += '<option value="'+service['service_name']+'" index="'+service['service_index']+'">'+service['service_name']+'</option>'+"\n";
			}

			$("#workItemsSearchContainer #FilterService").html(g_serviceHtml);
			$("#findCustomerDiv").find("#service_index").html(g_serviceHtml);
			$("#CrewDetails #crew_service_index").html(g_serviceHtml);
			$("#report_service_index").html(g_serviceHtml);
			$("#servicesEmp").html(g_serviceCheckBoxesHtml);
			$("#servicesCrew").html(g_serviceCheckBoxesHtml);
			$("#servicesVehicle").html(g_serviceCheckBoxesHtml);
			$("#servicesWFM").html(g_serviceCheckBoxesHtmlReadOnly);

			// Access levels
			g_accessLevelsHtml = "";
			g_accessLevelsActiveHtml = "";
			g_accessLevels = data.accesslevels.slice(0);
			g_accessLevelsHtml = '<option value="0">No Log On Access</option>'+"\n";

			for(var i = 0;i < g_accessLevels.length;i++) {
				g_accessLevelsHtml += '<option value="'+g_accessLevels[i][0]+'">'+g_accessLevels[i][1]+'</option>'+"\n";
				g_accessLevelsActiveHtml += '<option value="'+g_accessLevels[i][0]+'">'+g_accessLevels[i][1]+'</option>'+"\n";
			}

			$("#EmployeeDetails_admin #accesslevel").html(g_accessLevelsHtml);
			$("#EmployeeDetails #accesslevel").html(g_accessLevelsHtml);
			$("#delAccessLevel").html(g_accessLevelsActiveHtml);

			// service area names
			g_serviceAreaTypes = data.areas.slice(0);
			g_serviceAreaHtml = '<option value="">Select an Area</option>'+"\n";
			g_serviceAreaSelectList = [];
			g_serviceAreaSelectList[0] = { value: '', display: '' };

			for(var i = 0;i < g_serviceAreaTypes.length;i++) {
				var serviceArea = g_serviceAreaTypes[i];
				g_serviceAreaHtml += '<option value="'+serviceArea+'">'+serviceArea+'</option>'+"\n";
				g_serviceAreaSelectList[g_serviceAreaSelectList.length] = { value: serviceArea, display: serviceArea };
			}

			$("#workItemsSearchContainer #FilterServiceArea").html(g_serviceAreaHtml);
			var EmployeeDetails = $("#EmployeeDetails");
			$("#service_area", EmployeeDetails).html(g_serviceAreaHtml);
			var EmployeeDetails_admin = $("#EmployeeDetails_admin");
			$("#service_area", EmployeeDetails_admin).html(g_serviceAreaHtml);
			$("#VehicleDetails #service_area").html(g_serviceAreaHtml);
			$("#CrewDetails #service_area").html(g_serviceAreaHtml);
			$("#createSoDiv #service_area").html(g_serviceAreaHtml);
			$("#report_service_area").html(g_serviceAreaHtml);

			// Counties
			if(data.counties.length > 0) {
				g_countiesData = data.counties.slice(0);

				for(var i = 0;i < g_countiesData.length;i++) {
					var CountyName = g_countiesData[i]['name'];
					var CountyCode = g_countiesData[i]['code'];

					g_countiesHtml += '<option value="'+CountyCode+'">'+CountyCode+' '+CountyName+'</option>'+"\n";
				}
			}
			$("#report_county").html(g_countiesHtml);

			// districts
			if(data.districts.length === 0) {
				$("#createSoDiv #service_loc_district").attr("disabled","disabled");
				$("#workItemsSearchContainer #FilterDistrict").attr("disabled","disabled");
				$("#district", EmployeeDetails).attr("disabled","disabled");
				$("#VehicleDetails #district").attr("disabled","disabled");
				$("#CrewDetails #district").attr("disabled","disabled");
				$("#report_district").attr("disabled","disabled");
			}
			else {
				g_districtsData = data.districts.slice(0);
				g_districtHtml = '';
				g_districtEmployeeDetailsHtml = '';

				if(g_userIsAdministrator || g_userDistrictCode.length === 0 || g_userDistrictCode === 'ALL') {
					g_districtHtml = '<option value="">Select a District</option>'+"\n";
					g_districtEmployeeDetailsHtml = '<option value="ALL">All Districts</option>'+"\n";
				}

				g_districtsSelectList = [];
				g_districtsSelectList[0] = { value: '', display: '' };
				g_districtCheckboxHtml = '<fieldset style="border:1px solid #8aaaca;padding:4px 2px;margin:8px 0 2px;background-color:#ffffff;width:160px;">'+"\n";
				g_districtCheckboxHtml += '<legend style="font-weight:bold;color:#003366;">&nbsp;Districts&nbsp;</legend>'+"\n";
				g_districtCheckboxHtml += '<div class="districts_block" style="width:180px;overflow-y:auto;overflow-x:hidden;height:125px;white-space:nowrap;">'+"\n";
				g_districtMultiSelectHtml = '';

				for(var i = 0;i < g_districtsData.length;i++) {
					var DistrictName = g_districtsData[i]['DistrictName'];
					var DistrictCode = g_districtsData[i]['DistrictCode'];
					var disabled = '';
					var checked = 'checked="checked"';

					if(!g_userIsAdministrator && g_userDistrictCode.length > 0 && g_userDistrictCode !== 'ALL') {
						if(!g_userIsAdministrator && g_userDistrictCode !== DistrictCode) {
							disabled = 'disabled="disabled"';
							checked = '';
						}
					}

					if(DistrictName.toLowerCase() === 'blank') {
						g_districtCheckboxHtml += '<input name="'+DistrictName+'"type="checkbox" class="DistrictFilter" value="1" districtcode="'+DistrictCode+'"'+disabled+checked+'> '+DistrictName+'<br />';
						continue;
					}

					if(g_districtsData[i]['DistrictName'].toLowerCase() === 'invalid') {
						continue;
					}

					g_districtHtml += '<option value="'+DistrictCode+'"'+disabled+'>'+DistrictCode+' '+DistrictName+'</option>'+"\n";
					g_districtEmployeeDetailsHtml += '<option value="'+DistrictCode+'"'+disabled+'>'+DistrictName+'</option>'+"\n";
					g_districtsSelectList[g_districtsSelectList.length] = { value: DistrictCode, display: DistrictName };
					g_districtCheckboxHtml += '<input name="'+DistrictName+'"type="checkbox" class="DistrictFilter" value="1" districtcode="'+DistrictCode+'"'+disabled+checked+'> '+DistrictName+'<br />';

					if(g_userIsAdministrator || g_userDistrictCode.length === 0 || g_userDistrictCode === 'ALL' || g_userDistrictCode === DistrictCode) {
						g_districtMultiSelectHtml += '<div class="MultiSelectListItem" districtcode="'+DistrictCode+'">'+DistrictCode+' '+DistrictName+'</div>'+"\n";
					}
				}
			}

			g_districtCheckboxHtml += '</div></fieldset>';

			if(g_districtsSelectList.length > 0) {
//				$("div#workItemsSearchContainer div#divSearchContent td#FilterDistrictContainer").html(g_districtCheckboxHtml);
				$("div#workItemsSearchContainer div#divSearchContent td#FilterDistrict").html(g_districtMultiSelectHtml);
			}
			else {
				$("td#FilterDistrictContainer").parent().parent().find("tr:first>td:first>div").html("");
			}

//			$("#workItemsSearchContainer #FilterDistrict").html(g_districtHtml);
			$("#workItemsSearchContainer #FilterDistrict").html(g_districtMultiSelectHtml);
			$("#EmployeeDetails_admin #district").html(g_districtEmployeeDetailsHtml);
			$("#EmployeeDetails #district").html(g_districtEmployeeDetailsHtml);
			$("#VehicleDetails #district").html(g_districtHtml);
			$("#CrewDetails #district").html(g_districtHtml);
			$("#report_district").html(g_districtHtml);

			$("#FilterDistrict").find(".MultiSelectListItem").on('click', function() {
				$(this).toggleClass("ListItemSelected", !$(this).hasClass("ListItemSelected"));
				var count = $("#FilterDistrict").find(".ListItemSelected").length;

				if(count === 0) {
					$("#NumDistrictsSelected").html('&nbsp;');
				}
				else {
					$("#NumDistrictsSelected").html('('+count+' selected)');
				}
			});

			if(data.RetCount === 0) {
				$("#createSoDiv #substations").attr("disabled","disabled");
			}
			else {
				g_substationData = data.substations.slice(0);
				g_substationHtml = '<option value="">Select a Substation</option>'+"\n";
				g_substationSelectNumHtml = '<option value="">Select a Substation</option>'+"\n";
				g_substationsSelectList = [];
				g_substationsSelectList[0] = { value: '', display: '' };

				for(var i = 0;i < g_substationData.length;i++) {
					g_substationHtml += '<option value="'+g_substationData[i]['name']+'">'+g_substationData[i]['name']+'</option>'+"\n";		// sets value as name
					g_substationSelectNumHtml += '<option value="'+g_substationData[i]['id']+'">'+g_substationData[i]['name']+'</option>'+"\n";		// sets value as id
				}

				g_substationNumHtml = '';

				for(var i = 0;i < g_substationData.length;i++) {
					g_substationNumHtml += '<div class="MultiSelectListItem" value="'+g_substationData[i]['id']+'">'+g_substationData[i]['id']+' ('+g_substationData[i]['name']+')</div>'+"\n";	// sets value as gos_id
					g_substationsSelectList[g_substationsSelectList.length] = { value: g_substationData[i]['id'], display: g_substationData[i]['name'] };
				}
			}

			$("#createSoDiv #substations").html(g_substationHtml);
			$("#FilterSubstation").html(g_substationNumHtml);

			$("#FilterSubstation").find(".MultiSelectListItem").on('click', function() {
				$(this).toggleClass("ListItemSelected", !$(this).hasClass("ListItemSelected"));
				var count = $("#FilterSubstation").find(".ListItemSelected").length;

				if(count === 0) {
					$("#NumSubsSelected").html('&nbsp;');
					$("#FilterFeeder").html('');
				}
				else {
					$("#NumSubsSelected").html('('+count+' selected)');

					if(count === 1) {
						populateFeederList();
					}
					else {
						$("#FilterFeeder").html('');
					}
				}
			});
			$("#report_substation").html(g_substationHtml);

			// departments
			if(data.departments.length === 0) {
				$("#workItemsSearchContainer #FilterDepartment").attr("disabled","disabled");
			}
			else {
				g_departmentsData = data.departments.slice(0);
				g_departmentHtml = '<option value="">Select a Department</option>'+"\n";
				g_departmentEmployeeDetailsHtml = '<option value="ALL">All Departments</option>'+"\n";
				g_departmentSelectList = [];
				g_departmentSelectList[0] = { value: '', display: '' };

				for(var i = 0;i < g_departmentsData.length;i++) {
					var dept = g_departmentsData[i]['full_name'];

					if(dept.length === 0) {
						dept = g_departmentsData[i]['code'];
					}

					g_departmentHtml += '<option value="'+g_departmentsData[i]['code']+'">'+dept+'</option>'+"\n";
					g_departmentSelectList[g_departmentSelectList.length] = { value: g_departmentsData[i]['code'], display: dept };
				}
			}
			$("#workItemsSearchContainer #FilterDepartment").html(g_departmentHtml);
			$("#report_department").html(g_departmentHtml);

			// classifications
			g_classifications = data.classifications.slice(0);
			g_classificationsOptions = "<option value=''>Select a Classification</option>";

			if(g_classifications.length !== 0) {
				for(var i = 0;i < g_classifications.length;i++) {
					var classification = g_classifications[i][0];
					g_classificationsOptions += '<option value="'+classification+'">'+classification+'</option>'+"\n";
				}
			}

			// titles
			g_titles = data.titles.slice(0);
			g_titlesOptions = "<option value=''>Select a Title</option>";

			if(g_titles.length !== 0) {
				for(var i = 0;i < g_titles.length;i++) {
					var title = g_titles[i][0];
					g_titlesOptions += '<option value="'+title+'">'+title+'</option>'+"\n";
				}
			}

			// icons
			g_iconsData = data.icons.slice(0);
			g_iconsHtml = '';

			for(var i = 0;i < g_iconsData.length;i++) {	// name,icon_type,filename,description
				var row = g_iconsData[i];
				g_iconsHtml += '<img src="'+row['filename']+'" name="'+row['name']+'" class="imageNotSelected" width="32px" height="32px" />'+"\n";
			}
			$("#VehicleDetails #vehicle_icon").html(g_iconsHtml);

			$(".imageNotSelected").unbind().on("click", function() {
				$(".imageSelected").removeClass("imageSelected");
				$(this).addClass("imageSelected");
			});

			// events
			g_eventData = data.events.slice(0);
			g_eventHtml = '<option value="">Select an Event</option>'+"\n";
			g_eventsSelectList = [];
			g_eventsSelectList[0] = { value: '', display: '' };

			for(var i = 0;i < g_eventData.length;i++) {
				g_eventHtml += '<option value="'+g_eventData[i]['recordid']+'">'+g_eventData[i]['event_name']+'</option>'+"\n";		// sets value as name
			}

			g_eventNumHtml = '<option value="">Select an Event</option>'+"\n";

			for(var i = 0;i < g_eventData.length;i++) {
				g_eventNumHtml += '<option value="'+g_eventData[i]['recordid']+'">'+g_eventData[i]['recordid']+' ('+g_eventData[i]['event_name']+')</option>'+"\n";	// sets value as gos_id
				g_eventsSelectList[g_eventsSelectList.length] = { value: g_eventData[i]['recordid'], display: g_eventData[i]['event_name'] };
			}

			$("#report_event").html(g_eventHtml);

			// time classifications
			g_time_classifications = data.time_classifications.slice(0);

			g_backofficeDataLoaded = true;

			// service types
			g_service_types = data.service_types.slice(0);
			g_serviceTypesHtml = "<option value=''>Select a Service Type</option>";

			if(g_service_types.length !== 0) {
				for(var i = 0; i < g_service_types.length;i++) {
					var service_type = g_service_types[i]['so_type'];
					g_serviceTypesHtml += '<option value="'+service_type+'">'+service_type+'</option>'+"\n";
				}
			}

			// service codes
			g_service_codes = data.service_codes.slice(0);
			g_serviceCodesHtml = "<option value=''>Select a Service Code</option>";

			if(g_service_codes.length !== 0) {
				for(var i = 0; i < g_service_codes.length;i++) {
					var service_code = g_service_codes[i]['code'];
					g_serviceCodesHtml += '<option value="'+service_code+'">'+service_code+'</option>'+"\n";
				}
			}

			// equipment types
			if(typeof(data.equipment) !== 'undefined') {
				g_equipment = data.equipment.slice(0);
				g_equipmentHtml = "<option value=''>Select Equipment Type</option>";
			}

			if(g_equipment.length !== 0) {
				g_equipmentHtml += '<option value="all">all</option>'+"\n";

				for(var i = 0; i < g_equipment.length;i++) {
					var equipment = g_equipment[i];
					g_equipmentHtml += '<option value="'+equipment+'">'+equipment+'</option>'+"\n";
				}
			}

			// damage assessment types
			g_damage_assessment_type = data.damage_assessment_type.slice(0);
			g_damage_assessment_typeHtml = "<option value=''>Select Assessment Type</option>";

			if(g_damage_assessment_type.length !== 0) {
				for(var i = 0; i < g_damage_assessment_type.length;i++) {
					var damage_assessment_type = g_damage_assessment_type[i];
					g_damage_assessment_typeHtml += '<option value="'+damage_assessment_type+'">'+damage_assessment_type+'</option>'+"\n";
				}
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			g_backofficeDataLoaded = true;
			alert("AJAX failed to get backoffice data: "+errorThrown);
		}
	});

}

function getAccessNumberFromName(name) {
	for(var i = 0;i < g_accessLevels.length;i++) {
		if (name === g_accessLevels[i][1]) {
			return g_accessLevels[i][0];
		}
	}
	return "";
}

function getAccessNameFromNumber(number) {
	for(var i = 0;i < g_accessLevels.length;i++) {
		if (number === g_accessLevels[i][0]) {
			return g_accessLevels[i][1];
		}
	}
	return "";
}

function getDistrictNumberFromName(name) {
	for(var i = 0;i < g_districtsData.length;i++) {
		if(g_districtsData[i]['DistrictName'] === name) {
			return g_districtsData[i]['DistrictNum'];
		}
	}
	return '';
}

function getDistrictNameFromNumber(num) {
	for(var i = 0;i < g_districtsData.length;i++) {
		if(g_districtsData[i]['DistrictNumber'] === num) {
			return g_districtsData[i]['DistrictName'];
		}
	}
	return '';
}

function getDistrictCodeFromName(name) {
	for(var i = 0;i < g_districtsData.length;i++) {
		if(g_districtsData[i]['DistrictName'] === name) {
			return g_districtsData[i]['DistrictCode'];
		}
	}
	return '';
}

function getDistrictNameFromCode(num) {
	for(var i = 0;i < g_districtsData.length;i++) {
		if(g_districtsData[i]['DistrictCode'] === num) {
			return g_districtsData[i]['DistrictName'];
		}
	}
	return '';
}

function getDepartmentNameFromCode(code) {
	for(var i = 0;i < g_departmentsData.length;i++) {
		if(g_departmentsData[i]['code'] === code) {
			return g_departmentsData[i]['full_name'];
		}
	}
	return '';
}

var g_loadAccessLevels_done = false;

function loadAccessLevels() {
	var domain = '';
	if(typeof(clsCurrentWorkItem) !== 'undefined') {
		domain = clsCurrentWorkItem.domain;
	}
	$.ajax({
		type: "POST",
		url: domain+"/dvwfm2/backoffice/ajaxGetSystemInfo.php",
		data: "action=GetAccessLevels",
		dataType: "json",
		success: function(data) {
			if(data.result === "false") {
				jAlert(data.error, "Error");
				return;
			}

			g_accessLevelsHtml = "";
			g_accessLevelsActiveHtml = "";
			g_accessLevels = data.accesslevels.slice(0);
			g_accessLevelsHtml = '<option value="0">No Log On Access</option>'+"\n";

			for(var i = 0;i < g_accessLevels.length;i++) {
				g_accessLevelsHtml += '<option value="'+g_accessLevels[i][0]+'">'+g_accessLevels[i][1]+'</option>'+"\n";
				g_accessLevelsActiveHtml += '<option value="'+g_accessLevels[i][0]+'">'+g_accessLevels[i][1]+'</option>'+"\n";
			}

			$("#EmployeeDetails_admin #accesslevel").html(g_accessLevelsHtml);
			$("#delAccessLevel").html(g_accessLevelsActiveHtml);
			g_loadAccessLevels_done = true;
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			g_loadAccessLevels_done = true;
			alert("AJAX failed to get access levels: "+errorThrown);
		}
	});
}

var g_loadEntities_done = false;
var g_entities_loaded = false;

function loadEntitiesData() {
	if(g_entities_loaded === false) {
		getEntitiesData();
	}
}

function getEmployeesData() {
	var employees = localStorage.getItem('employeesData');

	if(employees === g_lastEmpData) {
		return;
	}

	g_lastEmpData = employees;

	if(employees) {
		g_employeesData = JSON.parse(employees);
		g_employeesHtml = '<option value="">Select an Employee</option>'+"\n";
		g_employeesMessaging = [];

		for(var r = 0;r < g_employeesData.length;r++) {
			var row = g_employeesData[r];
			g_employeesHtml += '<option value="'+row["employee_number"]+'">'+row["name"]+' ('+row["numDispatched"]+')';

			if(row["service_area"].length > 0) {
				g_employeesHtml += ' - '+row["service_area"];
			}

			g_employeesHtml += '</option>'+"\n";

			g_employeeNames += '<option value="'+row["employee_number"]+'">'+row["name"];

			if(row["service_area"].length > 0) {
				g_employeeNames += ' - '+row["service_area"];
			}

			g_employeeNames += '</option>'+"\n";

			if(row['uses_messaging'] === 't') {
				g_employeesMessagingOptions += '<option value="'+row["userid"]+'">'+row["name"]+'</option>';
			}
		}
		$("#CrewDetails #leader_employee_number").html("<option value=''>Select a Leader</option>" + g_employeeNames);
		$("#CrewDetails #escort").html(g_employeesHtml);
		$("#assign_unassign #EmployeeOptionslist").html(g_employeesHtml);
		$("#report_employee").html(g_employeesHtml);
	}
}

function getCrewsData() {
	var crews = localStorage.getItem('crewsData');

	if(crews === g_lastCrewData) {
		return;
	}

	g_lastCrewData = crews;

	if(crews) {
		g_crewsData = JSON.parse(crews);
		g_crewsHtml = '<option value="">Select a Crew</option>'+"\n";

		for(var r = 0;r < g_crewsData.length;r++) {
			var row = g_crewsData[r];
			g_crewsHtml += '<option value="'+row["crew_number"]+'">'+row["name"]+' ('+row["numDispatched"]+') '+row["service_area"]+'</option>'+"\n";
		}

		$("#assign_unassign #CrewOptionslist").html(g_crewsHtml);
		$("#report_crew").html(g_crewsHtml);
	}
}

function getVehiclesData() {
	var vehicles = localStorage.getItem('vehiclesData');

	if(vehicles === g_lastVehData) {
		return;
	}
	
	g_lastVehData = vehicles;

	if(vehicles) {
		g_vehiclesData = JSON.parse(vehicles);
		g_entities_loaded = true;
		g_vehiclesHtml = '<option value="">Select a Vehicle</option>'+"\n";
		g_vehiclesAllHtml = '<option value="">Select a Vehicle</option>'+"\n";

		for(var i = 0;i < g_vehiclesData.length;i++) {
			var row = g_vehiclesData[i];

			if(row['dispatchable'] === 't') {
				g_vehiclesHtml += '<option entity_name="'+row['name']+'" entity_number="'+row['vehicle_number']+'"  value="'+row["vehicle_number"]+'">'+row["name"]+' ('+row["numDispatched"]+') '+row["service_area"]+'</option>'+"\n";
			}

			g_vehiclesAllHtml += '<option entity_name="'+row['name']+'" entity_number="'+row['vehicle_number']+'"  value="'+row["vehicle_number"]+'">'+row["name"]+' ('+row["numDispatched"]+') '+row["service_area"]+'</option>'+"\n";
		}

		$("#assign_unassign #VehicleOptionslist").html(g_vehiclesHtml);
		$("#report_vehicle").html(g_vehiclesAllHtml);
	}
}

function getEntitiesData() {
	// This means this is our first time here. So to speed things up, the landing page (topmenu)
	// has already loaded this info. We just get it from local storage and process as usual.
	getEmployeesData();
	getCrewsData();
	getVehiclesData();

	g_loadEntities_done = true;
}

function getIconFilename(name) {
	for(var i = 0;i < g_iconsData.length;i++) {
		if(g_iconsData[i][0] === name) {
			return g_iconsData[i][2];
		}
	}
	return "";
}

var g_so_events = [];

function getEventNameFromID(id) {
	for(var i = 0;i < g_so_events.length;i++) {
		if(g_so_events[i].value === id) {
			return g_so_events[i].display;
		}
	}
	return '';
}

function populateSoEvents(elem, defaultVal) {
	var target = elem;
	var domain = '';

	if(typeof(clsCurrentWorkItem) !== 'undefined') {
		domain = clsCurrentWorkItem.domain;
	}

	$.ajax({
		type: "POST",
		url: domain+"/dvwfm2/backoffice/ajax/ajaxManageEvents.php",
		data: {action:'get_events_arr'},
		dataType: "json",
		success: function(msg) {
			if(msg.result === "false") {
				jAlert("Can't get event list", "Error");
				return;
			}

			g_so_events = $.extend(true, [], [{value:'', display:''}], msg.options);

			if($(target).length > 0 && $(target)[0].tagName.toLowerCase() === 'input') {
				if(typeof(defaultVal) === 'string' && defaultVal.length > 0) {
					$(target).val(getEventNameFromID(defaultVal));
				}
			}
			else {
				var options = '<option value=""></option>';

				for(var i = 0;i < msg.options.length;i++) {
					var option = msg.options[i];
					options += '<option value="'+option.value+'">'+option.display+'</option>';
				}

				$(target).html(options);

				if(typeof(defaultVal) === 'string' && defaultVal.length > 0) {
					$(target+' option[value='+defaultVal+']').prop('selected',true);
				}
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX failed to get event: "+errorThrown);
		}
	});
}

function populateWoTypes() {
	var domain = '';

	if(typeof(clsCurrentWorkItem) !== 'undefined') {
		domain = clsCurrentWorkItem.domain;
	}

	$.ajax({
		type: "POST",
		url: domain+"/dvwfm2/backoffice/ajaxConfigData.php",
		data: {type:'get', id:'config_wo_types', mode:'associative'},
		dataType: "json",
		success: function(msg) {
			if(msg.result === "false") {
				jAlert("Can't get work order types", "Error");
				return;
			}

			var options = '<option value=""></option>';

			for(var i = 0;i < msg.rows.length;i++) {
				var option = msg.rows[i];

				if(option.description.length === 0) {
					continue;
				}

				options += '<option value="'+option.order_type+'">'+option.description+'</option>';
			}
			$("#createSoDiv #wo_order_type").html(options).unbind().bind('change', function() {
				var type = $(this).val();

				if(type.length === 0) {
					return;
				}

				setTimeout('populateWoSubTypes("'+type+'");',0);
			});

		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX failed to get work order types: "+errorThrown);
		}
	});
}

function populateWoSubTypes(type) {
	var domain = '';
	$("#createSoDiv #wo_order_subtype").empty().html('');

	if(typeof(clsCurrentWorkItem) !== 'undefined') {
		domain = clsCurrentWorkItem.domain;
	}

	$.ajax({
		type: "POST",
		url: domain+"/dvwfm2/backoffice/ajaxConfigData.php",
		data: {type:'subtypes', mode:'associative', order_type:type },
		dataType: "json",
		success: function(msg) {
			if(msg.result === "false") {
				jAlert("Can't get work order sub-types", "Error");
				return;
			}

			var options = '<option value=""></option>';

			for(var i = 0;i < msg.rows.length;i++) {
				var option = msg.rows[i];
				options += '<option value="'+option.order_subtype+'">'+option.description+'</option>';
			}

			$("#createSoDiv #wo_order_subtype").empty().html(options);

		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX failed to get work order sub-types: "+errorThrown);
		}
	});
}
