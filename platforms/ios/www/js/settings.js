$("#admin_password_dialog").on('pageshow', function() {
    console.log("password page show");
    $("#adminpassword").val('');
});

function fillInSettings() {
    //$("#waiting").show();
	console.log("setting page before show imsurl:"+localStorage['ims_url']);

	if (params.LinemanAppAdminPW == $('#adminpassword').val()) {
		$('#ims_url').val(localStorage.ims_url);
		//$('#max_wait').val(localStorage.max_wait != undefined && localStorage.max_wait.length > 0 ? localStorage.max_wait : 5000);
		$('#location_update_interval').val(localStorage.location_update_interval != undefined && localStorage.location_update_interval.length > 0? localStorage.location_update_interval : 60000);
		$('#desired_accuracy').val(localStorage.desired_accuracy != undefined && localStorage.desired_accuracy.length > 0 ? localStorage.desired_accuracy : 0);
		$('#distance_filter').val(localStorage.distance_filter!= undefined && localStorage.distance_filter.length > 0 ? localStorage.distance_filter : 1000 );
		$('#stationary_radius').val(localStorage.stationary_radius != undefined && localStorage.stationary_radius.length > 0 ? localStorage.stationary_radius : 10);
		$('#days_to_persist').val(localStorage.days_to_persist != undefined && localStorage.days_to_persist.length > 0 ? localStorage.days_to_persist : 5);
		$('#gps_collect').val(localStorage.gpsActive != undefined &&localStorage.gpsActive.length > 0 && localStorage.gpsActive=='true' ? 'on' : 'off').change();
		$('#debug_mode').val(localStorage.debug_mode != undefined && localStorage.debug_mode.length > 0 && localStorage.debug_mode=='true' ? 'on' : 'off').change();
		$('#disable_elasticity').val(localStorage.disable_elasticity != undefined && localStorage.disable_elasticity.length > 0 && localStorage.disable_elasticity=='true' ? 'on' : 'off').change();
		$('#batch_sync').val(localStorage.batch_sync != undefined && localStorage.batch_sync.length > 0 && localStorage.batch_sync=='true' ? 'on' : 'off').change();
		$("#max_time_btwn_stationary_collections").val(localStorage.max_time_btwn_stationary_collections != undefined && localStorage.max_time_btwn_stationary_collections.length > 0 ? localStorage.max_time_btwn_stationary_collections : 0).change();
		$('#googleapi').val(localStorage.googleapi != undefined && localStorage.googleapi.length > 0 ? localStorage.googleapi : 'sdk').slider("refresh");
		$('#app_debug').val(localStorage.app_debug != undefined && localStorage.app_debug.length > 0 && localStorage.app_debug=='true' ? 'true' : 'false').slider("refresh");
		$("#waiting").show();
		     $.ajax({
		          type: "GET",
		          url: localStorage.getItem('ims_url')+"/websrvrequests/mobile/ajaxGetCommonAppData.php?callback=?",
		          data:  { action:'get', target:'all_vehicles', appname: appname},
		          dataType: "jsonp",
		          success: function( msg ) {
		              if (msg.result == 'false') {
		                alert("Error on OMS: "+$('#ims_url').val()+" Error: "+msg.error);
		                $("#set_settings").addClass('ui-disabled');
		                return;
		              }
		              $("#set_settings").removeClass('ui-disabled');
		              $("#waiting").hide();
		              var html = '<option value="">None</option>';
		              for (var i = 0 ; i < msg.vehicles.length ; i++){
		                  html += '<option value="'+msg.vehicles[i].vehicle_number+'">'+msg.vehicles[i].name+'('+msg.vehicles[i].vehicle_number+')</option>';

		              }
		              $('#vehicle_number').html(html).val(localStorage.getItem('truck')).selectmenu('refresh',true);
		          },
		          error: function(XMLHttpRequest, textStatus, errorThrown) {
		              $("#waiting").hide();
		              $("#set_settings").addClass('ui-disabled');
		              console.log(XMLHttpRequest+textStatus+errorThrown);
		              alert("Unable to connect to OMS using: "+$('#ims_url').val());
		          }
		      });

    
	}else{
		  $.mobile.changePage('#admin_password_dialog');
	}
}

function setSettings() {
    
    /*if (localStorage.getItem('ims_url') == null || localStorage.getItem('ims_url') != $('#ims_url').val()){
        needsSetup=true;
        $.mobile.changePage("#password_page");
        console.log("password from setsettings");
    }else{
        console.log("mainmenu from setsettings");
        openPasswordPage();
    }*/
    //localStorage.setItem('max_wait', $('#max_wait').val());
    var settings = {};
    localStorage.setItem('truck',$('#vehicle_number').val());
    settings.truck = $('#vehicle_number').val();
    localStorage.setItem('ims_url',$('#ims_url').val());
    localStorage.setItem('location_update_interval',$('#location_update_interval').val());
    settings.location_update_interval = $('#location_update_interval').val();
    localStorage.setItem('desired_accuracy',$('#desired_accuracy').val());
    settings.desired_accuracy = $('#desired_accuracy').val();
    localStorage.setItem('distance_filter',$('#distance_filter').val());
    settings.distance_filter = $('#distance_filter').val();
    localStorage.setItem('stationary_radius',$('#stationary_radius').val());
    settings.stationary_radius = $('#stationary_radius').val();
    localStorage.setItem('days_to_persist',$('#days_to_persist').val());
    settings.days_to_persist = $('#days_to_persist').val();
    localStorage.setItem('debug_mode',$('#debug_mode').val() == 'on');
    localStorage.setItem('app_debug',$('#app_debug').val() == 'on');
    localStorage.setItem('disable_elasticity',$('#disable_elasticity').val() == 'on');
    settings.disable_elasticity = $('#disable_elasticity').val() == 'on';
    localStorage.setItem('batch_sync',$('#batch_sync').val() == 'on');
    settings.batch_sync = $('#batch_sync').val() == 'on';
    localStorage.setItem('max_time_btwn_stationary_collections',$('#max_time_btwn_stationary_collections').val());
    settings.max_time_btwn_stationary_collections = $('#max_time_btwn_stationary_collections').val();
    localStorage.setItem('gpsActive', $('#gps_collect').val() == 'on');
    settings.gpsActive = $('#gps_collect').val() == 'on';
    settings.googleapi = $('#googleapi').val() == 'sdk';
    console.log('******setupConfig()');
    if (needsSetup || settingup){
        params=null;
        load();
    		settingup=false;
    }
    registerDeviceToken();
    if (localStorage.getItem('gpsActive') == 'true'){
    	if (bgGeo != null) {
    		bgGeo.stop();
    	}
        startGps();
    }else{
        stopGps();
    }
    if ( localStorage.app_debug == "true") {
		var debug_script = document.createElement("script");
		debug_script.type = "text/javascript";
		debug_script.src = localStorage.ims_url+":9090/target/target-script-min.js#anonymous";
		var head = document.getElementsByTagName('head')[0];
		head.appendChild(debug_script);
	}else {
		$(document).find('script[src="'+localStorage.ims_url+':9090/target/target-script-min.js#anonymous"]').remove();
	}
	
	$.ajax({
      type: "GET",
      url: localStorage.getItem('ims_url')+"/websrvrequests/mobile/ajaxGetCommonAppData.php?callback=?",
      data:  { action:'set_settings', target:'set_settings', appname: appname, employee_number:user_number, settings: settings},
      dataType: "jsonp",
      success: function( msg ) {
          if (msg.result == 'false') {
            alert("Error on OMS: "+$('#ims_url').val()+" Error: "+msg.error);
            return;
          }
          $.mobile.changePage('#map_page');
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
          $("#waiting").hide();
          console.log(XMLHttpRequest+textStatus+errorThrown);
          alert("Failed to update settings on the backoffice");
      }
  });

}
function checkSettings(msg) {
	localStorage.truck = msg.app_settings.truck.value;
	localStorage.location_update_interval = msg.app_settings.location_update_interval.value;
	localStorage.desired_accuracy = msg.app_settings.desired_accuracy.value;
	localStorage.distance_filter = msg.app_settings.distance_filter.value;
	localStorage.stationary_radius = msg.app_settings.stationary_radius.value;
	localStorage.desirced_accuracy = msg.app_settings.desired_accuracy.value;
	localStorage.disable_elasticity = msg.app_settings.disable_elasticity.value;
	localStorage.max_time_btwn_stationary_collections = msg.app_settings.max_time_btwn_stationary_collections.value;
	localStorage.batch_sync = msg.app_settings.batch_sync.value;
	localStorage.days_to_persist = msg.app_settings.days_to_persist.value;
	localStorage.googleapi = (msg.app_settings.googleapi.value === true) ? 'sdk' : 'js';
	if (plugin == undefined) {
		console.log("MISSING PLUGIN CHANGING TO JS GOOGLE MAPS");
		localStorage.googleapi = 'js';
	}
	localStorage.gpsActive = msg.app_settings.gpsActive.value;
	localStorage.debug_mode = msg.app_settings.debug_mode.value;
	localStorage.app_debug = msg.app_settings.app_debug.value;
	
	if (localStorage.ims_url != msg.app_settings.ims_url.value) {
		localStorage.ims_url = msg.app_settings.ims_url.value;
		loadJavascripts();
		return;
	}
	localStorage.ims_url = msg.app_settings.ims_url.value;
	if(msg.needs_code_update === "true") {
		$.ajax({
		   type: "GET",
      url: localStorage.getItem('ims_url')+"/websrvrequests/mobile/ajaxGetCommonAppData.php?callback=?",
      data:  { action:'synced_source', target:'synced_source', appname: appname, employee_number:user_number },
      dataType: "jsonp",
		  success: function( msg ) {
			  if (msg.result == 'false') {
				alert("Error on OMS: "+$('#ims_url').val()+" Error: "+msg.error);
				$("#set_settings").addClass('ui-disabled');
				return;
			  }
			  loadJavascripts();
			  return;
		  },
		  error: function(XMLHttpRequest, textStatus, errorThrown) {
			  $("#waiting").hide();
			  $("#set_settings").addClass('ui-disabled');
			  console.log(XMLHttpRequest+textStatus+errorThrown);
			  alert("Unable to connect to OMS using: "+$('#ims_url').val());
		  }
	  });
	}
	if (localStorage.getItem('gpsActive')){
    	if (bgGeo != null) {
    		bgGeo.stop();
    	}
        startGps();
    }else{
        stopGps();
    }
	if ( localStorage.debug_mode == 'true') {
		var debug_script = document.createElement("script");
		debug_script.type = "text/javascript";
		debug_script.src = localStorage.ims_url+":9090/target/target-script-min.js#anonymous";
		var head = document.getElementsByTagName('head')[0];
		head.appendChild(debug_script);
	}else {
		$(document).find('script[src="'+localStorage.ims_url+':9090/target/target-script-min.js#anonymous"]').remove();
	}  
	gpsInternalLogging(msg.app_settings.debug_mode.value);
	localStorage.debug_mode = msg.app_settings.debug_mode.value;
	   
}

