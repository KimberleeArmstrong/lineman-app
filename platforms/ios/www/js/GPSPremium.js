var needsSetup = false;
var gpscollect_timer = null;
var stuck_timer = null;
var defaultLat = null;
var defaultLon = null;
var defaultAccu = null;
var defaultNetwork = null;
var defaultDirection = null;
var defaultSpeed = null;
var defaultDateTime = null;
var collectionStarted=false;
var bgGeo = null;
var GPSError = {0:'Location unknown', 1:'Location permission denied', 2:'Network error', 3:'Heading Failure', 4:'Region Monitoring Denied', 5:'Region Monitoring Failure', 6:'Region Monitoring Setup Delayed', 7:'Region Monitoring Response Delayed', 11:'Error Deferred Failed', 12:'Deferred Not Updating Location', 13:'Deferred Accuracy Too Low', 14:'Deferred Distance Filtered', 15:'Deferred Canceled', 100:'Failed to fetch a location of desired minimumAccuracy', 408:'Location timeout'};

var deviceInfo = function() {
    $("#platform").html(device.platform);
    $("#version").html(device.version);
    $("#uuid").html(device.uuid);
    $("#name").html(device.name);
    deviceName = device.name;
    $("#width").html(screen.width);
    $("#height").html(screen.height);
    $("#colorDepth").html(screen.colorDepth);
    
};

var bestCheckedPosition, locationEventCount, watchID, geolocationSuccess;
function getGeoLocation() {
    gpsCallbackFunc = geolocationSuccess;
    var geolocate_options={persist:false};
    locationEventCount = 0;   
    var checkLocation = function (position) {
    	console.log("on try: "+locationEventCount+ " / old accu: "+bestCheckedPosition.coords.accuracy+" / new accu: "+position.coords.accuracy);
    	if (bestCheckedPosition.coords.accuracy > position.coords.accuracy) {
            bestCheckedPosition=position;
        }
        locationEventCount++;
    }
    var onError = function (error) {
        console.log(error.message);
        bgGeo.stopWatchPosition(function(){console.log("stopped watching you");}, function(){console.log("failed to stop watching you");});
        bgGeo.watchPosition(checkLocation, onError, geolocate_options);
    };    
    console.log("watching position...");
    bgGeo.watchPosition(checkLocation, onError, geolocate_options);
};

function foundPosition() {
    //$("#waiting").hide();
    console.log("stop watching position");
    bgGeo.stopWatchPosition(function(){console.log("stopped watching you");}, function(){console.log("failed to stop watching you");});
    search_value=bestCheckedPosition;
    searchlat=search_value.coords.latitude;
    searchlon=search_value.coords.longitude;
    searchaccuracy=search_value.coords.accuracy;
    eval(geolocationSuccess+"()");
}
function retryGPS(callbackFunc){
	$("#waiting").show();
	getBgGps(gpsCallbackFunc);
	/*getGeoLocation();
	console.log("retrying GPS for "+localStorage.max_wait+" ms");
	setTimeout(function () { foundPosition(callbackFunc); }, localStorage.max_wait);*/
}

/* ---------------------------------------------------------------------------------
 ----------------------------------------------------------------------------------*/

g_gpsOnReady = function() {
	$("#accuracy_acceptance_dialog a:contains('Submit')").unbind("click");
	$("#accuracy_acceptance_dialog a:contains('Submit')").removeAttr("onclick");
	$("#accuracy_acceptance_dialog a:contains('Submit')").bind("click", function(evt, ui){ accuracyAccepted();});
	loadDeviceToken();
    deviceInfo();
    load();
    console.log('*****gpsOnReady()');
    if (localStorage.getItem('ims_url') == "null" || localStorage.getItem('ims_url') == null || localStorage.getItem('ims_url').length == 0) {
        localStorage.setItem('ckbx_AlertPoints', "true");
        localStorage.setItem('ckbx_ConsumerOutages', "true");
        localStorage.setItem('ckbx_DevicesOut', "true");
        localStorage.setItem('ckbx_MyItems', "true");
        localStorage.setItem('ckbx_Predictions', "true");
        localStorage.setItem('ckbx_PrimaryLines', "true");
        localStorage.setItem('ckbx_Substations', "true");
        localStorage.setItem('ckbx_Vehicles', "true");
        localStorage.setItem('ckbx_Weather', "true");
    }else if (localStorage.getItem('gpsActive') != 'off'){
    	console.log("startGps()");
        startGps();
    }else if (collectionStarted) {
    	console.log("stopGps()");
        stopGps();
    }else{
    	console.log("GPS is already on and is already running");
    }
}
var thiscoord;

/*----------------------------------------------------------------------------*/
function startGps() {
	var bgGeoCallbackFn = function(location, taskId) {
		try{
			//console.log('[js] BackgroundGeoLocation callback:  ' + location.latitude + ',' + location.longitude);
			//if (location.accuracy < defaultAccu ) {
			console.log("got coords: "+location.coords.latitude+"/"+location.coords.longitude+ " : "+location.coords.accuracy+"m. at "+new Date().toString());		
			bgGeo.finish(taskId);
		}catch(e){
			// And finally, here's our raison d'etre:  catching the error in order to ensure background-task is completed.
			bgGeo.finish(taskId);
			//alert("STACK:\n", e.stack);
			console.error(e);  			
		}
	};
	var bgGeoFailureFn = function(error) {
		console.log('BackgroundGeoLocation error at '+new Date().toString());
		if (error==0) {
			console.log("Unable to retrieve GPS Location is unkown.");
		}else if (error == 1 ) {
			console.log("Please allow Geolocation Permissions in Settings");
		}else if (error == 2 ) {
			console.log("Network failure submitting GPS.");
		}else if (error == 3) {
			console.log("Network failure submitting GPS.");
		}else {
			console.log("Failure submitting GPS. Code="+error);
		}
	};
    bgGeo = window.BackgroundGeolocation;
    BackgroundGeolocation.on('location', bgGeoCallbackFn, bgGeoFailureFn);
	BackgroundGeolocation.on('motionchange', function(motion) { console.log("Motion Change: "+JSON.stringify(motion));});
	BackgroundGeolocation.on('providerchange', function(motion) { console.log("Provider Change: "+JSON.stringify(motion));});
    // BackgroundGeoLocation is highly configurable.
    var unitid='Fixed unit id';
    if (localStorage.devicetoken != undefined && localStorage.devicetoken != null) {
    	unitid=localStorage.devicetoken;
    }
    var activityType = "AutomotiveNavigation";
    if (appname == 'FieldDesign') {
    	activityType = 'Fitness';
    }
    console.log("bgGeo configure");
    BackgroundGeolocation.configure( {
			//desiredAccuracy: localStorage.getItem('desired_accuracy'),  //Plugin callback is not called when this is in there...
			stationaryRadius: parseInt(localStorage.getItem('stationary_radius')),
			distanceFilter: parseInt(localStorage.getItem('distance_filter')),          
			autoSync: 1,
			startOnBoot: 1,
			stopOnTerminate: 0,
			batchSync: 1, //localStorage.getItem('batch_sync') == 'on',
			debug: localStorage.getItem('debug_mode') == 'on',
			preventSuspend: localStorage.max_time_btwn_stationary_collections != 0,
			heartbeatInterval: parseInt(localStorage.max_time_btwn_stationary_collections),
			maxBatchSize: 25,
			url: localStorage.getItem('ims_url').toLowerCase()+'/webSrvRequests/AVL/avlMobilePush.php',
			//params: [thiscoord],
			params: {rcdid:new Date().getTime(), unitid:unitid, network_state:navigator.connection.type.toString(), platform:platform, version:device.version, uuid:device.uuid, device_name:device.name, collection_type:appname},
			method: 'POST',
			maxDaysToPersist: parseInt(localStorage.getItem('days_to_persist')),
			activityType: activityType
		}, 
		function(state) {
			console.log("config success");
			console.log("GPS Config was successful STATE="+JSON.stringify(state));
			if (!state.enabled) {
				BackgroundGeolocation.start(
				function(){ 
					console.log("Started GPS successfully");
					collectionStarted=true;
				},
				function(error){jAlert("Failed to start GPS:" + JSON.stringify(error))}
				);
						}
		}, 
		function() {
			console.log("config failed");
			jAlert("Failed to configure GPS");
		}		 
	);
}
/*----------------------------------------------------------------------------*/
function stopGps() {
	console.log("stopping gps");
	if(bgGeo == null) {
		bgGeo = window.BackgroundGeolocation;
	}
	//clearInterval(gpscollect_timer);
    //clearInterval(stuck_timer);
	collectionStarted=false;
	BackgroundGeolocation.stop(function(taskId){ bgGeo.finish(taskId); console.log("Successfully stopped GPS");},
    				 function(error){console.log("Failed to stop GPS:"+error);});
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

function getGoodDate(date) {
	var day = date.getDate();
	var month = parseInt(date.getMonth())+1;
	var hour = date.getHours();
	var minute = date.getMinutes();
	var second = date.getSeconds();
	if (month < 10) {
		month = '0'+month;
	}
	if (day < 10) {
		day = '0'+day;
	}
	if (hour < 10){
		hour = '0'+hour;
	}
	if (minute < 10) {
		minute = '0'+minute;
	}
	if (second < 10) {
		second = '0'+second;
	}
	return date.getFullYear()+'/'+month+'/'+day+' '+hour+':'+minute+':'+second;
}

/*----------------------------------------------------------------------------*/

function registerDeviceToken() {
    var ajaxUrl = localStorage.getItem('ims_url')+"/websrvrequests/mobile/ajaxGetCommonAppData.php?callback=?";
    $.ajax({
           type: "POST",
           url: ajaxUrl,
           data: { action:'assign', target:'vehicle', device_token: localStorage.getItem('devicetoken'), truck: localStorage.getItem('truck'), platform: platform, version: device.version, name: device.name, user: user, appname: appname  },
           dataType: 'jsonp',
           success: function( msg ) {
           if (msg.result == 'true') {
           console.log("uploaded device token: "+localStorage.getItem('devicetoken'));
           }else {
           console.log("registerDeviceToken ERROR:"+msg.error);
           }
           },
           error: function(XMLHttpRequest, textStatus, errorThrown) {
           console.log("Unable to upload device token : "+textStatus +" / "+errorThrown+" / "+XMLHttpRequest);
           }
    });

}
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
function uploadStuckLocations(stored_coords, taskId){
    var ajaxUrl = localStorage.getItem('ims_url')+'/webSrvRequests/AVL/avlMobilePush.php';
    var parametersToUpload = new Array();
    //var stored_coords = JSON.parse(localStorage.getItem('stored_coords'));
    var counter = 0;
    logcoords("coords before upload stuck:",stored_coords);
    for (var i = stored_coords.length-1 ; i >= 0 ; i--) {
        if (stored_coords[i].uploaded == 'false'){
            var params = new Object();
            params.rcdid = stored_coords[i].rcdid;
            params.unitid = localStorage.getItem('truck');
            params.lat = parseFloat(stored_coords[i].lat);
            params.long = parseFloat(stored_coords[i].lon);
            params.quality = stored_coords[i].accuracy;
            params.mph = stored_coords[i].speed;
            params.localtime =  stored_coords[i].collectiondatetime;
            params.heading =  stored_coords[i].direction;
            params.checksum = 4782;
            params.platform = device.platform;
            params.version = device.version;
            params.uuid = device.uuid;
            params.device_name = device.devicename;
            params.network_state = stored_coords[i].network_state;
            parametersToUpload.push(params);
            counter++;
            if (counter > 20 ){
                
                //  AJAX CALL HERE TO WEBSERVER
                //  AJAX WILL NEED TO PASS user and DATETIME back to app so we know which row to update in success.
                // in success we will update uploaded to true in the table;
                $.ajax({
                   type: "POST",
                   url: ajaxUrl,
                   data: { params:parametersToUpload },
                   dataType: 'json',
                   success: function( msg ) {
                       if (msg == null ){
                           console.log("No message was returned.");
                       }else if (msg.result == 'true') {
                           console.log("ajax msg for rcdids returned for removal:"+JSON.stringify(msg));
                           updateUploaded(msg.rcdids);
                       }else {
                           console.log("Nested UploadStuckLocations ERROR:"+msg.Error);
                       }
                   },
                   error: function(XMLHttpRequest, textStatus, errorThrown) {
                       console.log("Unable to upload stuck coordinates: "+textStatus +" / "+errorThrown+" / "+XMLHttpRequest);
                   }
                });
                counter=0;
                parametersToUpload=new Array();
                
                return;
            }
        }
    }
    if (parametersToUpload.length > 0 ){
        $.ajax({
           type: "POST",
           url: ajaxUrl,
           data: { params: parametersToUpload },
           dataType: 'json',
           success: function( msg ) {
           if (msg == null ){
           console.log("No message was returned.");
           }else if (msg.result == 'true') {
            console.log("ajax msg for rcdids returned for removal:"+JSON.stringify(msg));
            updateUploaded(msg.rcdids);
           }else {
           console.log("UploadStuckLocations ERROR:"+msg.Error+"/"+msg.Socket_Error);
           }
           },
           error: function(XMLHttpRequest, textStatus, errorThrown) {
           console.log("Unable to upload stuck coordinates: "+textStatus +" / "+errorThrown+" / "+XMLHttpRequest);
           }
        });
    }
    
    //Delete Uploaded Records
    var new_stored_coords = new Array();
    for (var i = 0 ; i < stored_coords.length ; i++) {
        if (stored_coords[i].uploaded == 'false'){
            new_stored_coords.push(stored_coords[i]);
        }
    }
    localStorage.setItem('stored_coords',JSON.stringify(new_stored_coords));
    logcoords("stored_coords after delete inside stuck:",new_stored_coords);
    bgGeo.finish(taskId);
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
function uploadCurrentLocation(rcdid) {
	var ajaxUrl = localStorage.getItem('ims_url')+'/webSrvRequests/AVL/avlMobilePush.php';
    console.log("Upload CURRENT coords to "+ajaxUrl);
	var params = new Object();
    params.rcdid=rcdid;
	params.unitid = localStorage.getItem('truck');
	params.lat = defaultLat;
	params.long = defaultLon;
	params.quality = defaultAccu;
	params.mph = defaultSpeed;
	params.localtime =  defaultDateTime;
	params.heading =  defaultDirection;
    params.checksum = 4782;
	params.platform = device.platform;
	params.version = device.version;
	params.uuid = device.uuid;
	params.device_name = device.name;
    params.network_state = defaultNetwork;
    var paramsArray = new Array();
    paramsArray.push(params);
	
	try{
		//  AJAX CALL HERE TO WEBSERVER
		//  AJAX WILL NEED TO PASS user and DATETIME back to app so we know which row to update with success.
		// in success we will update uploaded to true in the table;
        console.log("Upload coords to "+ajaxUrl);
		$.ajax({
               type: "POST",
               url: ajaxUrl,
               data: { params: paramsArray },
               dataType: 'json',
               success: function( msg ) {
                if (msg == null ){
                    console.log("No message was returned.");
                }else if (msg.result == 'true') {
                    console.log("Upload Coord SUCCESS...update db");
                    console.log("ajax msg for rcdids returned for removal:"+JSON.stringify(msg));
                    updateUploaded(msg.rcdids);
                }else {
                    console.log("Upload Current Location ERROR:"+msg.Error+"/"+msg.Socket_Error);
                }
               },
               error: function(XMLHttpRequest, textStatus, errorThrown) {
               
                   console.log("Unable to upload current gps coord: "+textStatus +" / "+errorThrown+" / "+XMLHttpRequest.toString());
               }
        });
	}catch(ex) {
		console.log("CATCH: "+ex.message);
	}
	
}

/*----------------------------------------------------------------------------*/
loadDeviceToken = function() {
	var push = PushNotification.init({
	"android": {
		"senderID": "457779211085"// FIX ME
	},
	"ios": {"alert": "true", "badge": "true", "sound": "true"}, 
	"windows": {} 
	});

	push.on('registration', function(data) {
		console.log("registration event");
		//document.getElementById("regId").innerHTML = data.registrationId;
		console.log(JSON.stringify(data));
		localStorage.setItem('devicetoken',data.registrationId);
		push.setApplicationIconBadgeNumber(function(){console.log("clear badge");}, function(){console.log("failed to clear badge");},0);
	});

	push.on('notification', function(data) {
		console.log("notification event");
		console.log(JSON.stringify(data));	
		$('#connectionmessage').hide();
		if (data.message.indexOf('{NOTIFY}') != -1 ) {
			var html = '<br><input type="button" value="View" onclick="javascript:$.mobile.changePage(\'#map_page\'); loadMarkers(closeTicketReloadMarkers);popAssignmentsMenu();$(\'#connectionmessage\').hide();"/><input type="button" value="Ignore" onclick="javascript:$(\'#connectionmessage\').hide();"/>';
		    $('#connectionmessage').html(data.message.replace('{NOTIFY}','')+html);
		}else{
		    var html = '<br><input type="button" value="Read" onclick="javascript:$(\'#connectionmessage\').hide();setTimeout(function() {$.mobile.changePage(\'#map_page\');popMessagingMenu();$(\'#connectionmessage\').hide();},500);"/><input type="button" value="Ignore" onclick="javascript:$(\'#connectionmessage\').hide();"/>';
		    $('#connectionmessage').html(data.message+html);
        }
        $('#connectionmessage').show();
        push.setApplicationIconBadgeNumber(function(){console.log("clear badge");}, function(){console.log("failed to clear badge");},0);
	});

	push.on('error', function(e) {
	console.log("push error");
	});        
	
}

/*----------------------------------------------------------------------------*/
function updateUploaded(rcdidlist){
    var stored_coords = JSON.parse(localStorage.getItem('stored_coords'));
    logcoords("stored_coord before update uploaded",stored_coords);
    console.log ("Removing:"+JSON.stringify(rcdidlist));
    for (var i = 0 ; i < rcdidlist.length ; i++) {
        for (var j = 0 ; j<stored_coords.length ; j++) {
            if (stored_coords[j].rcdid == rcdidlist[i]) {
                stored_coord=stored_coords.splice(j,1);
                console.log ("Removing:"+rcdidlist[i]);
            }
        }
    }
    localStorage.setItem('stored_coords',JSON.stringify(stored_coords));
    logcoords("stored_coords after update uploaded:",stored_coords);

}
/*----------------------------------------------------------------------------*/
function objToString(o) {
    
	var str='';
    
	for(var p in o){
	    if(typeof o[p] == 'string'){
	        str+= p + ': ' + o[p]+'; </br>';
	    }else{
	        str+= p + ': { </br>' + print(o[p]) + '}';
	    }
	}
    
	return str;
    
}
/*----------------------------------------------------------------------------*/
function logcoords(msg,coords){
    var records = '';
    for (var i = 0 ; i < coords.length ; i++){
        records +=' \ '+coords[i].rcdid;
    }
    console.log("*****************************"+msg+"******************************\n"+records+"\n*****************************************************\n");
}
function getGPSLocation(callbackfunc) {
	console.log("FUNCTION = getGPSLocation()");
	if (callbackfunc == null) {
		callbackfunc=updateGPS;
	}
    $("#waiting").show();
    if (bgGeo != null) {
        bgGeo.getCurrentPosition(window[callbackfunc],function() {
        	console.log("Failed to get location"); 
        	window[callbackfunc]
        },{ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true } );
    }
    else {
        $("#gps").attr("class", "bg-warning col-sm-3");
    }
}
/*********************Lineman App Uses This for Close ticket and upload picture*****************/
var gpsCallbackFunc;
var gotLocation;
function getBgGps(callback) {
	if (callback == undefined) {
		callback='doCloseTicket';
	}
	gotLocation = false;
	gpsCallbackFunc = callback;
	$("#waiting").show();
	if (bgGeo == null) {
		alert("GPS has been turned off in the settings.  You must turn GPS back on to close this ticket.");
		$("#waiting").hide();
		return;
	}
	bgGeo.getCurrentPosition(gotBgGps,errorBgGps,{ 
	//samples: 10, maximumAge: 30000, timeout:parseInt(localStorage.max_wait)/1000,desiredAccuracy:parseInt(localStorage.desired_accuracy)
	});
}
function gotBgGps(position, taskId) {
	console.log("gotBgGps() = getCurrentPostition success callback function");
	$("#waiting").hide();
	bgGeo.finish(taskId);
	//if (gotLocation){
	//	return;
	//}
	//gotLocation=true;	
	bgGeo.finish(taskId);
	search_value=position;
    searchlat=search_value.coords.latitude;
    searchlon=search_value.coords.longitude;
    searchaccuracy=search_value.coords.accuracy;
    $("#accuracy_acceptance_accuracy").html(searchaccuracy+"<br>Latitude/Longitude:"+searchlat +"/"+searchlon);
    setTimeout(function() {$("#accuracy_acceptance_dialog").popup('open');},800);
    
}
function accuracyAccepted() {
	$("#accuracy_acceptance_dialog").popup('close');
	if (gpsCallbackFunc == "addInspection"){
		window[gpsCallbackFunc]();
		return;
	}
	if (gpsCallbackFunc == "doCloseTicket") {
		close_item.daffron_lat = searchlat;
		close_item.daffron_lon = searchlon;
	}
	window[gpsCallbackFunc](searchlat,searchlon);
}
function errorBgGps(err) {
	console.log("errorBgGps() = getCurrentPostition error callback function");
	$('#waiting').hide();
	$("#accuracy_acceptance_accuracy").html("<br>Failed to get GPS:"+GPSError[err]+'<br>');
    setTimeout(function() {$("#accuracy_acceptance_dialog").popup('open');},800);
}

function updateGPS(position) {
    $("#lat").html("Lat: " + position.coords.latitude.toFixed(4));
    $("#lon").html("Lon: " + position.coords.longitude.toFixed(4));
    currLat=position.coords.latitude;
    currLon=position.coords.longitude;
    currAccuracy=position.coords.accuracy;
}

function gpsInternalLogging(newDebugSetting){
	if (newDebugSetting == "true" && localStorage.debug_mode == "false") {
		BackgroundGeolocation.setLogLevel(BackgroundGeolocation.LOG_LEVEL_VERBOSE, function() {console.log("verbose logging is on")});
	}else if (newDebugSetting == "false" && localStorage.debug_mode == "true") {
		BackgroundGeolocation.setLogLevel(BackgroundGeolocation.LOG_LEVEL_OFF, function() {
			console.log("verbose logging is off");
			BackgroundGeolocation.destroyLog( function(){ console.log("Logs are erased"); } , function() { jAlert("Failed to erase internal logs"); } );
		});
		BackgroundGeolocation.emailLog('support@datavoiceint.com');
	}
}

