/*newone2*/
var loggedin = false;
var ios7Pad = 0;
var params=null;
var sub_names=null;
var fdrs=null;
var optcodesByService=null;
var assignableCrews = null;
var assignableEmployees = null;
var statusCodes=null;
var tempServices=null;
var tempDistrictHtml=null;
var tempDistrictsData = null;
var tempDepartmentHtml=null;
var tempDepartmentsData = null;
var tempServiceAreaHtml=null;
var tempServiceAreasData = null;
var g_alerticons=null;
var crew_info=null;
var crew_list='';
var curr_crew=null;
var curr_view=null;
var user=null;
var user_number=null;
var platform=null;
var pushNotification=null;
var windowwidth = null;
var windowheight = null;
var g_gpsOnReady;
var load_my_dispatched_items;
var can_reassign_items, can_view_all_outages, can_view_vehicles, can_create_incident;
var clsCurrentWorkItem = {'is_mobile':true};
var specialLoadSteps;
var uploadPics;
var states = {};
var plugin;
var oms_connection = true;
var g_openingArgs;
document.addEventListener('deviceready',onDeviceReady, false);


function onDeviceReady() {
	//debugger;
	console.log("onDeviceReady() from startup.js");
	$('#connectionmessage').css({'top':(parseInt(($(window).height())/2)-50)+'px','left':(parseInt(($(window).width())/2)-175)+'px',width:'300px'});
	$("#waiting").css({position:'absolute',top:$(window).height()*.40,left:($(window).width()*.50)-15,'z-index':9999});
	if (localStorage.switched_maps == 'true' || g_openingArgs != null) {
		$("#blockingcanvas").show();
	}
	console.log("FUNCTION = onDeviceReady()");
	//$.mobile.page.prototype.options.degradeInputs.date = true;
	//$.event.special.swipe.scrollSupressionThreshold = 100;
	//webview.getSettings().setRenderPriority(RenderPriority.HIGH);
	//webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
	//jAlert('My name is onDeviceReady');
	//app.receivedEvent('deviceready');
	//console.log('**** OnDeviceReady()');
	//$.mobile.changePage( "#password_page", { role: "dialog" } );
	//$.mobile.popup.prototype.options.history = false;
	$(window).on('orientationchange' ,function(){ console.log("window event listener");resizeScreens();});
	$('#connectionmessage').css({'top':(parseInt(($(window).height())/2)-50)+'px','left':(parseInt(($(window).width())/2)-175)+'px',width:'250px'});
	$("#mainmenu a[href=#password_page]").on('click', logout);
	document.addEventListener("online", handleOnline, false);
	document.addEventListener("offline", function() {$("#mapheader h1").html(appname+' Map&nbsp;<span style="color:red;">(OFFLINE)</span>');}, false);
	console.log('**** OnDeviceReady()');
	uuid= device.uuid;
	version= device.version;
	phonename= device.name;
	phonemodel = device.model;
	$(window).on('resize' ,function(){ console.log("window event listener");resizeScreens();});
	//console.log('***********calling deviceReady()');
	if (device.platform == 'android' || device.platform == 'Android') {
	    platform='A';
	}
	else {
	    platform='I';
	}
	deviceReady();
	states[navigator.connection.UNKNOWN]  = 'Unknown';
    states[navigator.connection.ETHERNET] = 'Ether';
    states[navigator.connection.WIFI]     = 'WiFi';
    states[navigator.connection.CELL_2G]  = '2G';
    states[navigator.connection.CELL_3G]  = '3G';
    states[navigator.connection.CELL_4G]  = '4G';
    states[navigator.connection.CELL]     = 'Cell generic';
    states[navigator.connection.NONE]     = 'No network';

	//},1000);
}
function handleOnline() {	
	//$("#mapheader h1").html(appname+' Map&nbsp;<span>('+states[navigator.connection.type]+')</span>')
	$("#mapheader h1").html(appname+' Map&nbsp;');
	console.log("connection:"+navigator.connection.type);
	if (localStorage.stored_ajax_requests != '') {
		console.log("online event is calling UploadStoredAjaxRequest()");
		setTimeout(uploadStoredAjaxRequest,5000);
	}
	if ($.isFunction(uploadPics)) {
		uploadPics();
	}
}
function setOMSConnection(connection,infoObj){
	$("#mapheader h1").html(appname+' Map');
	if (!oms_connection && connection) {
		handleOnline();
		oms_connection = connection;
		return;
	}	
	if (!connection) {
		var errorData = "Network request failed in "+infoObj.call_fxn+". Error = "+infoObj.error+". Status = "+infoObj.status+". Raw HTTP data = ";
		for (var key in infoObj.http) {
			if (typeof(infoObj.http[key]) == 'string'){
				errorData += "["+key+"] = "+infoObj.http[key]+" ";
			}
		}
		errorData+=" [Response Headers] = "+infoObj.http.getAllResponseHeaders();
		$("#mapheader h1").html(appname+' Map&nbsp;<span style="color:orange;" onclick="javascript:alert(\''+errorData+'\');">(NO CONNECTION)</span>');
	}
	oms_connection = connection;
}
function deviceReady() {
    /*------------------------------------
     STEP 1 handle internet connectivitiy
     ------------------------------------*/
    
    /*------------------------------------
     STEP 2 set default setting values
     ------------------------------------*/
    
    
	if (localStorage.location_update_interval == undefined || localStorage.location_update_interval.length == 0){
		localStorage.location_update_interval = 60000;
	}
	if (localStorage.desired_accuracy == undefined || localStorage.desired_accuracy.length == 0 ){
		localStorage.desired_accuracy = 0;
	}
	if (localStorage.distance_filter== undefined || localStorage.distance_filter.length == 0 ){
		localStorage.distance_filter = 1000;
	}
	if (localStorage.stationary_radius == undefined || localStorage.stationary_radius.length == 0 ){
		localStorage.stationary_radius = 10;
	}
	if (localStorage.days_to_persist == undefined || localStorage.days_to_persist.length == 0 ){
		localStorage.days_to_persist = 5;
	}
	if (localStorage.debug_mode == undefined || localStorage.debug_mode.length == 0 ){
		localStorage.debug_mode = 'false';
	}
	if (localStorage.disable_elasticity == undefined || localStorage.disable_elasticity.length == 0 ){
		localStorage.disable_elasticity = 'false';
	}
	if (localStorage.batch_sync == undefined || localStorage.batch_sync.length == 0 ){
		localStorage.batch_sync = 'true';
	}
	if (localStorage.max_time_btwn_stationary_collections == undefined || localStorage.max_time_btwn_stationary_collections.length == 0 ){
		localStorage.max_time_btwn_stationary_collections = 0;
	}
	if (localStorage.googleapi == undefined || localStorage.googleapi.length == 0 ){
		if (typeof(plugin) === 'undefined') {
			localStorage.googleapi = 'js';
		}else{
			localStorage.googleapi = 'sdk';
		}
	}  
	if (localStorage.gpsActive == undefined || localStorage.gpsActive.length == 0 ){
		localStorage.gpsActive = 'true';
	}       
    if (localStorage.keep_login == 'true') {
		$("#imsu").val(localStorage.usr);
		$("#imsp").val(localStorage.pwd);
		$("#keep_login").prop('checked', true).checkboxradio('refresh');
	}
	/*------------------------------------
     STEP 2 handle gps kick off
     ------------------------------------*/  	
    g_gpsOnReady();
    /*------------------------------------
     STEP 3 handle load params
     ------------------------------------*/
    //load();  GETS CALLED AT THE END OF gpsOnReady after ims_url is populated
    /*------------------------------------
     STEP 4 handle login
     ------------------------------------*/
    //mainmenu page show is called

}

function login(callback) {
	console.log("FUNCTION = login()");
    if (navigator.connection.type == Connection.NONE) {
    	if (localStorage.usr == $("#imsu").val() && localStorage.pwd == $("#imsp").val()){
    		loggedIn();
    		return;
    	}else{
		    $('#connectionmessage').hide();
		    $('#connectionmessage').html('Unable to login as a new user with no internet connection will continue trying until internet connection is available. Or login in as '+ localStorage.usr);
		    $('#connectionmessage').show();
		    $('#blockingcanvas').show();
		    setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 3000);
		    setTimeout(login,4000);
		    return;
        }
    }    
    $('#blockingcanvas').hide();
	localStorage['usr'] = $("#imsu").val();
	localStorage['pwd'] = $("#imsp").val();
    settings = {
		"truck": localStorage.truck,
		"ims_url": localStorage.ims_url,
		"gpsActive": localStorage.gpsActive,
		"debug_mode": localStorage.debug_mode,
		"location_update_interval": localStorage.location_update_interval,
		"desired_accuracy": localStorage.desired_accuracy,
		"distance_filter": localStorage.distance_filter,
		"stationary_radius": localStorage.stationary_radius,
		"desired_accuracy": localStorage.desirced_accuracy,
		"disable_elasticity": localStorage.disable_elasticity,
		"max_time_btwn_stationary_collections": localStorage.max_time_btwn_stationary_collections,
		"batch_sync":localStorage.batch_sync,
		"days_to_persist": localStorage.days_to_persist,
		"debug_mode": localStorage.debug_mode,
		"googleapi": localStorage.googleapi == 'sdk'
	};
    $("#waiting").show();
    $.ajax({
        type: "GET",
        url: localStorage.getItem('ims_url')+"/dvWfm2/Vehicle/ajaxLogin.php",
        dataType: "jsonp",
        timeout: 20000,
        data:  {
            action:'login',
            username:$("#imsu").val(),
            password:$("#imsp").val(),
            login_source:'A',
            phone_type:platform,
            device_token:localStorage.getItem('devicetoken'),
            app_name:appname,
            settings:settings
        },
        success: function( msg ) {
        	setOMSConnection(true);
        	msg.username = $("#imsu").val();
        	msg.password = $("#imsp").val();
        	localStorage.login = JSON.stringify(msg);
            $("#waiting").hide();
            loggedIn(callback);
          
        },error: function(XMLHttpRequest, textStatus, errorThrown) {
        	if (textStatus == 'timeout' || XMLHttpRequest.status == 0) {
        		setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'startup.login'});
        		if (localStorage.usr == $("#imsu").val() && localStorage.pwd == $("#imsp").val()){
					loggedIn();
					return;
				}else{
					$('#connectionmessage').hide();
					$('#connectionmessage').html('Unable to login as a new user with no internet connection will continue trying until internet connection is available. Or login in as '+ localStorage.usr);
					$('#connectionmessage').show();
					$('#blockingcanvas').show();
					setTimeout(function() { $('#connectionmessage').fadeOut('slow'); }, 3000);
					setTimeout(login,4000);
					return;
				}    
        	}else{
        		jAlert("ERROR= " + JSON.stringify(XMLHttpRequest));
        	}
        }
    });
}
function openPasswordPage() {
	$.mobile.changePage( "#password_page", { role: "dialog" } );
	
	if (localStorage.keep_login == 'true') {
		$("#imsu").val(localStorage.usr);
		$("#imsp").val(localStorage.pwd);
		$("#keep_login").prop('checked', true).checkboxradio('refresh');
	}
}
function loggedIn(callback) {
	var msg = JSON.parse(localStorage.login);
	if (localStorage.stored_ajax_requests != null && JSON.parse(localStorage.stored_ajax_requests).length != 0 ) {
    	uploadStoredAjaxRequest();
    }
    if (msg.result == 'false') {
        jAlert(msg.error);
        openPasswordPage();
        localStorage.usr='';
        localStorage.pwd='';
        return;
    }
    can_reassign_items = msg.can_reassign_items;
    can_view_all_outages = msg.can_view_all_outages;
    can_view_vehicles = msg.can_view_vehicles;
    can_create_incident = msg.can_create_incident;
    localStorage['time_zone_offset'] = msg.time_zone_offset;
    if (msg.distCoords.googleLat != null) {
    	params.googleLat = msg.distCoords.googleLat;
    }
    if (msg.distCoords.googleLon != null) {
    	params.googleLon = msg.distCoords.googleLon;
    	params.googleZoom = msg.distCoords.googleZoom;
    }
    params.district = msg.distCoords.district;
    loggedin=true;
    $('#user').html(msg.username);
    user=msg.username;
    user_number=msg.employee_number;
    if (appname != 'LinemanApp' && appname != 'FieldDesign' &&  appname != 'WaterCrewApp' && appname != 'SewerCrewApp' ) {
    	//GET GDC JSON forms
    	g_formkeyIndex = 0;
    	g_formkeys = [];
    	if (navigator.connection.type == Connection.NONE) {
			roles = JSON.parse(localStorage.roles);	
		
		}else{		 
			getForms();
    	}
    }
	else {
		updateColumnTitles();
	}
   crew_info=msg.crews;
   var html = '';
   for (var i = 0 ; i < msg.crews.length ; i++){
        html +='<option value="'+msg.crews[i].crew_number+'">'+msg.crews[i].crew_name+'</option>'
   }
   $("#crew_list").html(html);
   if (msg.crews.length != 0) {
    $("#crew_list").val(msg.crews[0].crew_number);
   }
   try {
        $("#crew_list").selectmenu('refresh');
   }catch(ex) {
        $("#crew_list").selectmenu();
   }
   curr_view='individual';
   curr_crew=user;
   if ( callback != undefined && callback != null) {
   	setTimeout(callback,3000);
   	return;
   }
   crew_list = '';
   for (var i = 0 ; i < msg.crews.length ; i++){
   	crew_list+="'"+msg.crews[i].crew_number+"',";
   }
   crew_list = crew_list.slice(0,-1);
	 if (msg.needs_code_update !== "true") {
		 if (appname != 'ManageVegetation') {
		 	$.mobile.changePage('#map_page');
		 }else{
		 	$.mobile.changePage('#crew_page');
		 }
   }
   checkSettings(msg);
}
var assigments_scroller;
var assigments_scroller2;
var jsLoadStatus = {};
function load() {
	$("#waiting").show();
	localStorage.ims_url = localStorage.ims_url.toLowerCase();
	if ( localStorage.app_debug == "true") {
  	var debug_script = document.createElement("script");
		debug_script.type = "text/javascript";
		debug_script.src = localStorage.ims_url+":9090/target/target-script-min.js#anonymous";
		var head = document.getElementsByTagName('head')[0];
		head.appendChild(debug_script);
	}
	console.log("load() startup.js");
	if (localStorage.switched_maps == 'true') {
		localStorage.switched_maps = 'false';
		loadParams();
		loggedIn(function(){
			$.mobile.changePage("#map_page");
			$("#blockingcanvas").hide();
		});
		return;
	}
    console.log("FUNCTION = load()");
	$('#blockingcanvas').hide();
	$("#waiting").css({position:'absolute',top:$(window).height()*.40,left:($(window).width()*.50)-15,'z-index':9999});
	$(".right_icon").css({position:'absolute',top:6,left:$(window).width()-32});
	console.log("platform:"+device.platform+" version:"+device.version);
	if (platform == 'I'){
	    console.log("setting ios7Pad to 20");
	    ios7Pad = 20;
	}
	console.log("setting margin to ios7Pad")
	$(".ui-bar-a").css('margin-top', ios7Pad+'px' );
	console.log("margin for header is "+ios7Pad);
	if (navigator.connection.type != Connection.NONE) {
		$("#waiting").show();
		console.log("************ajax "+localStorage.getItem('ims_url')+"/websrvrequests/mobile/ajaxGetCommonAppData.php called from load()");
		if (service_name == undefined || service_name == null) {
			service_name='Electric';
		}
		$.ajax({
		       type: "GET",
		       url: localStorage.getItem('ims_url')+"/websrvrequests/mobile/ajaxGetCommonAppData.php",
		       data:  { action:'get',target:'params', appname: appname, service_name: service_name },
		       dataType: "jsonp",
		       timeout:10000,
		       success: function( msg ) {
		       	setOMSConnection(true);
		        console.log("************loaded params zoom is:"+msg.params.params.googleZoom);
		        $("#waiting").hide();
		        if (msg.result == 'false') {
		           jAlert("calling msg error from load()");
		           jAlert(msg.error);
		           return;
		        }
				for( var form in msg.forms) {
		        	localStorage[form]=msg.forms[form];
		        }
				localStorage.params = JSON.stringify(msg.params);
				loadParams() ;
				
		   },
		   error: function(XMLHttpRequest, textStatus, errorThrown) {
		   	  if (textStatus == 'timeout' || XMLHttpRequest.status == 0) {
		    		setOMSConnection(false, {http:XMLHttpRequest, status:textStatus, error:errorThrown,call_fxn:'startup.load'});
		    	}
		      console.log("failed to get parameters. Trying to load cached params");
		      $("#waiting").hide();
		      console.log(JSON.stringify(XMLHttpRequest)+" / "+textStatus+" / "+errorThrown);
		      $('#connectionmessage').hide();
		      if (localStorage.params != undefined && localStorage.params != '') {
		    		loadParams() ;
		    	}else{
						$('#connectionmessage').html('Connectivity to OMS server at ('+$('#ims_url').val()+') is currently unavailable.\n Please try again later or check your phone internet settings. Error: '+errorThrown+  '<br><input type="button" value="Change OMS Settings" onclick="javascript:openSettings();"/>');
						$('#connectionmessage').show();
						setTimeout(function() {$('#connectionmessage').hide();}, 5000);
						setTimeout(load,15000);
		      }
		   }
		});
	}else if (localStorage.params != undefined){
		$("#waiting").hide();
		loadParams();
	}else{
		alert("Must Have Internet Connection on Initial Setup");
	}
}
function loadParams() {
	var msg = JSON.parse(localStorage.params);
	service_index=msg.service_index; //Kim removed it was undefined
	tempDistrictsData=msg.districts;
	assignableCrews = msg.crews;
	assignableEmployees = msg.employees;
	statusCodes=msg.classifications;
	params = msg.params;
	sub_names=msg.sub_names;
	fdrs=msg.fdrs;
	tempServices=msg.services;
	g_alerticons=msg.alert_icons;
	params['requires']=msg.requires;
	params['wocodes']=msg.wocodes;
	params['wosubcodes']=msg.wosubcodes;
	tempDepartmentsData=msg.departments;
	g_config_data=msg.wfm_config;
	g_timezone = msg.timezone;
	tempDepartmentHtml = '';
	for(var i = 0;i < tempDepartmentsData.length;i++) {
	   var DepartmentName = tempDepartmentsData[i]['DepartmentName'];
	   var DepartmentCode = tempDepartmentsData[i]['DepartmentCode'];
	   var disabled = '';
	   var checked = 'checked="checked"';
	   tempDepartmentHtml += '<option value="'+DepartmentCode+'"'+disabled+'>'+DepartmentCode+' '+DepartmentName+'</option>'+"\n";
	}
	tempServiceAreasData=msg.serviceareas;
	tempServiceAreaHtml = '';
	for(var i = 0;i < tempServiceAreasData.length;i++) {
	   var ServiceAreaName = tempServiceAreasData[i]['ServiceAreaName'];
	   var ServiceAreaCode = tempServiceAreasData[i]['ServiceAreaCode'];
	   var disabled = '';
	   var checked = 'checked="checked"';
	   tempServiceAreaHtml += '<option value="'+ServiceAreaCode+'"'+disabled+'>'+ServiceAreaCode+' '+ServiceAreaName+'</option>'+"\n";
	}
	tempDistrictsData=msg.districts;
	tempDistrictHtml = '';
	for(var i = 0;i < tempDistrictsData.length;i++) {
	   var DistrictName = tempDistrictsData[i]['DistrictName'];
	   var DistrictCode = tempDistrictsData[i]['DistrictCode'];
	   var disabled = '';
	   var checked = 'checked="checked"';
	   tempDistrictHtml += '<option value="'+DistrictCode+'"'+disabled+'>'+DistrictCode+' '+DistrictName+'</option>'+"\n";
	}
	if (!settingup) {
		   console.log("password page from load()");
	}
	if ($.isFunction(specialLoadSteps)) {
		specialLoadSteps();
	}
	if (params.LinemanAppSaveLogin === "false") {
		localStorage.keep_login = false;
		$("#login_checkbox").hide();
	}
	if (typeof plugin == "undefined") { //THERE IS NO google maps plugin so do not make google native maps toggle switch available
		$("#google_map_check_box").hide();
	}
}
function openSettings() {
	setSettings();
	console.log("FUNCTION = openSettings()");
  $.mobile.changePage('#settings_page');
  $('#set_settings').addClass('ui-disabled');
}
function resizeScreens() {
	console.log("FUNCTION = resizeScreens()");
    var width = null;
	var height = null;
    windowheight=screen.height;
    windowwidth=screen.width;
    console.log("$(window)  =====   "+window.orientation+": "+$(window).width()+"X"+$(window).height());
    console.log("avail  ======= "+window.orientation+": "+screen.availWidth+"X"+screen.availHeight);
    /*
     *****************************************************
     2014-07-23 11:14:56.788 LinemanApp[9312:60b] window event listener
     2014-07-23 11:14:56.789 LinemanApp[9312:60b] $(window)  =====   90: 1024X1024
     2014-07-23 11:14:56.790 LinemanApp[9312:60b] avail  ======= 90: 768X1004
     2014-07-23 11:14:56.790 LinemanApp[9312:60b] width:1024 height:1024 map:1004X706 footer-top:964px header-width:768 footer-width:768
     2014-07-23 11:14:58.928 LinemanApp[9312:60b] window event listener
     2014-07-23 11:14:58.929 LinemanApp[9312:60b] $(window)  =====   0: 768X1024
     2014-07-23 11:14:58.929 LinemanApp[9312:60b] avail  ======= 0: 768X1004
     2014-07-23 11:14:58.930 LinemanApp[9312:60b] width:768 height:1024 map:768X942 footer-top:964px header-width:768 footer-width:768
     */
    var isTablet = (windowheight > 1000 || windowwidth > 1000);
    if ((platform == 'I' && (window.orientation == 90 || window.orientation == -90)) ||
        (platform == 'A' && isTablet && (window.orientation == 0 || window.orientation == 180)) ||
        (platform == 'A' && !isTablet && (window.orientation == 90 || window.orientation == -90))){ //LANDSCAPE
        width = Math.max(windowheight,windowwidth);
        height = Math.min(windowheight,windowwidth);
        console.log("landscape: "+width+"X"+height);
    }else { //PORTRAIT
        height = Math.max(windowheight,windowwidth);
        width = Math.min(windowheight,windowwidth);
        console.log("portrait: "+width+"X"+height);
    }
    if (platform == 'A' && device.version.substr(0,3) >= 4.4){
        height = height - 25;
        width = width - 5;
    }
    windowheight=height;
    windowwidth=width;
    var iconpadding=35;
    if (isTablet==false) {
        iconpadding=5;
    }
    $(".right_icon1").css({left:(width-(iconpadding+$(".right_icon3").width())*3)+'px',position:'absolute', margin:'0px', top:$(".top_icon").css('top')});
    $(".right_icon2").css({left:(width-(iconpadding+$(".right_icon3").width())*2)+'px',position:'absolute', margin:'0px', top:$(".top_icon").css('top')});
    $(".right_icon3").css({left:(width-iconpadding-$(".right_icon3").width())+'px',position:'absolute', margin:'0px', top:$(".top_icon").css('top')});


    $(".ui-page").css({'min-height':height,'height':height,'width':width,'min-width':width});
    $('#mapheader').css({width: width , position: 'absolute', left: '0px', top:ios7Pad});
    $('#map_canvas').css({height: (height - $("#mapheader").height() - ios7Pad - $("#mapfooter").height()) - androidFooterPad, width: width-25, position: 'absolute', top: ($("#mapheader").height() + ios7Pad) + 'px', left: '0px'});
    $('#mapfooter').css({width: width + 'px', position: 'absolute', left: '0px',top: $("#map_canvas").height()+ios7Pad+$("#mapheader").height() });
    //alert("mapcanvas height:"+(height - $("#mapheader").height() - $("#mapfooter").height() - ios7Pad));
    //alert("footer's top:"+($("#mapheader").height() + ios7Pad + $("#map_canvas").height()));
    if (googleMap != null && localStorage.googleapi != 'sdk') {
        google.maps.event.trigger(googleMap, 'resize');
    }
    console.log("width:"+width+" height:"+height+" map:"+$("#map_canvas").width()+"X"+ $("#map_canvas").height()+" footer-top:"+$("#mapfooter").css('top')+" header-width:"+$("#mapheader").width()+" footer-width:"+$("#mapfooter").width());
    $('#connectionmessage').css({'top':((parseInt(windowheight)/2)-50)+'px','left':(parseInt((windowwidth)/2)-175)+'px',width:'250px'});
    /*$(".right_icon").css('left',(width-35)+'px');
     reloadGoogleMap();
     if (localStorage.getItem('ckbx_PrimaryLines') !== 'false') {
     addPrimLineLayer();
     }*/
}

function ScanBarCode(index) {
	
	var inspectfieldindex=index;
	var scanSuccess = function(result) {
		if (appname == 'LinemanApp' || appname == 'WaterCrewApp' || appname == 'SewerCrewApp' || appname == 'ManageVegetation') {
			$("#"+index).val(result.text);
			$("#"+index).trigger('change');
		}else{
			$("#"+inspectfields[inspectfieldindex].column).val(result.text.substring(inspectfields[inspectfieldindex].substr_start, inspectfields[inspectfieldindex].substr_end));
			$("#"+inspectfields[inspectfieldindex].column).trigger('change');
		}
		console.log("We got a barcode\n" +
		            "Result: " + result.text + "\n" +
		            "Format: " + result.format + "\n" +
		            "Cancelled: " + result.cancelled);

    };
    var scanFail = function (error) {
		alert("Scanning failed: " + error);
	};
	if (window.plugins == undefined) {
		window.plugins = {};
	}
	if (cordova.plugins.zbarScanner && navigator.userAgent.match(/iPhone|iPad|iPod/i)) { // We must make sure it's triggered ONLY for iOS
    // If ZBar scanner is installed - we use it
    	window.plugins.barcodeScanner = cordova.plugins.zbarScanner;
	} else if (cordova.plugins && cordova.plugins.barcodeScanner) {
		// BarcodeScanner is already loaded into cordova.plugins
		// This is as-of Cordova v3.4.
		window.plugins.barcodeScanner = cordova.plugins.barcodeScanner;
	} else if (!window.plugins.barcodeScanner) {
		// Legacy support
		window.plugins.barcodeScanner = cordova.require('cordova/plugin/BarcodeScanner');
	}
    //cordova.plugins.barcodeScanner.scan(scanSuccess, scanFail);
    //cloudSky.zBar.scan({},scanSuccess,scanFail);
    window.plugins.barcodeScanner.scan(scanSuccess, scanFail);
}
function doManateeScan(index) {
	
	var inspectfieldindex=index;
	var scanSuccess = function(result) {
		if (appname == 'LinemanApp'  || appname == 'WaterCrewApp' || appname == 'SewerCrewApp' ) {
			$("#"+index).val(result.code);
		}else{
			$("#"+inspectfields[inspectfieldindex].column).val(result.code);
		}
		console.log("We got a barcode\n" +
		            "Result: " + result.code + "\n" +
		            "Format: " + result.type );

    };
    var scanFail = function (error) {
		alert("Scanning failed: " + error);
	}
	scanner.startScanning(function(){},scanSuccess, 0,4,100,50);
}
function updateColumnTitles() {
	$("#waiting").show();
	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url')+"/websrvrequests/mobile/ajaxGetCommonAppData.php",
		data:  { action:'get', target:'column_titles', appname: appname },
			dataType: "jsonp",
			timeout:40000,
			success: function( msg ) {
				
				console.log("************loaded column titles");
				$("#waiting").hide();
				if (msg.result == 'false') {
					jAlert("calling msg error from updateColumnTitles()");
					jAlert(msg.error);
					return;
				}

				if(msg.column_titles && msg.column_titles.length > 0) {
					localStorage.setItem('column_titles', JSON.stringify(msg.column_titles));
				}
			},
           error: function(XMLHttpRequest, textStatus, errorThrown) {
               console.log("************ajax error getting column titles");
               $("#waiting").hide();
               alert(XMLHttpRequest+textStatus+errorThrown);
           }
	});
	//getLatestColumnTitles('fixTheTitles', localStorage.ims_url);
}
function changeGoogleMap() {
	if (navigator.connection.type == Connection.NONE) {
		jAlert("Can't chanage map type without internet connection");
		$('#googleapi').val(localStorage.googleapi).slider("refresh");
		return;
	}
	setSettings();
	localStorage.googleapi = $('#googleapi').val();
	localStorage.switched_maps = 'true';
	$.mobile.changePage('#mainmenu');
	loadJavascripts();
}

function is_iPad() {
	return true;
}
function logout() {
	$("#waiting").show();
	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url')+"/dvWfm2/Vehicle/ajaxLogin.php",
		dataType: "json",
		timeout: 20000,
		data:  {
			action:'logout',
			username:$("#imsu").val(),
			password:$("#imsp").val(),
			login_source:'A',
			phone_type:platform,
			device_token:localStorage.getItem('devicetoken'),
			app_name:appname
		},
		success:function(msg) {
			$("#waiting").hide();
			$("#imsu").val('');
			$("#imsp").val('');
			openPasswordPage();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("************ajax error getting column titles");
			$("#waiting").hide();
			alert(XMLHttpRequest+textStatus+errorThrown);
		}
	});
	//getLatestColumnTitles('fixTheTitles', localStorage.ims_url);
}
/*
(function(){
	var _log = console.log;
	var _error = console.error;
	var _warning = console.warning;
	console.error = function(errMessage){
		var message = '';
		if (typeof errMessage == 'string') {
			message = errMessage;
		}else{
			for (var key in errMessage) {
				if (typeof(errMessage[key]) == 'string'){
					message += "["+key+"] = "+errMessage[key]+" ";
				}
			}
		}
		var stack = {};
		
		if (typeof errMessage.stack !== 'undefined') {
			stack = errMessage.stack;
		}else{
			stack = console.trace();
		}
		sendErrorLog(message, stack);
		error.apply(console,arguments);
	};
	console.log = function(logMessage){
		_log.apply(console,arguments);
	};
	console.warning = function(warnMessage){
		_warning.apply(console,arguments);
	};
})();
function sendErrorLog(errMessage,stack){
	var logdata = {
		action:'log_error',
		target: 'log_error',
		crew:localStorage.usr,
		appname:appname,
		time: new Date().toUTCString(),
		error: errMessage,
		stack: stack,
		ims_url: localStorage.getItem('ims_url')
	};
	if (navigator.connection.type == Connection.NONE) {
    storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php", logdata);
    return;
  }
	$.ajax({
		type: "GET",
		url: localStorage.getItem('ims_url')+"/webSrvRequests/mobile/ajaxGetCommonAppData.php",
		dataType: "json",
		timeout: 20000,
		data:  logdata,
		success:function(msg) {
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("************ajax error getting column titles");
			alert(XMLHttpRequest+textStatus+errorThrown);
			storeAjaxRequest(localStorage.getItem('ims_url') + "/websrvrequests/mobile/ajaxGetCommonAppData.php", logdata);
		}
	});
}
*/


